window.onload = function(){
	var o_js_fieldset_transaction_details = ( document.getElementById("js_fieldset_transaction_details") != null ) ? document.getElementById("js_fieldset_transaction_details") : '';
	
	addTransacDetail = function( productFieldName, productFieldValue, quantityFieldName, quantityFieldValue, productSerialFieldName, productSerialFieldValue ){
		if( 	productFieldName != '' 
			&&	productFieldValue != '' 
			&&	quantityFieldName != '' 
			&&	quantityFieldValue != '' 
			&&	o_js_fieldset_transaction_details != '' 
		)
		{
			//if i_product_index '', this is a new transaction detail to add, else update value of the existing
			var i_product_index = getTransacDetailIndex( productFieldName, productFieldValue );
			if( i_product_index === '' )
			{
				var o_container = document.createElement("DIV");
				o_container.setAttribute("class", "transac-detail");

				var o_remove = document.createElement("a");
				o_remove.setAttribute("onclick", "removeTransacDetail(event)");
				var o_text = document.createTextNode("remove detail");
				o_remove.appendChild(o_text);

				var o_product_input = document.createElement("INPUT");
				o_product_input.setAttribute("type", "text");
				o_product_input.setAttribute("name", productFieldName + "[]");
				o_product_input.setAttribute("value", productFieldValue);

				var o_quantity_input = document.createElement("INPUT");
				o_quantity_input.setAttribute("type", "text");
				o_quantity_input.setAttribute("name", quantityFieldName + "[]");
				o_quantity_input.setAttribute("value", quantityFieldValue);

				o_container.appendChild(o_remove);
				o_container.appendChild(o_product_input);
				o_container.appendChild(o_quantity_input);

				//if product has serial
				if( productSerialFieldName != '' )
				{
					var o_product_serial_input = document.createElement("INPUT");
					o_product_serial_input.setAttribute("type", "text");
					o_product_serial_input.setAttribute("name", productSerialFieldName + "[]");
					o_product_serial_input.setAttribute("value", productSerialFieldValue);
					o_container.appendChild(o_product_serial_input);
				}

				o_js_fieldset_transaction_details.appendChild(o_container);

			}
			else
			{
				document.getElementsByName(quantityFieldName + '[]')[i_product_index].value = quantityFieldValue;

				//if product has serial
				if( productSerialFieldName != '' )
				{
					try {
						document.getElementsByName(productSerialFieldName + '[]')[i_product_index].value = productSerialFieldValue;
					}
					catch(e) {
						alert('Try removing the specific detail and add again.');
					}
				}
			}
		}
	};


	getTransacDetailIndex = function( s_name, i_value ){
		var i_product_index = '';
		if( 	o_js_fieldset_transaction_details != '' 
			&&	s_name != ''
			&&	i_value != ''
		)
		{
			var o_nodes = document.getElementsByName( s_name + '[]');
			for (var i = 0; i < o_nodes.length; ++i) {
				if( i_value == o_nodes[i].value )
				{
					i_product_index = i;
					return i_product_index;
				}
			}
		}
		return i_product_index;
	};

	removeTransacDetail = function(event){
		event.preventDefault();
		var target = findAncestor( event.target, 'transac-detail' );
		target.remove();
	};

	function findAncestor (el, cls) {
	    while ((el = el.parentElement) && !el.classList.contains(cls));
	    return el;
	}

};