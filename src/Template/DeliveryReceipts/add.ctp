<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Delivery Receipts'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Sales Invoices'), ['controller' => 'SalesInvoices', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sales Invoice'), ['controller' => 'SalesInvoices', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Delivery Receipt Status'), ['controller' => 'DeliveryReceiptStatus', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Delivery Receipt Status'), ['controller' => 'DeliveryReceiptStatus', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Delivery Receipt Details'), ['controller' => 'DeliveryReceiptDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Delivery Receipt Detail'), ['controller' => 'DeliveryReceiptDetails', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="deliveryReceipts form large-9 medium-8 columns content">
    <?= $this->Form->create($deliveryReceipt) ?>
    <fieldset>
        <legend><?= __('Add Delivery Receipt') ?></legend>
        <?php
            echo $this->Form->input('sales_invoice_id', ['options' => $salesInvoices]);
            echo $this->Form->input('prepared_by_id', ['options' => $users]);
            echo $this->Form->input('approved_by_id', ['options' => $users]);
            echo $this->Form->input('date_registration');
            echo $this->Form->input('title');
            echo $this->Form->input('comment');
            echo $this->Form->input('delivery_receipt_status_id', ['options' => $deliveryReceiptStatus]);
        ?>
    </fieldset>

    <fieldset>
        <?php 
            if( isset($product_list) && !empty($product_list) ) :
        ?>
            <label>
                Product
            </label>
            <select id="js_add_transac_detail_id">
                <?php 
                    foreach( $product_list AS $product_list_detail ):
                        echo '<option value="'. $product_list_detail['id'] .'">' .$product_list_detail['name'] .' - '. $product_list_detail['model'] .' - '. $product_list_detail['product_color']['name'] .' - '. $product_list_detail['product_dimension']['name'] .' - '. $product_list_detail['product_unit']['name']. '</option>';
                    endforeach;
                ?>
            </select>
            <label>
                Quantity
            </label>
            <input type="text" id="js_add_transac_detail_qty" value="" placeholder="0" />
            <label>
                Serial
            </label>
            <input type="text" id="js_add_transac_detail_serial" value="" />

            <button type="button" onclick="addTransacDetail( 'product_id', document.getElementById('js_add_transac_detail_id').value, 'quantity_delivering', document.getElementById('js_add_transac_detail_qty').value, 'product_serial', document.getElementById('js_add_transac_detail_serial').value )">Add Detail</button>
        <?php 
            endif;
        ?>
    </fieldset>

    <fieldset id="js_fieldset_transaction_details">
    </fieldset>
    
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
