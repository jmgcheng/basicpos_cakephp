<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Delivery Receipt'), ['action' => 'edit', $deliveryReceipt->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Delivery Receipt'), ['action' => 'delete', $deliveryReceipt->id], ['confirm' => __('Are you sure you want to delete # {0}?', $deliveryReceipt->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Delivery Receipts'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Delivery Receipt'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sales Invoices'), ['controller' => 'SalesInvoices', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sales Invoice'), ['controller' => 'SalesInvoices', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Delivery Receipt Status'), ['controller' => 'DeliveryReceiptStatus', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Delivery Receipt Status'), ['controller' => 'DeliveryReceiptStatus', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Delivery Receipt Details'), ['controller' => 'DeliveryReceiptDetails', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Delivery Receipt Detail'), ['controller' => 'DeliveryReceiptDetails', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="deliveryReceipts view large-9 medium-8 columns content">
    <h3><?= h($deliveryReceipt->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Sales Invoice') ?></th>
            <td><?= $deliveryReceipt->has('sales_invoice') ? $this->Html->link($deliveryReceipt->sales_invoice->title, ['controller' => 'SalesInvoices', 'action' => 'view', $deliveryReceipt->sales_invoice->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $deliveryReceipt->has('user') ? $this->Html->link($deliveryReceipt->user->id, ['controller' => 'Users', 'action' => 'view', $deliveryReceipt->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Title') ?></th>
            <td><?= h($deliveryReceipt->title) ?></td>
        </tr>
        <tr>
            <th><?= __('Comment') ?></th>
            <td><?= h($deliveryReceipt->comment) ?></td>
        </tr>
        <tr>
            <th><?= __('Delivery Receipt Status') ?></th>
            <td><?= $deliveryReceipt->has('delivery_receipt_status') ? $this->Html->link($deliveryReceipt->delivery_receipt_status->name, ['controller' => 'DeliveryReceiptStatus', 'action' => 'view', $deliveryReceipt->delivery_receipt_status->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($deliveryReceipt->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Prepared By Id') ?></th>
            <td><?= $this->Number->format($deliveryReceipt->prepared_by_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Date Registration') ?></th>
            <td><?= h($deliveryReceipt->date_registration) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Delivery Receipt Details') ?></h4>
        <?php if (!empty($deliveryReceipt->delivery_receipt_details)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Delivery Receipt Id') ?></th>
                <th><?= __('Product Id') ?></th>
                <th><?= __('Quantity Delivering') ?></th>
                <th><?= __('Product Serial') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($deliveryReceipt->delivery_receipt_details as $deliveryReceiptDetails): ?>
            <tr>
                <td><?= h($deliveryReceiptDetails->delivery_receipt_id) ?></td>
                <td><?= h($deliveryReceiptDetails->product_id) ?></td>
                <td><?= h($deliveryReceiptDetails->quantity_delivering) ?></td>
                <td><?= h($deliveryReceiptDetails->product_serial) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'DeliveryReceiptDetails', 'action' => 'view', $deliveryReceiptDetails->delivery_receipt_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'DeliveryReceiptDetails', 'action' => 'edit', $deliveryReceiptDetails->delivery_receipt_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'DeliveryReceiptDetails', 'action' => 'delete', $deliveryReceiptDetails->delivery_receipt_id], ['confirm' => __('Are you sure you want to delete # {0}?', $deliveryReceiptDetails->delivery_receipt_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
