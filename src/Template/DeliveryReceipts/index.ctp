<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Delivery Receipt'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sales Invoices'), ['controller' => 'SalesInvoices', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sales Invoice'), ['controller' => 'SalesInvoices', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Delivery Receipt Status'), ['controller' => 'DeliveryReceiptStatus', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Delivery Receipt Status'), ['controller' => 'DeliveryReceiptStatus', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Delivery Receipt Details'), ['controller' => 'DeliveryReceiptDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Delivery Receipt Detail'), ['controller' => 'DeliveryReceiptDetails', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="deliveryReceipts index large-9 medium-8 columns content">
    <h3><?= __('Delivery Receipts') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('sales_invoice_id') ?></th>
                <th><?= $this->Paginator->sort('prepared_by_id') ?></th>
                <th><?= $this->Paginator->sort('approved_by_id') ?></th>
                <th><?= $this->Paginator->sort('date_registration') ?></th>
                <th><?= $this->Paginator->sort('title') ?></th>
                <th><?= $this->Paginator->sort('comment') ?></th>
                <th><?= $this->Paginator->sort('delivery_receipt_status_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($deliveryReceipts as $deliveryReceipt): ?>
            <tr>
                <td><?= $this->Number->format($deliveryReceipt->id) ?></td>
                <td><?= $deliveryReceipt->has('sales_invoice') ? $this->Html->link($deliveryReceipt->sales_invoice->title, ['controller' => 'SalesInvoices', 'action' => 'view', $deliveryReceipt->sales_invoice->id]) : '' ?></td>
                <td><?= $this->Number->format($deliveryReceipt->prepared_by_id) ?></td>
                <td><?= $deliveryReceipt->has('user') ? $this->Html->link($deliveryReceipt->user->id, ['controller' => 'Users', 'action' => 'view', $deliveryReceipt->user->id]) : '' ?></td>
                <td><?= h($deliveryReceipt->date_registration) ?></td>
                <td><?= h($deliveryReceipt->title) ?></td>
                <td><?= h($deliveryReceipt->comment) ?></td>
                <td><?= $deliveryReceipt->has('delivery_receipt_status') ? $this->Html->link($deliveryReceipt->delivery_receipt_status->name, ['controller' => 'DeliveryReceiptStatus', 'action' => 'view', $deliveryReceipt->delivery_receipt_status->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $deliveryReceipt->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $deliveryReceipt->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $deliveryReceipt->id], ['confirm' => __('Are you sure you want to delete # {0}?', $deliveryReceipt->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
