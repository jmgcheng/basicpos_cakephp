<div>
	
	<?php 
		if( isset($stock_results) && !empty($stock_results) ) :
	?>
			<table>
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Model</th>
					<th>Color</th>
					<th>Dimension</th>
					<th>Unit</th>
					<th>II</th>
					<th>EI</th>
					<th>PO</th>
					<th>RPO</th>
					<th>MPO</th>
					<th>WSPO</th>
					<th>AF</th>
					<th>TR</th>
					<th>OR</th>
					<th>SR</th>
					<th>QA</th>
					<th>QLA</th>
					<th>TO</th>
					<th>RTO</th>
				</tr>

				<?php 
					foreach( $stock_results AS $stock_result_detail ):
				?>
				<tr>
					<td>
						<?php echo $stock_result_detail['product_id']; ?>
					</td>
					<td>
						<?php echo $stock_result_detail['product_name']; ?>
					</td>
					<td>
						<?php echo $stock_result_detail['product_model']; ?>
					</td>
					<td>
						<?php echo $stock_result_detail['product_color_name']; ?>
					</td>
					<td>
						<?php echo $stock_result_detail['product_dimension_name']; ?>
					</td>
					<td>
						<?php echo $stock_result_detail['product_unit_name']; ?>
					</td>
					<td>
						<?php echo $stock_result_detail['included_inventory_total']; ?>
					</td>
					<td>
						<?php echo $stock_result_detail['excluded_inventory_total']; ?>
					</td>
					<td>
						<?php echo $stock_result_detail['purchased_order_total']; ?>
					</td>
					<td>
						<?php echo $stock_result_detail['received_po_total']; ?>
					</td>
					<td>
						<?php echo $stock_result_detail['missed_purchased_order_total']; ?>
					</td>
					<td>
						<?php echo $stock_result_detail['wrong_send_purchased_order_total']; ?>
					</td>
					<td>
						<?php echo $stock_result_detail['af']; ?>
					</td>
					<td>
						<?php echo $stock_result_detail['tr']; ?>
					</td>
					<td>
						<?php echo $stock_result_detail['official_receipt_total']; ?>
					</td>
					<td>
						<?php echo $stock_result_detail['sales_return_total']; ?>
					</td>
					<td>
						<?php echo $stock_result_detail['qa']; ?>
					</td>
					<td>
						<?php echo $stock_result_detail['product_qty_low_alert']; ?>
					</td>
					<td>
						<?php echo $stock_result_detail['transfer_order_total']; ?>
					</td>
					<td>
						<?php echo $stock_result_detail['received_to_total']; ?>
					</td>
				</tr>
				<?php 
					endforeach;
				?>
			</table>
	<?php 
		endif ;
	?>

</div>