<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Product'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Product Status'), ['controller' => 'ProductStatus', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product Status'), ['controller' => 'ProductStatus', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Product Colors'), ['controller' => 'ProductColors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product Color'), ['controller' => 'ProductColors', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Product Dimensions'), ['controller' => 'ProductDimensions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product Dimension'), ['controller' => 'ProductDimensions', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Product Units'), ['controller' => 'ProductUnits', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product Unit'), ['controller' => 'ProductUnits', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Delivery Receipt Details'), ['controller' => 'DeliveryReceiptDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Delivery Receipt Detail'), ['controller' => 'DeliveryReceiptDetails', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Excluded Inventory Details'), ['controller' => 'ExcludedInventoryDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Excluded Inventory Detail'), ['controller' => 'ExcludedInventoryDetails', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Included Inventory Details'), ['controller' => 'IncludedInventoryDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Included Inventory Detail'), ['controller' => 'IncludedInventoryDetails', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Missed Po Details'), ['controller' => 'MissedPoDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Missed Po Detail'), ['controller' => 'MissedPoDetails', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Official Receipt Details'), ['controller' => 'OfficialReceiptDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Official Receipt Detail'), ['controller' => 'OfficialReceiptDetails', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Purchase Order Details'), ['controller' => 'PurchaseOrderDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Purchase Order Detail'), ['controller' => 'PurchaseOrderDetails', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Received Po Details'), ['controller' => 'ReceivedPoDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Received Po Detail'), ['controller' => 'ReceivedPoDetails', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Received To Details'), ['controller' => 'ReceivedToDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Received To Detail'), ['controller' => 'ReceivedToDetails', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sales Invoice Details'), ['controller' => 'SalesInvoiceDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sales Invoice Detail'), ['controller' => 'SalesInvoiceDetails', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Transfer Order Details'), ['controller' => 'TransferOrderDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Transfer Order Detail'), ['controller' => 'TransferOrderDetails', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Wrong Send Po Details'), ['controller' => 'WrongSendPoDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Wrong Send Po Detail'), ['controller' => 'WrongSendPoDetails', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="products index large-9 medium-8 columns content">
    <h3><?= __('Products') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th><?= $this->Paginator->sort('model') ?></th>
                <th><?= $this->Paginator->sort('description') ?></th>
                <th><?= $this->Paginator->sort('qty_low_alert') ?></th>
                <th><?= $this->Paginator->sort('product_status_id') ?></th>
                <th><?= $this->Paginator->sort('product_color_id') ?></th>
                <th><?= $this->Paginator->sort('product_dimension_id') ?></th>
                <th><?= $this->Paginator->sort('product_unit_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($products as $product): ?>
            <tr>
                <td><?= $this->Number->format($product->id) ?></td>
                <td><?= h($product->created) ?></td>
                <td><?= h($product->name) ?></td>
                <td><?= h($product->model) ?></td>
                <td><?= h($product->description) ?></td>
                <td><?= $this->Number->format($product->qty_low_alert) ?></td>
                <td><?= $product->has('product_status') ? $this->Html->link($product->product_status->name, ['controller' => 'ProductStatus', 'action' => 'view', $product->product_status->id]) : '' ?></td>
                <td><?= $product->has('product_color') ? $this->Html->link($product->product_color->name, ['controller' => 'ProductColors', 'action' => 'view', $product->product_color->id]) : '' ?></td>
                <td><?= $product->has('product_dimension') ? $this->Html->link($product->product_dimension->name, ['controller' => 'ProductDimensions', 'action' => 'view', $product->product_dimension->id]) : '' ?></td>
                <td><?= $product->has('product_unit') ? $this->Html->link($product->product_unit->name, ['controller' => 'ProductUnits', 'action' => 'view', $product->product_unit->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $product->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $product->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $product->id], ['confirm' => __('Are you sure you want to delete # {0}?', $product->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
