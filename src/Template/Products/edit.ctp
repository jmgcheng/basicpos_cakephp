<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $product->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $product->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Products'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Product Status'), ['controller' => 'ProductStatus', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product Status'), ['controller' => 'ProductStatus', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Product Colors'), ['controller' => 'ProductColors', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product Color'), ['controller' => 'ProductColors', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Product Dimensions'), ['controller' => 'ProductDimensions', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product Dimension'), ['controller' => 'ProductDimensions', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Product Units'), ['controller' => 'ProductUnits', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product Unit'), ['controller' => 'ProductUnits', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Delivery Receipt Details'), ['controller' => 'DeliveryReceiptDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Delivery Receipt Detail'), ['controller' => 'DeliveryReceiptDetails', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Excluded Inventory Details'), ['controller' => 'ExcludedInventoryDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Excluded Inventory Detail'), ['controller' => 'ExcludedInventoryDetails', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Included Inventory Details'), ['controller' => 'IncludedInventoryDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Included Inventory Detail'), ['controller' => 'IncludedInventoryDetails', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Missed Po Details'), ['controller' => 'MissedPoDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Missed Po Detail'), ['controller' => 'MissedPoDetails', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Official Receipt Details'), ['controller' => 'OfficialReceiptDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Official Receipt Detail'), ['controller' => 'OfficialReceiptDetails', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Purchase Order Details'), ['controller' => 'PurchaseOrderDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Purchase Order Detail'), ['controller' => 'PurchaseOrderDetails', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Received Po Details'), ['controller' => 'ReceivedPoDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Received Po Detail'), ['controller' => 'ReceivedPoDetails', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Received To Details'), ['controller' => 'ReceivedToDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Received To Detail'), ['controller' => 'ReceivedToDetails', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sales Invoice Details'), ['controller' => 'SalesInvoiceDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sales Invoice Detail'), ['controller' => 'SalesInvoiceDetails', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Transfer Order Details'), ['controller' => 'TransferOrderDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Transfer Order Detail'), ['controller' => 'TransferOrderDetails', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Wrong Send Po Details'), ['controller' => 'WrongSendPoDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Wrong Send Po Detail'), ['controller' => 'WrongSendPoDetails', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="products form large-9 medium-8 columns content">
    <?= $this->Form->create($product) ?>
    <fieldset>
        <legend><?= __('Edit Product') ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('model');
            echo $this->Form->input('description');
            echo $this->Form->input('qty_low_alert');
            echo $this->Form->input('product_status_id', ['options' => $productStatus]);
            echo $this->Form->input('product_color_id', ['options' => $productColors]);
            echo $this->Form->input('product_dimension_id', ['options' => $productDimensions]);
            echo $this->Form->input('product_unit_id', ['options' => $productUnits]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
