<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Product'), ['action' => 'edit', $product->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Product'), ['action' => 'delete', $product->id], ['confirm' => __('Are you sure you want to delete # {0}?', $product->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Product Status'), ['controller' => 'ProductStatus', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product Status'), ['controller' => 'ProductStatus', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Product Colors'), ['controller' => 'ProductColors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product Color'), ['controller' => 'ProductColors', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Product Dimensions'), ['controller' => 'ProductDimensions', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product Dimension'), ['controller' => 'ProductDimensions', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Product Units'), ['controller' => 'ProductUnits', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product Unit'), ['controller' => 'ProductUnits', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Delivery Receipt Details'), ['controller' => 'DeliveryReceiptDetails', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Delivery Receipt Detail'), ['controller' => 'DeliveryReceiptDetails', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Excluded Inventory Details'), ['controller' => 'ExcludedInventoryDetails', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Excluded Inventory Detail'), ['controller' => 'ExcludedInventoryDetails', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Included Inventory Details'), ['controller' => 'IncludedInventoryDetails', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Included Inventory Detail'), ['controller' => 'IncludedInventoryDetails', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Missed Po Details'), ['controller' => 'MissedPoDetails', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Missed Po Detail'), ['controller' => 'MissedPoDetails', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Official Receipt Details'), ['controller' => 'OfficialReceiptDetails', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Official Receipt Detail'), ['controller' => 'OfficialReceiptDetails', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Purchase Order Details'), ['controller' => 'PurchaseOrderDetails', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Purchase Order Detail'), ['controller' => 'PurchaseOrderDetails', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Received Po Details'), ['controller' => 'ReceivedPoDetails', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Received Po Detail'), ['controller' => 'ReceivedPoDetails', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Received To Details'), ['controller' => 'ReceivedToDetails', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Received To Detail'), ['controller' => 'ReceivedToDetails', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sales Invoice Details'), ['controller' => 'SalesInvoiceDetails', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sales Invoice Detail'), ['controller' => 'SalesInvoiceDetails', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Transfer Order Details'), ['controller' => 'TransferOrderDetails', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Transfer Order Detail'), ['controller' => 'TransferOrderDetails', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Wrong Send Po Details'), ['controller' => 'WrongSendPoDetails', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Wrong Send Po Detail'), ['controller' => 'WrongSendPoDetails', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="products view large-9 medium-8 columns content">
    <h3><?= h($product->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($product->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Model') ?></th>
            <td><?= h($product->model) ?></td>
        </tr>
        <tr>
            <th><?= __('Description') ?></th>
            <td><?= h($product->description) ?></td>
        </tr>
        <tr>
            <th><?= __('Product Status') ?></th>
            <td><?= $product->has('product_status') ? $this->Html->link($product->product_status->name, ['controller' => 'ProductStatus', 'action' => 'view', $product->product_status->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Product Color') ?></th>
            <td><?= $product->has('product_color') ? $this->Html->link($product->product_color->name, ['controller' => 'ProductColors', 'action' => 'view', $product->product_color->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Product Dimension') ?></th>
            <td><?= $product->has('product_dimension') ? $this->Html->link($product->product_dimension->name, ['controller' => 'ProductDimensions', 'action' => 'view', $product->product_dimension->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Product Unit') ?></th>
            <td><?= $product->has('product_unit') ? $this->Html->link($product->product_unit->name, ['controller' => 'ProductUnits', 'action' => 'view', $product->product_unit->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($product->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Qty Low Alert') ?></th>
            <td><?= $this->Number->format($product->qty_low_alert) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($product->created) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Delivery Receipt Details') ?></h4>
        <?php if (!empty($product->delivery_receipt_details)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Delivery Receipt Id') ?></th>
                <th><?= __('Product Id') ?></th>
                <th><?= __('Quantity Delivering') ?></th>
                <th><?= __('Product Serial') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($product->delivery_receipt_details as $deliveryReceiptDetails): ?>
            <tr>
                <td><?= h($deliveryReceiptDetails->delivery_receipt_id) ?></td>
                <td><?= h($deliveryReceiptDetails->product_id) ?></td>
                <td><?= h($deliveryReceiptDetails->quantity_delivering) ?></td>
                <td><?= h($deliveryReceiptDetails->product_serial) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'DeliveryReceiptDetails', 'action' => 'view', $deliveryReceiptDetails->delivery_receipt_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'DeliveryReceiptDetails', 'action' => 'edit', $deliveryReceiptDetails->delivery_receipt_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'DeliveryReceiptDetails', 'action' => 'delete', $deliveryReceiptDetails->delivery_receipt_id], ['confirm' => __('Are you sure you want to delete # {0}?', $deliveryReceiptDetails->delivery_receipt_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Excluded Inventory Details') ?></h4>
        <?php if (!empty($product->excluded_inventory_details)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Excluded Inventory Id') ?></th>
                <th><?= __('Product Id') ?></th>
                <th><?= __('Quantity Excluded') ?></th>
                <th><?= __('Product Serial') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($product->excluded_inventory_details as $excludedInventoryDetails): ?>
            <tr>
                <td><?= h($excludedInventoryDetails->excluded_inventory_id) ?></td>
                <td><?= h($excludedInventoryDetails->product_id) ?></td>
                <td><?= h($excludedInventoryDetails->quantity_excluded) ?></td>
                <td><?= h($excludedInventoryDetails->product_serial) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ExcludedInventoryDetails', 'action' => 'view', $excludedInventoryDetails->excluded_inventory_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ExcludedInventoryDetails', 'action' => 'edit', $excludedInventoryDetails->excluded_inventory_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ExcludedInventoryDetails', 'action' => 'delete', $excludedInventoryDetails->excluded_inventory_id], ['confirm' => __('Are you sure you want to delete # {0}?', $excludedInventoryDetails->excluded_inventory_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Included Inventory Details') ?></h4>
        <?php if (!empty($product->included_inventory_details)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Included Inventory Id') ?></th>
                <th><?= __('Product Id') ?></th>
                <th><?= __('Quantity Included') ?></th>
                <th><?= __('Product Serial') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($product->included_inventory_details as $includedInventoryDetails): ?>
            <tr>
                <td><?= h($includedInventoryDetails->included_inventory_id) ?></td>
                <td><?= h($includedInventoryDetails->product_id) ?></td>
                <td><?= h($includedInventoryDetails->quantity_included) ?></td>
                <td><?= h($includedInventoryDetails->product_serial) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'IncludedInventoryDetails', 'action' => 'view', $includedInventoryDetails->included_inventory_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'IncludedInventoryDetails', 'action' => 'edit', $includedInventoryDetails->included_inventory_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'IncludedInventoryDetails', 'action' => 'delete', $includedInventoryDetails->included_inventory_id], ['confirm' => __('Are you sure you want to delete # {0}?', $includedInventoryDetails->included_inventory_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Missed Po Details') ?></h4>
        <?php if (!empty($product->missed_po_details)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Missed Po Id') ?></th>
                <th><?= __('Product Id') ?></th>
                <th><?= __('Quantity Missed') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($product->missed_po_details as $missedPoDetails): ?>
            <tr>
                <td><?= h($missedPoDetails->missed_po_id) ?></td>
                <td><?= h($missedPoDetails->product_id) ?></td>
                <td><?= h($missedPoDetails->quantity_missed) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'MissedPoDetails', 'action' => 'view', $missedPoDetails->missed_po_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'MissedPoDetails', 'action' => 'edit', $missedPoDetails->missed_po_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'MissedPoDetails', 'action' => 'delete', $missedPoDetails->missed_po_id], ['confirm' => __('Are you sure you want to delete # {0}?', $missedPoDetails->missed_po_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Official Receipt Details') ?></h4>
        <?php if (!empty($product->official_receipt_details)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Official Receipt Id') ?></th>
                <th><?= __('Product Id') ?></th>
                <th><?= __('Quantity Released') ?></th>
                <th><?= __('Product Serial') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($product->official_receipt_details as $officialReceiptDetails): ?>
            <tr>
                <td><?= h($officialReceiptDetails->official_receipt_id) ?></td>
                <td><?= h($officialReceiptDetails->product_id) ?></td>
                <td><?= h($officialReceiptDetails->quantity_released) ?></td>
                <td><?= h($officialReceiptDetails->product_serial) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'OfficialReceiptDetails', 'action' => 'view', $officialReceiptDetails->official_receipt_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'OfficialReceiptDetails', 'action' => 'edit', $officialReceiptDetails->official_receipt_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'OfficialReceiptDetails', 'action' => 'delete', $officialReceiptDetails->official_receipt_id], ['confirm' => __('Are you sure you want to delete # {0}?', $officialReceiptDetails->official_receipt_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Purchase Order Details') ?></h4>
        <?php if (!empty($product->purchase_order_details)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Purchase Order Id') ?></th>
                <th><?= __('Product Id') ?></th>
                <th><?= __('Quantity Ordered') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($product->purchase_order_details as $purchaseOrderDetails): ?>
            <tr>
                <td><?= h($purchaseOrderDetails->purchase_order_id) ?></td>
                <td><?= h($purchaseOrderDetails->product_id) ?></td>
                <td><?= h($purchaseOrderDetails->quantity_ordered) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PurchaseOrderDetails', 'action' => 'view', $purchaseOrderDetails->purchase_order_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PurchaseOrderDetails', 'action' => 'edit', $purchaseOrderDetails->purchase_order_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PurchaseOrderDetails', 'action' => 'delete', $purchaseOrderDetails->purchase_order_id], ['confirm' => __('Are you sure you want to delete # {0}?', $purchaseOrderDetails->purchase_order_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Received Po Details') ?></h4>
        <?php if (!empty($product->received_po_details)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Received Po Id') ?></th>
                <th><?= __('Product Id') ?></th>
                <th><?= __('Quantity Received') ?></th>
                <th><?= __('Product Serial') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($product->received_po_details as $receivedPoDetails): ?>
            <tr>
                <td><?= h($receivedPoDetails->received_po_id) ?></td>
                <td><?= h($receivedPoDetails->product_id) ?></td>
                <td><?= h($receivedPoDetails->quantity_received) ?></td>
                <td><?= h($receivedPoDetails->product_serial) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ReceivedPoDetails', 'action' => 'view', $receivedPoDetails->received_po_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ReceivedPoDetails', 'action' => 'edit', $receivedPoDetails->received_po_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ReceivedPoDetails', 'action' => 'delete', $receivedPoDetails->received_po_id], ['confirm' => __('Are you sure you want to delete # {0}?', $receivedPoDetails->received_po_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Received To Details') ?></h4>
        <?php if (!empty($product->received_to_details)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Received To Id') ?></th>
                <th><?= __('Product Id') ?></th>
                <th><?= __('Quantity Received') ?></th>
                <th><?= __('Product Serial') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($product->received_to_details as $receivedToDetails): ?>
            <tr>
                <td><?= h($receivedToDetails->received_to_id) ?></td>
                <td><?= h($receivedToDetails->product_id) ?></td>
                <td><?= h($receivedToDetails->quantity_received) ?></td>
                <td><?= h($receivedToDetails->product_serial) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ReceivedToDetails', 'action' => 'view', $receivedToDetails->received_to_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ReceivedToDetails', 'action' => 'edit', $receivedToDetails->received_to_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ReceivedToDetails', 'action' => 'delete', $receivedToDetails->received_to_id], ['confirm' => __('Are you sure you want to delete # {0}?', $receivedToDetails->received_to_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Sales Invoice Details') ?></h4>
        <?php if (!empty($product->sales_invoice_details)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Sales Invoice Id') ?></th>
                <th><?= __('Product Id') ?></th>
                <th><?= __('Quantity To Release') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($product->sales_invoice_details as $salesInvoiceDetails): ?>
            <tr>
                <td><?= h($salesInvoiceDetails->sales_invoice_id) ?></td>
                <td><?= h($salesInvoiceDetails->product_id) ?></td>
                <td><?= h($salesInvoiceDetails->quantity_to_release) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'SalesInvoiceDetails', 'action' => 'view', $salesInvoiceDetails->sales_invoice_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'SalesInvoiceDetails', 'action' => 'edit', $salesInvoiceDetails->sales_invoice_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'SalesInvoiceDetails', 'action' => 'delete', $salesInvoiceDetails->sales_invoice_id], ['confirm' => __('Are you sure you want to delete # {0}?', $salesInvoiceDetails->sales_invoice_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Transfer Order Details') ?></h4>
        <?php if (!empty($product->transfer_order_details)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Transfer Order Id') ?></th>
                <th><?= __('Product Id') ?></th>
                <th><?= __('Quantity Transfering') ?></th>
                <th><?= __('Product Serial') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($product->transfer_order_details as $transferOrderDetails): ?>
            <tr>
                <td><?= h($transferOrderDetails->transfer_order_id) ?></td>
                <td><?= h($transferOrderDetails->product_id) ?></td>
                <td><?= h($transferOrderDetails->quantity_transfering) ?></td>
                <td><?= h($transferOrderDetails->product_serial) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TransferOrderDetails', 'action' => 'view', $transferOrderDetails->transfer_order_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TransferOrderDetails', 'action' => 'edit', $transferOrderDetails->transfer_order_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TransferOrderDetails', 'action' => 'delete', $transferOrderDetails->transfer_order_id], ['confirm' => __('Are you sure you want to delete # {0}?', $transferOrderDetails->transfer_order_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Wrong Send Po Details') ?></h4>
        <?php if (!empty($product->wrong_send_po_details)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Wrong Send Po Id') ?></th>
                <th><?= __('Product Id') ?></th>
                <th><?= __('Quantity Wrong Send') ?></th>
                <th><?= __('Product Serial') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($product->wrong_send_po_details as $wrongSendPoDetails): ?>
            <tr>
                <td><?= h($wrongSendPoDetails->wrong_send_po_id) ?></td>
                <td><?= h($wrongSendPoDetails->product_id) ?></td>
                <td><?= h($wrongSendPoDetails->quantity_wrong_send) ?></td>
                <td><?= h($wrongSendPoDetails->product_serial) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'WrongSendPoDetails', 'action' => 'view', $wrongSendPoDetails->wrong_send_po_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'WrongSendPoDetails', 'action' => 'edit', $wrongSendPoDetails->wrong_send_po_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'WrongSendPoDetails', 'action' => 'delete', $wrongSendPoDetails->wrong_send_po_id], ['confirm' => __('Are you sure you want to delete # {0}?', $wrongSendPoDetails->wrong_send_po_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
