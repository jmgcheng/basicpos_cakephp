<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Missed Po Detail'), ['action' => 'edit', $missedPoDetail->missed_po_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Missed Po Detail'), ['action' => 'delete', $missedPoDetail->missed_po_id], ['confirm' => __('Are you sure you want to delete # {0}?', $missedPoDetail->missed_po_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Missed Po Details'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Missed Po Detail'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Missed Pos'), ['controller' => 'MissedPos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Missed Po'), ['controller' => 'MissedPos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="missedPoDetails view large-9 medium-8 columns content">
    <h3><?= h($missedPoDetail->missed_po_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Missed Po') ?></th>
            <td><?= $missedPoDetail->has('missed_po') ? $this->Html->link($missedPoDetail->missed_po->id, ['controller' => 'MissedPos', 'action' => 'view', $missedPoDetail->missed_po->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Product') ?></th>
            <td><?= $missedPoDetail->has('product') ? $this->Html->link($missedPoDetail->product->name, ['controller' => 'Products', 'action' => 'view', $missedPoDetail->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Quantity Missed') ?></th>
            <td><?= $this->Number->format($missedPoDetail->quantity_missed) ?></td>
        </tr>
    </table>
</div>
