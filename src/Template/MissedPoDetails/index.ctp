<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Missed Po Detail'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Missed Pos'), ['controller' => 'MissedPos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Missed Po'), ['controller' => 'MissedPos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="missedPoDetails index large-9 medium-8 columns content">
    <h3><?= __('Missed Po Details') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('missed_po_id') ?></th>
                <th><?= $this->Paginator->sort('product_id') ?></th>
                <th><?= $this->Paginator->sort('quantity_missed') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($missedPoDetails as $missedPoDetail): ?>
            <tr>
                <td><?= $missedPoDetail->has('missed_po') ? $this->Html->link($missedPoDetail->missed_po->id, ['controller' => 'MissedPos', 'action' => 'view', $missedPoDetail->missed_po->id]) : '' ?></td>
                <td><?= $missedPoDetail->has('product') ? $this->Html->link($missedPoDetail->product->name, ['controller' => 'Products', 'action' => 'view', $missedPoDetail->product->id]) : '' ?></td>
                <td><?= $this->Number->format($missedPoDetail->quantity_missed) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $missedPoDetail->missed_po_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $missedPoDetail->missed_po_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $missedPoDetail->missed_po_id], ['confirm' => __('Are you sure you want to delete # {0}?', $missedPoDetail->missed_po_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
