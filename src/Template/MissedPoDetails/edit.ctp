<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $missedPoDetail->missed_po_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $missedPoDetail->missed_po_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Missed Po Details'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Missed Pos'), ['controller' => 'MissedPos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Missed Po'), ['controller' => 'MissedPos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="missedPoDetails form large-9 medium-8 columns content">
    <?= $this->Form->create($missedPoDetail) ?>
    <fieldset>
        <legend><?= __('Edit Missed Po Detail') ?></legend>
        <?php
            echo $this->Form->input('quantity_missed');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
