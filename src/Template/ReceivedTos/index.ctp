<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Received To'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Transfer Orders'), ['controller' => 'TransferOrders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Transfer Order'), ['controller' => 'TransferOrders', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Received To Status'), ['controller' => 'ReceivedToStatus', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Received To Status'), ['controller' => 'ReceivedToStatus', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Received To Details'), ['controller' => 'ReceivedToDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Received To Detail'), ['controller' => 'ReceivedToDetails', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="receivedTos index large-9 medium-8 columns content">
    <h3><?= __('Received Tos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('transfer_order_id') ?></th>
                <th><?= $this->Paginator->sort('depot_id') ?></th>
                <th><?= $this->Paginator->sort('prepared_by_id') ?></th>
                <th><?= $this->Paginator->sort('received_by_id') ?></th>
                <th><?= $this->Paginator->sort('date_registration') ?></th>
                <th><?= $this->Paginator->sort('comment') ?></th>
                <th><?= $this->Paginator->sort('received_to_status_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($receivedTos as $receivedTo): ?>
            <tr>
                <td><?= $this->Number->format($receivedTo->id) ?></td>
                <td><?= $receivedTo->has('transfer_order') ? $this->Html->link($receivedTo->transfer_order->id, ['controller' => 'TransferOrders', 'action' => 'view', $receivedTo->transfer_order->id]) : '' ?></td>
                <td><?= $receivedTo->has('depot') ? $this->Html->link($receivedTo->depot->name, ['controller' => 'Depots', 'action' => 'view', $receivedTo->depot->id]) : '' ?></td>
                <td><?= $this->Number->format($receivedTo->prepared_by_id) ?></td>
                <td><?= $receivedTo->has('user') ? $this->Html->link($receivedTo->user->id, ['controller' => 'Users', 'action' => 'view', $receivedTo->user->id]) : '' ?></td>
                <td><?= h($receivedTo->date_registration) ?></td>
                <td><?= h($receivedTo->comment) ?></td>
                <td><?= $receivedTo->has('received_to_status') ? $this->Html->link($receivedTo->received_to_status->name, ['controller' => 'ReceivedToStatus', 'action' => 'view', $receivedTo->received_to_status->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $receivedTo->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $receivedTo->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $receivedTo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $receivedTo->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
