<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Received To'), ['action' => 'edit', $receivedTo->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Received To'), ['action' => 'delete', $receivedTo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $receivedTo->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Received Tos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Received To'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Transfer Orders'), ['controller' => 'TransferOrders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Transfer Order'), ['controller' => 'TransferOrders', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Received To Status'), ['controller' => 'ReceivedToStatus', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Received To Status'), ['controller' => 'ReceivedToStatus', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Received To Details'), ['controller' => 'ReceivedToDetails', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Received To Detail'), ['controller' => 'ReceivedToDetails', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="receivedTos view large-9 medium-8 columns content">
    <h3><?= h($receivedTo->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Transfer Order') ?></th>
            <td><?= $receivedTo->has('transfer_order') ? $this->Html->link($receivedTo->transfer_order->id, ['controller' => 'TransferOrders', 'action' => 'view', $receivedTo->transfer_order->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Depot') ?></th>
            <td><?= $receivedTo->has('depot') ? $this->Html->link($receivedTo->depot->name, ['controller' => 'Depots', 'action' => 'view', $receivedTo->depot->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $receivedTo->has('user') ? $this->Html->link($receivedTo->user->id, ['controller' => 'Users', 'action' => 'view', $receivedTo->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Comment') ?></th>
            <td><?= h($receivedTo->comment) ?></td>
        </tr>
        <tr>
            <th><?= __('Received To Status') ?></th>
            <td><?= $receivedTo->has('received_to_status') ? $this->Html->link($receivedTo->received_to_status->name, ['controller' => 'ReceivedToStatus', 'action' => 'view', $receivedTo->received_to_status->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($receivedTo->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Prepared By Id') ?></th>
            <td><?= $this->Number->format($receivedTo->prepared_by_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Date Registration') ?></th>
            <td><?= h($receivedTo->date_registration) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Received To Details') ?></h4>
        <?php if (!empty($receivedTo->received_to_details)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Received To Id') ?></th>
                <th><?= __('Product Id') ?></th>
                <th><?= __('Quantity Received') ?></th>
                <th><?= __('Product Serial') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($receivedTo->received_to_details as $receivedToDetails): ?>
            <tr>
                <td><?= h($receivedToDetails->received_to_id) ?></td>
                <td><?= h($receivedToDetails->product_id) ?></td>
                <td><?= h($receivedToDetails->quantity_received) ?></td>
                <td><?= h($receivedToDetails->product_serial) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ReceivedToDetails', 'action' => 'view', $receivedToDetails->received_to_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ReceivedToDetails', 'action' => 'edit', $receivedToDetails->received_to_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ReceivedToDetails', 'action' => 'delete', $receivedToDetails->received_to_id], ['confirm' => __('Are you sure you want to delete # {0}?', $receivedToDetails->received_to_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
