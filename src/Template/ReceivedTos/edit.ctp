<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $receivedTo->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $receivedTo->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Received Tos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Transfer Orders'), ['controller' => 'TransferOrders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Transfer Order'), ['controller' => 'TransferOrders', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Received To Status'), ['controller' => 'ReceivedToStatus', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Received To Status'), ['controller' => 'ReceivedToStatus', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Received To Details'), ['controller' => 'ReceivedToDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Received To Detail'), ['controller' => 'ReceivedToDetails', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="receivedTos form large-9 medium-8 columns content">
    <?= $this->Form->create($receivedTo) ?>
    <fieldset>
        <legend><?= __('Edit Received To') ?></legend>
        <?php
            echo $this->Form->input('transfer_order_id', ['options' => $transferOrders]);
            echo $this->Form->input('depot_id', ['options' => $depots]);
            echo $this->Form->input('prepared_by_id', ['options' => $users]);
            echo $this->Form->input('received_by_id', ['options' => $users]);
            echo $this->Form->input('date_registration');
            echo $this->Form->input('comment');
            echo $this->Form->input('received_to_status_id', ['options' => $receivedToStatus]);
        ?>
    </fieldset>

    <fieldset>
        <?php 
            if( isset($product_list) && !empty($product_list) ) :
        ?>
            <label>
                Product
            </label>
            <select id="js_add_transac_detail_id">
                <?php 
                    foreach( $product_list AS $product_list_detail ):
                        echo '<option value="'. $product_list_detail['id'] .'">' .$product_list_detail['name'] .' - '. $product_list_detail['model'] .' - '. $product_list_detail['product_color']['name'] .' - '. $product_list_detail['product_dimension']['name'] .' - '. $product_list_detail['product_unit']['name']. '</option>';
                    endforeach;
                ?>
            </select>
            <label>
                Quantity
            </label>
            <input type="text" id="js_add_transac_detail_qty" value="" placeholder="0" />
            <label>
                Serial
            </label>
            <input type="text" id="js_add_transac_detail_serial" value="" />

            <button type="button" onclick="addTransacDetail( 'product_id', document.getElementById('js_add_transac_detail_id').value, 'quantity_received', document.getElementById('js_add_transac_detail_qty').value, 'product_serial', document.getElementById('js_add_transac_detail_serial').value )">Add Detail</button>
        <?php 
            endif;
        ?>
    </fieldset>

    <fieldset id="js_fieldset_transaction_details">
        <?php 
            if( isset($transac_detail_list) && !empty($transac_detail_list) ):
                foreach( $transac_detail_list AS $transac_detail_list_detail ):
        ?>
                <div class="transac-detail">
                    <a href="#" onclick="removeTransacDetail(event)">remove detail</a>
                    <input value="<?php echo $transac_detail_list_detail['product_id']; ?>" name="product_id[]" type="text" />
                    <input value="<?php echo $transac_detail_list_detail['quantity_received'] ?>" name="quantity_received[]" type="text" />
                    <input value="<?php echo $transac_detail_list_detail['product_serial'] ?>" name="product_serial[]" type="text" />
                </div>
        <?php 
                endforeach;
            endif;
        ?>
    </fieldset>
    
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
