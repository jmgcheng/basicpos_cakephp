<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Sales Return'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Official Receipts'), ['controller' => 'OfficialReceipts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Official Receipt'), ['controller' => 'OfficialReceipts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sales Return Status'), ['controller' => 'SalesReturnStatus', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sales Return Status'), ['controller' => 'SalesReturnStatus', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sales Return Details'), ['controller' => 'SalesReturnDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sales Return Detail'), ['controller' => 'SalesReturnDetails', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="salesReturns index large-9 medium-8 columns content">
    <h3><?= __('Sales Returns') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('official_receipt_id') ?></th>
                <th><?= $this->Paginator->sort('depot_id') ?></th>
                <th><?= $this->Paginator->sort('prepared_by_id') ?></th>
                <th><?= $this->Paginator->sort('approved_by_id') ?></th>
                <th><?= $this->Paginator->sort('date_registration') ?></th>
                <th><?= $this->Paginator->sort('comment') ?></th>
                <th><?= $this->Paginator->sort('sales_return_status_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($salesReturns as $salesReturn): ?>
            <tr>
                <td><?= $this->Number->format($salesReturn->id) ?></td>
                <td><?= $salesReturn->has('official_receipt') ? $this->Html->link($salesReturn->official_receipt->id, ['controller' => 'OfficialReceipts', 'action' => 'view', $salesReturn->official_receipt->id]) : '' ?></td>
                <td><?= $salesReturn->has('depot') ? $this->Html->link($salesReturn->depot->name, ['controller' => 'Depots', 'action' => 'view', $salesReturn->depot->id]) : '' ?></td>
                <td><?= $this->Number->format($salesReturn->prepared_by_id) ?></td>
                <td><?= $salesReturn->has('user') ? $this->Html->link($salesReturn->user->id, ['controller' => 'Users', 'action' => 'view', $salesReturn->user->id]) : '' ?></td>
                <td><?= h($salesReturn->date_registration) ?></td>
                <td><?= h($salesReturn->comment) ?></td>
                <td><?= $salesReturn->has('sales_return_status') ? $this->Html->link($salesReturn->sales_return_status->name, ['controller' => 'SalesReturnStatus', 'action' => 'view', $salesReturn->sales_return_status->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $salesReturn->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $salesReturn->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $salesReturn->id], ['confirm' => __('Are you sure you want to delete # {0}?', $salesReturn->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
