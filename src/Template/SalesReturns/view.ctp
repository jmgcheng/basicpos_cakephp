<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Sales Return'), ['action' => 'edit', $salesReturn->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Sales Return'), ['action' => 'delete', $salesReturn->id], ['confirm' => __('Are you sure you want to delete # {0}?', $salesReturn->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Sales Returns'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sales Return'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Official Receipts'), ['controller' => 'OfficialReceipts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Official Receipt'), ['controller' => 'OfficialReceipts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sales Return Status'), ['controller' => 'SalesReturnStatus', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sales Return Status'), ['controller' => 'SalesReturnStatus', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sales Return Details'), ['controller' => 'SalesReturnDetails', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sales Return Detail'), ['controller' => 'SalesReturnDetails', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="salesReturns view large-9 medium-8 columns content">
    <h3><?= h($salesReturn->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Official Receipt') ?></th>
            <td><?= $salesReturn->has('official_receipt') ? $this->Html->link($salesReturn->official_receipt->id, ['controller' => 'OfficialReceipts', 'action' => 'view', $salesReturn->official_receipt->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Depot') ?></th>
            <td><?= $salesReturn->has('depot') ? $this->Html->link($salesReturn->depot->name, ['controller' => 'Depots', 'action' => 'view', $salesReturn->depot->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $salesReturn->has('user') ? $this->Html->link($salesReturn->user->id, ['controller' => 'Users', 'action' => 'view', $salesReturn->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Comment') ?></th>
            <td><?= h($salesReturn->comment) ?></td>
        </tr>
        <tr>
            <th><?= __('Sales Return Status') ?></th>
            <td><?= $salesReturn->has('sales_return_status') ? $this->Html->link($salesReturn->sales_return_status->name, ['controller' => 'SalesReturnStatus', 'action' => 'view', $salesReturn->sales_return_status->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($salesReturn->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Prepared By Id') ?></th>
            <td><?= $this->Number->format($salesReturn->prepared_by_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Date Registration') ?></th>
            <td><?= h($salesReturn->date_registration) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Sales Return Details') ?></h4>
        <?php if (!empty($salesReturn->sales_return_details)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Sales Return Id') ?></th>
                <th><?= __('Product Id') ?></th>
                <th><?= __('Quantity Received') ?></th>
                <th><?= __('Product Serial') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($salesReturn->sales_return_details as $salesReturnDetails): ?>
            <tr>
                <td><?= h($salesReturnDetails->sales_return_id) ?></td>
                <td><?= h($salesReturnDetails->product_id) ?></td>
                <td><?= h($salesReturnDetails->quantity_received) ?></td>
                <td><?= h($salesReturnDetails->product_serial) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'SalesReturnDetails', 'action' => 'view', $salesReturnDetails->sales_return_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'SalesReturnDetails', 'action' => 'edit', $salesReturnDetails->sales_return_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'SalesReturnDetails', 'action' => 'delete', $salesReturnDetails->sales_return_id], ['confirm' => __('Are you sure you want to delete # {0}?', $salesReturnDetails->sales_return_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
