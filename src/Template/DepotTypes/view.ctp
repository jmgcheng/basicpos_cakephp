<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Depot Type'), ['action' => 'edit', $depotType->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Depot Type'), ['action' => 'delete', $depotType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $depotType->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Depot Types'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Depot Type'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="depotTypes view large-9 medium-8 columns content">
    <h3><?= h($depotType->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($depotType->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($depotType->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Depots') ?></h4>
        <?php if (!empty($depotType->depots)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Depot Type Id') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Address') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($depotType->depots as $depots): ?>
            <tr>
                <td><?= h($depots->id) ?></td>
                <td><?= h($depots->depot_type_id) ?></td>
                <td><?= h($depots->name) ?></td>
                <td><?= h($depots->description) ?></td>
                <td><?= h($depots->address) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Depots', 'action' => 'view', $depots->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Depots', 'action' => 'edit', $depots->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Depots', 'action' => 'delete', $depots->id], ['confirm' => __('Are you sure you want to delete # {0}?', $depots->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
