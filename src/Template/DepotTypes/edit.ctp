<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $depotType->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $depotType->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Depot Types'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="depotTypes form large-9 medium-8 columns content">
    <?= $this->Form->create($depotType) ?>
    <fieldset>
        <legend><?= __('Edit Depot Type') ?></legend>
        <?php
            echo $this->Form->input('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
