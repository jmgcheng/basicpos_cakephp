<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Transfer Order Status'), ['action' => 'edit', $transferOrderStatus->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Transfer Order Status'), ['action' => 'delete', $transferOrderStatus->id], ['confirm' => __('Are you sure you want to delete # {0}?', $transferOrderStatus->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Transfer Order Status'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Transfer Order Status'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Transfer Orders'), ['controller' => 'TransferOrders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Transfer Order'), ['controller' => 'TransferOrders', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="transferOrderStatus view large-9 medium-8 columns content">
    <h3><?= h($transferOrderStatus->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($transferOrderStatus->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($transferOrderStatus->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Transfer Orders') ?></h4>
        <?php if (!empty($transferOrderStatus->transfer_orders)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Releasing Depot Id') ?></th>
                <th><?= __('Receiving Depot Id') ?></th>
                <th><?= __('Prepared By Id') ?></th>
                <th><?= __('Approved By Id') ?></th>
                <th><?= __('Date Registration') ?></th>
                <th><?= __('Comment') ?></th>
                <th><?= __('Transfer Order Status Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($transferOrderStatus->transfer_orders as $transferOrders): ?>
            <tr>
                <td><?= h($transferOrders->id) ?></td>
                <td><?= h($transferOrders->releasing_depot_id) ?></td>
                <td><?= h($transferOrders->receiving_depot_id) ?></td>
                <td><?= h($transferOrders->prepared_by_id) ?></td>
                <td><?= h($transferOrders->approved_by_id) ?></td>
                <td><?= h($transferOrders->date_registration) ?></td>
                <td><?= h($transferOrders->comment) ?></td>
                <td><?= h($transferOrders->transfer_order_status_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TransferOrders', 'action' => 'view', $transferOrders->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TransferOrders', 'action' => 'edit', $transferOrders->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TransferOrders', 'action' => 'delete', $transferOrders->id], ['confirm' => __('Are you sure you want to delete # {0}?', $transferOrders->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
