<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $transferOrderStatus->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $transferOrderStatus->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Transfer Order Status'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Transfer Orders'), ['controller' => 'TransferOrders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Transfer Order'), ['controller' => 'TransferOrders', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="transferOrderStatus form large-9 medium-8 columns content">
    <?= $this->Form->create($transferOrderStatus) ?>
    <fieldset>
        <legend><?= __('Edit Transfer Order Status') ?></legend>
        <?php
            echo $this->Form->input('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
