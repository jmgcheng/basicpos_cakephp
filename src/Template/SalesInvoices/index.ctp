<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Sales Invoice'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sales Invoice Status'), ['controller' => 'SalesInvoiceStatus', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sales Invoice Status'), ['controller' => 'SalesInvoiceStatus', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Delivery Receipts'), ['controller' => 'DeliveryReceipts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Delivery Receipt'), ['controller' => 'DeliveryReceipts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Official Receipts'), ['controller' => 'OfficialReceipts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Official Receipt'), ['controller' => 'OfficialReceipts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sales Invoice Details'), ['controller' => 'SalesInvoiceDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sales Invoice Detail'), ['controller' => 'SalesInvoiceDetails', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="salesInvoices index large-9 medium-8 columns content">
    <h3><?= __('Sales Invoices') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('depot_id') ?></th>
                <th><?= $this->Paginator->sort('prepared_by_id') ?></th>
                <th><?= $this->Paginator->sort('approved_by_id') ?></th>
                <th><?= $this->Paginator->sort('date_registration') ?></th>
                <th><?= $this->Paginator->sort('title') ?></th>
                <th><?= $this->Paginator->sort('comment') ?></th>
                <th><?= $this->Paginator->sort('sales_invoice_status_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($salesInvoices as $salesInvoice): ?>
            <tr>
                <td><?= $this->Number->format($salesInvoice->id) ?></td>
                <td><?= $salesInvoice->has('depot') ? $this->Html->link($salesInvoice->depot->name, ['controller' => 'Depots', 'action' => 'view', $salesInvoice->depot->id]) : '' ?></td>
                <td><?= $this->Number->format($salesInvoice->prepared_by_id) ?></td>
                <td><?= $salesInvoice->has('user') ? $this->Html->link($salesInvoice->user->id, ['controller' => 'Users', 'action' => 'view', $salesInvoice->user->id]) : '' ?></td>
                <td><?= h($salesInvoice->date_registration) ?></td>
                <td><?= h($salesInvoice->title) ?></td>
                <td><?= h($salesInvoice->comment) ?></td>
                <td><?= $salesInvoice->has('sales_invoice_status') ? $this->Html->link($salesInvoice->sales_invoice_status->name, ['controller' => 'SalesInvoiceStatus', 'action' => 'view', $salesInvoice->sales_invoice_status->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $salesInvoice->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $salesInvoice->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $salesInvoice->id], ['confirm' => __('Are you sure you want to delete # {0}?', $salesInvoice->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
