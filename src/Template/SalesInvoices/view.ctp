<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Sales Invoice'), ['action' => 'edit', $salesInvoice->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Sales Invoice'), ['action' => 'delete', $salesInvoice->id], ['confirm' => __('Are you sure you want to delete # {0}?', $salesInvoice->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Sales Invoices'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sales Invoice'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sales Invoice Status'), ['controller' => 'SalesInvoiceStatus', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sales Invoice Status'), ['controller' => 'SalesInvoiceStatus', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Delivery Receipts'), ['controller' => 'DeliveryReceipts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Delivery Receipt'), ['controller' => 'DeliveryReceipts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Official Receipts'), ['controller' => 'OfficialReceipts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Official Receipt'), ['controller' => 'OfficialReceipts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sales Invoice Details'), ['controller' => 'SalesInvoiceDetails', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sales Invoice Detail'), ['controller' => 'SalesInvoiceDetails', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="salesInvoices view large-9 medium-8 columns content">
    <h3><?= h($salesInvoice->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Depot') ?></th>
            <td><?= $salesInvoice->has('depot') ? $this->Html->link($salesInvoice->depot->name, ['controller' => 'Depots', 'action' => 'view', $salesInvoice->depot->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $salesInvoice->has('user') ? $this->Html->link($salesInvoice->user->id, ['controller' => 'Users', 'action' => 'view', $salesInvoice->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Title') ?></th>
            <td><?= h($salesInvoice->title) ?></td>
        </tr>
        <tr>
            <th><?= __('Comment') ?></th>
            <td><?= h($salesInvoice->comment) ?></td>
        </tr>
        <tr>
            <th><?= __('Sales Invoice Status') ?></th>
            <td><?= $salesInvoice->has('sales_invoice_status') ? $this->Html->link($salesInvoice->sales_invoice_status->name, ['controller' => 'SalesInvoiceStatus', 'action' => 'view', $salesInvoice->sales_invoice_status->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($salesInvoice->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Prepared By Id') ?></th>
            <td><?= $this->Number->format($salesInvoice->prepared_by_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Date Registration') ?></th>
            <td><?= h($salesInvoice->date_registration) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Delivery Receipts') ?></h4>
        <?php if (!empty($salesInvoice->delivery_receipts)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Sales Invoice Id') ?></th>
                <th><?= __('Prepared By Id') ?></th>
                <th><?= __('Approved By Id') ?></th>
                <th><?= __('Date Registration') ?></th>
                <th><?= __('Title') ?></th>
                <th><?= __('Comment') ?></th>
                <th><?= __('Delivery Receipt Status Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($salesInvoice->delivery_receipts as $deliveryReceipts): ?>
            <tr>
                <td><?= h($deliveryReceipts->id) ?></td>
                <td><?= h($deliveryReceipts->sales_invoice_id) ?></td>
                <td><?= h($deliveryReceipts->prepared_by_id) ?></td>
                <td><?= h($deliveryReceipts->approved_by_id) ?></td>
                <td><?= h($deliveryReceipts->date_registration) ?></td>
                <td><?= h($deliveryReceipts->title) ?></td>
                <td><?= h($deliveryReceipts->comment) ?></td>
                <td><?= h($deliveryReceipts->delivery_receipt_status_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'DeliveryReceipts', 'action' => 'view', $deliveryReceipts->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'DeliveryReceipts', 'action' => 'edit', $deliveryReceipts->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'DeliveryReceipts', 'action' => 'delete', $deliveryReceipts->id], ['confirm' => __('Are you sure you want to delete # {0}?', $deliveryReceipts->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Official Receipts') ?></h4>
        <?php if (!empty($salesInvoice->official_receipts)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Sales Invoice Id') ?></th>
                <th><?= __('Depot Id') ?></th>
                <th><?= __('Prepared By Id') ?></th>
                <th><?= __('Approved By Id') ?></th>
                <th><?= __('Date Registration') ?></th>
                <th><?= __('Comment') ?></th>
                <th><?= __('Official Receipt Status Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($salesInvoice->official_receipts as $officialReceipts): ?>
            <tr>
                <td><?= h($officialReceipts->id) ?></td>
                <td><?= h($officialReceipts->sales_invoice_id) ?></td>
                <td><?= h($officialReceipts->depot_id) ?></td>
                <td><?= h($officialReceipts->prepared_by_id) ?></td>
                <td><?= h($officialReceipts->approved_by_id) ?></td>
                <td><?= h($officialReceipts->date_registration) ?></td>
                <td><?= h($officialReceipts->comment) ?></td>
                <td><?= h($officialReceipts->official_receipt_status_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'OfficialReceipts', 'action' => 'view', $officialReceipts->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'OfficialReceipts', 'action' => 'edit', $officialReceipts->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'OfficialReceipts', 'action' => 'delete', $officialReceipts->id], ['confirm' => __('Are you sure you want to delete # {0}?', $officialReceipts->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Sales Invoice Details') ?></h4>
        <?php if (!empty($salesInvoice->sales_invoice_details)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Sales Invoice Id') ?></th>
                <th><?= __('Product Id') ?></th>
                <th><?= __('Quantity To Release') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($salesInvoice->sales_invoice_details as $salesInvoiceDetails): ?>
            <tr>
                <td><?= h($salesInvoiceDetails->sales_invoice_id) ?></td>
                <td><?= h($salesInvoiceDetails->product_id) ?></td>
                <td><?= h($salesInvoiceDetails->quantity_to_release) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'SalesInvoiceDetails', 'action' => 'view', $salesInvoiceDetails->sales_invoice_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'SalesInvoiceDetails', 'action' => 'edit', $salesInvoiceDetails->sales_invoice_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'SalesInvoiceDetails', 'action' => 'delete', $salesInvoiceDetails->sales_invoice_id], ['confirm' => __('Are you sure you want to delete # {0}?', $salesInvoiceDetails->sales_invoice_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
