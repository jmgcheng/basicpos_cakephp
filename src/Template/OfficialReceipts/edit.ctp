<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $officialReceipt->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $officialReceipt->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Official Receipts'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Sales Invoices'), ['controller' => 'SalesInvoices', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sales Invoice'), ['controller' => 'SalesInvoices', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Official Receipt Status'), ['controller' => 'OfficialReceiptStatus', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Official Receipt Status'), ['controller' => 'OfficialReceiptStatus', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Official Receipt Details'), ['controller' => 'OfficialReceiptDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Official Receipt Detail'), ['controller' => 'OfficialReceiptDetails', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="officialReceipts form large-9 medium-8 columns content">
    <?= $this->Form->create($officialReceipt) ?>
    <fieldset>
        <legend><?= __('Edit Official Receipt') ?></legend>
        <?php
            echo $this->Form->input('sales_invoice_id', ['options' => $salesInvoices]);
            echo $this->Form->input('depot_id', ['options' => $depots]);
            echo $this->Form->input('prepared_by_id', ['options' => $users, 'empty' => true]);
            echo $this->Form->input('approved_by_id', ['options' => $users, 'empty' => true]);
            echo $this->Form->input('date_registration');
            echo $this->Form->input('comment');
            echo $this->Form->input('official_receipt_status_id', ['options' => $officialReceiptStatus]);
        ?>
    </fieldset>

    <fieldset>
        <?php 
            if( isset($product_list) && !empty($product_list) ) :
        ?>
            <label>
                Product
            </label>
            <select id="js_add_transac_detail_id">
                <?php 
                    foreach( $product_list AS $product_list_detail ):
                        echo '<option value="'. $product_list_detail['id'] .'">' .$product_list_detail['name'] .' - '. $product_list_detail['model'] .' - '. $product_list_detail['product_color']['name'] .' - '. $product_list_detail['product_dimension']['name'] .' - '. $product_list_detail['product_unit']['name']. '</option>';
                    endforeach;
                ?>
            </select>
            <label>
                Quantity
            </label>
            <input type="text" id="js_add_transac_detail_qty" value="" placeholder="0" />
            <label>
                Serial
            </label>
            <input type="text" id="js_add_transac_detail_serial" value="" />

            <button type="button" onclick="addTransacDetail( 'product_id', document.getElementById('js_add_transac_detail_id').value, 'quantity_released', document.getElementById('js_add_transac_detail_qty').value, 'product_serial', document.getElementById('js_add_transac_detail_serial').value )">Add Detail</button>
        <?php 
            endif;
        ?>
    </fieldset>

    <fieldset id="js_fieldset_transaction_details">
        <?php 
            if( isset($transac_detail_list) && !empty($transac_detail_list) ):
                foreach( $transac_detail_list AS $transac_detail_list_detail ):
        ?>
                <div class="transac-detail">
                    <a href="#" onclick="removeTransacDetail(event)">remove detail</a>
                    <input value="<?php echo $transac_detail_list_detail['product_id']; ?>" name="product_id[]" type="text" />
                    <input value="<?php echo $transac_detail_list_detail['quantity_released'] ?>" name="quantity_released[]" type="text" />
                    <input value="<?php echo $transac_detail_list_detail['product_serial'] ?>" name="product_serial[]" type="text" />
                </div>
        <?php 
                endforeach;
            endif;
        ?>
    </fieldset>
    
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
