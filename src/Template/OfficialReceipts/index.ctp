<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Official Receipt'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sales Invoices'), ['controller' => 'SalesInvoices', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sales Invoice'), ['controller' => 'SalesInvoices', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Official Receipt Status'), ['controller' => 'OfficialReceiptStatus', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Official Receipt Status'), ['controller' => 'OfficialReceiptStatus', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Official Receipt Details'), ['controller' => 'OfficialReceiptDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Official Receipt Detail'), ['controller' => 'OfficialReceiptDetails', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="officialReceipts index large-9 medium-8 columns content">
    <h3><?= __('Official Receipts') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('sales_invoice_id') ?></th>
                <th><?= $this->Paginator->sort('depot_id') ?></th>
                <th><?= $this->Paginator->sort('prepared_by_id') ?></th>
                <th><?= $this->Paginator->sort('approved_by_id') ?></th>
                <th><?= $this->Paginator->sort('date_registration') ?></th>
                <th><?= $this->Paginator->sort('comment') ?></th>
                <th><?= $this->Paginator->sort('official_receipt_status_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($officialReceipts as $officialReceipt): ?>
            <tr>
                <td><?= $this->Number->format($officialReceipt->id) ?></td>
                <td><?= $officialReceipt->has('sales_invoice') ? $this->Html->link($officialReceipt->sales_invoice->title, ['controller' => 'SalesInvoices', 'action' => 'view', $officialReceipt->sales_invoice->id]) : '' ?></td>
                <td><?= $officialReceipt->has('depot') ? $this->Html->link($officialReceipt->depot->name, ['controller' => 'Depots', 'action' => 'view', $officialReceipt->depot->id]) : '' ?></td>
                <td><?= $this->Number->format($officialReceipt->prepared_by_id) ?></td>
                <td><?= $officialReceipt->has('user') ? $this->Html->link($officialReceipt->user->id, ['controller' => 'Users', 'action' => 'view', $officialReceipt->user->id]) : '' ?></td>
                <td><?= h($officialReceipt->date_registration) ?></td>
                <td><?= h($officialReceipt->comment) ?></td>
                <td><?= $officialReceipt->has('official_receipt_status') ? $this->Html->link($officialReceipt->official_receipt_status->name, ['controller' => 'OfficialReceiptStatus', 'action' => 'view', $officialReceipt->official_receipt_status->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $officialReceipt->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $officialReceipt->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $officialReceipt->id], ['confirm' => __('Are you sure you want to delete # {0}?', $officialReceipt->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
