<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Official Receipt'), ['action' => 'edit', $officialReceipt->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Official Receipt'), ['action' => 'delete', $officialReceipt->id], ['confirm' => __('Are you sure you want to delete # {0}?', $officialReceipt->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Official Receipts'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Official Receipt'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sales Invoices'), ['controller' => 'SalesInvoices', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sales Invoice'), ['controller' => 'SalesInvoices', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Official Receipt Status'), ['controller' => 'OfficialReceiptStatus', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Official Receipt Status'), ['controller' => 'OfficialReceiptStatus', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Official Receipt Details'), ['controller' => 'OfficialReceiptDetails', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Official Receipt Detail'), ['controller' => 'OfficialReceiptDetails', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="officialReceipts view large-9 medium-8 columns content">
    <h3><?= h($officialReceipt->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Sales Invoice') ?></th>
            <td><?= $officialReceipt->has('sales_invoice') ? $this->Html->link($officialReceipt->sales_invoice->title, ['controller' => 'SalesInvoices', 'action' => 'view', $officialReceipt->sales_invoice->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Depot') ?></th>
            <td><?= $officialReceipt->has('depot') ? $this->Html->link($officialReceipt->depot->name, ['controller' => 'Depots', 'action' => 'view', $officialReceipt->depot->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $officialReceipt->has('user') ? $this->Html->link($officialReceipt->user->id, ['controller' => 'Users', 'action' => 'view', $officialReceipt->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Comment') ?></th>
            <td><?= h($officialReceipt->comment) ?></td>
        </tr>
        <tr>
            <th><?= __('Official Receipt Status') ?></th>
            <td><?= $officialReceipt->has('official_receipt_status') ? $this->Html->link($officialReceipt->official_receipt_status->name, ['controller' => 'OfficialReceiptStatus', 'action' => 'view', $officialReceipt->official_receipt_status->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($officialReceipt->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Prepared By Id') ?></th>
            <td><?= $this->Number->format($officialReceipt->prepared_by_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Date Registration') ?></th>
            <td><?= h($officialReceipt->date_registration) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Official Receipt Details') ?></h4>
        <?php if (!empty($officialReceipt->official_receipt_details)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Official Receipt Id') ?></th>
                <th><?= __('Product Id') ?></th>
                <th><?= __('Quantity Released') ?></th>
                <th><?= __('Product Serial') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($officialReceipt->official_receipt_details as $officialReceiptDetails): ?>
            <tr>
                <td><?= h($officialReceiptDetails->official_receipt_id) ?></td>
                <td><?= h($officialReceiptDetails->product_id) ?></td>
                <td><?= h($officialReceiptDetails->quantity_released) ?></td>
                <td><?= h($officialReceiptDetails->product_serial) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'OfficialReceiptDetails', 'action' => 'view', $officialReceiptDetails->official_receipt_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'OfficialReceiptDetails', 'action' => 'edit', $officialReceiptDetails->official_receipt_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'OfficialReceiptDetails', 'action' => 'delete', $officialReceiptDetails->official_receipt_id], ['confirm' => __('Are you sure you want to delete # {0}?', $officialReceiptDetails->official_receipt_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
