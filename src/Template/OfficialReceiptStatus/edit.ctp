<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $officialReceiptStatus->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $officialReceiptStatus->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Official Receipt Status'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Official Receipts'), ['controller' => 'OfficialReceipts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Official Receipt'), ['controller' => 'OfficialReceipts', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="officialReceiptStatus form large-9 medium-8 columns content">
    <?= $this->Form->create($officialReceiptStatus) ?>
    <fieldset>
        <legend><?= __('Edit Official Receipt Status') ?></legend>
        <?php
            echo $this->Form->input('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
