<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Official Receipt Status'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Official Receipts'), ['controller' => 'OfficialReceipts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Official Receipt'), ['controller' => 'OfficialReceipts', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="officialReceiptStatus index large-9 medium-8 columns content">
    <h3><?= __('Official Receipt Status') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($officialReceiptStatus as $officialReceiptStatus): ?>
            <tr>
                <td><?= $this->Number->format($officialReceiptStatus->id) ?></td>
                <td><?= h($officialReceiptStatus->name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $officialReceiptStatus->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $officialReceiptStatus->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $officialReceiptStatus->id], ['confirm' => __('Are you sure you want to delete # {0}?', $officialReceiptStatus->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
