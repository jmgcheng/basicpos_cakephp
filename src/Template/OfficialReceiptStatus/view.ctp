<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Official Receipt Status'), ['action' => 'edit', $officialReceiptStatus->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Official Receipt Status'), ['action' => 'delete', $officialReceiptStatus->id], ['confirm' => __('Are you sure you want to delete # {0}?', $officialReceiptStatus->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Official Receipt Status'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Official Receipt Status'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Official Receipts'), ['controller' => 'OfficialReceipts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Official Receipt'), ['controller' => 'OfficialReceipts', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="officialReceiptStatus view large-9 medium-8 columns content">
    <h3><?= h($officialReceiptStatus->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($officialReceiptStatus->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($officialReceiptStatus->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Official Receipts') ?></h4>
        <?php if (!empty($officialReceiptStatus->official_receipts)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Sales Invoice Id') ?></th>
                <th><?= __('Depot Id') ?></th>
                <th><?= __('Prepared By Id') ?></th>
                <th><?= __('Approved By Id') ?></th>
                <th><?= __('Date Registration') ?></th>
                <th><?= __('Comment') ?></th>
                <th><?= __('Official Receipt Status Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($officialReceiptStatus->official_receipts as $officialReceipts): ?>
            <tr>
                <td><?= h($officialReceipts->id) ?></td>
                <td><?= h($officialReceipts->sales_invoice_id) ?></td>
                <td><?= h($officialReceipts->depot_id) ?></td>
                <td><?= h($officialReceipts->prepared_by_id) ?></td>
                <td><?= h($officialReceipts->approved_by_id) ?></td>
                <td><?= h($officialReceipts->date_registration) ?></td>
                <td><?= h($officialReceipts->comment) ?></td>
                <td><?= h($officialReceipts->official_receipt_status_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'OfficialReceipts', 'action' => 'view', $officialReceipts->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'OfficialReceipts', 'action' => 'edit', $officialReceipts->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'OfficialReceipts', 'action' => 'delete', $officialReceipts->id], ['confirm' => __('Are you sure you want to delete # {0}?', $officialReceipts->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
