<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Excluded Inventory'), ['action' => 'edit', $excludedInventory->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Excluded Inventory'), ['action' => 'delete', $excludedInventory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $excludedInventory->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Excluded Inventories'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Excluded Inventory'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Excluded Inventory Status'), ['controller' => 'ExcludedInventoryStatus', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Excluded Inventory Status'), ['controller' => 'ExcludedInventoryStatus', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Excluded Inventory Details'), ['controller' => 'ExcludedInventoryDetails', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Excluded Inventory Detail'), ['controller' => 'ExcludedInventoryDetails', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="excludedInventories view large-9 medium-8 columns content">
    <h3><?= h($excludedInventory->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Depot') ?></th>
            <td><?= $excludedInventory->has('depot') ? $this->Html->link($excludedInventory->depot->name, ['controller' => 'Depots', 'action' => 'view', $excludedInventory->depot->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $excludedInventory->has('user') ? $this->Html->link($excludedInventory->user->id, ['controller' => 'Users', 'action' => 'view', $excludedInventory->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Comment') ?></th>
            <td><?= h($excludedInventory->comment) ?></td>
        </tr>
        <tr>
            <th><?= __('Excluded Inventory Status') ?></th>
            <td><?= $excludedInventory->has('excluded_inventory_status') ? $this->Html->link($excludedInventory->excluded_inventory_status->name, ['controller' => 'ExcludedInventoryStatus', 'action' => 'view', $excludedInventory->excluded_inventory_status->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($excludedInventory->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Date Registration') ?></th>
            <td><?= h($excludedInventory->date_registration) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Excluded Inventory Details') ?></h4>
        <?php if (!empty($excludedInventory->excluded_inventory_details)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Excluded Inventory Id') ?></th>
                <th><?= __('Product Id') ?></th>
                <th><?= __('Quantity Excluded') ?></th>
                <th><?= __('Product Serial') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($excludedInventory->excluded_inventory_details as $excludedInventoryDetails): ?>
            <tr>
                <td><?= h($excludedInventoryDetails->excluded_inventory_id) ?></td>
                <td><?= h($excludedInventoryDetails->product_id) ?></td>
                <td><?= h($excludedInventoryDetails->quantity_excluded) ?></td>
                <td><?= h($excludedInventoryDetails->product_serial) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ExcludedInventoryDetails', 'action' => 'view', $excludedInventoryDetails->excluded_inventory_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ExcludedInventoryDetails', 'action' => 'edit', $excludedInventoryDetails->excluded_inventory_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ExcludedInventoryDetails', 'action' => 'delete', $excludedInventoryDetails->excluded_inventory_id], ['confirm' => __('Are you sure you want to delete # {0}?', $excludedInventoryDetails->excluded_inventory_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
