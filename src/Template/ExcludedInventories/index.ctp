<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Excluded Inventory'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Excluded Inventory Status'), ['controller' => 'ExcludedInventoryStatus', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Excluded Inventory Status'), ['controller' => 'ExcludedInventoryStatus', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Excluded Inventory Details'), ['controller' => 'ExcludedInventoryDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Excluded Inventory Detail'), ['controller' => 'ExcludedInventoryDetails', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="excludedInventories index large-9 medium-8 columns content">
    <h3><?= __('Excluded Inventories') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('depot_id') ?></th>
                <th><?= $this->Paginator->sort('prepared_by_id') ?></th>
                <th><?= $this->Paginator->sort('date_registration') ?></th>
                <th><?= $this->Paginator->sort('comment') ?></th>
                <th><?= $this->Paginator->sort('excluded_inventory_status_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($excludedInventories as $excludedInventory): ?>
            <tr>
                <td><?= $this->Number->format($excludedInventory->id) ?></td>
                <td><?= $excludedInventory->has('depot') ? $this->Html->link($excludedInventory->depot->name, ['controller' => 'Depots', 'action' => 'view', $excludedInventory->depot->id]) : '' ?></td>
                <td><?= $excludedInventory->has('user') ? $this->Html->link($excludedInventory->user->id, ['controller' => 'Users', 'action' => 'view', $excludedInventory->user->id]) : '' ?></td>
                <td><?= h($excludedInventory->date_registration) ?></td>
                <td><?= h($excludedInventory->comment) ?></td>
                <td><?= $excludedInventory->has('excluded_inventory_status') ? $this->Html->link($excludedInventory->excluded_inventory_status->name, ['controller' => 'ExcludedInventoryStatus', 'action' => 'view', $excludedInventory->excluded_inventory_status->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $excludedInventory->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $excludedInventory->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $excludedInventory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $excludedInventory->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
