<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Missed Po Status'), ['action' => 'edit', $missedPoStatus->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Missed Po Status'), ['action' => 'delete', $missedPoStatus->id], ['confirm' => __('Are you sure you want to delete # {0}?', $missedPoStatus->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Missed Po Status'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Missed Po Status'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Missed Pos'), ['controller' => 'MissedPos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Missed Po'), ['controller' => 'MissedPos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="missedPoStatus view large-9 medium-8 columns content">
    <h3><?= h($missedPoStatus->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($missedPoStatus->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($missedPoStatus->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Missed Pos') ?></h4>
        <?php if (!empty($missedPoStatus->missed_pos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Purchase Order Id') ?></th>
                <th><?= __('Depot Id') ?></th>
                <th><?= __('Received By Id') ?></th>
                <th><?= __('Approved By Id') ?></th>
                <th><?= __('Date Registration') ?></th>
                <th><?= __('Comment') ?></th>
                <th><?= __('Missed Po Status Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($missedPoStatus->missed_pos as $missedPos): ?>
            <tr>
                <td><?= h($missedPos->id) ?></td>
                <td><?= h($missedPos->purchase_order_id) ?></td>
                <td><?= h($missedPos->depot_id) ?></td>
                <td><?= h($missedPos->received_by_id) ?></td>
                <td><?= h($missedPos->approved_by_id) ?></td>
                <td><?= h($missedPos->date_registration) ?></td>
                <td><?= h($missedPos->comment) ?></td>
                <td><?= h($missedPos->missed_po_status_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'MissedPos', 'action' => 'view', $missedPos->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'MissedPos', 'action' => 'edit', $missedPos->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'MissedPos', 'action' => 'delete', $missedPos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $missedPos->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
