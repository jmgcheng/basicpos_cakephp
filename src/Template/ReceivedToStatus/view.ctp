<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Received To Status'), ['action' => 'edit', $receivedToStatus->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Received To Status'), ['action' => 'delete', $receivedToStatus->id], ['confirm' => __('Are you sure you want to delete # {0}?', $receivedToStatus->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Received To Status'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Received To Status'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Received Tos'), ['controller' => 'ReceivedTos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Received To'), ['controller' => 'ReceivedTos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="receivedToStatus view large-9 medium-8 columns content">
    <h3><?= h($receivedToStatus->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($receivedToStatus->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($receivedToStatus->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Received Tos') ?></h4>
        <?php if (!empty($receivedToStatus->received_tos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Transfer Order Id') ?></th>
                <th><?= __('Depot Id') ?></th>
                <th><?= __('Prepared By Id') ?></th>
                <th><?= __('Received By Id') ?></th>
                <th><?= __('Date Registration') ?></th>
                <th><?= __('Comment') ?></th>
                <th><?= __('Received To Status Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($receivedToStatus->received_tos as $receivedTos): ?>
            <tr>
                <td><?= h($receivedTos->id) ?></td>
                <td><?= h($receivedTos->transfer_order_id) ?></td>
                <td><?= h($receivedTos->depot_id) ?></td>
                <td><?= h($receivedTos->prepared_by_id) ?></td>
                <td><?= h($receivedTos->received_by_id) ?></td>
                <td><?= h($receivedTos->date_registration) ?></td>
                <td><?= h($receivedTos->comment) ?></td>
                <td><?= h($receivedTos->received_to_status_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ReceivedTos', 'action' => 'view', $receivedTos->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ReceivedTos', 'action' => 'edit', $receivedTos->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ReceivedTos', 'action' => 'delete', $receivedTos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $receivedTos->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
