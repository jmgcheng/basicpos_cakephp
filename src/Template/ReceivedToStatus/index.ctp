<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Received To Status'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Received Tos'), ['controller' => 'ReceivedTos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Received To'), ['controller' => 'ReceivedTos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="receivedToStatus index large-9 medium-8 columns content">
    <h3><?= __('Received To Status') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($receivedToStatus as $receivedToStatus): ?>
            <tr>
                <td><?= $this->Number->format($receivedToStatus->id) ?></td>
                <td><?= h($receivedToStatus->name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $receivedToStatus->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $receivedToStatus->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $receivedToStatus->id], ['confirm' => __('Are you sure you want to delete # {0}?', $receivedToStatus->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
