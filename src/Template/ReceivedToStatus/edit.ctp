<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $receivedToStatus->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $receivedToStatus->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Received To Status'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Received Tos'), ['controller' => 'ReceivedTos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Received To'), ['controller' => 'ReceivedTos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="receivedToStatus form large-9 medium-8 columns content">
    <?= $this->Form->create($receivedToStatus) ?>
    <fieldset>
        <legend><?= __('Edit Received To Status') ?></legend>
        <?php
            echo $this->Form->input('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
