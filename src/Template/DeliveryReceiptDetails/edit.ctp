<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $deliveryReceiptDetail->delivery_receipt_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $deliveryReceiptDetail->delivery_receipt_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Delivery Receipt Details'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Delivery Receipts'), ['controller' => 'DeliveryReceipts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Delivery Receipt'), ['controller' => 'DeliveryReceipts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="deliveryReceiptDetails form large-9 medium-8 columns content">
    <?= $this->Form->create($deliveryReceiptDetail) ?>
    <fieldset>
        <legend><?= __('Edit Delivery Receipt Detail') ?></legend>
        <?php
            echo $this->Form->input('quantity_delivering');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
