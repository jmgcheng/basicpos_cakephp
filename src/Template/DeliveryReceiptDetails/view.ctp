<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Delivery Receipt Detail'), ['action' => 'edit', $deliveryReceiptDetail->delivery_receipt_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Delivery Receipt Detail'), ['action' => 'delete', $deliveryReceiptDetail->delivery_receipt_id], ['confirm' => __('Are you sure you want to delete # {0}?', $deliveryReceiptDetail->delivery_receipt_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Delivery Receipt Details'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Delivery Receipt Detail'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Delivery Receipts'), ['controller' => 'DeliveryReceipts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Delivery Receipt'), ['controller' => 'DeliveryReceipts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="deliveryReceiptDetails view large-9 medium-8 columns content">
    <h3><?= h($deliveryReceiptDetail->delivery_receipt_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Delivery Receipt') ?></th>
            <td><?= $deliveryReceiptDetail->has('delivery_receipt') ? $this->Html->link($deliveryReceiptDetail->delivery_receipt->title, ['controller' => 'DeliveryReceipts', 'action' => 'view', $deliveryReceiptDetail->delivery_receipt->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Product') ?></th>
            <td><?= $deliveryReceiptDetail->has('product') ? $this->Html->link($deliveryReceiptDetail->product->name, ['controller' => 'Products', 'action' => 'view', $deliveryReceiptDetail->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Product Serial') ?></th>
            <td><?= h($deliveryReceiptDetail->product_serial) ?></td>
        </tr>
        <tr>
            <th><?= __('Quantity Delivering') ?></th>
            <td><?= $this->Number->format($deliveryReceiptDetail->quantity_delivering) ?></td>
        </tr>
    </table>
</div>
