<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Depot'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Depot Types'), ['controller' => 'DepotTypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Depot Type'), ['controller' => 'DepotTypes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Excluded Inventories'), ['controller' => 'ExcludedInventories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Excluded Inventory'), ['controller' => 'ExcludedInventories', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Included Inventories'), ['controller' => 'IncludedInventories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Included Inventory'), ['controller' => 'IncludedInventories', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Missed Pos'), ['controller' => 'MissedPos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Missed Po'), ['controller' => 'MissedPos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Official Receipts'), ['controller' => 'OfficialReceipts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Official Receipt'), ['controller' => 'OfficialReceipts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Purchase Orders'), ['controller' => 'PurchaseOrders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Purchase Order'), ['controller' => 'PurchaseOrders', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Received Pos'), ['controller' => 'ReceivedPos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Received Po'), ['controller' => 'ReceivedPos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Received Tos'), ['controller' => 'ReceivedTos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Received To'), ['controller' => 'ReceivedTos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sales Invoices'), ['controller' => 'SalesInvoices', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sales Invoice'), ['controller' => 'SalesInvoices', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Wrong Send Pos'), ['controller' => 'WrongSendPos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Wrong Send Po'), ['controller' => 'WrongSendPos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="depots index large-9 medium-8 columns content">
    <h3><?= __('Depots') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('depot_type_id') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th><?= $this->Paginator->sort('description') ?></th>
                <th><?= $this->Paginator->sort('address') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($depots as $depot): ?>
            <tr>
                <td><?= $this->Number->format($depot->id) ?></td>
                <td><?= $depot->has('depot_type') ? $this->Html->link($depot->depot_type->name, ['controller' => 'DepotTypes', 'action' => 'view', $depot->depot_type->id]) : '' ?></td>
                <td><?= h($depot->name) ?></td>
                <td><?= h($depot->description) ?></td>
                <td><?= h($depot->address) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $depot->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $depot->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $depot->id], ['confirm' => __('Are you sure you want to delete # {0}?', $depot->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
