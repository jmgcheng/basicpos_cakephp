<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Depot'), ['action' => 'edit', $depot->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Depot'), ['action' => 'delete', $depot->id], ['confirm' => __('Are you sure you want to delete # {0}?', $depot->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Depots'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Depot'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Depot Types'), ['controller' => 'DepotTypes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Depot Type'), ['controller' => 'DepotTypes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Excluded Inventories'), ['controller' => 'ExcludedInventories', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Excluded Inventory'), ['controller' => 'ExcludedInventories', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Included Inventories'), ['controller' => 'IncludedInventories', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Included Inventory'), ['controller' => 'IncludedInventories', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Missed Pos'), ['controller' => 'MissedPos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Missed Po'), ['controller' => 'MissedPos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Official Receipts'), ['controller' => 'OfficialReceipts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Official Receipt'), ['controller' => 'OfficialReceipts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Purchase Orders'), ['controller' => 'PurchaseOrders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Purchase Order'), ['controller' => 'PurchaseOrders', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Received Pos'), ['controller' => 'ReceivedPos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Received Po'), ['controller' => 'ReceivedPos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Received Tos'), ['controller' => 'ReceivedTos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Received To'), ['controller' => 'ReceivedTos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sales Invoices'), ['controller' => 'SalesInvoices', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sales Invoice'), ['controller' => 'SalesInvoices', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Wrong Send Pos'), ['controller' => 'WrongSendPos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Wrong Send Po'), ['controller' => 'WrongSendPos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="depots view large-9 medium-8 columns content">
    <h3><?= h($depot->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Depot Type') ?></th>
            <td><?= $depot->has('depot_type') ? $this->Html->link($depot->depot_type->name, ['controller' => 'DepotTypes', 'action' => 'view', $depot->depot_type->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($depot->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Description') ?></th>
            <td><?= h($depot->description) ?></td>
        </tr>
        <tr>
            <th><?= __('Address') ?></th>
            <td><?= h($depot->address) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($depot->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Excluded Inventories') ?></h4>
        <?php if (!empty($depot->excluded_inventories)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Depot Id') ?></th>
                <th><?= __('Prepared By Id') ?></th>
                <th><?= __('Date Registration') ?></th>
                <th><?= __('Comment') ?></th>
                <th><?= __('Excluded Inventory Status Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($depot->excluded_inventories as $excludedInventories): ?>
            <tr>
                <td><?= h($excludedInventories->id) ?></td>
                <td><?= h($excludedInventories->depot_id) ?></td>
                <td><?= h($excludedInventories->prepared_by_id) ?></td>
                <td><?= h($excludedInventories->date_registration) ?></td>
                <td><?= h($excludedInventories->comment) ?></td>
                <td><?= h($excludedInventories->excluded_inventory_status_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ExcludedInventories', 'action' => 'view', $excludedInventories->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ExcludedInventories', 'action' => 'edit', $excludedInventories->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ExcludedInventories', 'action' => 'delete', $excludedInventories->id], ['confirm' => __('Are you sure you want to delete # {0}?', $excludedInventories->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Included Inventories') ?></h4>
        <?php if (!empty($depot->included_inventories)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Depot Id') ?></th>
                <th><?= __('Prepared By Id') ?></th>
                <th><?= __('Date Registration') ?></th>
                <th><?= __('Comment') ?></th>
                <th><?= __('Included Inventory Status Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($depot->included_inventories as $includedInventories): ?>
            <tr>
                <td><?= h($includedInventories->id) ?></td>
                <td><?= h($includedInventories->depot_id) ?></td>
                <td><?= h($includedInventories->prepared_by_id) ?></td>
                <td><?= h($includedInventories->date_registration) ?></td>
                <td><?= h($includedInventories->comment) ?></td>
                <td><?= h($includedInventories->included_inventory_status_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'IncludedInventories', 'action' => 'view', $includedInventories->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'IncludedInventories', 'action' => 'edit', $includedInventories->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'IncludedInventories', 'action' => 'delete', $includedInventories->id], ['confirm' => __('Are you sure you want to delete # {0}?', $includedInventories->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Missed Pos') ?></h4>
        <?php if (!empty($depot->missed_pos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Purchase Order Id') ?></th>
                <th><?= __('Depot Id') ?></th>
                <th><?= __('Received By Id') ?></th>
                <th><?= __('Approved By Id') ?></th>
                <th><?= __('Date Registration') ?></th>
                <th><?= __('Comment') ?></th>
                <th><?= __('Missed Po Status Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($depot->missed_pos as $missedPos): ?>
            <tr>
                <td><?= h($missedPos->id) ?></td>
                <td><?= h($missedPos->purchase_order_id) ?></td>
                <td><?= h($missedPos->depot_id) ?></td>
                <td><?= h($missedPos->received_by_id) ?></td>
                <td><?= h($missedPos->approved_by_id) ?></td>
                <td><?= h($missedPos->date_registration) ?></td>
                <td><?= h($missedPos->comment) ?></td>
                <td><?= h($missedPos->missed_po_status_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'MissedPos', 'action' => 'view', $missedPos->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'MissedPos', 'action' => 'edit', $missedPos->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'MissedPos', 'action' => 'delete', $missedPos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $missedPos->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Official Receipts') ?></h4>
        <?php if (!empty($depot->official_receipts)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Sales Invoice Id') ?></th>
                <th><?= __('Depot Id') ?></th>
                <th><?= __('Prepared By Id') ?></th>
                <th><?= __('Approved By Id') ?></th>
                <th><?= __('Date Registration') ?></th>
                <th><?= __('Comment') ?></th>
                <th><?= __('Official Receipt Status Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($depot->official_receipts as $officialReceipts): ?>
            <tr>
                <td><?= h($officialReceipts->id) ?></td>
                <td><?= h($officialReceipts->sales_invoice_id) ?></td>
                <td><?= h($officialReceipts->depot_id) ?></td>
                <td><?= h($officialReceipts->prepared_by_id) ?></td>
                <td><?= h($officialReceipts->approved_by_id) ?></td>
                <td><?= h($officialReceipts->date_registration) ?></td>
                <td><?= h($officialReceipts->comment) ?></td>
                <td><?= h($officialReceipts->official_receipt_status_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'OfficialReceipts', 'action' => 'view', $officialReceipts->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'OfficialReceipts', 'action' => 'edit', $officialReceipts->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'OfficialReceipts', 'action' => 'delete', $officialReceipts->id], ['confirm' => __('Are you sure you want to delete # {0}?', $officialReceipts->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Purchase Orders') ?></h4>
        <?php if (!empty($depot->purchase_orders)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Depot Id') ?></th>
                <th><?= __('Prepared By Id') ?></th>
                <th><?= __('Approved By Id') ?></th>
                <th><?= __('Date Registration') ?></th>
                <th><?= __('Title') ?></th>
                <th><?= __('Order Description') ?></th>
                <th><?= __('Comment') ?></th>
                <th><?= __('Purchase Order Status Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($depot->purchase_orders as $purchaseOrders): ?>
            <tr>
                <td><?= h($purchaseOrders->id) ?></td>
                <td><?= h($purchaseOrders->depot_id) ?></td>
                <td><?= h($purchaseOrders->prepared_by_id) ?></td>
                <td><?= h($purchaseOrders->approved_by_id) ?></td>
                <td><?= h($purchaseOrders->date_registration) ?></td>
                <td><?= h($purchaseOrders->title) ?></td>
                <td><?= h($purchaseOrders->order_description) ?></td>
                <td><?= h($purchaseOrders->comment) ?></td>
                <td><?= h($purchaseOrders->purchase_order_status_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PurchaseOrders', 'action' => 'view', $purchaseOrders->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PurchaseOrders', 'action' => 'edit', $purchaseOrders->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PurchaseOrders', 'action' => 'delete', $purchaseOrders->id], ['confirm' => __('Are you sure you want to delete # {0}?', $purchaseOrders->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Received Pos') ?></h4>
        <?php if (!empty($depot->received_pos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Purchase Order Id') ?></th>
                <th><?= __('Depot Id') ?></th>
                <th><?= __('Prepared By Id') ?></th>
                <th><?= __('Approved By Id') ?></th>
                <th><?= __('Date Registration') ?></th>
                <th><?= __('Comment') ?></th>
                <th><?= __('Received Po Status Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($depot->received_pos as $receivedPos): ?>
            <tr>
                <td><?= h($receivedPos->id) ?></td>
                <td><?= h($receivedPos->purchase_order_id) ?></td>
                <td><?= h($receivedPos->depot_id) ?></td>
                <td><?= h($receivedPos->prepared_by_id) ?></td>
                <td><?= h($receivedPos->approved_by_id) ?></td>
                <td><?= h($receivedPos->date_registration) ?></td>
                <td><?= h($receivedPos->comment) ?></td>
                <td><?= h($receivedPos->received_po_status_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ReceivedPos', 'action' => 'view', $receivedPos->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ReceivedPos', 'action' => 'edit', $receivedPos->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ReceivedPos', 'action' => 'delete', $receivedPos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $receivedPos->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Received Tos') ?></h4>
        <?php if (!empty($depot->received_tos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Transfer Order Id') ?></th>
                <th><?= __('Depot Id') ?></th>
                <th><?= __('Prepared By Id') ?></th>
                <th><?= __('Received By Id') ?></th>
                <th><?= __('Date Registration') ?></th>
                <th><?= __('Comment') ?></th>
                <th><?= __('Received To Status Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($depot->received_tos as $receivedTos): ?>
            <tr>
                <td><?= h($receivedTos->id) ?></td>
                <td><?= h($receivedTos->transfer_order_id) ?></td>
                <td><?= h($receivedTos->depot_id) ?></td>
                <td><?= h($receivedTos->prepared_by_id) ?></td>
                <td><?= h($receivedTos->received_by_id) ?></td>
                <td><?= h($receivedTos->date_registration) ?></td>
                <td><?= h($receivedTos->comment) ?></td>
                <td><?= h($receivedTos->received_to_status_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ReceivedTos', 'action' => 'view', $receivedTos->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ReceivedTos', 'action' => 'edit', $receivedTos->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ReceivedTos', 'action' => 'delete', $receivedTos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $receivedTos->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Sales Invoices') ?></h4>
        <?php if (!empty($depot->sales_invoices)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Depot Id') ?></th>
                <th><?= __('Prepared By Id') ?></th>
                <th><?= __('Approved By Id') ?></th>
                <th><?= __('Date Registration') ?></th>
                <th><?= __('Title') ?></th>
                <th><?= __('Comment') ?></th>
                <th><?= __('Sales Invoice Status Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($depot->sales_invoices as $salesInvoices): ?>
            <tr>
                <td><?= h($salesInvoices->id) ?></td>
                <td><?= h($salesInvoices->depot_id) ?></td>
                <td><?= h($salesInvoices->prepared_by_id) ?></td>
                <td><?= h($salesInvoices->approved_by_id) ?></td>
                <td><?= h($salesInvoices->date_registration) ?></td>
                <td><?= h($salesInvoices->title) ?></td>
                <td><?= h($salesInvoices->comment) ?></td>
                <td><?= h($salesInvoices->sales_invoice_status_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'SalesInvoices', 'action' => 'view', $salesInvoices->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'SalesInvoices', 'action' => 'edit', $salesInvoices->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'SalesInvoices', 'action' => 'delete', $salesInvoices->id], ['confirm' => __('Are you sure you want to delete # {0}?', $salesInvoices->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Wrong Send Pos') ?></h4>
        <?php if (!empty($depot->wrong_send_pos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Purchase Order Id') ?></th>
                <th><?= __('Depot Id') ?></th>
                <th><?= __('Received By Id') ?></th>
                <th><?= __('Approved By Id') ?></th>
                <th><?= __('Date Registration') ?></th>
                <th><?= __('Comment') ?></th>
                <th><?= __('Wrong Send Po Status Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($depot->wrong_send_pos as $wrongSendPos): ?>
            <tr>
                <td><?= h($wrongSendPos->id) ?></td>
                <td><?= h($wrongSendPos->purchase_order_id) ?></td>
                <td><?= h($wrongSendPos->depot_id) ?></td>
                <td><?= h($wrongSendPos->received_by_id) ?></td>
                <td><?= h($wrongSendPos->approved_by_id) ?></td>
                <td><?= h($wrongSendPos->date_registration) ?></td>
                <td><?= h($wrongSendPos->comment) ?></td>
                <td><?= h($wrongSendPos->wrong_send_po_status_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'WrongSendPos', 'action' => 'view', $wrongSendPos->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'WrongSendPos', 'action' => 'edit', $wrongSendPos->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'WrongSendPos', 'action' => 'delete', $wrongSendPos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $wrongSendPos->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
