<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Excluded Inventory Status'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Excluded Inventories'), ['controller' => 'ExcludedInventories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Excluded Inventory'), ['controller' => 'ExcludedInventories', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="excludedInventoryStatus index large-9 medium-8 columns content">
    <h3><?= __('Excluded Inventory Status') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($excludedInventoryStatus as $excludedInventoryStatus): ?>
            <tr>
                <td><?= $this->Number->format($excludedInventoryStatus->id) ?></td>
                <td><?= h($excludedInventoryStatus->name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $excludedInventoryStatus->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $excludedInventoryStatus->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $excludedInventoryStatus->id], ['confirm' => __('Are you sure you want to delete # {0}?', $excludedInventoryStatus->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
