<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Excluded Inventory Status'), ['action' => 'edit', $excludedInventoryStatus->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Excluded Inventory Status'), ['action' => 'delete', $excludedInventoryStatus->id], ['confirm' => __('Are you sure you want to delete # {0}?', $excludedInventoryStatus->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Excluded Inventory Status'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Excluded Inventory Status'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Excluded Inventories'), ['controller' => 'ExcludedInventories', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Excluded Inventory'), ['controller' => 'ExcludedInventories', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="excludedInventoryStatus view large-9 medium-8 columns content">
    <h3><?= h($excludedInventoryStatus->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($excludedInventoryStatus->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($excludedInventoryStatus->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Excluded Inventories') ?></h4>
        <?php if (!empty($excludedInventoryStatus->excluded_inventories)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Depot Id') ?></th>
                <th><?= __('Prepared By Id') ?></th>
                <th><?= __('Date Registration') ?></th>
                <th><?= __('Comment') ?></th>
                <th><?= __('Excluded Inventory Status Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($excludedInventoryStatus->excluded_inventories as $excludedInventories): ?>
            <tr>
                <td><?= h($excludedInventories->id) ?></td>
                <td><?= h($excludedInventories->depot_id) ?></td>
                <td><?= h($excludedInventories->prepared_by_id) ?></td>
                <td><?= h($excludedInventories->date_registration) ?></td>
                <td><?= h($excludedInventories->comment) ?></td>
                <td><?= h($excludedInventories->excluded_inventory_status_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ExcludedInventories', 'action' => 'view', $excludedInventories->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ExcludedInventories', 'action' => 'edit', $excludedInventories->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ExcludedInventories', 'action' => 'delete', $excludedInventories->id], ['confirm' => __('Are you sure you want to delete # {0}?', $excludedInventories->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
