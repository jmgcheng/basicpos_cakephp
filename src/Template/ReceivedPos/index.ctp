<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Received Po'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Purchase Orders'), ['controller' => 'PurchaseOrders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Purchase Order'), ['controller' => 'PurchaseOrders', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Received Po Status'), ['controller' => 'ReceivedPoStatus', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Received Po Status'), ['controller' => 'ReceivedPoStatus', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Received Po Details'), ['controller' => 'ReceivedPoDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Received Po Detail'), ['controller' => 'ReceivedPoDetails', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="receivedPos index large-9 medium-8 columns content">
    <h3><?= __('Received Pos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('purchase_order_id') ?></th>
                <th><?= $this->Paginator->sort('depot_id') ?></th>
                <th><?= $this->Paginator->sort('prepared_by_id') ?></th>
                <th><?= $this->Paginator->sort('approved_by_id') ?></th>
                <th><?= $this->Paginator->sort('date_registration') ?></th>
                <th><?= $this->Paginator->sort('comment') ?></th>
                <th><?= $this->Paginator->sort('received_po_status_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($receivedPos as $receivedPo): ?>
            <tr>
                <td><?= $this->Number->format($receivedPo->id) ?></td>
                <td><?= $receivedPo->has('purchase_order') ? $this->Html->link($receivedPo->purchase_order->title, ['controller' => 'PurchaseOrders', 'action' => 'view', $receivedPo->purchase_order->id]) : '' ?></td>
                <td><?= $receivedPo->has('depot') ? $this->Html->link($receivedPo->depot->name, ['controller' => 'Depots', 'action' => 'view', $receivedPo->depot->id]) : '' ?></td>
                <td><?= $this->Number->format($receivedPo->prepared_by_id) ?></td>
                <td><?= $receivedPo->has('user') ? $this->Html->link($receivedPo->user->id, ['controller' => 'Users', 'action' => 'view', $receivedPo->user->id]) : '' ?></td>
                <td><?= h($receivedPo->date_registration) ?></td>
                <td><?= h($receivedPo->comment) ?></td>
                <td><?= $receivedPo->has('received_po_status') ? $this->Html->link($receivedPo->received_po_status->name, ['controller' => 'ReceivedPoStatus', 'action' => 'view', $receivedPo->received_po_status->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $receivedPo->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $receivedPo->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $receivedPo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $receivedPo->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
