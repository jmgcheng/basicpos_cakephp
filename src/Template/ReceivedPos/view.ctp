<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Received Po'), ['action' => 'edit', $receivedPo->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Received Po'), ['action' => 'delete', $receivedPo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $receivedPo->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Received Pos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Received Po'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Purchase Orders'), ['controller' => 'PurchaseOrders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Purchase Order'), ['controller' => 'PurchaseOrders', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Received Po Status'), ['controller' => 'ReceivedPoStatus', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Received Po Status'), ['controller' => 'ReceivedPoStatus', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Received Po Details'), ['controller' => 'ReceivedPoDetails', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Received Po Detail'), ['controller' => 'ReceivedPoDetails', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="receivedPos view large-9 medium-8 columns content">
    <h3><?= h($receivedPo->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Purchase Order') ?></th>
            <td><?= $receivedPo->has('purchase_order') ? $this->Html->link($receivedPo->purchase_order->title, ['controller' => 'PurchaseOrders', 'action' => 'view', $receivedPo->purchase_order->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Depot') ?></th>
            <td><?= $receivedPo->has('depot') ? $this->Html->link($receivedPo->depot->name, ['controller' => 'Depots', 'action' => 'view', $receivedPo->depot->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $receivedPo->has('user') ? $this->Html->link($receivedPo->user->id, ['controller' => 'Users', 'action' => 'view', $receivedPo->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Comment') ?></th>
            <td><?= h($receivedPo->comment) ?></td>
        </tr>
        <tr>
            <th><?= __('Received Po Status') ?></th>
            <td><?= $receivedPo->has('received_po_status') ? $this->Html->link($receivedPo->received_po_status->name, ['controller' => 'ReceivedPoStatus', 'action' => 'view', $receivedPo->received_po_status->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($receivedPo->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Prepared By Id') ?></th>
            <td><?= $this->Number->format($receivedPo->prepared_by_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Date Registration') ?></th>
            <td><?= h($receivedPo->date_registration) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Received Po Details') ?></h4>
        <?php if (!empty($receivedPo->received_po_details)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Received Po Id') ?></th>
                <th><?= __('Product Id') ?></th>
                <th><?= __('Quantity Received') ?></th>
                <th><?= __('Product Serial') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($receivedPo->received_po_details as $receivedPoDetails): ?>
            <tr>
                <td><?= h($receivedPoDetails->received_po_id) ?></td>
                <td><?= h($receivedPoDetails->product_id) ?></td>
                <td><?= h($receivedPoDetails->quantity_received) ?></td>
                <td><?= h($receivedPoDetails->product_serial) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ReceivedPoDetails', 'action' => 'view', $receivedPoDetails->received_po_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ReceivedPoDetails', 'action' => 'edit', $receivedPoDetails->received_po_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ReceivedPoDetails', 'action' => 'delete', $receivedPoDetails->received_po_id], ['confirm' => __('Are you sure you want to delete # {0}?', $receivedPoDetails->received_po_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
