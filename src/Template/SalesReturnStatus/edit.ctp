<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $salesReturnStatus->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $salesReturnStatus->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Sales Return Status'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Sales Returns'), ['controller' => 'SalesReturns', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sales Return'), ['controller' => 'SalesReturns', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="salesReturnStatus form large-9 medium-8 columns content">
    <?= $this->Form->create($salesReturnStatus) ?>
    <fieldset>
        <legend><?= __('Edit Sales Return Status') ?></legend>
        <?php
            echo $this->Form->input('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
