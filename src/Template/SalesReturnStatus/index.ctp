<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Sales Return Status'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sales Returns'), ['controller' => 'SalesReturns', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sales Return'), ['controller' => 'SalesReturns', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="salesReturnStatus index large-9 medium-8 columns content">
    <h3><?= __('Sales Return Status') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($salesReturnStatus as $salesReturnStatus): ?>
            <tr>
                <td><?= $this->Number->format($salesReturnStatus->id) ?></td>
                <td><?= h($salesReturnStatus->name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $salesReturnStatus->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $salesReturnStatus->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $salesReturnStatus->id], ['confirm' => __('Are you sure you want to delete # {0}?', $salesReturnStatus->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
