<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Sales Return Status'), ['action' => 'edit', $salesReturnStatus->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Sales Return Status'), ['action' => 'delete', $salesReturnStatus->id], ['confirm' => __('Are you sure you want to delete # {0}?', $salesReturnStatus->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Sales Return Status'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sales Return Status'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sales Returns'), ['controller' => 'SalesReturns', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sales Return'), ['controller' => 'SalesReturns', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="salesReturnStatus view large-9 medium-8 columns content">
    <h3><?= h($salesReturnStatus->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($salesReturnStatus->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($salesReturnStatus->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Sales Returns') ?></h4>
        <?php if (!empty($salesReturnStatus->sales_returns)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Official Receipt Id') ?></th>
                <th><?= __('Depot Id') ?></th>
                <th><?= __('Prepared By Id') ?></th>
                <th><?= __('Approved By Id') ?></th>
                <th><?= __('Date Registration') ?></th>
                <th><?= __('Comment') ?></th>
                <th><?= __('Sales Return Status Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($salesReturnStatus->sales_returns as $salesReturns): ?>
            <tr>
                <td><?= h($salesReturns->id) ?></td>
                <td><?= h($salesReturns->official_receipt_id) ?></td>
                <td><?= h($salesReturns->depot_id) ?></td>
                <td><?= h($salesReturns->prepared_by_id) ?></td>
                <td><?= h($salesReturns->approved_by_id) ?></td>
                <td><?= h($salesReturns->date_registration) ?></td>
                <td><?= h($salesReturns->comment) ?></td>
                <td><?= h($salesReturns->sales_return_status_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'SalesReturns', 'action' => 'view', $salesReturns->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'SalesReturns', 'action' => 'edit', $salesReturns->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'SalesReturns', 'action' => 'delete', $salesReturns->id], ['confirm' => __('Are you sure you want to delete # {0}?', $salesReturns->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
