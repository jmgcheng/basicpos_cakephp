<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Included Inventory'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Included Inventory Status'), ['controller' => 'IncludedInventoryStatus', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Included Inventory Status'), ['controller' => 'IncludedInventoryStatus', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Included Inventory Details'), ['controller' => 'IncludedInventoryDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Included Inventory Detail'), ['controller' => 'IncludedInventoryDetails', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="includedInventories index large-9 medium-8 columns content">
    <h3><?= __('Included Inventories') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('depot_id') ?></th>
                <th><?= $this->Paginator->sort('prepared_by_id') ?></th>
                <th><?= $this->Paginator->sort('date_registration') ?></th>
                <th><?= $this->Paginator->sort('comment') ?></th>
                <th><?= $this->Paginator->sort('included_inventory_status_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($includedInventories as $includedInventory): ?>
            <tr>
                <td><?= $this->Number->format($includedInventory->id) ?></td>
                <td><?= $includedInventory->has('depot') ? $this->Html->link($includedInventory->depot->name, ['controller' => 'Depots', 'action' => 'view', $includedInventory->depot->id]) : '' ?></td>
                <td><?= $includedInventory->has('user') ? $this->Html->link($includedInventory->user->id, ['controller' => 'Users', 'action' => 'view', $includedInventory->user->id]) : '' ?></td>
                <td><?= h($includedInventory->date_registration) ?></td>
                <td><?= h($includedInventory->comment) ?></td>
                <td><?= $includedInventory->has('included_inventory_status') ? $this->Html->link($includedInventory->included_inventory_status->name, ['controller' => 'IncludedInventoryStatus', 'action' => 'view', $includedInventory->included_inventory_status->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $includedInventory->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $includedInventory->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $includedInventory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $includedInventory->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
