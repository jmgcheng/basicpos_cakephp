<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $includedInventory->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $includedInventory->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Included Inventories'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Included Inventory Status'), ['controller' => 'IncludedInventoryStatus', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Included Inventory Status'), ['controller' => 'IncludedInventoryStatus', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Included Inventory Details'), ['controller' => 'IncludedInventoryDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Included Inventory Detail'), ['controller' => 'IncludedInventoryDetails', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="includedInventories form large-9 medium-8 columns content">
    <?= $this->Form->create($includedInventory) ?>
    <fieldset>
        <legend><?= __('Edit Included Inventory') ?></legend>
        <?php
            echo $this->Form->input('depot_id', ['options' => $depots]);
            echo $this->Form->input('prepared_by_id', ['options' => $users]);
            echo $this->Form->input('date_registration');
            echo $this->Form->input('comment');
            echo $this->Form->input('included_inventory_status_id', ['options' => $includedInventoryStatus]);
        ?>
    </fieldset>

    <fieldset>
        <?php 
            if( isset($product_list) && !empty($product_list) ) :
        ?>
            <label>
                Product
            </label>
            <select id="js_add_transac_detail_id">
                <?php 
                    foreach( $product_list AS $product_list_detail ):
                        echo '<option value="'. $product_list_detail['id'] .'">' .$product_list_detail['name'] .' - '. $product_list_detail['model'] .' - '. $product_list_detail['product_color']['name'] .' - '. $product_list_detail['product_dimension']['name'] .' - '. $product_list_detail['product_unit']['name']. '</option>';
                    endforeach;
                ?>
            </select>
            <label>
                Quantity
            </label>
            <input type="text" id="js_add_transac_detail_qty" value="" placeholder="0" />
            <label>
                Serial
            </label>
            <input type="text" id="js_add_transac_detail_serial" value="" />

            <button type="button" onclick="addTransacDetail( 'product_id', document.getElementById('js_add_transac_detail_id').value, 'quantity_included', document.getElementById('js_add_transac_detail_qty').value, 'product_serial', document.getElementById('js_add_transac_detail_serial').value )">Add Detail</button>
        <?php 
            endif;
        ?>
    </fieldset>

    <fieldset id="js_fieldset_transaction_details">
        <?php 
            if( isset($transac_detail_list) && !empty($transac_detail_list) ):
                foreach( $transac_detail_list AS $transac_detail_list_detail ):
        ?>
                <div class="transac-detail">
                    <a href="#" onclick="removeTransacDetail(event)">remove detail</a>
                    <input value="<?php echo $transac_detail_list_detail['product_id']; ?>" name="product_id[]" type="text" />
                    <input value="<?php echo $transac_detail_list_detail['quantity_included'] ?>" name="quantity_included[]" type="text" />
                    <input value="<?php echo $transac_detail_list_detail['product_serial'] ?>" name="product_serial[]" type="text" />
                </div>
        <?php 
                endforeach;
            endif;
        ?>
    </fieldset>
    
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
