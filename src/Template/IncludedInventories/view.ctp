<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Included Inventory'), ['action' => 'edit', $includedInventory->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Included Inventory'), ['action' => 'delete', $includedInventory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $includedInventory->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Included Inventories'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Included Inventory'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Included Inventory Status'), ['controller' => 'IncludedInventoryStatus', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Included Inventory Status'), ['controller' => 'IncludedInventoryStatus', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Included Inventory Details'), ['controller' => 'IncludedInventoryDetails', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Included Inventory Detail'), ['controller' => 'IncludedInventoryDetails', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="includedInventories view large-9 medium-8 columns content">
    <h3><?= h($includedInventory->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Depot') ?></th>
            <td><?= $includedInventory->has('depot') ? $this->Html->link($includedInventory->depot->name, ['controller' => 'Depots', 'action' => 'view', $includedInventory->depot->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $includedInventory->has('user') ? $this->Html->link($includedInventory->user->id, ['controller' => 'Users', 'action' => 'view', $includedInventory->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Comment') ?></th>
            <td><?= h($includedInventory->comment) ?></td>
        </tr>
        <tr>
            <th><?= __('Included Inventory Status') ?></th>
            <td><?= $includedInventory->has('included_inventory_status') ? $this->Html->link($includedInventory->included_inventory_status->name, ['controller' => 'IncludedInventoryStatus', 'action' => 'view', $includedInventory->included_inventory_status->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($includedInventory->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Date Registration') ?></th>
            <td><?= h($includedInventory->date_registration) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Included Inventory Details') ?></h4>
        <?php if (!empty($includedInventory->included_inventory_details)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Included Inventory Id') ?></th>
                <th><?= __('Product Id') ?></th>
                <th><?= __('Quantity Included') ?></th>
                <th><?= __('Product Serial') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($includedInventory->included_inventory_details as $includedInventoryDetails): ?>
            <tr>
                <td><?= h($includedInventoryDetails->included_inventory_id) ?></td>
                <td><?= h($includedInventoryDetails->product_id) ?></td>
                <td><?= h($includedInventoryDetails->quantity_included) ?></td>
                <td><?= h($includedInventoryDetails->product_serial) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'IncludedInventoryDetails', 'action' => 'view', $includedInventoryDetails->included_inventory_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'IncludedInventoryDetails', 'action' => 'edit', $includedInventoryDetails->included_inventory_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'IncludedInventoryDetails', 'action' => 'delete', $includedInventoryDetails->included_inventory_id], ['confirm' => __('Are you sure you want to delete # {0}?', $includedInventoryDetails->included_inventory_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
