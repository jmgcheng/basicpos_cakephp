<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Wrong Send Po'), ['action' => 'edit', $wrongSendPo->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Wrong Send Po'), ['action' => 'delete', $wrongSendPo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $wrongSendPo->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Wrong Send Pos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Wrong Send Po'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Purchase Orders'), ['controller' => 'PurchaseOrders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Purchase Order'), ['controller' => 'PurchaseOrders', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Wrong Send Po Status'), ['controller' => 'WrongSendPoStatus', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Wrong Send Po Status'), ['controller' => 'WrongSendPoStatus', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Wrong Send Po Details'), ['controller' => 'WrongSendPoDetails', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Wrong Send Po Detail'), ['controller' => 'WrongSendPoDetails', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="wrongSendPos view large-9 medium-8 columns content">
    <h3><?= h($wrongSendPo->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Purchase Order') ?></th>
            <td><?= $wrongSendPo->has('purchase_order') ? $this->Html->link($wrongSendPo->purchase_order->title, ['controller' => 'PurchaseOrders', 'action' => 'view', $wrongSendPo->purchase_order->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Depot') ?></th>
            <td><?= $wrongSendPo->has('depot') ? $this->Html->link($wrongSendPo->depot->name, ['controller' => 'Depots', 'action' => 'view', $wrongSendPo->depot->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $wrongSendPo->has('user') ? $this->Html->link($wrongSendPo->user->id, ['controller' => 'Users', 'action' => 'view', $wrongSendPo->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Comment') ?></th>
            <td><?= h($wrongSendPo->comment) ?></td>
        </tr>
        <tr>
            <th><?= __('Wrong Send Po Status') ?></th>
            <td><?= $wrongSendPo->has('wrong_send_po_status') ? $this->Html->link($wrongSendPo->wrong_send_po_status->name, ['controller' => 'WrongSendPoStatus', 'action' => 'view', $wrongSendPo->wrong_send_po_status->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($wrongSendPo->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Received By Id') ?></th>
            <td><?= $this->Number->format($wrongSendPo->received_by_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Date Registration') ?></th>
            <td><?= h($wrongSendPo->date_registration) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Wrong Send Po Details') ?></h4>
        <?php if (!empty($wrongSendPo->wrong_send_po_details)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Wrong Send Po Id') ?></th>
                <th><?= __('Product Id') ?></th>
                <th><?= __('Quantity Wrong Send') ?></th>
                <th><?= __('Product Serial') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($wrongSendPo->wrong_send_po_details as $wrongSendPoDetails): ?>
            <tr>
                <td><?= h($wrongSendPoDetails->wrong_send_po_id) ?></td>
                <td><?= h($wrongSendPoDetails->product_id) ?></td>
                <td><?= h($wrongSendPoDetails->quantity_wrong_send) ?></td>
                <td><?= h($wrongSendPoDetails->product_serial) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'WrongSendPoDetails', 'action' => 'view', $wrongSendPoDetails->wrong_send_po_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'WrongSendPoDetails', 'action' => 'edit', $wrongSendPoDetails->wrong_send_po_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'WrongSendPoDetails', 'action' => 'delete', $wrongSendPoDetails->wrong_send_po_id], ['confirm' => __('Are you sure you want to delete # {0}?', $wrongSendPoDetails->wrong_send_po_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
