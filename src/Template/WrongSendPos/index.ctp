<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Wrong Send Po'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Purchase Orders'), ['controller' => 'PurchaseOrders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Purchase Order'), ['controller' => 'PurchaseOrders', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Wrong Send Po Status'), ['controller' => 'WrongSendPoStatus', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Wrong Send Po Status'), ['controller' => 'WrongSendPoStatus', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Wrong Send Po Details'), ['controller' => 'WrongSendPoDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Wrong Send Po Detail'), ['controller' => 'WrongSendPoDetails', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="wrongSendPos index large-9 medium-8 columns content">
    <h3><?= __('Wrong Send Pos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('purchase_order_id') ?></th>
                <th><?= $this->Paginator->sort('depot_id') ?></th>
                <th><?= $this->Paginator->sort('received_by_id') ?></th>
                <th><?= $this->Paginator->sort('approved_by_id') ?></th>
                <th><?= $this->Paginator->sort('date_registration') ?></th>
                <th><?= $this->Paginator->sort('comment') ?></th>
                <th><?= $this->Paginator->sort('wrong_send_po_status_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($wrongSendPos as $wrongSendPo): ?>
            <tr>
                <td><?= $this->Number->format($wrongSendPo->id) ?></td>
                <td><?= $wrongSendPo->has('purchase_order') ? $this->Html->link($wrongSendPo->purchase_order->title, ['controller' => 'PurchaseOrders', 'action' => 'view', $wrongSendPo->purchase_order->id]) : '' ?></td>
                <td><?= $wrongSendPo->has('depot') ? $this->Html->link($wrongSendPo->depot->name, ['controller' => 'Depots', 'action' => 'view', $wrongSendPo->depot->id]) : '' ?></td>
                <td><?= $this->Number->format($wrongSendPo->received_by_id) ?></td>
                <td><?= $wrongSendPo->has('user') ? $this->Html->link($wrongSendPo->user->id, ['controller' => 'Users', 'action' => 'view', $wrongSendPo->user->id]) : '' ?></td>
                <td><?= h($wrongSendPo->date_registration) ?></td>
                <td><?= h($wrongSendPo->comment) ?></td>
                <td><?= $wrongSendPo->has('wrong_send_po_status') ? $this->Html->link($wrongSendPo->wrong_send_po_status->name, ['controller' => 'WrongSendPoStatus', 'action' => 'view', $wrongSendPo->wrong_send_po_status->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $wrongSendPo->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $wrongSendPo->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $wrongSendPo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $wrongSendPo->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
