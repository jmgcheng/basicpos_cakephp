<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Purchase Order Detail'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Purchase Orders'), ['controller' => 'PurchaseOrders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Purchase Order'), ['controller' => 'PurchaseOrders', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="purchaseOrderDetails index large-9 medium-8 columns content">
    <h3><?= __('Purchase Order Details') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('purchase_order_id') ?></th>
                <th><?= $this->Paginator->sort('product_id') ?></th>
                <th><?= $this->Paginator->sort('quantity_ordered') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($purchaseOrderDetails as $purchaseOrderDetail): ?>
            <tr>
                <td><?= $purchaseOrderDetail->has('purchase_order') ? $this->Html->link($purchaseOrderDetail->purchase_order->title, ['controller' => 'PurchaseOrders', 'action' => 'view', $purchaseOrderDetail->purchase_order->id]) : '' ?></td>
                <td><?= $purchaseOrderDetail->has('product') ? $this->Html->link($purchaseOrderDetail->product->name, ['controller' => 'Products', 'action' => 'view', $purchaseOrderDetail->product->id]) : '' ?></td>
                <td><?= $this->Number->format($purchaseOrderDetail->quantity_ordered) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $purchaseOrderDetail->purchase_order_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $purchaseOrderDetail->purchase_order_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $purchaseOrderDetail->purchase_order_id], ['confirm' => __('Are you sure you want to delete # {0}?', $purchaseOrderDetail->purchase_order_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
