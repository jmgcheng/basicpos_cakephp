<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $purchaseOrderDetail->purchase_order_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $purchaseOrderDetail->purchase_order_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Purchase Order Details'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Purchase Orders'), ['controller' => 'PurchaseOrders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Purchase Order'), ['controller' => 'PurchaseOrders', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="purchaseOrderDetails form large-9 medium-8 columns content">
    <?= $this->Form->create($purchaseOrderDetail) ?>
    <fieldset>
        <legend><?= __('Edit Purchase Order Detail') ?></legend>
        <?php
            echo $this->Form->input('quantity_ordered');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
