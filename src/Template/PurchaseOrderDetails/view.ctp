<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Purchase Order Detail'), ['action' => 'edit', $purchaseOrderDetail->purchase_order_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Purchase Order Detail'), ['action' => 'delete', $purchaseOrderDetail->purchase_order_id], ['confirm' => __('Are you sure you want to delete # {0}?', $purchaseOrderDetail->purchase_order_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Purchase Order Details'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Purchase Order Detail'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Purchase Orders'), ['controller' => 'PurchaseOrders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Purchase Order'), ['controller' => 'PurchaseOrders', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="purchaseOrderDetails view large-9 medium-8 columns content">
    <h3><?= h($purchaseOrderDetail->purchase_order_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Purchase Order') ?></th>
            <td><?= $purchaseOrderDetail->has('purchase_order') ? $this->Html->link($purchaseOrderDetail->purchase_order->title, ['controller' => 'PurchaseOrders', 'action' => 'view', $purchaseOrderDetail->purchase_order->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Product') ?></th>
            <td><?= $purchaseOrderDetail->has('product') ? $this->Html->link($purchaseOrderDetail->product->name, ['controller' => 'Products', 'action' => 'view', $purchaseOrderDetail->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Quantity Ordered') ?></th>
            <td><?= $this->Number->format($purchaseOrderDetail->quantity_ordered) ?></td>
        </tr>
    </table>
</div>
