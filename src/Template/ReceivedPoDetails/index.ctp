<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Received Po Detail'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Received Pos'), ['controller' => 'ReceivedPos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Received Po'), ['controller' => 'ReceivedPos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="receivedPoDetails index large-9 medium-8 columns content">
    <h3><?= __('Received Po Details') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('received_po_id') ?></th>
                <th><?= $this->Paginator->sort('product_id') ?></th>
                <th><?= $this->Paginator->sort('quantity_received') ?></th>
                <th><?= $this->Paginator->sort('product_serial') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($receivedPoDetails as $receivedPoDetail): ?>
            <tr>
                <td><?= $receivedPoDetail->has('received_po') ? $this->Html->link($receivedPoDetail->received_po->id, ['controller' => 'ReceivedPos', 'action' => 'view', $receivedPoDetail->received_po->id]) : '' ?></td>
                <td><?= $receivedPoDetail->has('product') ? $this->Html->link($receivedPoDetail->product->name, ['controller' => 'Products', 'action' => 'view', $receivedPoDetail->product->id]) : '' ?></td>
                <td><?= $this->Number->format($receivedPoDetail->quantity_received) ?></td>
                <td><?= h($receivedPoDetail->product_serial) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $receivedPoDetail->received_po_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $receivedPoDetail->received_po_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $receivedPoDetail->received_po_id], ['confirm' => __('Are you sure you want to delete # {0}?', $receivedPoDetail->received_po_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
