<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $receivedPoDetail->received_po_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $receivedPoDetail->received_po_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Received Po Details'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Received Pos'), ['controller' => 'ReceivedPos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Received Po'), ['controller' => 'ReceivedPos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="receivedPoDetails form large-9 medium-8 columns content">
    <?= $this->Form->create($receivedPoDetail) ?>
    <fieldset>
        <legend><?= __('Edit Received Po Detail') ?></legend>
        <?php
            echo $this->Form->input('quantity_received');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
