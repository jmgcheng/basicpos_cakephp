<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Received Po Detail'), ['action' => 'edit', $receivedPoDetail->received_po_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Received Po Detail'), ['action' => 'delete', $receivedPoDetail->received_po_id], ['confirm' => __('Are you sure you want to delete # {0}?', $receivedPoDetail->received_po_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Received Po Details'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Received Po Detail'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Received Pos'), ['controller' => 'ReceivedPos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Received Po'), ['controller' => 'ReceivedPos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="receivedPoDetails view large-9 medium-8 columns content">
    <h3><?= h($receivedPoDetail->received_po_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Received Po') ?></th>
            <td><?= $receivedPoDetail->has('received_po') ? $this->Html->link($receivedPoDetail->received_po->id, ['controller' => 'ReceivedPos', 'action' => 'view', $receivedPoDetail->received_po->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Product') ?></th>
            <td><?= $receivedPoDetail->has('product') ? $this->Html->link($receivedPoDetail->product->name, ['controller' => 'Products', 'action' => 'view', $receivedPoDetail->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Product Serial') ?></th>
            <td><?= h($receivedPoDetail->product_serial) ?></td>
        </tr>
        <tr>
            <th><?= __('Quantity Received') ?></th>
            <td><?= $this->Number->format($receivedPoDetail->quantity_received) ?></td>
        </tr>
    </table>
</div>
