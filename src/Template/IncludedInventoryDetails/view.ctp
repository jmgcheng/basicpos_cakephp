<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Included Inventory Detail'), ['action' => 'edit', $includedInventoryDetail->included_inventory_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Included Inventory Detail'), ['action' => 'delete', $includedInventoryDetail->included_inventory_id], ['confirm' => __('Are you sure you want to delete # {0}?', $includedInventoryDetail->included_inventory_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Included Inventory Details'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Included Inventory Detail'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Included Inventories'), ['controller' => 'IncludedInventories', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Included Inventory'), ['controller' => 'IncludedInventories', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="includedInventoryDetails view large-9 medium-8 columns content">
    <h3><?= h($includedInventoryDetail->included_inventory_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Included Inventory') ?></th>
            <td><?= $includedInventoryDetail->has('included_inventory') ? $this->Html->link($includedInventoryDetail->included_inventory->id, ['controller' => 'IncludedInventories', 'action' => 'view', $includedInventoryDetail->included_inventory->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Product') ?></th>
            <td><?= $includedInventoryDetail->has('product') ? $this->Html->link($includedInventoryDetail->product->name, ['controller' => 'Products', 'action' => 'view', $includedInventoryDetail->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Product Serial') ?></th>
            <td><?= h($includedInventoryDetail->product_serial) ?></td>
        </tr>
        <tr>
            <th><?= __('Quantity Included') ?></th>
            <td><?= $this->Number->format($includedInventoryDetail->quantity_included) ?></td>
        </tr>
    </table>
</div>
