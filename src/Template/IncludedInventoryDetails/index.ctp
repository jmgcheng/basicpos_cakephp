<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Included Inventory Detail'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Included Inventories'), ['controller' => 'IncludedInventories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Included Inventory'), ['controller' => 'IncludedInventories', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="includedInventoryDetails index large-9 medium-8 columns content">
    <h3><?= __('Included Inventory Details') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('included_inventory_id') ?></th>
                <th><?= $this->Paginator->sort('product_id') ?></th>
                <th><?= $this->Paginator->sort('quantity_included') ?></th>
                <th><?= $this->Paginator->sort('product_serial') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($includedInventoryDetails as $includedInventoryDetail): ?>
            <tr>
                <td><?= $includedInventoryDetail->has('included_inventory') ? $this->Html->link($includedInventoryDetail->included_inventory->id, ['controller' => 'IncludedInventories', 'action' => 'view', $includedInventoryDetail->included_inventory->id]) : '' ?></td>
                <td><?= $includedInventoryDetail->has('product') ? $this->Html->link($includedInventoryDetail->product->name, ['controller' => 'Products', 'action' => 'view', $includedInventoryDetail->product->id]) : '' ?></td>
                <td><?= $this->Number->format($includedInventoryDetail->quantity_included) ?></td>
                <td><?= h($includedInventoryDetail->product_serial) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $includedInventoryDetail->included_inventory_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $includedInventoryDetail->included_inventory_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $includedInventoryDetail->included_inventory_id], ['confirm' => __('Are you sure you want to delete # {0}?', $includedInventoryDetail->included_inventory_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
