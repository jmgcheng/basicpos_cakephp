<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Included Inventory Details'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Included Inventories'), ['controller' => 'IncludedInventories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Included Inventory'), ['controller' => 'IncludedInventories', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="includedInventoryDetails form large-9 medium-8 columns content">
    <?= $this->Form->create($includedInventoryDetail) ?>
    <fieldset>
        <legend><?= __('Add Included Inventory Detail') ?></legend>
        <?php
            echo $this->Form->input('quantity_included');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
