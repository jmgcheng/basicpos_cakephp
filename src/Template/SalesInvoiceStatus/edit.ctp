<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $salesInvoiceStatus->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $salesInvoiceStatus->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Sales Invoice Status'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Sales Invoices'), ['controller' => 'SalesInvoices', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sales Invoice'), ['controller' => 'SalesInvoices', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="salesInvoiceStatus form large-9 medium-8 columns content">
    <?= $this->Form->create($salesInvoiceStatus) ?>
    <fieldset>
        <legend><?= __('Edit Sales Invoice Status') ?></legend>
        <?php
            echo $this->Form->input('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
