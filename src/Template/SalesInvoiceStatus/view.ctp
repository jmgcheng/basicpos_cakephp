<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Sales Invoice Status'), ['action' => 'edit', $salesInvoiceStatus->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Sales Invoice Status'), ['action' => 'delete', $salesInvoiceStatus->id], ['confirm' => __('Are you sure you want to delete # {0}?', $salesInvoiceStatus->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Sales Invoice Status'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sales Invoice Status'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sales Invoices'), ['controller' => 'SalesInvoices', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sales Invoice'), ['controller' => 'SalesInvoices', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="salesInvoiceStatus view large-9 medium-8 columns content">
    <h3><?= h($salesInvoiceStatus->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($salesInvoiceStatus->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($salesInvoiceStatus->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Sales Invoices') ?></h4>
        <?php if (!empty($salesInvoiceStatus->sales_invoices)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Depot Id') ?></th>
                <th><?= __('Prepared By Id') ?></th>
                <th><?= __('Approved By Id') ?></th>
                <th><?= __('Date Registration') ?></th>
                <th><?= __('Title') ?></th>
                <th><?= __('Comment') ?></th>
                <th><?= __('Sales Invoice Status Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($salesInvoiceStatus->sales_invoices as $salesInvoices): ?>
            <tr>
                <td><?= h($salesInvoices->id) ?></td>
                <td><?= h($salesInvoices->depot_id) ?></td>
                <td><?= h($salesInvoices->prepared_by_id) ?></td>
                <td><?= h($salesInvoices->approved_by_id) ?></td>
                <td><?= h($salesInvoices->date_registration) ?></td>
                <td><?= h($salesInvoices->title) ?></td>
                <td><?= h($salesInvoices->comment) ?></td>
                <td><?= h($salesInvoices->sales_invoice_status_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'SalesInvoices', 'action' => 'view', $salesInvoices->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'SalesInvoices', 'action' => 'edit', $salesInvoices->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'SalesInvoices', 'action' => 'delete', $salesInvoices->id], ['confirm' => __('Are you sure you want to delete # {0}?', $salesInvoices->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
