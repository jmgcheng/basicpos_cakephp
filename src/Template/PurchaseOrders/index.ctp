<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Purchase Order'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Purchase Order Status'), ['controller' => 'PurchaseOrderStatus', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Purchase Order Status'), ['controller' => 'PurchaseOrderStatus', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Missed Pos'), ['controller' => 'MissedPos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Missed Po'), ['controller' => 'MissedPos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Purchase Order Details'), ['controller' => 'PurchaseOrderDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Purchase Order Detail'), ['controller' => 'PurchaseOrderDetails', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Received Pos'), ['controller' => 'ReceivedPos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Received Po'), ['controller' => 'ReceivedPos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Wrong Send Pos'), ['controller' => 'WrongSendPos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Wrong Send Po'), ['controller' => 'WrongSendPos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="purchaseOrders index large-9 medium-8 columns content">
    <h3><?= __('Purchase Orders') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('depot_id') ?></th>
                <th><?= $this->Paginator->sort('prepared_by_id') ?></th>
                <th><?= $this->Paginator->sort('approved_by_id') ?></th>
                <th><?= $this->Paginator->sort('date_registration') ?></th>
                <th><?= $this->Paginator->sort('title') ?></th>
                <th><?= $this->Paginator->sort('order_description') ?></th>
                <th><?= $this->Paginator->sort('comment') ?></th>
                <th><?= $this->Paginator->sort('purchase_order_status_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($purchaseOrders as $purchaseOrder): ?>
            <tr>
                <td><?= $this->Number->format($purchaseOrder->id) ?></td>
                <td><?= $purchaseOrder->has('depot') ? $this->Html->link($purchaseOrder->depot->name, ['controller' => 'Depots', 'action' => 'view', $purchaseOrder->depot->id]) : '' ?></td>
                <td><?= $this->Number->format($purchaseOrder->prepared_by_id) ?></td>
                <td><?= $purchaseOrder->has('user') ? $this->Html->link($purchaseOrder->user->id, ['controller' => 'Users', 'action' => 'view', $purchaseOrder->user->id]) : '' ?></td>
                <td><?= h($purchaseOrder->date_registration) ?></td>
                <td><?= h($purchaseOrder->title) ?></td>
                <td><?= h($purchaseOrder->order_description) ?></td>
                <td><?= h($purchaseOrder->comment) ?></td>
                <td><?= $purchaseOrder->has('purchase_order_status') ? $this->Html->link($purchaseOrder->purchase_order_status->name, ['controller' => 'PurchaseOrderStatus', 'action' => 'view', $purchaseOrder->purchase_order_status->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $purchaseOrder->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $purchaseOrder->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $purchaseOrder->id], ['confirm' => __('Are you sure you want to delete # {0}?', $purchaseOrder->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
