<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Purchase Order'), ['action' => 'edit', $purchaseOrder->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Purchase Order'), ['action' => 'delete', $purchaseOrder->id], ['confirm' => __('Are you sure you want to delete # {0}?', $purchaseOrder->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Purchase Orders'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Purchase Order'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Purchase Order Status'), ['controller' => 'PurchaseOrderStatus', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Purchase Order Status'), ['controller' => 'PurchaseOrderStatus', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Missed Pos'), ['controller' => 'MissedPos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Missed Po'), ['controller' => 'MissedPos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Purchase Order Details'), ['controller' => 'PurchaseOrderDetails', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Purchase Order Detail'), ['controller' => 'PurchaseOrderDetails', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Received Pos'), ['controller' => 'ReceivedPos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Received Po'), ['controller' => 'ReceivedPos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Wrong Send Pos'), ['controller' => 'WrongSendPos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Wrong Send Po'), ['controller' => 'WrongSendPos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="purchaseOrders view large-9 medium-8 columns content">
    <h3><?= h($purchaseOrder->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Depot') ?></th>
            <td><?= $purchaseOrder->has('depot') ? $this->Html->link($purchaseOrder->depot->name, ['controller' => 'Depots', 'action' => 'view', $purchaseOrder->depot->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $purchaseOrder->has('user') ? $this->Html->link($purchaseOrder->user->id, ['controller' => 'Users', 'action' => 'view', $purchaseOrder->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Title') ?></th>
            <td><?= h($purchaseOrder->title) ?></td>
        </tr>
        <tr>
            <th><?= __('Order Description') ?></th>
            <td><?= h($purchaseOrder->order_description) ?></td>
        </tr>
        <tr>
            <th><?= __('Comment') ?></th>
            <td><?= h($purchaseOrder->comment) ?></td>
        </tr>
        <tr>
            <th><?= __('Purchase Order Status') ?></th>
            <td><?= $purchaseOrder->has('purchase_order_status') ? $this->Html->link($purchaseOrder->purchase_order_status->name, ['controller' => 'PurchaseOrderStatus', 'action' => 'view', $purchaseOrder->purchase_order_status->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($purchaseOrder->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Prepared By Id') ?></th>
            <td><?= $this->Number->format($purchaseOrder->prepared_by_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Date Registration') ?></th>
            <td><?= h($purchaseOrder->date_registration) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Missed Pos') ?></h4>
        <?php if (!empty($purchaseOrder->missed_pos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Purchase Order Id') ?></th>
                <th><?= __('Depot Id') ?></th>
                <th><?= __('Received By Id') ?></th>
                <th><?= __('Approved By Id') ?></th>
                <th><?= __('Date Registration') ?></th>
                <th><?= __('Comment') ?></th>
                <th><?= __('Missed Po Status Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($purchaseOrder->missed_pos as $missedPos): ?>
            <tr>
                <td><?= h($missedPos->id) ?></td>
                <td><?= h($missedPos->purchase_order_id) ?></td>
                <td><?= h($missedPos->depot_id) ?></td>
                <td><?= h($missedPos->received_by_id) ?></td>
                <td><?= h($missedPos->approved_by_id) ?></td>
                <td><?= h($missedPos->date_registration) ?></td>
                <td><?= h($missedPos->comment) ?></td>
                <td><?= h($missedPos->missed_po_status_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'MissedPos', 'action' => 'view', $missedPos->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'MissedPos', 'action' => 'edit', $missedPos->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'MissedPos', 'action' => 'delete', $missedPos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $missedPos->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Purchase Order Details') ?></h4>
        <?php if (!empty($purchaseOrder->purchase_order_details)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Purchase Order Id') ?></th>
                <th><?= __('Product Id') ?></th>
                <th><?= __('Quantity Ordered') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($purchaseOrder->purchase_order_details as $purchaseOrderDetails): ?>
            <tr>
                <td><?= h($purchaseOrderDetails->purchase_order_id) ?></td>
                <td><?= h($purchaseOrderDetails->product_id) ?></td>
                <td><?= h($purchaseOrderDetails->quantity_ordered) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PurchaseOrderDetails', 'action' => 'view', $purchaseOrderDetails->purchase_order_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PurchaseOrderDetails', 'action' => 'edit', $purchaseOrderDetails->purchase_order_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PurchaseOrderDetails', 'action' => 'delete', $purchaseOrderDetails->purchase_order_id], ['confirm' => __('Are you sure you want to delete # {0}?', $purchaseOrderDetails->purchase_order_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Received Pos') ?></h4>
        <?php if (!empty($purchaseOrder->received_pos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Purchase Order Id') ?></th>
                <th><?= __('Depot Id') ?></th>
                <th><?= __('Prepared By Id') ?></th>
                <th><?= __('Approved By Id') ?></th>
                <th><?= __('Date Registration') ?></th>
                <th><?= __('Comment') ?></th>
                <th><?= __('Received Po Status Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($purchaseOrder->received_pos as $receivedPos): ?>
            <tr>
                <td><?= h($receivedPos->id) ?></td>
                <td><?= h($receivedPos->purchase_order_id) ?></td>
                <td><?= h($receivedPos->depot_id) ?></td>
                <td><?= h($receivedPos->prepared_by_id) ?></td>
                <td><?= h($receivedPos->approved_by_id) ?></td>
                <td><?= h($receivedPos->date_registration) ?></td>
                <td><?= h($receivedPos->comment) ?></td>
                <td><?= h($receivedPos->received_po_status_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ReceivedPos', 'action' => 'view', $receivedPos->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ReceivedPos', 'action' => 'edit', $receivedPos->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ReceivedPos', 'action' => 'delete', $receivedPos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $receivedPos->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Wrong Send Pos') ?></h4>
        <?php if (!empty($purchaseOrder->wrong_send_pos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Purchase Order Id') ?></th>
                <th><?= __('Depot Id') ?></th>
                <th><?= __('Received By Id') ?></th>
                <th><?= __('Approved By Id') ?></th>
                <th><?= __('Date Registration') ?></th>
                <th><?= __('Comment') ?></th>
                <th><?= __('Wrong Send Po Status Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($purchaseOrder->wrong_send_pos as $wrongSendPos): ?>
            <tr>
                <td><?= h($wrongSendPos->id) ?></td>
                <td><?= h($wrongSendPos->purchase_order_id) ?></td>
                <td><?= h($wrongSendPos->depot_id) ?></td>
                <td><?= h($wrongSendPos->received_by_id) ?></td>
                <td><?= h($wrongSendPos->approved_by_id) ?></td>
                <td><?= h($wrongSendPos->date_registration) ?></td>
                <td><?= h($wrongSendPos->comment) ?></td>
                <td><?= h($wrongSendPos->wrong_send_po_status_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'WrongSendPos', 'action' => 'view', $wrongSendPos->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'WrongSendPos', 'action' => 'edit', $wrongSendPos->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'WrongSendPos', 'action' => 'delete', $wrongSendPos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $wrongSendPos->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
