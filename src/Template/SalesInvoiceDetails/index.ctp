<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Sales Invoice Detail'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sales Invoices'), ['controller' => 'SalesInvoices', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sales Invoice'), ['controller' => 'SalesInvoices', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="salesInvoiceDetails index large-9 medium-8 columns content">
    <h3><?= __('Sales Invoice Details') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('sales_invoice_id') ?></th>
                <th><?= $this->Paginator->sort('product_id') ?></th>
                <th><?= $this->Paginator->sort('quantity_to_release') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($salesInvoiceDetails as $salesInvoiceDetail): ?>
            <tr>
                <td><?= $salesInvoiceDetail->has('sales_invoice') ? $this->Html->link($salesInvoiceDetail->sales_invoice->title, ['controller' => 'SalesInvoices', 'action' => 'view', $salesInvoiceDetail->sales_invoice->id]) : '' ?></td>
                <td><?= $salesInvoiceDetail->has('product') ? $this->Html->link($salesInvoiceDetail->product->name, ['controller' => 'Products', 'action' => 'view', $salesInvoiceDetail->product->id]) : '' ?></td>
                <td><?= $this->Number->format($salesInvoiceDetail->quantity_to_release) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $salesInvoiceDetail->sales_invoice_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $salesInvoiceDetail->sales_invoice_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $salesInvoiceDetail->sales_invoice_id], ['confirm' => __('Are you sure you want to delete # {0}?', $salesInvoiceDetail->sales_invoice_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
