<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $salesInvoiceDetail->sales_invoice_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $salesInvoiceDetail->sales_invoice_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Sales Invoice Details'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Sales Invoices'), ['controller' => 'SalesInvoices', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sales Invoice'), ['controller' => 'SalesInvoices', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="salesInvoiceDetails form large-9 medium-8 columns content">
    <?= $this->Form->create($salesInvoiceDetail) ?>
    <fieldset>
        <legend><?= __('Edit Sales Invoice Detail') ?></legend>
        <?php
            echo $this->Form->input('quantity_to_release');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
