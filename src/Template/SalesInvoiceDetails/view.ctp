<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Sales Invoice Detail'), ['action' => 'edit', $salesInvoiceDetail->sales_invoice_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Sales Invoice Detail'), ['action' => 'delete', $salesInvoiceDetail->sales_invoice_id], ['confirm' => __('Are you sure you want to delete # {0}?', $salesInvoiceDetail->sales_invoice_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Sales Invoice Details'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sales Invoice Detail'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sales Invoices'), ['controller' => 'SalesInvoices', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sales Invoice'), ['controller' => 'SalesInvoices', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="salesInvoiceDetails view large-9 medium-8 columns content">
    <h3><?= h($salesInvoiceDetail->sales_invoice_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Sales Invoice') ?></th>
            <td><?= $salesInvoiceDetail->has('sales_invoice') ? $this->Html->link($salesInvoiceDetail->sales_invoice->title, ['controller' => 'SalesInvoices', 'action' => 'view', $salesInvoiceDetail->sales_invoice->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Product') ?></th>
            <td><?= $salesInvoiceDetail->has('product') ? $this->Html->link($salesInvoiceDetail->product->name, ['controller' => 'Products', 'action' => 'view', $salesInvoiceDetail->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Quantity To Release') ?></th>
            <td><?= $this->Number->format($salesInvoiceDetail->quantity_to_release) ?></td>
        </tr>
    </table>
</div>
