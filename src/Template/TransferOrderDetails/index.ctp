<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Transfer Order Detail'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Transfer Orders'), ['controller' => 'TransferOrders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Transfer Order'), ['controller' => 'TransferOrders', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="transferOrderDetails index large-9 medium-8 columns content">
    <h3><?= __('Transfer Order Details') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('transfer_order_id') ?></th>
                <th><?= $this->Paginator->sort('product_id') ?></th>
                <th><?= $this->Paginator->sort('quantity_transfering') ?></th>
                <th><?= $this->Paginator->sort('product_serial') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($transferOrderDetails as $transferOrderDetail): ?>
            <tr>
                <td><?= $transferOrderDetail->has('transfer_order') ? $this->Html->link($transferOrderDetail->transfer_order->id, ['controller' => 'TransferOrders', 'action' => 'view', $transferOrderDetail->transfer_order->id]) : '' ?></td>
                <td><?= $transferOrderDetail->has('product') ? $this->Html->link($transferOrderDetail->product->name, ['controller' => 'Products', 'action' => 'view', $transferOrderDetail->product->id]) : '' ?></td>
                <td><?= $this->Number->format($transferOrderDetail->quantity_transfering) ?></td>
                <td><?= h($transferOrderDetail->product_serial) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $transferOrderDetail->transfer_order_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $transferOrderDetail->transfer_order_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $transferOrderDetail->transfer_order_id], ['confirm' => __('Are you sure you want to delete # {0}?', $transferOrderDetail->transfer_order_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
