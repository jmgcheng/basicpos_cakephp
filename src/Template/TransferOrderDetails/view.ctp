<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Transfer Order Detail'), ['action' => 'edit', $transferOrderDetail->transfer_order_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Transfer Order Detail'), ['action' => 'delete', $transferOrderDetail->transfer_order_id], ['confirm' => __('Are you sure you want to delete # {0}?', $transferOrderDetail->transfer_order_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Transfer Order Details'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Transfer Order Detail'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Transfer Orders'), ['controller' => 'TransferOrders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Transfer Order'), ['controller' => 'TransferOrders', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="transferOrderDetails view large-9 medium-8 columns content">
    <h3><?= h($transferOrderDetail->transfer_order_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Transfer Order') ?></th>
            <td><?= $transferOrderDetail->has('transfer_order') ? $this->Html->link($transferOrderDetail->transfer_order->id, ['controller' => 'TransferOrders', 'action' => 'view', $transferOrderDetail->transfer_order->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Product') ?></th>
            <td><?= $transferOrderDetail->has('product') ? $this->Html->link($transferOrderDetail->product->name, ['controller' => 'Products', 'action' => 'view', $transferOrderDetail->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Product Serial') ?></th>
            <td><?= h($transferOrderDetail->product_serial) ?></td>
        </tr>
        <tr>
            <th><?= __('Quantity Transfering') ?></th>
            <td><?= $this->Number->format($transferOrderDetail->quantity_transfering) ?></td>
        </tr>
    </table>
</div>
