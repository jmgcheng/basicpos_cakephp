<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $transferOrderDetail->transfer_order_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $transferOrderDetail->transfer_order_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Transfer Order Details'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Transfer Orders'), ['controller' => 'TransferOrders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Transfer Order'), ['controller' => 'TransferOrders', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="transferOrderDetails form large-9 medium-8 columns content">
    <?= $this->Form->create($transferOrderDetail) ?>
    <fieldset>
        <legend><?= __('Edit Transfer Order Detail') ?></legend>
        <?php
            echo $this->Form->input('quantity_transfering');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
