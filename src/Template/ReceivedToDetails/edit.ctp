<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $receivedToDetail->received_to_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $receivedToDetail->received_to_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Received To Details'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Received Tos'), ['controller' => 'ReceivedTos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Received To'), ['controller' => 'ReceivedTos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="receivedToDetails form large-9 medium-8 columns content">
    <?= $this->Form->create($receivedToDetail) ?>
    <fieldset>
        <legend><?= __('Edit Received To Detail') ?></legend>
        <?php
            echo $this->Form->input('quantity_received');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
