<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Received To Detail'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Received Tos'), ['controller' => 'ReceivedTos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Received To'), ['controller' => 'ReceivedTos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="receivedToDetails index large-9 medium-8 columns content">
    <h3><?= __('Received To Details') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('received_to_id') ?></th>
                <th><?= $this->Paginator->sort('product_id') ?></th>
                <th><?= $this->Paginator->sort('quantity_received') ?></th>
                <th><?= $this->Paginator->sort('product_serial') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($receivedToDetails as $receivedToDetail): ?>
            <tr>
                <td><?= $receivedToDetail->has('received_to') ? $this->Html->link($receivedToDetail->received_to->id, ['controller' => 'ReceivedTos', 'action' => 'view', $receivedToDetail->received_to->id]) : '' ?></td>
                <td><?= $receivedToDetail->has('product') ? $this->Html->link($receivedToDetail->product->name, ['controller' => 'Products', 'action' => 'view', $receivedToDetail->product->id]) : '' ?></td>
                <td><?= $this->Number->format($receivedToDetail->quantity_received) ?></td>
                <td><?= h($receivedToDetail->product_serial) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $receivedToDetail->received_to_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $receivedToDetail->received_to_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $receivedToDetail->received_to_id], ['confirm' => __('Are you sure you want to delete # {0}?', $receivedToDetail->received_to_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
