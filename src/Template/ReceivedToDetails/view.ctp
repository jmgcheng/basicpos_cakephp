<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Received To Detail'), ['action' => 'edit', $receivedToDetail->received_to_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Received To Detail'), ['action' => 'delete', $receivedToDetail->received_to_id], ['confirm' => __('Are you sure you want to delete # {0}?', $receivedToDetail->received_to_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Received To Details'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Received To Detail'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Received Tos'), ['controller' => 'ReceivedTos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Received To'), ['controller' => 'ReceivedTos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="receivedToDetails view large-9 medium-8 columns content">
    <h3><?= h($receivedToDetail->received_to_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Received To') ?></th>
            <td><?= $receivedToDetail->has('received_to') ? $this->Html->link($receivedToDetail->received_to->id, ['controller' => 'ReceivedTos', 'action' => 'view', $receivedToDetail->received_to->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Product') ?></th>
            <td><?= $receivedToDetail->has('product') ? $this->Html->link($receivedToDetail->product->name, ['controller' => 'Products', 'action' => 'view', $receivedToDetail->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Product Serial') ?></th>
            <td><?= h($receivedToDetail->product_serial) ?></td>
        </tr>
        <tr>
            <th><?= __('Quantity Received') ?></th>
            <td><?= $this->Number->format($receivedToDetail->quantity_received) ?></td>
        </tr>
    </table>
</div>
