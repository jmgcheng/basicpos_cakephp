<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Excluded Inventory Detail'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Excluded Inventories'), ['controller' => 'ExcludedInventories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Excluded Inventory'), ['controller' => 'ExcludedInventories', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="excludedInventoryDetails index large-9 medium-8 columns content">
    <h3><?= __('Excluded Inventory Details') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('excluded_inventory_id') ?></th>
                <th><?= $this->Paginator->sort('product_id') ?></th>
                <th><?= $this->Paginator->sort('quantity_excluded') ?></th>
                <th><?= $this->Paginator->sort('product_serial') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($excludedInventoryDetails as $excludedInventoryDetail): ?>
            <tr>
                <td><?= $excludedInventoryDetail->has('excluded_inventory') ? $this->Html->link($excludedInventoryDetail->excluded_inventory->id, ['controller' => 'ExcludedInventories', 'action' => 'view', $excludedInventoryDetail->excluded_inventory->id]) : '' ?></td>
                <td><?= $excludedInventoryDetail->has('product') ? $this->Html->link($excludedInventoryDetail->product->name, ['controller' => 'Products', 'action' => 'view', $excludedInventoryDetail->product->id]) : '' ?></td>
                <td><?= $this->Number->format($excludedInventoryDetail->quantity_excluded) ?></td>
                <td><?= h($excludedInventoryDetail->product_serial) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $excludedInventoryDetail->excluded_inventory_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $excludedInventoryDetail->excluded_inventory_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $excludedInventoryDetail->excluded_inventory_id], ['confirm' => __('Are you sure you want to delete # {0}?', $excludedInventoryDetail->excluded_inventory_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
