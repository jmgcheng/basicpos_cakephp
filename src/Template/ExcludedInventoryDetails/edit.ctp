<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $excludedInventoryDetail->excluded_inventory_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $excludedInventoryDetail->excluded_inventory_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Excluded Inventory Details'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Excluded Inventories'), ['controller' => 'ExcludedInventories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Excluded Inventory'), ['controller' => 'ExcludedInventories', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="excludedInventoryDetails form large-9 medium-8 columns content">
    <?= $this->Form->create($excludedInventoryDetail) ?>
    <fieldset>
        <legend><?= __('Edit Excluded Inventory Detail') ?></legend>
        <?php
            echo $this->Form->input('quantity_excluded');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
