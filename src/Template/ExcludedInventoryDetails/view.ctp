<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Excluded Inventory Detail'), ['action' => 'edit', $excludedInventoryDetail->excluded_inventory_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Excluded Inventory Detail'), ['action' => 'delete', $excludedInventoryDetail->excluded_inventory_id], ['confirm' => __('Are you sure you want to delete # {0}?', $excludedInventoryDetail->excluded_inventory_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Excluded Inventory Details'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Excluded Inventory Detail'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Excluded Inventories'), ['controller' => 'ExcludedInventories', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Excluded Inventory'), ['controller' => 'ExcludedInventories', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="excludedInventoryDetails view large-9 medium-8 columns content">
    <h3><?= h($excludedInventoryDetail->excluded_inventory_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Excluded Inventory') ?></th>
            <td><?= $excludedInventoryDetail->has('excluded_inventory') ? $this->Html->link($excludedInventoryDetail->excluded_inventory->id, ['controller' => 'ExcludedInventories', 'action' => 'view', $excludedInventoryDetail->excluded_inventory->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Product') ?></th>
            <td><?= $excludedInventoryDetail->has('product') ? $this->Html->link($excludedInventoryDetail->product->name, ['controller' => 'Products', 'action' => 'view', $excludedInventoryDetail->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Product Serial') ?></th>
            <td><?= h($excludedInventoryDetail->product_serial) ?></td>
        </tr>
        <tr>
            <th><?= __('Quantity Excluded') ?></th>
            <td><?= $this->Number->format($excludedInventoryDetail->quantity_excluded) ?></td>
        </tr>
    </table>
</div>
