<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Wrong Send Po Status'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Wrong Send Pos'), ['controller' => 'WrongSendPos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Wrong Send Po'), ['controller' => 'WrongSendPos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="wrongSendPoStatus index large-9 medium-8 columns content">
    <h3><?= __('Wrong Send Po Status') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($wrongSendPoStatus as $wrongSendPoStatus): ?>
            <tr>
                <td><?= $this->Number->format($wrongSendPoStatus->id) ?></td>
                <td><?= h($wrongSendPoStatus->name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $wrongSendPoStatus->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $wrongSendPoStatus->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $wrongSendPoStatus->id], ['confirm' => __('Are you sure you want to delete # {0}?', $wrongSendPoStatus->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
