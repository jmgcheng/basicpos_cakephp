<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Wrong Send Po Status'), ['action' => 'edit', $wrongSendPoStatus->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Wrong Send Po Status'), ['action' => 'delete', $wrongSendPoStatus->id], ['confirm' => __('Are you sure you want to delete # {0}?', $wrongSendPoStatus->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Wrong Send Po Status'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Wrong Send Po Status'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Wrong Send Pos'), ['controller' => 'WrongSendPos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Wrong Send Po'), ['controller' => 'WrongSendPos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="wrongSendPoStatus view large-9 medium-8 columns content">
    <h3><?= h($wrongSendPoStatus->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($wrongSendPoStatus->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($wrongSendPoStatus->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Wrong Send Pos') ?></h4>
        <?php if (!empty($wrongSendPoStatus->wrong_send_pos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Purchase Order Id') ?></th>
                <th><?= __('Depot Id') ?></th>
                <th><?= __('Received By Id') ?></th>
                <th><?= __('Approved By Id') ?></th>
                <th><?= __('Date Registration') ?></th>
                <th><?= __('Comment') ?></th>
                <th><?= __('Wrong Send Po Status Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($wrongSendPoStatus->wrong_send_pos as $wrongSendPos): ?>
            <tr>
                <td><?= h($wrongSendPos->id) ?></td>
                <td><?= h($wrongSendPos->purchase_order_id) ?></td>
                <td><?= h($wrongSendPos->depot_id) ?></td>
                <td><?= h($wrongSendPos->received_by_id) ?></td>
                <td><?= h($wrongSendPos->approved_by_id) ?></td>
                <td><?= h($wrongSendPos->date_registration) ?></td>
                <td><?= h($wrongSendPos->comment) ?></td>
                <td><?= h($wrongSendPos->wrong_send_po_status_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'WrongSendPos', 'action' => 'view', $wrongSendPos->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'WrongSendPos', 'action' => 'edit', $wrongSendPos->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'WrongSendPos', 'action' => 'delete', $wrongSendPos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $wrongSendPos->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
