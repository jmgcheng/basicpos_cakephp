<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $wrongSendPoStatus->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $wrongSendPoStatus->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Wrong Send Po Status'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Wrong Send Pos'), ['controller' => 'WrongSendPos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Wrong Send Po'), ['controller' => 'WrongSendPos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="wrongSendPoStatus form large-9 medium-8 columns content">
    <?= $this->Form->create($wrongSendPoStatus) ?>
    <fieldset>
        <legend><?= __('Edit Wrong Send Po Status') ?></legend>
        <?php
            echo $this->Form->input('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
