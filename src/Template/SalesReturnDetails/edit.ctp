<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $salesReturnDetail->sales_return_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $salesReturnDetail->sales_return_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Sales Return Details'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Sales Returns'), ['controller' => 'SalesReturns', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sales Return'), ['controller' => 'SalesReturns', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="salesReturnDetails form large-9 medium-8 columns content">
    <?= $this->Form->create($salesReturnDetail) ?>
    <fieldset>
        <legend><?= __('Edit Sales Return Detail') ?></legend>
        <?php
            echo $this->Form->input('quantity_received');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
