<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Sales Return Detail'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sales Returns'), ['controller' => 'SalesReturns', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sales Return'), ['controller' => 'SalesReturns', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="salesReturnDetails index large-9 medium-8 columns content">
    <h3><?= __('Sales Return Details') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('sales_return_id') ?></th>
                <th><?= $this->Paginator->sort('product_id') ?></th>
                <th><?= $this->Paginator->sort('quantity_received') ?></th>
                <th><?= $this->Paginator->sort('product_serial') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($salesReturnDetails as $salesReturnDetail): ?>
            <tr>
                <td><?= $salesReturnDetail->has('sales_return') ? $this->Html->link($salesReturnDetail->sales_return->id, ['controller' => 'SalesReturns', 'action' => 'view', $salesReturnDetail->sales_return->id]) : '' ?></td>
                <td><?= $salesReturnDetail->has('product') ? $this->Html->link($salesReturnDetail->product->name, ['controller' => 'Products', 'action' => 'view', $salesReturnDetail->product->id]) : '' ?></td>
                <td><?= $this->Number->format($salesReturnDetail->quantity_received) ?></td>
                <td><?= h($salesReturnDetail->product_serial) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $salesReturnDetail->sales_return_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $salesReturnDetail->sales_return_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $salesReturnDetail->sales_return_id], ['confirm' => __('Are you sure you want to delete # {0}?', $salesReturnDetail->sales_return_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
