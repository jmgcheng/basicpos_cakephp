<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Sales Return Detail'), ['action' => 'edit', $salesReturnDetail->sales_return_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Sales Return Detail'), ['action' => 'delete', $salesReturnDetail->sales_return_id], ['confirm' => __('Are you sure you want to delete # {0}?', $salesReturnDetail->sales_return_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Sales Return Details'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sales Return Detail'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sales Returns'), ['controller' => 'SalesReturns', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sales Return'), ['controller' => 'SalesReturns', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="salesReturnDetails view large-9 medium-8 columns content">
    <h3><?= h($salesReturnDetail->sales_return_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Sales Return') ?></th>
            <td><?= $salesReturnDetail->has('sales_return') ? $this->Html->link($salesReturnDetail->sales_return->id, ['controller' => 'SalesReturns', 'action' => 'view', $salesReturnDetail->sales_return->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Product') ?></th>
            <td><?= $salesReturnDetail->has('product') ? $this->Html->link($salesReturnDetail->product->name, ['controller' => 'Products', 'action' => 'view', $salesReturnDetail->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Product Serial') ?></th>
            <td><?= h($salesReturnDetail->product_serial) ?></td>
        </tr>
        <tr>
            <th><?= __('Quantity Received') ?></th>
            <td><?= $this->Number->format($salesReturnDetail->quantity_received) ?></td>
        </tr>
    </table>
</div>
