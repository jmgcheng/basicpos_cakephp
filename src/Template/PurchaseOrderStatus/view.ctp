<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Purchase Order Status'), ['action' => 'edit', $purchaseOrderStatus->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Purchase Order Status'), ['action' => 'delete', $purchaseOrderStatus->id], ['confirm' => __('Are you sure you want to delete # {0}?', $purchaseOrderStatus->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Purchase Order Status'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Purchase Order Status'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Purchase Orders'), ['controller' => 'PurchaseOrders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Purchase Order'), ['controller' => 'PurchaseOrders', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="purchaseOrderStatus view large-9 medium-8 columns content">
    <h3><?= h($purchaseOrderStatus->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($purchaseOrderStatus->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($purchaseOrderStatus->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Purchase Orders') ?></h4>
        <?php if (!empty($purchaseOrderStatus->purchase_orders)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Depot Id') ?></th>
                <th><?= __('Prepared By Id') ?></th>
                <th><?= __('Approved By Id') ?></th>
                <th><?= __('Date Registration') ?></th>
                <th><?= __('Title') ?></th>
                <th><?= __('Order Description') ?></th>
                <th><?= __('Comment') ?></th>
                <th><?= __('Purchase Order Status Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($purchaseOrderStatus->purchase_orders as $purchaseOrders): ?>
            <tr>
                <td><?= h($purchaseOrders->id) ?></td>
                <td><?= h($purchaseOrders->depot_id) ?></td>
                <td><?= h($purchaseOrders->prepared_by_id) ?></td>
                <td><?= h($purchaseOrders->approved_by_id) ?></td>
                <td><?= h($purchaseOrders->date_registration) ?></td>
                <td><?= h($purchaseOrders->title) ?></td>
                <td><?= h($purchaseOrders->order_description) ?></td>
                <td><?= h($purchaseOrders->comment) ?></td>
                <td><?= h($purchaseOrders->purchase_order_status_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PurchaseOrders', 'action' => 'view', $purchaseOrders->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PurchaseOrders', 'action' => 'edit', $purchaseOrders->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PurchaseOrders', 'action' => 'delete', $purchaseOrders->id], ['confirm' => __('Are you sure you want to delete # {0}?', $purchaseOrders->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
