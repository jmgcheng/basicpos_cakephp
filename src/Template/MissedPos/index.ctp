<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Missed Po'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Purchase Orders'), ['controller' => 'PurchaseOrders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Purchase Order'), ['controller' => 'PurchaseOrders', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Missed Po Status'), ['controller' => 'MissedPoStatus', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Missed Po Status'), ['controller' => 'MissedPoStatus', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Missed Po Details'), ['controller' => 'MissedPoDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Missed Po Detail'), ['controller' => 'MissedPoDetails', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="missedPos index large-9 medium-8 columns content">
    <h3><?= __('Missed Pos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('purchase_order_id') ?></th>
                <th><?= $this->Paginator->sort('depot_id') ?></th>
                <th><?= $this->Paginator->sort('received_by_id') ?></th>
                <th><?= $this->Paginator->sort('approved_by_id') ?></th>
                <th><?= $this->Paginator->sort('date_registration') ?></th>
                <th><?= $this->Paginator->sort('comment') ?></th>
                <th><?= $this->Paginator->sort('missed_po_status_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($missedPos as $missedPo): ?>
            <tr>
                <td><?= $this->Number->format($missedPo->id) ?></td>
                <td><?= $missedPo->has('purchase_order') ? $this->Html->link($missedPo->purchase_order->title, ['controller' => 'PurchaseOrders', 'action' => 'view', $missedPo->purchase_order->id]) : '' ?></td>
                <td><?= $missedPo->has('depot') ? $this->Html->link($missedPo->depot->name, ['controller' => 'Depots', 'action' => 'view', $missedPo->depot->id]) : '' ?></td>
                <td><?= $this->Number->format($missedPo->received_by_id) ?></td>
                <td><?= $missedPo->has('user') ? $this->Html->link($missedPo->user->id, ['controller' => 'Users', 'action' => 'view', $missedPo->user->id]) : '' ?></td>
                <td><?= h($missedPo->date_registration) ?></td>
                <td><?= h($missedPo->comment) ?></td>
                <td><?= $missedPo->has('missed_po_status') ? $this->Html->link($missedPo->missed_po_status->name, ['controller' => 'MissedPoStatus', 'action' => 'view', $missedPo->missed_po_status->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $missedPo->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $missedPo->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $missedPo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $missedPo->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
