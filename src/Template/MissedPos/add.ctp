<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Missed Pos'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Purchase Orders'), ['controller' => 'PurchaseOrders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Purchase Order'), ['controller' => 'PurchaseOrders', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Missed Po Status'), ['controller' => 'MissedPoStatus', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Missed Po Status'), ['controller' => 'MissedPoStatus', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Missed Po Details'), ['controller' => 'MissedPoDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Missed Po Detail'), ['controller' => 'MissedPoDetails', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="missedPos form large-9 medium-8 columns content">
    <?= $this->Form->create($missedPo) ?>
    <fieldset>
        <legend><?= __('Add Missed Po') ?></legend>
        <?php
            echo $this->Form->input('purchase_order_id', ['options' => $purchaseOrders]);
            echo $this->Form->input('depot_id', ['options' => $depots]);
            echo $this->Form->input('received_by_id', ['options' => $users, 'empty' => true]);
            echo $this->Form->input('approved_by_id', ['options' => $users, 'empty' => true]);
            echo $this->Form->input('date_registration');
            echo $this->Form->input('comment');
            echo $this->Form->input('missed_po_status_id', ['options' => $missedPoStatus]);
        ?>
    </fieldset>

    <fieldset>
        <?php 
            if( isset($product_list) && !empty($product_list) ) :
        ?>
            <label>
                Product
            </label>
            <select id="js_add_transac_detail_id">
                <?php 
                    foreach( $product_list AS $product_list_detail ):
                        echo '<option value="'. $product_list_detail['id'] .'">' .$product_list_detail['name'] .' - '. $product_list_detail['model'] .' - '. $product_list_detail['product_color']['name'] .' - '. $product_list_detail['product_dimension']['name'] .' - '. $product_list_detail['product_unit']['name']. '</option>';
                    endforeach;
                ?>
            </select>
            <label>
                Quantity
            </label>
            <input type="text" id="js_add_transac_detail_qty" value="" placeholder="0" />

            <button type="button" onclick="addTransacDetail( 'product_id', document.getElementById('js_add_transac_detail_id').value, 'quantity_missed', document.getElementById('js_add_transac_detail_qty').value, '', '' )">Add Detail</button>
        <?php 
            endif;
        ?>
    </fieldset>

    <fieldset id="js_fieldset_transaction_details">
    </fieldset>

    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
