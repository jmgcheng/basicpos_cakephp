<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Missed Po'), ['action' => 'edit', $missedPo->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Missed Po'), ['action' => 'delete', $missedPo->id], ['confirm' => __('Are you sure you want to delete # {0}?', $missedPo->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Missed Pos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Missed Po'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Purchase Orders'), ['controller' => 'PurchaseOrders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Purchase Order'), ['controller' => 'PurchaseOrders', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Missed Po Status'), ['controller' => 'MissedPoStatus', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Missed Po Status'), ['controller' => 'MissedPoStatus', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Missed Po Details'), ['controller' => 'MissedPoDetails', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Missed Po Detail'), ['controller' => 'MissedPoDetails', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="missedPos view large-9 medium-8 columns content">
    <h3><?= h($missedPo->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Purchase Order') ?></th>
            <td><?= $missedPo->has('purchase_order') ? $this->Html->link($missedPo->purchase_order->title, ['controller' => 'PurchaseOrders', 'action' => 'view', $missedPo->purchase_order->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Depot') ?></th>
            <td><?= $missedPo->has('depot') ? $this->Html->link($missedPo->depot->name, ['controller' => 'Depots', 'action' => 'view', $missedPo->depot->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $missedPo->has('user') ? $this->Html->link($missedPo->user->id, ['controller' => 'Users', 'action' => 'view', $missedPo->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Comment') ?></th>
            <td><?= h($missedPo->comment) ?></td>
        </tr>
        <tr>
            <th><?= __('Missed Po Status') ?></th>
            <td><?= $missedPo->has('missed_po_status') ? $this->Html->link($missedPo->missed_po_status->name, ['controller' => 'MissedPoStatus', 'action' => 'view', $missedPo->missed_po_status->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($missedPo->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Received By Id') ?></th>
            <td><?= $this->Number->format($missedPo->received_by_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Date Registration') ?></th>
            <td><?= h($missedPo->date_registration) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Missed Po Details') ?></h4>
        <?php if (!empty($missedPo->missed_po_details)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Missed Po Id') ?></th>
                <th><?= __('Product Id') ?></th>
                <th><?= __('Quantity Missed') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($missedPo->missed_po_details as $missedPoDetails): ?>
            <tr>
                <td><?= h($missedPoDetails->missed_po_id) ?></td>
                <td><?= h($missedPoDetails->product_id) ?></td>
                <td><?= h($missedPoDetails->quantity_missed) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'MissedPoDetails', 'action' => 'view', $missedPoDetails->missed_po_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'MissedPoDetails', 'action' => 'edit', $missedPoDetails->missed_po_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'MissedPoDetails', 'action' => 'delete', $missedPoDetails->missed_po_id], ['confirm' => __('Are you sure you want to delete # {0}?', $missedPoDetails->missed_po_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
