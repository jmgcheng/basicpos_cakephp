<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $deliveryReceiptStatus->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $deliveryReceiptStatus->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Delivery Receipt Status'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Delivery Receipts'), ['controller' => 'DeliveryReceipts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Delivery Receipt'), ['controller' => 'DeliveryReceipts', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="deliveryReceiptStatus form large-9 medium-8 columns content">
    <?= $this->Form->create($deliveryReceiptStatus) ?>
    <fieldset>
        <legend><?= __('Edit Delivery Receipt Status') ?></legend>
        <?php
            echo $this->Form->input('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
