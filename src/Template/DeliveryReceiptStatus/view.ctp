<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Delivery Receipt Status'), ['action' => 'edit', $deliveryReceiptStatus->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Delivery Receipt Status'), ['action' => 'delete', $deliveryReceiptStatus->id], ['confirm' => __('Are you sure you want to delete # {0}?', $deliveryReceiptStatus->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Delivery Receipt Status'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Delivery Receipt Status'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Delivery Receipts'), ['controller' => 'DeliveryReceipts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Delivery Receipt'), ['controller' => 'DeliveryReceipts', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="deliveryReceiptStatus view large-9 medium-8 columns content">
    <h3><?= h($deliveryReceiptStatus->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($deliveryReceiptStatus->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($deliveryReceiptStatus->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Delivery Receipts') ?></h4>
        <?php if (!empty($deliveryReceiptStatus->delivery_receipts)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Sales Invoice Id') ?></th>
                <th><?= __('Prepared By Id') ?></th>
                <th><?= __('Approved By Id') ?></th>
                <th><?= __('Date Registration') ?></th>
                <th><?= __('Title') ?></th>
                <th><?= __('Comment') ?></th>
                <th><?= __('Delivery Receipt Status Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($deliveryReceiptStatus->delivery_receipts as $deliveryReceipts): ?>
            <tr>
                <td><?= h($deliveryReceipts->id) ?></td>
                <td><?= h($deliveryReceipts->sales_invoice_id) ?></td>
                <td><?= h($deliveryReceipts->prepared_by_id) ?></td>
                <td><?= h($deliveryReceipts->approved_by_id) ?></td>
                <td><?= h($deliveryReceipts->date_registration) ?></td>
                <td><?= h($deliveryReceipts->title) ?></td>
                <td><?= h($deliveryReceipts->comment) ?></td>
                <td><?= h($deliveryReceipts->delivery_receipt_status_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'DeliveryReceipts', 'action' => 'view', $deliveryReceipts->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'DeliveryReceipts', 'action' => 'edit', $deliveryReceipts->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'DeliveryReceipts', 'action' => 'delete', $deliveryReceipts->id], ['confirm' => __('Are you sure you want to delete # {0}?', $deliveryReceipts->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
