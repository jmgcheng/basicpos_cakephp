<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $includedInventoryStatus->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $includedInventoryStatus->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Included Inventory Status'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Included Inventories'), ['controller' => 'IncludedInventories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Included Inventory'), ['controller' => 'IncludedInventories', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="includedInventoryStatus form large-9 medium-8 columns content">
    <?= $this->Form->create($includedInventoryStatus) ?>
    <fieldset>
        <legend><?= __('Edit Included Inventory Status') ?></legend>
        <?php
            echo $this->Form->input('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
