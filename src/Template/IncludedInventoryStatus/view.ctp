<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Included Inventory Status'), ['action' => 'edit', $includedInventoryStatus->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Included Inventory Status'), ['action' => 'delete', $includedInventoryStatus->id], ['confirm' => __('Are you sure you want to delete # {0}?', $includedInventoryStatus->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Included Inventory Status'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Included Inventory Status'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Included Inventories'), ['controller' => 'IncludedInventories', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Included Inventory'), ['controller' => 'IncludedInventories', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="includedInventoryStatus view large-9 medium-8 columns content">
    <h3><?= h($includedInventoryStatus->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($includedInventoryStatus->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($includedInventoryStatus->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Included Inventories') ?></h4>
        <?php if (!empty($includedInventoryStatus->included_inventories)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Depot Id') ?></th>
                <th><?= __('Prepared By Id') ?></th>
                <th><?= __('Date Registration') ?></th>
                <th><?= __('Comment') ?></th>
                <th><?= __('Included Inventory Status Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($includedInventoryStatus->included_inventories as $includedInventories): ?>
            <tr>
                <td><?= h($includedInventories->id) ?></td>
                <td><?= h($includedInventories->depot_id) ?></td>
                <td><?= h($includedInventories->prepared_by_id) ?></td>
                <td><?= h($includedInventories->date_registration) ?></td>
                <td><?= h($includedInventories->comment) ?></td>
                <td><?= h($includedInventories->included_inventory_status_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'IncludedInventories', 'action' => 'view', $includedInventories->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'IncludedInventories', 'action' => 'edit', $includedInventories->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'IncludedInventories', 'action' => 'delete', $includedInventories->id], ['confirm' => __('Are you sure you want to delete # {0}?', $includedInventories->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
