<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Official Receipt Detail'), ['action' => 'edit', $officialReceiptDetail->official_receipt_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Official Receipt Detail'), ['action' => 'delete', $officialReceiptDetail->official_receipt_id], ['confirm' => __('Are you sure you want to delete # {0}?', $officialReceiptDetail->official_receipt_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Official Receipt Details'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Official Receipt Detail'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Official Receipts'), ['controller' => 'OfficialReceipts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Official Receipt'), ['controller' => 'OfficialReceipts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="officialReceiptDetails view large-9 medium-8 columns content">
    <h3><?= h($officialReceiptDetail->official_receipt_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Official Receipt') ?></th>
            <td><?= $officialReceiptDetail->has('official_receipt') ? $this->Html->link($officialReceiptDetail->official_receipt->id, ['controller' => 'OfficialReceipts', 'action' => 'view', $officialReceiptDetail->official_receipt->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Product') ?></th>
            <td><?= $officialReceiptDetail->has('product') ? $this->Html->link($officialReceiptDetail->product->name, ['controller' => 'Products', 'action' => 'view', $officialReceiptDetail->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Product Serial') ?></th>
            <td><?= h($officialReceiptDetail->product_serial) ?></td>
        </tr>
        <tr>
            <th><?= __('Quantity Released') ?></th>
            <td><?= $this->Number->format($officialReceiptDetail->quantity_released) ?></td>
        </tr>
    </table>
</div>
