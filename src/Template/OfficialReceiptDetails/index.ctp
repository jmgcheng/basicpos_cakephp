<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Official Receipt Detail'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Official Receipts'), ['controller' => 'OfficialReceipts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Official Receipt'), ['controller' => 'OfficialReceipts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="officialReceiptDetails index large-9 medium-8 columns content">
    <h3><?= __('Official Receipt Details') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('official_receipt_id') ?></th>
                <th><?= $this->Paginator->sort('product_id') ?></th>
                <th><?= $this->Paginator->sort('quantity_released') ?></th>
                <th><?= $this->Paginator->sort('product_serial') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($officialReceiptDetails as $officialReceiptDetail): ?>
            <tr>
                <td><?= $officialReceiptDetail->has('official_receipt') ? $this->Html->link($officialReceiptDetail->official_receipt->id, ['controller' => 'OfficialReceipts', 'action' => 'view', $officialReceiptDetail->official_receipt->id]) : '' ?></td>
                <td><?= $officialReceiptDetail->has('product') ? $this->Html->link($officialReceiptDetail->product->name, ['controller' => 'Products', 'action' => 'view', $officialReceiptDetail->product->id]) : '' ?></td>
                <td><?= $this->Number->format($officialReceiptDetail->quantity_released) ?></td>
                <td><?= h($officialReceiptDetail->product_serial) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $officialReceiptDetail->official_receipt_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $officialReceiptDetail->official_receipt_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $officialReceiptDetail->official_receipt_id], ['confirm' => __('Are you sure you want to delete # {0}?', $officialReceiptDetail->official_receipt_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
