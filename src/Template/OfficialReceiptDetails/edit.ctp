<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $officialReceiptDetail->official_receipt_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $officialReceiptDetail->official_receipt_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Official Receipt Details'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Official Receipts'), ['controller' => 'OfficialReceipts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Official Receipt'), ['controller' => 'OfficialReceipts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="officialReceiptDetails form large-9 medium-8 columns content">
    <?= $this->Form->create($officialReceiptDetail) ?>
    <fieldset>
        <legend><?= __('Edit Official Receipt Detail') ?></legend>
        <?php
            echo $this->Form->input('quantity_released');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
