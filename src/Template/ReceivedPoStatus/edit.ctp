<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $receivedPoStatus->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $receivedPoStatus->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Received Po Status'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Received Pos'), ['controller' => 'ReceivedPos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Received Po'), ['controller' => 'ReceivedPos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="receivedPoStatus form large-9 medium-8 columns content">
    <?= $this->Form->create($receivedPoStatus) ?>
    <fieldset>
        <legend><?= __('Edit Received Po Status') ?></legend>
        <?php
            echo $this->Form->input('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
