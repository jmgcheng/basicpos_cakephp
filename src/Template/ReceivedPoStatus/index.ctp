<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Received Po Status'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Received Pos'), ['controller' => 'ReceivedPos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Received Po'), ['controller' => 'ReceivedPos', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="receivedPoStatus index large-9 medium-8 columns content">
    <h3><?= __('Received Po Status') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($receivedPoStatus as $receivedPoStatus): ?>
            <tr>
                <td><?= $this->Number->format($receivedPoStatus->id) ?></td>
                <td><?= h($receivedPoStatus->name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $receivedPoStatus->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $receivedPoStatus->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $receivedPoStatus->id], ['confirm' => __('Are you sure you want to delete # {0}?', $receivedPoStatus->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
