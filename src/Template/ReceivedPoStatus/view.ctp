<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Received Po Status'), ['action' => 'edit', $receivedPoStatus->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Received Po Status'), ['action' => 'delete', $receivedPoStatus->id], ['confirm' => __('Are you sure you want to delete # {0}?', $receivedPoStatus->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Received Po Status'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Received Po Status'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Received Pos'), ['controller' => 'ReceivedPos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Received Po'), ['controller' => 'ReceivedPos', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="receivedPoStatus view large-9 medium-8 columns content">
    <h3><?= h($receivedPoStatus->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($receivedPoStatus->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($receivedPoStatus->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Received Pos') ?></h4>
        <?php if (!empty($receivedPoStatus->received_pos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Purchase Order Id') ?></th>
                <th><?= __('Depot Id') ?></th>
                <th><?= __('Prepared By Id') ?></th>
                <th><?= __('Approved By Id') ?></th>
                <th><?= __('Date Registration') ?></th>
                <th><?= __('Comment') ?></th>
                <th><?= __('Received Po Status Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($receivedPoStatus->received_pos as $receivedPos): ?>
            <tr>
                <td><?= h($receivedPos->id) ?></td>
                <td><?= h($receivedPos->purchase_order_id) ?></td>
                <td><?= h($receivedPos->depot_id) ?></td>
                <td><?= h($receivedPos->prepared_by_id) ?></td>
                <td><?= h($receivedPos->approved_by_id) ?></td>
                <td><?= h($receivedPos->date_registration) ?></td>
                <td><?= h($receivedPos->comment) ?></td>
                <td><?= h($receivedPos->received_po_status_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ReceivedPos', 'action' => 'view', $receivedPos->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ReceivedPos', 'action' => 'edit', $receivedPos->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ReceivedPos', 'action' => 'delete', $receivedPos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $receivedPos->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
