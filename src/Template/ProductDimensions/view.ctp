<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Product Dimension'), ['action' => 'edit', $productDimension->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Product Dimension'), ['action' => 'delete', $productDimension->id], ['confirm' => __('Are you sure you want to delete # {0}?', $productDimension->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Product Dimensions'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product Dimension'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="productDimensions view large-9 medium-8 columns content">
    <h3><?= h($productDimension->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($productDimension->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($productDimension->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Products') ?></h4>
        <?php if (!empty($productDimension->products)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Name') ?></th>
                <th><?= __('Model') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Qty Low Alert') ?></th>
                <th><?= __('Product Status Id') ?></th>
                <th><?= __('Product Color Id') ?></th>
                <th><?= __('Product Dimension Id') ?></th>
                <th><?= __('Product Unit Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($productDimension->products as $products): ?>
            <tr>
                <td><?= h($products->id) ?></td>
                <td><?= h($products->created) ?></td>
                <td><?= h($products->name) ?></td>
                <td><?= h($products->model) ?></td>
                <td><?= h($products->description) ?></td>
                <td><?= h($products->qty_low_alert) ?></td>
                <td><?= h($products->product_status_id) ?></td>
                <td><?= h($products->product_color_id) ?></td>
                <td><?= h($products->product_dimension_id) ?></td>
                <td><?= h($products->product_unit_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Products', 'action' => 'view', $products->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Products', 'action' => 'edit', $products->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Products', 'action' => 'delete', $products->id], ['confirm' => __('Are you sure you want to delete # {0}?', $products->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
