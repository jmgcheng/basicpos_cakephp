<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Transfer Orders'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Transfer Order Status'), ['controller' => 'TransferOrderStatus', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Transfer Order Status'), ['controller' => 'TransferOrderStatus', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Received Tos'), ['controller' => 'ReceivedTos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Received To'), ['controller' => 'ReceivedTos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Transfer Order Details'), ['controller' => 'TransferOrderDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Transfer Order Detail'), ['controller' => 'TransferOrderDetails', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="transferOrders form large-9 medium-8 columns content">
    <?= $this->Form->create($transferOrder) ?>
    <fieldset>
        <legend><?= __('Add Transfer Order') ?></legend>
        <?php
            echo $this->Form->input('releasing_depot_id', ['options' => $depots]);
            echo $this->Form->input('receiving_depot_id', ['options' => $depots]);
            echo $this->Form->input('prepared_by_id', ['options' => $users]);
            echo $this->Form->input('approved_by_id', ['options' => $users]);
            echo $this->Form->input('date_registration');
            echo $this->Form->input('comment');
            echo $this->Form->input('transfer_order_status_id', ['options' => $transferOrderStatus]);
        ?>
    </fieldset>

    <fieldset>
        <?php 
            if( isset($product_list) && !empty($product_list) ) :
        ?>
            <label>
                Product
            </label>
            <select id="js_add_transac_detail_id">
                <?php 
                    foreach( $product_list AS $product_list_detail ):
                        echo '<option value="'. $product_list_detail['id'] .'">' .$product_list_detail['name'] .' - '. $product_list_detail['model'] .' - '. $product_list_detail['product_color']['name'] .' - '. $product_list_detail['product_dimension']['name'] .' - '. $product_list_detail['product_unit']['name']. '</option>';
                    endforeach;
                ?>
            </select>
            <label>
                Quantity
            </label>
            <input type="text" id="js_add_transac_detail_qty" value="" placeholder="0" />
            <label>
                Serial
            </label>
            <input type="text" id="js_add_transac_detail_serial" value="" />

            <button type="button" onclick="addTransacDetail( 'product_id', document.getElementById('js_add_transac_detail_id').value, 'quantity_transfering', document.getElementById('js_add_transac_detail_qty').value, 'product_serial', document.getElementById('js_add_transac_detail_serial').value )">Add Detail</button>
        <?php 
            endif;
        ?>
    </fieldset>

    <fieldset id="js_fieldset_transaction_details">
    </fieldset>
    
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
