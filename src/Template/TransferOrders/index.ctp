<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Transfer Order'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Transfer Order Status'), ['controller' => 'TransferOrderStatus', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Transfer Order Status'), ['controller' => 'TransferOrderStatus', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Received Tos'), ['controller' => 'ReceivedTos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Received To'), ['controller' => 'ReceivedTos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Transfer Order Details'), ['controller' => 'TransferOrderDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Transfer Order Detail'), ['controller' => 'TransferOrderDetails', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="transferOrders index large-9 medium-8 columns content">
    <h3><?= __('Transfer Orders') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('releasing_depot_id') ?></th>
                <th><?= $this->Paginator->sort('receiving_depot_id') ?></th>
                <th><?= $this->Paginator->sort('prepared_by_id') ?></th>
                <th><?= $this->Paginator->sort('approved_by_id') ?></th>
                <th><?= $this->Paginator->sort('date_registration') ?></th>
                <th><?= $this->Paginator->sort('comment') ?></th>
                <th><?= $this->Paginator->sort('transfer_order_status_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($transferOrders as $transferOrder): ?>
            <tr>
                <td><?= $this->Number->format($transferOrder->id) ?></td>
                <td><?= $this->Number->format($transferOrder->releasing_depot_id) ?></td>
                <td><?= $transferOrder->has('depot') ? $this->Html->link($transferOrder->depot->name, ['controller' => 'Depots', 'action' => 'view', $transferOrder->depot->id]) : '' ?></td>
                <td><?= $this->Number->format($transferOrder->prepared_by_id) ?></td>
                <td><?= $transferOrder->has('user') ? $this->Html->link($transferOrder->user->id, ['controller' => 'Users', 'action' => 'view', $transferOrder->user->id]) : '' ?></td>
                <td><?= h($transferOrder->date_registration) ?></td>
                <td><?= h($transferOrder->comment) ?></td>
                <td><?= $transferOrder->has('transfer_order_status') ? $this->Html->link($transferOrder->transfer_order_status->name, ['controller' => 'TransferOrderStatus', 'action' => 'view', $transferOrder->transfer_order_status->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $transferOrder->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $transferOrder->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $transferOrder->id], ['confirm' => __('Are you sure you want to delete # {0}?', $transferOrder->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
