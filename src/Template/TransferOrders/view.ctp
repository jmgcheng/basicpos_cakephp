<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Transfer Order'), ['action' => 'edit', $transferOrder->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Transfer Order'), ['action' => 'delete', $transferOrder->id], ['confirm' => __('Are you sure you want to delete # {0}?', $transferOrder->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Transfer Orders'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Transfer Order'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Depots'), ['controller' => 'Depots', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Depot'), ['controller' => 'Depots', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Transfer Order Status'), ['controller' => 'TransferOrderStatus', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Transfer Order Status'), ['controller' => 'TransferOrderStatus', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Received Tos'), ['controller' => 'ReceivedTos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Received To'), ['controller' => 'ReceivedTos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Transfer Order Details'), ['controller' => 'TransferOrderDetails', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Transfer Order Detail'), ['controller' => 'TransferOrderDetails', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="transferOrders view large-9 medium-8 columns content">
    <h3><?= h($transferOrder->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Depot') ?></th>
            <td><?= $transferOrder->has('depot') ? $this->Html->link($transferOrder->depot->name, ['controller' => 'Depots', 'action' => 'view', $transferOrder->depot->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $transferOrder->has('user') ? $this->Html->link($transferOrder->user->id, ['controller' => 'Users', 'action' => 'view', $transferOrder->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Comment') ?></th>
            <td><?= h($transferOrder->comment) ?></td>
        </tr>
        <tr>
            <th><?= __('Transfer Order Status') ?></th>
            <td><?= $transferOrder->has('transfer_order_status') ? $this->Html->link($transferOrder->transfer_order_status->name, ['controller' => 'TransferOrderStatus', 'action' => 'view', $transferOrder->transfer_order_status->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($transferOrder->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Releasing Depot Id') ?></th>
            <td><?= $this->Number->format($transferOrder->releasing_depot_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Prepared By Id') ?></th>
            <td><?= $this->Number->format($transferOrder->prepared_by_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Date Registration') ?></th>
            <td><?= h($transferOrder->date_registration) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Received Tos') ?></h4>
        <?php if (!empty($transferOrder->received_tos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Transfer Order Id') ?></th>
                <th><?= __('Depot Id') ?></th>
                <th><?= __('Prepared By Id') ?></th>
                <th><?= __('Received By Id') ?></th>
                <th><?= __('Date Registration') ?></th>
                <th><?= __('Comment') ?></th>
                <th><?= __('Received To Status Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($transferOrder->received_tos as $receivedTos): ?>
            <tr>
                <td><?= h($receivedTos->id) ?></td>
                <td><?= h($receivedTos->transfer_order_id) ?></td>
                <td><?= h($receivedTos->depot_id) ?></td>
                <td><?= h($receivedTos->prepared_by_id) ?></td>
                <td><?= h($receivedTos->received_by_id) ?></td>
                <td><?= h($receivedTos->date_registration) ?></td>
                <td><?= h($receivedTos->comment) ?></td>
                <td><?= h($receivedTos->received_to_status_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ReceivedTos', 'action' => 'view', $receivedTos->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ReceivedTos', 'action' => 'edit', $receivedTos->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ReceivedTos', 'action' => 'delete', $receivedTos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $receivedTos->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Transfer Order Details') ?></h4>
        <?php if (!empty($transferOrder->transfer_order_details)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Transfer Order Id') ?></th>
                <th><?= __('Product Id') ?></th>
                <th><?= __('Quantity Transfering') ?></th>
                <th><?= __('Product Serial') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($transferOrder->transfer_order_details as $transferOrderDetails): ?>
            <tr>
                <td><?= h($transferOrderDetails->transfer_order_id) ?></td>
                <td><?= h($transferOrderDetails->product_id) ?></td>
                <td><?= h($transferOrderDetails->quantity_transfering) ?></td>
                <td><?= h($transferOrderDetails->product_serial) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TransferOrderDetails', 'action' => 'view', $transferOrderDetails->transfer_order_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TransferOrderDetails', 'action' => 'edit', $transferOrderDetails->transfer_order_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TransferOrderDetails', 'action' => 'delete', $transferOrderDetails->transfer_order_id], ['confirm' => __('Are you sure you want to delete # {0}?', $transferOrderDetails->transfer_order_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
