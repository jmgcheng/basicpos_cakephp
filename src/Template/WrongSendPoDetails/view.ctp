<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Wrong Send Po Detail'), ['action' => 'edit', $wrongSendPoDetail->wrong_send_po_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Wrong Send Po Detail'), ['action' => 'delete', $wrongSendPoDetail->wrong_send_po_id], ['confirm' => __('Are you sure you want to delete # {0}?', $wrongSendPoDetail->wrong_send_po_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Wrong Send Po Details'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Wrong Send Po Detail'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Wrong Send Pos'), ['controller' => 'WrongSendPos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Wrong Send Po'), ['controller' => 'WrongSendPos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="wrongSendPoDetails view large-9 medium-8 columns content">
    <h3><?= h($wrongSendPoDetail->wrong_send_po_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Wrong Send Po') ?></th>
            <td><?= $wrongSendPoDetail->has('wrong_send_po') ? $this->Html->link($wrongSendPoDetail->wrong_send_po->id, ['controller' => 'WrongSendPos', 'action' => 'view', $wrongSendPoDetail->wrong_send_po->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Product') ?></th>
            <td><?= $wrongSendPoDetail->has('product') ? $this->Html->link($wrongSendPoDetail->product->name, ['controller' => 'Products', 'action' => 'view', $wrongSendPoDetail->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Product Serial') ?></th>
            <td><?= h($wrongSendPoDetail->product_serial) ?></td>
        </tr>
        <tr>
            <th><?= __('Quantity Wrong Send') ?></th>
            <td><?= $this->Number->format($wrongSendPoDetail->quantity_wrong_send) ?></td>
        </tr>
    </table>
</div>
