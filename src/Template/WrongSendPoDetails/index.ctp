<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Wrong Send Po Detail'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Wrong Send Pos'), ['controller' => 'WrongSendPos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Wrong Send Po'), ['controller' => 'WrongSendPos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="wrongSendPoDetails index large-9 medium-8 columns content">
    <h3><?= __('Wrong Send Po Details') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('wrong_send_po_id') ?></th>
                <th><?= $this->Paginator->sort('product_id') ?></th>
                <th><?= $this->Paginator->sort('quantity_wrong_send') ?></th>
                <th><?= $this->Paginator->sort('product_serial') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($wrongSendPoDetails as $wrongSendPoDetail): ?>
            <tr>
                <td><?= $wrongSendPoDetail->has('wrong_send_po') ? $this->Html->link($wrongSendPoDetail->wrong_send_po->id, ['controller' => 'WrongSendPos', 'action' => 'view', $wrongSendPoDetail->wrong_send_po->id]) : '' ?></td>
                <td><?= $wrongSendPoDetail->has('product') ? $this->Html->link($wrongSendPoDetail->product->name, ['controller' => 'Products', 'action' => 'view', $wrongSendPoDetail->product->id]) : '' ?></td>
                <td><?= $this->Number->format($wrongSendPoDetail->quantity_wrong_send) ?></td>
                <td><?= h($wrongSendPoDetail->product_serial) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $wrongSendPoDetail->wrong_send_po_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $wrongSendPoDetail->wrong_send_po_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $wrongSendPoDetail->wrong_send_po_id], ['confirm' => __('Are you sure you want to delete # {0}?', $wrongSendPoDetail->wrong_send_po_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
