<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $wrongSendPoDetail->wrong_send_po_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $wrongSendPoDetail->wrong_send_po_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Wrong Send Po Details'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Wrong Send Pos'), ['controller' => 'WrongSendPos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Wrong Send Po'), ['controller' => 'WrongSendPos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="wrongSendPoDetails form large-9 medium-8 columns content">
    <?= $this->Form->create($wrongSendPoDetail) ?>
    <fieldset>
        <legend><?= __('Edit Wrong Send Po Detail') ?></legend>
        <?php
            echo $this->Form->input('quantity_wrong_send');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
