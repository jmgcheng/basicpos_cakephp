<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ReceivedTo Entity.
 *
 * @property int $id
 * @property int $transfer_order_id
 * @property \App\Model\Entity\TransferOrder $transfer_order
 * @property int $depot_id
 * @property \App\Model\Entity\Depot $depot
 * @property int $prepared_by_id
 * @property int $received_by_id
 * @property \App\Model\Entity\User $user
 * @property \Cake\I18n\Time $date_registration
 * @property string $comment
 * @property int $received_to_status_id
 * @property \App\Model\Entity\ReceivedToStatus $received_to_status
 * @property \App\Model\Entity\ReceivedToDetail[] $received_to_details
 */
class ReceivedTo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
