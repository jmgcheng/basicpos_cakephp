<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MissedPoDetail Entity.
 *
 * @property int $missed_po_id
 * @property \App\Model\Entity\MissedPo $missed_po
 * @property int $product_id
 * @property \App\Model\Entity\Product $product
 * @property int $quantity_missed
 */
class MissedPoDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'missed_po_id' => true,
        'product_id' => true,
    ];
}
