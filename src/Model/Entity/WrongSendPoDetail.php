<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * WrongSendPoDetail Entity.
 *
 * @property int $wrong_send_po_id
 * @property \App\Model\Entity\WrongSendPo $wrong_send_po
 * @property int $product_id
 * @property \App\Model\Entity\Product $product
 * @property int $quantity_wrong_send
 * @property string $product_serial
 */
class WrongSendPoDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'wrong_send_po_id' => true,
        'product_id' => true,
        'product_serial' => true,
    ];
}
