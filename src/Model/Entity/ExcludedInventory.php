<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ExcludedInventory Entity.
 *
 * @property int $id
 * @property int $depot_id
 * @property \App\Model\Entity\Depot $depot
 * @property int $prepared_by_id
 * @property \App\Model\Entity\User $user
 * @property \Cake\I18n\Time $date_registration
 * @property string $comment
 * @property int $excluded_inventory_status_id
 * @property \App\Model\Entity\ExcludedInventoryStatus $excluded_inventory_status
 * @property \App\Model\Entity\ExcludedInventoryDetail[] $excluded_inventory_details
 */
class ExcludedInventory extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
