<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Product Entity.
 *
 * @property int $id
 * @property \Cake\I18n\Time $created
 * @property string $name
 * @property string $model
 * @property string $description
 * @property int $qty_low_alert
 * @property int $product_status_id
 * @property \App\Model\Entity\ProductStatus $product_status
 * @property int $product_color_id
 * @property \App\Model\Entity\ProductColor $product_color
 * @property int $product_dimension_id
 * @property \App\Model\Entity\ProductDimension $product_dimension
 * @property int $product_unit_id
 * @property \App\Model\Entity\ProductUnit $product_unit
 * @property \App\Model\Entity\DeliveryReceiptDetail[] $delivery_receipt_details
 * @property \App\Model\Entity\ExcludedInventoryDetail[] $excluded_inventory_details
 * @property \App\Model\Entity\IncludedInventoryDetail[] $included_inventory_details
 * @property \App\Model\Entity\MissedPoDetail[] $missed_po_details
 * @property \App\Model\Entity\OfficialReceiptDetail[] $official_receipt_details
 * @property \App\Model\Entity\PurchaseOrderDetail[] $purchase_order_details
 * @property \App\Model\Entity\ReceivedPoDetail[] $received_po_details
 * @property \App\Model\Entity\ReceivedToDetail[] $received_to_details
 * @property \App\Model\Entity\SalesInvoiceDetail[] $sales_invoice_details
 * @property \App\Model\Entity\TransferOrderDetail[] $transfer_order_details
 * @property \App\Model\Entity\WrongSendPoDetail[] $wrong_send_po_details
 */
class Product extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
