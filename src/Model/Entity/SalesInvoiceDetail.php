<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SalesInvoiceDetail Entity.
 *
 * @property int $sales_invoice_id
 * @property \App\Model\Entity\SalesInvoice $sales_invoice
 * @property int $product_id
 * @property \App\Model\Entity\Product $product
 * @property int $quantity_to_release
 */
class SalesInvoiceDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'sales_invoice_id' => true,
        'product_id' => true,
    ];
}
