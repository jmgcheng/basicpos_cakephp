<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SalesInvoice Entity.
 *
 * @property int $id
 * @property int $depot_id
 * @property \App\Model\Entity\Depot $depot
 * @property int $prepared_by_id
 * @property int $approved_by_id
 * @property \App\Model\Entity\User $user
 * @property \Cake\I18n\Time $date_registration
 * @property string $title
 * @property string $comment
 * @property int $sales_invoice_status_id
 * @property \App\Model\Entity\SalesInvoiceStatus $sales_invoice_status
 * @property \App\Model\Entity\DeliveryReceipt[] $delivery_receipts
 * @property \App\Model\Entity\OfficialReceipt[] $official_receipts
 * @property \App\Model\Entity\SalesInvoiceDetail[] $sales_invoice_details
 */
class SalesInvoice extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
