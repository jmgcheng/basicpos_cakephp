<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PurchaseOrder Entity.
 *
 * @property int $id
 * @property int $depot_id
 * @property \App\Model\Entity\Depot $depot
 * @property int $prepared_by_id
 * @property int $approved_by_id
 * @property \App\Model\Entity\User $user
 * @property \Cake\I18n\Time $date_registration
 * @property string $title
 * @property string $order_description
 * @property string $comment
 * @property int $purchase_order_status_id
 * @property \App\Model\Entity\PurchaseOrderStatus $purchase_order_status
 * @property \App\Model\Entity\MissedPo[] $missed_pos
 * @property \App\Model\Entity\PurchaseOrderDetail[] $purchase_order_details
 * @property \App\Model\Entity\ReceivedPo[] $received_pos
 * @property \App\Model\Entity\WrongSendPo[] $wrong_send_pos
 */
class PurchaseOrder extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
