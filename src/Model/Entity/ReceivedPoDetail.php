<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ReceivedPoDetail Entity.
 *
 * @property int $received_po_id
 * @property \App\Model\Entity\ReceivedPo $received_po
 * @property int $product_id
 * @property \App\Model\Entity\Product $product
 * @property int $quantity_received
 * @property string $product_serial
 */
class ReceivedPoDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'received_po_id' => true,
        'product_id' => true,
        'product_serial' => true,
    ];
}
