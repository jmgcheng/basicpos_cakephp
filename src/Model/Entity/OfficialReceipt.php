<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OfficialReceipt Entity.
 *
 * @property int $id
 * @property int $sales_invoice_id
 * @property \App\Model\Entity\SalesInvoice $sales_invoice
 * @property int $depot_id
 * @property \App\Model\Entity\Depot $depot
 * @property int $prepared_by_id
 * @property int $approved_by_id
 * @property \App\Model\Entity\User $user
 * @property \Cake\I18n\Time $date_registration
 * @property string $comment
 * @property int $official_receipt_status_id
 * @property \App\Model\Entity\OfficialReceiptStatus $official_receipt_status
 * @property \App\Model\Entity\OfficialReceiptDetail[] $official_receipt_details
 */
class OfficialReceipt extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
