<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Depot Entity.
 *
 * @property int $id
 * @property int $depot_type_id
 * @property \App\Model\Entity\DepotType $depot_type
 * @property string $name
 * @property string $description
 * @property string $address
 * @property \App\Model\Entity\ExcludedInventory[] $excluded_inventories
 * @property \App\Model\Entity\IncludedInventory[] $included_inventories
 * @property \App\Model\Entity\MissedPo[] $missed_pos
 * @property \App\Model\Entity\OfficialReceipt[] $official_receipts
 * @property \App\Model\Entity\PurchaseOrder[] $purchase_orders
 * @property \App\Model\Entity\ReceivedPo[] $received_pos
 * @property \App\Model\Entity\ReceivedTo[] $received_tos
 * @property \App\Model\Entity\SalesInvoice[] $sales_invoices
 * @property \App\Model\Entity\WrongSendPo[] $wrong_send_pos
 */
class Depot extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
