<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DeliveryReceipt Entity.
 *
 * @property int $id
 * @property int $sales_invoice_id
 * @property \App\Model\Entity\SalesInvoice $sales_invoice
 * @property int $prepared_by_id
 * @property int $approved_by_id
 * @property \App\Model\Entity\User $user
 * @property \Cake\I18n\Time $date_registration
 * @property string $title
 * @property string $comment
 * @property int $delivery_receipt_status_id
 * @property \App\Model\Entity\DeliveryReceiptStatus $delivery_receipt_status
 * @property \App\Model\Entity\DeliveryReceiptDetail[] $delivery_receipt_details
 */
class DeliveryReceipt extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
