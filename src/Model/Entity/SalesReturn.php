<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SalesReturn Entity.
 *
 * @property int $id
 * @property int $official_receipt_id
 * @property \App\Model\Entity\OfficialReceipt $official_receipt
 * @property int $depot_id
 * @property \App\Model\Entity\Depot $depot
 * @property int $prepared_by_id
 * @property int $approved_by_id
 * @property \App\Model\Entity\User $user
 * @property \Cake\I18n\Time $date_registration
 * @property string $comment
 * @property int $sales_return_status_id
 * @property \App\Model\Entity\SalesReturnStatus $sales_return_status
 * @property \App\Model\Entity\SalesReturnDetail[] $sales_return_details
 */
class SalesReturn extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
