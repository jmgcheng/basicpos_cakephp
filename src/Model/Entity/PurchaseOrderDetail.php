<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PurchaseOrderDetail Entity.
 *
 * @property int $purchase_order_id
 * @property \App\Model\Entity\PurchaseOrder $purchase_order
 * @property int $product_id
 * @property \App\Model\Entity\Product $product
 * @property int $quantity_ordered
 */
class PurchaseOrderDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'purchase_order_id' => true,
        'product_id' => true,
    ];
}
