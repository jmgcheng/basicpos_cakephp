<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MissedPo Entity.
 *
 * @property int $id
 * @property int $purchase_order_id
 * @property \App\Model\Entity\PurchaseOrder $purchase_order
 * @property int $depot_id
 * @property \App\Model\Entity\Depot $depot
 * @property int $received_by_id
 * @property int $approved_by_id
 * @property \App\Model\Entity\User $user
 * @property \Cake\I18n\Time $date_registration
 * @property string $comment
 * @property int $missed_po_status_id
 * @property \App\Model\Entity\MissedPoStatus $missed_po_status
 * @property \App\Model\Entity\MissedPoDetail[] $missed_po_details
 */
class MissedPo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
