<?php
namespace App\Model\Table;

use App\Model\Entity\DeliveryReceiptDetail;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DeliveryReceiptDetails Model
 *
 * @property \Cake\ORM\Association\BelongsTo $DeliveryReceipts
 * @property \Cake\ORM\Association\BelongsTo $Products
 */
class DeliveryReceiptDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('delivery_receipt_details');
        $this->displayField('delivery_receipt_id');
        //$this->primaryKey(['delivery_receipt_id', 'product_id', 'product_serial']);
        $this->primaryKey(['delivery_receipt_id', 'product_id']);

        $this->belongsTo('DeliveryReceipts', [
            'foreignKey' => 'delivery_receipt_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('quantity_delivering')
            ->requirePresence('quantity_delivering', 'create')
            ->notEmpty('quantity_delivering');

        $validator
            ->allowEmpty('product_serial', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['delivery_receipt_id', 'product_id', 'product_serial'],
            'Transaction Detail Duplication for delivery_receipt_id, product_id, and product_serial?'
        ));
        $rules->add($rules->existsIn(['delivery_receipt_id'], 'DeliveryReceipts'));
        $rules->add($rules->existsIn(['product_id'], 'Products'));
        return $rules;
    }
}
