<?php
namespace App\Model\Table;

use App\Model\Entity\OfficialReceiptDetail;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OfficialReceiptDetails Model
 *
 * @property \Cake\ORM\Association\BelongsTo $OfficialReceipts
 * @property \Cake\ORM\Association\BelongsTo $Products
 */
class OfficialReceiptDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('official_receipt_details');
        $this->displayField('official_receipt_id');
        //$this->primaryKey(['official_receipt_id', 'product_id', 'product_serial']);
        $this->primaryKey(['official_receipt_id', 'product_id']);

        $this->belongsTo('OfficialReceipts', [
            'foreignKey' => 'official_receipt_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('quantity_released')
            ->requirePresence('quantity_released', 'create')
            ->notEmpty('quantity_released');

        $validator
            ->allowEmpty('product_serial', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['official_receipt_id', 'product_id', 'product_serial'],
            'Transaction Detail Duplication for official_receipt_id, product_id, and product_serial?'
        ));
        $rules->add($rules->existsIn(['official_receipt_id'], 'OfficialReceipts'));
        $rules->add($rules->existsIn(['product_id'], 'Products'));
        return $rules;
    }
}
