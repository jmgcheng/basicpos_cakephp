<?php
namespace App\Model\Table;

use App\Model\Entity\ExcludedInventory;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ExcludedInventories Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Depots
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $ExcludedInventoryStatus
 * @property \Cake\ORM\Association\HasMany $ExcludedInventoryDetails
 */
class ExcludedInventoriesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('excluded_inventories');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Depots', [
            'foreignKey' => 'depot_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'prepared_by_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('ExcludedInventoryStatus', [
            'foreignKey' => 'excluded_inventory_status_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('ExcludedInventoryDetails', [
            'foreignKey' => 'excluded_inventory_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('date_registration')
            ->requirePresence('date_registration', 'create')
            ->notEmpty('date_registration');

        $validator
            ->allowEmpty('comment');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['depot_id'], 'Depots'));
        $rules->add($rules->existsIn(['prepared_by_id'], 'Users'));
        $rules->add($rules->existsIn(['excluded_inventory_status_id'], 'ExcludedInventoryStatus'));
        return $rules;
    }
}
