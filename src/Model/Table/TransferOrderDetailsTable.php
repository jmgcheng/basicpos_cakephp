<?php
namespace App\Model\Table;

use App\Model\Entity\TransferOrderDetail;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TransferOrderDetails Model
 *
 * @property \Cake\ORM\Association\BelongsTo $TransferOrders
 * @property \Cake\ORM\Association\BelongsTo $Products
 */
class TransferOrderDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('transfer_order_details');
        $this->displayField('transfer_order_id');
        //$this->primaryKey(['transfer_order_id', 'product_id', 'product_serial']);
        $this->primaryKey(['transfer_order_id', 'product_id']);

        $this->belongsTo('TransferOrders', [
            'foreignKey' => 'transfer_order_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('quantity_transfering')
            ->requirePresence('quantity_transfering', 'create')
            ->notEmpty('quantity_transfering');

        $validator
            ->allowEmpty('product_serial', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['transfer_order_id', 'product_id', 'product_serial'],
            'Transaction Detail Duplication for transfer_order_id, product_id, and product_serial?'
        ));
        $rules->add($rules->existsIn(['transfer_order_id'], 'TransferOrders'));
        $rules->add($rules->existsIn(['product_id'], 'Products'));
        return $rules;
    }
}
