<?php
namespace App\Model\Table;

use App\Model\Entity\DeliveryReceipt;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DeliveryReceipts Model
 *
 * @property \Cake\ORM\Association\BelongsTo $SalesInvoices
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $DeliveryReceiptStatus
 * @property \Cake\ORM\Association\HasMany $DeliveryReceiptDetails
 */
class DeliveryReceiptsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('delivery_receipts');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->belongsTo('SalesInvoices', [
            'foreignKey' => 'sales_invoice_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'prepared_by_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'approved_by_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('DeliveryReceiptStatus', [
            'foreignKey' => 'delivery_receipt_status_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('DeliveryReceiptDetails', [
            'foreignKey' => 'delivery_receipt_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('date_registration')
            ->requirePresence('date_registration', 'create')
            ->notEmpty('date_registration');

        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->allowEmpty('comment');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['sales_invoice_id'], 'SalesInvoices'));
        $rules->add($rules->existsIn(['prepared_by_id'], 'Users'));
        $rules->add($rules->existsIn(['approved_by_id'], 'Users'));
        $rules->add($rules->existsIn(['delivery_receipt_status_id'], 'DeliveryReceiptStatus'));
        return $rules;
    }
}
