<?php
namespace App\Model\Table;

use App\Model\Entity\SalesReturnDetail;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SalesReturnDetails Model
 *
 * @property \Cake\ORM\Association\BelongsTo $SalesReturns
 * @property \Cake\ORM\Association\BelongsTo $Products
 */
class SalesReturnDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('sales_return_details');
        $this->displayField('sales_return_id');
        //$this->primaryKey(['sales_return_id', 'product_id', 'product_serial']);
        $this->primaryKey(['sales_return_id', 'product_id']);

        $this->belongsTo('SalesReturns', [
            'foreignKey' => 'sales_return_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('quantity_received')
            ->requirePresence('quantity_received', 'create')
            ->notEmpty('quantity_received');

        $validator
            ->allowEmpty('product_serial', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['sales_return_id', 'product_id', 'product_serial'],
            'Transaction Detail Duplication for sales_return_id, product_id, and product_serial?'
        ));
        $rules->add($rules->existsIn(['sales_return_id'], 'SalesReturns'));
        $rules->add($rules->existsIn(['product_id'], 'Products'));
        return $rules;
    }
}
