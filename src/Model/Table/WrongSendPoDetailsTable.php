<?php
namespace App\Model\Table;

use App\Model\Entity\WrongSendPoDetail;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WrongSendPoDetails Model
 *
 * @property \Cake\ORM\Association\BelongsTo $WrongSendPos
 * @property \Cake\ORM\Association\BelongsTo $Products
 */
class WrongSendPoDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('wrong_send_po_details');
        $this->displayField('wrong_send_po_id');
        //$this->primaryKey(['wrong_send_po_id', 'product_id', 'product_serial']);
        $this->primaryKey(['wrong_send_po_id', 'product_id']);

        $this->belongsTo('WrongSendPos', [
            'foreignKey' => 'wrong_send_po_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('quantity_wrong_send')
            ->requirePresence('quantity_wrong_send', 'create')
            ->notEmpty('quantity_wrong_send');

        $validator
            ->allowEmpty('product_serial', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['wrong_send_po_id', 'product_id', 'product_serial'],
            'Transaction Detail Duplication for wrong_send_po_id, product_id, and product_serial?'
        ));
        $rules->add($rules->existsIn(['wrong_send_po_id'], 'WrongSendPos'));
        $rules->add($rules->existsIn(['product_id'], 'Products'));
        return $rules;
    }
}
