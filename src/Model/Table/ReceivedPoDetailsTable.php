<?php
namespace App\Model\Table;

use App\Model\Entity\ReceivedPoDetail;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ReceivedPoDetails Model
 *
 * @property \Cake\ORM\Association\BelongsTo $ReceivedPos
 * @property \Cake\ORM\Association\BelongsTo $Products
 */
class ReceivedPoDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('received_po_details');
        $this->displayField('received_po_id');
        //$this->primaryKey(['received_po_id', 'product_id', 'product_serial']);
        $this->primaryKey(['received_po_id', 'product_id']);

        $this->belongsTo('ReceivedPos', [
            'foreignKey' => 'received_po_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('quantity_received')
            ->requirePresence('quantity_received', 'create')
            ->notEmpty('quantity_received');

        $validator
            ->allowEmpty('product_serial', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['received_po_id', 'product_id', 'product_serial'],
            'Transaction Detail Duplication for received_po_id, product_id, and product_serial?'
        ));
        $rules->add($rules->existsIn(['received_po_id'], 'ReceivedPos'));
        $rules->add($rules->existsIn(['product_id'], 'Products'));
        return $rules;
    }
}
