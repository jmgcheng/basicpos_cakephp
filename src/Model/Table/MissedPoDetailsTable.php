<?php
namespace App\Model\Table;

use App\Model\Entity\MissedPoDetail;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MissedPoDetails Model
 *
 * @property \Cake\ORM\Association\BelongsTo $MissedPos
 * @property \Cake\ORM\Association\BelongsTo $Products
 */
class MissedPoDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('missed_po_details');
        $this->displayField('missed_po_id');
        $this->primaryKey(['missed_po_id', 'product_id']);

        $this->belongsTo('MissedPos', [
            'foreignKey' => 'missed_po_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('quantity_missed')
            ->requirePresence('quantity_missed', 'create')
            ->notEmpty('quantity_missed');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['missed_po_id'], 'MissedPos'));
        $rules->add($rules->existsIn(['product_id'], 'Products'));
        return $rules;
    }
}
