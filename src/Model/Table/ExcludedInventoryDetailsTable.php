<?php
namespace App\Model\Table;

use App\Model\Entity\ExcludedInventoryDetail;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ExcludedInventoryDetails Model
 *
 * @property \Cake\ORM\Association\BelongsTo $ExcludedInventories
 * @property \Cake\ORM\Association\BelongsTo $Products
 */
class ExcludedInventoryDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('excluded_inventory_details');
        $this->displayField('excluded_inventory_id');
        //$this->primaryKey(['excluded_inventory_id', 'product_id', 'product_serial']);
        $this->primaryKey(['excluded_inventory_id', 'product_id']);

        $this->belongsTo('ExcludedInventories', [
            'foreignKey' => 'excluded_inventory_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('quantity_excluded')
            ->requirePresence('quantity_excluded', 'create')
            ->notEmpty('quantity_excluded');

        $validator
            ->allowEmpty('product_serial', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['excluded_inventory_id', 'product_id', 'product_serial'],
            'Transaction Detail Duplication for excluded_inventory_id, product_id, and product_serial?'
        ));
        $rules->add($rules->existsIn(['excluded_inventory_id'], 'ExcludedInventories'));
        $rules->add($rules->existsIn(['product_id'], 'Products'));
        return $rules;
    }
}
