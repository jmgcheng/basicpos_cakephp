<?php
namespace App\Model\Table;

use App\Model\Entity\TransferOrder;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TransferOrders Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Depots
 * @property \Cake\ORM\Association\BelongsTo $Depots
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $TransferOrderStatus
 * @property \Cake\ORM\Association\HasMany $ReceivedTos
 * @property \Cake\ORM\Association\HasMany $TransferOrderDetails
 */
class TransferOrdersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('transfer_orders');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Depots', [
            'foreignKey' => 'releasing_depot_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Depots', [
            'foreignKey' => 'receiving_depot_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'prepared_by_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'approved_by_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('TransferOrderStatus', [
            'foreignKey' => 'transfer_order_status_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('ReceivedTos', [
            'foreignKey' => 'transfer_order_id'
        ]);
        $this->hasMany('TransferOrderDetails', [
            'foreignKey' => 'transfer_order_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('date_registration')
            ->requirePresence('date_registration', 'create')
            ->notEmpty('date_registration');

        $validator
            ->allowEmpty('comment');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['releasing_depot_id'], 'Depots'));
        $rules->add($rules->existsIn(['receiving_depot_id'], 'Depots'));
        $rules->add($rules->existsIn(['prepared_by_id'], 'Users'));
        $rules->add($rules->existsIn(['approved_by_id'], 'Users'));
        $rules->add($rules->existsIn(['transfer_order_status_id'], 'TransferOrderStatus'));
        return $rules;
    }
}
