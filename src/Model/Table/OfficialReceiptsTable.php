<?php
namespace App\Model\Table;

use App\Model\Entity\OfficialReceipt;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OfficialReceipts Model
 *
 * @property \Cake\ORM\Association\BelongsTo $SalesInvoices
 * @property \Cake\ORM\Association\BelongsTo $Depots
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $OfficialReceiptStatus
 * @property \Cake\ORM\Association\HasMany $OfficialReceiptDetails
 */
class OfficialReceiptsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('official_receipts');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('SalesInvoices', [
            'foreignKey' => 'sales_invoice_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Depots', [
            'foreignKey' => 'depot_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'prepared_by_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'approved_by_id'
        ]);
        $this->belongsTo('OfficialReceiptStatus', [
            'foreignKey' => 'official_receipt_status_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('OfficialReceiptDetails', [
            'foreignKey' => 'official_receipt_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('date_registration')
            ->requirePresence('date_registration', 'create')
            ->notEmpty('date_registration');

        $validator
            ->allowEmpty('comment');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['sales_invoice_id'], 'SalesInvoices'));
        $rules->add($rules->existsIn(['depot_id'], 'Depots'));
        $rules->add($rules->existsIn(['prepared_by_id'], 'Users'));
        $rules->add($rules->existsIn(['approved_by_id'], 'Users'));
        $rules->add($rules->existsIn(['official_receipt_status_id'], 'OfficialReceiptStatus'));
        return $rules;
    }
}
