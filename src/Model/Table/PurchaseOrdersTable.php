<?php
namespace App\Model\Table;

use App\Model\Entity\PurchaseOrder;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PurchaseOrders Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Depots
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $PurchaseOrderStatus
 * @property \Cake\ORM\Association\HasMany $MissedPos
 * @property \Cake\ORM\Association\HasMany $PurchaseOrderDetails
 * @property \Cake\ORM\Association\HasMany $ReceivedPos
 * @property \Cake\ORM\Association\HasMany $WrongSendPos
 */
class PurchaseOrdersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('purchase_orders');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->belongsTo('Depots', [
            'foreignKey' => 'depot_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'prepared_by_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'approved_by_id'
        ]);
        $this->belongsTo('PurchaseOrderStatus', [
            'foreignKey' => 'purchase_order_status_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('MissedPos', [
            'foreignKey' => 'purchase_order_id'
        ]);
        $this->hasMany('PurchaseOrderDetails', [
            'foreignKey' => 'purchase_order_id'
        ]);
        $this->hasMany('ReceivedPos', [
            'foreignKey' => 'purchase_order_id'
        ]);
        $this->hasMany('WrongSendPos', [
            'foreignKey' => 'purchase_order_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('date_registration')
            ->requirePresence('date_registration', 'create')
            ->notEmpty('date_registration');

        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title')
            ->add('title', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->allowEmpty('order_description');

        $validator
            ->allowEmpty('comment');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['title']));
        $rules->add($rules->existsIn(['depot_id'], 'Depots'));
        $rules->add($rules->existsIn(['prepared_by_id'], 'Users'));
        $rules->add($rules->existsIn(['approved_by_id'], 'Users'));
        $rules->add($rules->existsIn(['purchase_order_status_id'], 'PurchaseOrderStatus'));
        return $rules;
    }
}
