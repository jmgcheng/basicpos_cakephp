<?php
namespace App\Model\Table;

use App\Model\Entity\Depot;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Depots Model
 *
 * @property \Cake\ORM\Association\BelongsTo $DepotTypes
 * @property \Cake\ORM\Association\HasMany $ExcludedInventories
 * @property \Cake\ORM\Association\HasMany $IncludedInventories
 * @property \Cake\ORM\Association\HasMany $MissedPos
 * @property \Cake\ORM\Association\HasMany $OfficialReceipts
 * @property \Cake\ORM\Association\HasMany $PurchaseOrders
 * @property \Cake\ORM\Association\HasMany $ReceivedPos
 * @property \Cake\ORM\Association\HasMany $ReceivedTos
 * @property \Cake\ORM\Association\HasMany $SalesInvoices
 * @property \Cake\ORM\Association\HasMany $WrongSendPos
 */
class DepotsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('depots');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->belongsTo('DepotTypes', [
            'foreignKey' => 'depot_type_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('ExcludedInventories', [
            'foreignKey' => 'depot_id'
        ]);
        $this->hasMany('IncludedInventories', [
            'foreignKey' => 'depot_id'
        ]);
        $this->hasMany('MissedPos', [
            'foreignKey' => 'depot_id'
        ]);
        $this->hasMany('OfficialReceipts', [
            'foreignKey' => 'depot_id'
        ]);
        $this->hasMany('PurchaseOrders', [
            'foreignKey' => 'depot_id'
        ]);
        $this->hasMany('ReceivedPos', [
            'foreignKey' => 'depot_id'
        ]);
        $this->hasMany('ReceivedTos', [
            'foreignKey' => 'depot_id'
        ]);
        $this->hasMany('SalesInvoices', [
            'foreignKey' => 'depot_id'
        ]);
        $this->hasMany('WrongSendPos', [
            'foreignKey' => 'depot_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name')
            ->add('name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        $validator
            ->requirePresence('address', 'create')
            ->notEmpty('address');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['name']));
        $rules->add($rules->existsIn(['depot_type_id'], 'DepotTypes'));
        return $rules;
    }
}
