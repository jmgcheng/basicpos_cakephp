<?php
namespace App\Model\Table;

use App\Model\Entity\Product;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Products Model
 *
 * @property \Cake\ORM\Association\BelongsTo $ProductStatus
 * @property \Cake\ORM\Association\BelongsTo $ProductColors
 * @property \Cake\ORM\Association\BelongsTo $ProductDimensions
 * @property \Cake\ORM\Association\BelongsTo $ProductUnits
 * @property \Cake\ORM\Association\HasMany $DeliveryReceiptDetails
 * @property \Cake\ORM\Association\HasMany $ExcludedInventoryDetails
 * @property \Cake\ORM\Association\HasMany $IncludedInventoryDetails
 * @property \Cake\ORM\Association\HasMany $MissedPoDetails
 * @property \Cake\ORM\Association\HasMany $OfficialReceiptDetails
 * @property \Cake\ORM\Association\HasMany $PurchaseOrderDetails
 * @property \Cake\ORM\Association\HasMany $ReceivedPoDetails
 * @property \Cake\ORM\Association\HasMany $ReceivedToDetails
 * @property \Cake\ORM\Association\HasMany $SalesInvoiceDetails
 * @property \Cake\ORM\Association\HasMany $TransferOrderDetails
 * @property \Cake\ORM\Association\HasMany $WrongSendPoDetails
 */
class ProductsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('products');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('ProductStatus', [
            'foreignKey' => 'product_status_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('ProductColors', [
            'foreignKey' => 'product_color_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('ProductDimensions', [
            'foreignKey' => 'product_dimension_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('ProductUnits', [
            'foreignKey' => 'product_unit_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('DeliveryReceiptDetails', [
            'foreignKey' => 'product_id'
        ]);
        $this->hasMany('ExcludedInventoryDetails', [
            'foreignKey' => 'product_id'
        ]);
        $this->hasMany('IncludedInventoryDetails', [
            'foreignKey' => 'product_id'
        ]);
        $this->hasMany('MissedPoDetails', [
            'foreignKey' => 'product_id'
        ]);
        $this->hasMany('OfficialReceiptDetails', [
            'foreignKey' => 'product_id'
        ]);
        $this->hasMany('PurchaseOrderDetails', [
            'foreignKey' => 'product_id'
        ]);
        $this->hasMany('ReceivedPoDetails', [
            'foreignKey' => 'product_id'
        ]);
        $this->hasMany('ReceivedToDetails', [
            'foreignKey' => 'product_id'
        ]);
        $this->hasMany('SalesInvoiceDetails', [
            'foreignKey' => 'product_id'
        ]);
        $this->hasMany('TransferOrderDetails', [
            'foreignKey' => 'product_id'
        ]);
        $this->hasMany('WrongSendPoDetails', [
            'foreignKey' => 'product_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->allowEmpty('model');

        $validator
            ->allowEmpty('description');

        $validator
            ->integer('qty_low_alert')
            ->allowEmpty('qty_low_alert');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['name', 'model', 'product_color_id', 'product_dimension_id', 'product_unit_id'],
            'This specific product with details already exist'
        ));
        $rules->add($rules->existsIn(['product_status_id'], 'ProductStatus'));
        $rules->add($rules->existsIn(['product_color_id'], 'ProductColors'));
        $rules->add($rules->existsIn(['product_dimension_id'], 'ProductDimensions'));
        $rules->add($rules->existsIn(['product_unit_id'], 'ProductUnits'));
        return $rules;
    }
}
