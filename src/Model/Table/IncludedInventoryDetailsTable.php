<?php
namespace App\Model\Table;

use App\Model\Entity\IncludedInventoryDetail;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * IncludedInventoryDetails Model
 *
 * @property \Cake\ORM\Association\BelongsTo $IncludedInventories
 * @property \Cake\ORM\Association\BelongsTo $Products
 */
class IncludedInventoryDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('included_inventory_details');
        $this->displayField('included_inventory_id');
        //$this->primaryKey(['included_inventory_id', 'product_id', 'product_serial']);
        $this->primaryKey(['included_inventory_id', 'product_id']);

        $this->belongsTo('IncludedInventories', [
            'foreignKey' => 'included_inventory_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('quantity_included')
            ->requirePresence('quantity_included', 'create')
            ->notEmpty('quantity_included');

        $validator
            ->allowEmpty('product_serial', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['included_inventory_id', 'product_id', 'product_serial'],
            'Transaction Detail Duplication for included_inventory_id, product_id, and product_serial?'
        ));
        $rules->add($rules->existsIn(['included_inventory_id'], 'IncludedInventories'));
        $rules->add($rules->existsIn(['product_id'], 'Products'));
        return $rules;
    }
}
