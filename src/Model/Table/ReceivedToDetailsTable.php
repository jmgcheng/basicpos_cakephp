<?php
namespace App\Model\Table;

use App\Model\Entity\ReceivedToDetail;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ReceivedToDetails Model
 *
 * @property \Cake\ORM\Association\BelongsTo $ReceivedTos
 * @property \Cake\ORM\Association\BelongsTo $Products
 */
class ReceivedToDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('received_to_details');
        $this->displayField('received_to_id');
        //$this->primaryKey(['received_to_id', 'product_id', 'product_serial']);
        $this->primaryKey(['received_to_id', 'product_id']);

        $this->belongsTo('ReceivedTos', [
            'foreignKey' => 'received_to_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('quantity_received')
            ->requirePresence('quantity_received', 'create')
            ->notEmpty('quantity_received');

        $validator
            ->allowEmpty('product_serial', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['received_to_id', 'product_id', 'product_serial'],
            'Transaction Detail Duplication for received_to_id, product_id, and product_serial?'
        ));
        $rules->add($rules->existsIn(['received_to_id'], 'ReceivedTos'));
        $rules->add($rules->existsIn(['product_id'], 'Products'));
        return $rules;
    }
}
