<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DeliveryReceiptStatus Controller
 *
 * @property \App\Model\Table\DeliveryReceiptStatusTable $DeliveryReceiptStatus
 */
class DeliveryReceiptStatusController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $deliveryReceiptStatus = $this->paginate($this->DeliveryReceiptStatus);

        $this->set(compact('deliveryReceiptStatus'));
        $this->set('_serialize', ['deliveryReceiptStatus']);
    }

    /**
     * View method
     *
     * @param string|null $id Delivery Receipt Status id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $deliveryReceiptStatus = $this->DeliveryReceiptStatus->get($id, [
            'contain' => ['DeliveryReceipts']
        ]);

        $this->set('deliveryReceiptStatus', $deliveryReceiptStatus);
        $this->set('_serialize', ['deliveryReceiptStatus']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $deliveryReceiptStatus = $this->DeliveryReceiptStatus->newEntity();
        if ($this->request->is('post')) {
            $deliveryReceiptStatus = $this->DeliveryReceiptStatus->patchEntity($deliveryReceiptStatus, $this->request->data);
            if ($this->DeliveryReceiptStatus->save($deliveryReceiptStatus)) {
                $this->Flash->success(__('The delivery receipt status has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The delivery receipt status could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('deliveryReceiptStatus'));
        $this->set('_serialize', ['deliveryReceiptStatus']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Delivery Receipt Status id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $deliveryReceiptStatus = $this->DeliveryReceiptStatus->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $deliveryReceiptStatus = $this->DeliveryReceiptStatus->patchEntity($deliveryReceiptStatus, $this->request->data);
            if ($this->DeliveryReceiptStatus->save($deliveryReceiptStatus)) {
                $this->Flash->success(__('The delivery receipt status has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The delivery receipt status could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('deliveryReceiptStatus'));
        $this->set('_serialize', ['deliveryReceiptStatus']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Delivery Receipt Status id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $deliveryReceiptStatus = $this->DeliveryReceiptStatus->get($id);
        if ($this->DeliveryReceiptStatus->delete($deliveryReceiptStatus)) {
            $this->Flash->success(__('The delivery receipt status has been deleted.'));
        } else {
            $this->Flash->error(__('The delivery receipt status could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
