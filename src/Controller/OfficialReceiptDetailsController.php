<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * OfficialReceiptDetails Controller
 *
 * @property \App\Model\Table\OfficialReceiptDetailsTable $OfficialReceiptDetails
 */
class OfficialReceiptDetailsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['OfficialReceipts', 'Products']
        ];
        $officialReceiptDetails = $this->paginate($this->OfficialReceiptDetails);

        $this->set(compact('officialReceiptDetails'));
        $this->set('_serialize', ['officialReceiptDetails']);
    }

    /**
     * View method
     *
     * @param string|null $id Official Receipt Detail id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $officialReceiptDetail = $this->OfficialReceiptDetails->get($id, [
            'contain' => ['OfficialReceipts', 'Products']
        ]);

        $this->set('officialReceiptDetail', $officialReceiptDetail);
        $this->set('_serialize', ['officialReceiptDetail']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $officialReceiptDetail = $this->OfficialReceiptDetails->newEntity();
        if ($this->request->is('post')) {
            $officialReceiptDetail = $this->OfficialReceiptDetails->patchEntity($officialReceiptDetail, $this->request->data);
            if ($this->OfficialReceiptDetails->save($officialReceiptDetail)) {
                $this->Flash->success(__('The official receipt detail has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The official receipt detail could not be saved. Please, try again.'));
            }
        }
        $officialReceipts = $this->OfficialReceiptDetails->OfficialReceipts->find('list', ['limit' => 200]);
        $products = $this->OfficialReceiptDetails->Products->find('list', ['limit' => 200]);
        $this->set(compact('officialReceiptDetail', 'officialReceipts', 'products'));
        $this->set('_serialize', ['officialReceiptDetail']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Official Receipt Detail id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $officialReceiptDetail = $this->OfficialReceiptDetails->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $officialReceiptDetail = $this->OfficialReceiptDetails->patchEntity($officialReceiptDetail, $this->request->data);
            if ($this->OfficialReceiptDetails->save($officialReceiptDetail)) {
                $this->Flash->success(__('The official receipt detail has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The official receipt detail could not be saved. Please, try again.'));
            }
        }
        $officialReceipts = $this->OfficialReceiptDetails->OfficialReceipts->find('list', ['limit' => 200]);
        $products = $this->OfficialReceiptDetails->Products->find('list', ['limit' => 200]);
        $this->set(compact('officialReceiptDetail', 'officialReceipts', 'products'));
        $this->set('_serialize', ['officialReceiptDetail']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Official Receipt Detail id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $officialReceiptDetail = $this->OfficialReceiptDetails->get($id);
        if ($this->OfficialReceiptDetails->delete($officialReceiptDetail)) {
            $this->Flash->success(__('The official receipt detail has been deleted.'));
        } else {
            $this->Flash->error(__('The official receipt detail could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
