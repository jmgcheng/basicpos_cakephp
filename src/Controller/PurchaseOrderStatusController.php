<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PurchaseOrderStatus Controller
 *
 * @property \App\Model\Table\PurchaseOrderStatusTable $PurchaseOrderStatus
 */
class PurchaseOrderStatusController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $purchaseOrderStatus = $this->paginate($this->PurchaseOrderStatus);

        $this->set(compact('purchaseOrderStatus'));
        $this->set('_serialize', ['purchaseOrderStatus']);
    }

    /**
     * View method
     *
     * @param string|null $id Purchase Order Status id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $purchaseOrderStatus = $this->PurchaseOrderStatus->get($id, [
            'contain' => ['PurchaseOrders']
        ]);

        $this->set('purchaseOrderStatus', $purchaseOrderStatus);
        $this->set('_serialize', ['purchaseOrderStatus']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $purchaseOrderStatus = $this->PurchaseOrderStatus->newEntity();
        if ($this->request->is('post')) {
            $purchaseOrderStatus = $this->PurchaseOrderStatus->patchEntity($purchaseOrderStatus, $this->request->data);
            if ($this->PurchaseOrderStatus->save($purchaseOrderStatus)) {
                $this->Flash->success(__('The purchase order status has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The purchase order status could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('purchaseOrderStatus'));
        $this->set('_serialize', ['purchaseOrderStatus']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Purchase Order Status id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $purchaseOrderStatus = $this->PurchaseOrderStatus->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $purchaseOrderStatus = $this->PurchaseOrderStatus->patchEntity($purchaseOrderStatus, $this->request->data);
            if ($this->PurchaseOrderStatus->save($purchaseOrderStatus)) {
                $this->Flash->success(__('The purchase order status has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The purchase order status could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('purchaseOrderStatus'));
        $this->set('_serialize', ['purchaseOrderStatus']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Purchase Order Status id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $purchaseOrderStatus = $this->PurchaseOrderStatus->get($id);
        if ($this->PurchaseOrderStatus->delete($purchaseOrderStatus)) {
            $this->Flash->success(__('The purchase order status has been deleted.'));
        } else {
            $this->Flash->error(__('The purchase order status could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
