<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * IncludedInventories Controller
 *
 * @property \App\Model\Table\IncludedInventoriesTable $IncludedInventories
 */
class IncludedInventoriesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Depots', 'Users', 'IncludedInventoryStatus']
        ];
        $includedInventories = $this->paginate($this->IncludedInventories);

        $this->set(compact('includedInventories'));
        $this->set('_serialize', ['includedInventories']);
    }

    /**
     * View method
     *
     * @param string|null $id Included Inventory id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $includedInventory = $this->IncludedInventories->get($id, [
            'contain' => ['Depots', 'Users', 'IncludedInventoryStatus', 'IncludedInventoryDetails']
        ]);

        $this->set('includedInventory', $includedInventory);
        $this->set('_serialize', ['includedInventory']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $includedInventory = $this->IncludedInventories->newEntity();
        if ($this->request->is('post')) {

           $a_details = array();
            if(     isset($this->request->data['product_id']) && !empty($this->request->data['product_id']) 
                &&  isset($this->request->data['quantity_included']) && !empty($this->request->data['quantity_included']) 
                &&  isset($this->request->data['product_serial'])
            )
            {
                $a_product_id = $this->request->data['product_id'];
                $a_qty = $this->request->data['quantity_included'];
                $a_product_serial = $this->request->data['product_serial'];
                foreach ($a_product_id as $key => $i_product_id) {
                    $a_temp = array();
                    $a_temp['product_id'] = $a_product_id[$key];
                    $a_temp['quantity_included'] = $a_qty[$key];
                    $a_temp['product_serial'] = $a_product_serial[$key];
                    array_push($a_details, $a_temp);
                }
            }
            unset($this->request->data['product_id']);
            unset($this->request->data['quantity_included']);
            unset($this->request->data['product_serial']);
            $this->request->data['included_inventory_details'] = $a_details;            

            $includedInventory = $this->IncludedInventories->patchEntity($includedInventory, $this->request->data);
            if ($this->IncludedInventories->save($includedInventory)) {
                $this->Flash->success(__('The included inventory has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The included inventory could not be saved. Please, try again.'));
            }
        }
        $depots = $this->IncludedInventories->Depots->find('list', ['limit' => 200]);
        $users = $this->IncludedInventories->Users->find('list', ['limit' => 200]);
        $includedInventoryStatus = $this->IncludedInventories->IncludedInventoryStatus->find('list', ['limit' => 200]);
        $Products = TableRegistry::get('Products');
        $product_list = $Products->find()
                    ->contain(['ProductColors', 'ProductDimensions', 'ProductUnits']);
        $this->set(compact('includedInventory', 'depots', 'users', 'includedInventoryStatus', 'product_list'));
        $this->set('_serialize', ['includedInventory']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Included Inventory id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $IncludedInventoryDetails = TableRegistry::get('IncludedInventoryDetails');
        $includedInventory = $this->IncludedInventories->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $IncludedInventoryDetails->deleteAll(['included_inventory_id' => $id]);
            $a_details = array();
            if(     isset($this->request->data['product_id']) && !empty($this->request->data['product_id']) 
                &&  isset($this->request->data['quantity_included']) && !empty($this->request->data['quantity_included']) 
                &&  isset($this->request->data['product_serial'])
            )
            {
                $a_product_id = $this->request->data['product_id'];
                $a_qty = $this->request->data['quantity_included'];
                $a_product_serial = $this->request->data['product_serial'];
                foreach ($a_product_id as $key => $i_product_id) {
                    $a_temp = array();
                    $a_temp['product_id'] = $a_product_id[$key];
                    $a_temp['quantity_included'] = $a_qty[$key];
                    $a_temp['product_serial'] = $a_product_serial[$key];
                    array_push($a_details, $a_temp);
                }
                $this->request->data['included_inventory_details'] = $a_details;
            }
            unset($this->request->data['product_id']);
            unset($this->request->data['quantity_included']);
            unset($this->request->data['product_serial']);            
            
            $includedInventory = $this->IncludedInventories->patchEntity($includedInventory, $this->request->data);
            if ($this->IncludedInventories->save($includedInventory)) {
                $this->Flash->success(__('The included inventory has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The included inventory could not be saved. Please, try again.'));
            }
        }
        $depots = $this->IncludedInventories->Depots->find('list', ['limit' => 200]);
        $users = $this->IncludedInventories->Users->find('list', ['limit' => 200]);
        $includedInventoryStatus = $this->IncludedInventories->IncludedInventoryStatus->find('list', ['limit' => 200]);
        $transac_detail_list = $IncludedInventoryDetails->find()
                                        ->where(['included_inventory_id' => $id]);
        $Products = TableRegistry::get('Products');
        $product_list = $Products->find()
                    ->contain(['ProductColors', 'ProductDimensions', 'ProductUnits']);
        $this->set(compact('includedInventory', 'depots', 'users', 'includedInventoryStatus', 'product_list', 'transac_detail_list'));
        $this->set('_serialize', ['includedInventory']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Included Inventory id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $includedInventory = $this->IncludedInventories->get($id);
        if ($this->IncludedInventories->delete($includedInventory)) {
            $this->Flash->success(__('The included inventory has been deleted.'));
        } else {
            $this->Flash->error(__('The included inventory could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
