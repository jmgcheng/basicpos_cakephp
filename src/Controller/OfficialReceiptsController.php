<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * OfficialReceipts Controller
 *
 * @property \App\Model\Table\OfficialReceiptsTable $OfficialReceipts
 */
class OfficialReceiptsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SalesInvoices', 'Depots', 'Users', 'OfficialReceiptStatus']
        ];
        $officialReceipts = $this->paginate($this->OfficialReceipts);

        $this->set(compact('officialReceipts'));
        $this->set('_serialize', ['officialReceipts']);
    }

    /**
     * View method
     *
     * @param string|null $id Official Receipt id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $officialReceipt = $this->OfficialReceipts->get($id, [
            'contain' => ['SalesInvoices', 'Depots', 'Users', 'OfficialReceiptStatus', 'OfficialReceiptDetails']
        ]);

        $this->set('officialReceipt', $officialReceipt);
        $this->set('_serialize', ['officialReceipt']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $officialReceipt = $this->OfficialReceipts->newEntity();
        if ($this->request->is('post')) {

           $a_details = array();
            if(     isset($this->request->data['product_id']) && !empty($this->request->data['product_id']) 
                &&  isset($this->request->data['quantity_released']) && !empty($this->request->data['quantity_released']) 
                &&  isset($this->request->data['product_serial'])
            )
            {
                $a_product_id = $this->request->data['product_id'];
                $a_qty = $this->request->data['quantity_released'];
                $a_product_serial = $this->request->data['product_serial'];
                foreach ($a_product_id as $key => $i_product_id) {
                    $a_temp = array();
                    $a_temp['product_id'] = $a_product_id[$key];
                    $a_temp['quantity_released'] = $a_qty[$key];
                    $a_temp['product_serial'] = $a_product_serial[$key];
                    array_push($a_details, $a_temp);
                }
            }
            unset($this->request->data['product_id']);
            unset($this->request->data['quantity_released']);
            unset($this->request->data['product_serial']);
            $this->request->data['official_receipt_details'] = $a_details;            

            $officialReceipt = $this->OfficialReceipts->patchEntity($officialReceipt, $this->request->data);
            if ($this->OfficialReceipts->save($officialReceipt)) {
                $this->Flash->success(__('The official receipt has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The official receipt could not be saved. Please, try again.'));
            }
        }
        $salesInvoices = $this->OfficialReceipts->SalesInvoices->find('list', ['limit' => 200]);
        $depots = $this->OfficialReceipts->Depots->find('list', ['limit' => 200]);
        $users = $this->OfficialReceipts->Users->find('list', ['limit' => 200]);
        $officialReceiptStatus = $this->OfficialReceipts->OfficialReceiptStatus->find('list', ['limit' => 200]);
        $Products = TableRegistry::get('Products');
        $product_list = $Products->find()
                    ->contain(['ProductColors', 'ProductDimensions', 'ProductUnits']);
        $this->set(compact('officialReceipt', 'salesInvoices', 'depots', 'users', 'officialReceiptStatus', 'product_list'));
        $this->set('_serialize', ['officialReceipt']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Official Receipt id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $OfficialReceiptDetails = TableRegistry::get('OfficialReceiptDetails');
        $officialReceipt = $this->OfficialReceipts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $OfficialReceiptDetails->deleteAll(['official_receipt_id' => $id]);
            $a_details = array();
            if(     isset($this->request->data['product_id']) && !empty($this->request->data['product_id']) 
                &&  isset($this->request->data['quantity_released']) && !empty($this->request->data['quantity_released']) 
                &&  isset($this->request->data['product_serial'])
            )
            {
                $a_product_id = $this->request->data['product_id'];
                $a_qty = $this->request->data['quantity_released'];
                $a_product_serial = $this->request->data['product_serial'];
                foreach ($a_product_id as $key => $i_product_id) {
                    $a_temp = array();
                    $a_temp['product_id'] = $a_product_id[$key];
                    $a_temp['quantity_released'] = $a_qty[$key];
                    $a_temp['product_serial'] = $a_product_serial[$key];
                    array_push($a_details, $a_temp);
                }
                $this->request->data['official_receipt_details'] = $a_details;
            }
            unset($this->request->data['product_id']);
            unset($this->request->data['quantity_released']);
            unset($this->request->data['product_serial']);            
            
            $officialReceipt = $this->OfficialReceipts->patchEntity($officialReceipt, $this->request->data);
            if ($this->OfficialReceipts->save($officialReceipt)) {
                $this->Flash->success(__('The official receipt has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The official receipt could not be saved. Please, try again.'));
            }
        }
        $salesInvoices = $this->OfficialReceipts->SalesInvoices->find('list', ['limit' => 200]);
        $depots = $this->OfficialReceipts->Depots->find('list', ['limit' => 200]);
        $users = $this->OfficialReceipts->Users->find('list', ['limit' => 200]);
        $officialReceiptStatus = $this->OfficialReceipts->OfficialReceiptStatus->find('list', ['limit' => 200]);
        $transac_detail_list = $OfficialReceiptDetails->find()
                                        ->where(['official_receipt_id' => $id]);
        $Products = TableRegistry::get('Products');
        $product_list = $Products->find()
                    ->contain(['ProductColors', 'ProductDimensions', 'ProductUnits']);
        $this->set(compact('officialReceipt', 'salesInvoices', 'depots', 'users', 'officialReceiptStatus', 'product_list', 'transac_detail_list'));
        $this->set('_serialize', ['officialReceipt']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Official Receipt id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $officialReceipt = $this->OfficialReceipts->get($id);
        if ($this->OfficialReceipts->delete($officialReceipt)) {
            $this->Flash->success(__('The official receipt has been deleted.'));
        } else {
            $this->Flash->error(__('The official receipt could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
