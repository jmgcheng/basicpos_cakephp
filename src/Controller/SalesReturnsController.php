<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * SalesReturns Controller
 *
 * @property \App\Model\Table\SalesReturnsTable $SalesReturns
 */
class SalesReturnsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['OfficialReceipts', 'Depots', 'Users', 'SalesReturnStatus']
        ];
        $salesReturns = $this->paginate($this->SalesReturns);

        $this->set(compact('salesReturns'));
        $this->set('_serialize', ['salesReturns']);
    }

    /**
     * View method
     *
     * @param string|null $id Sales Return id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $salesReturn = $this->SalesReturns->get($id, [
            'contain' => ['OfficialReceipts', 'Depots', 'Users', 'SalesReturnStatus', 'SalesReturnDetails']
        ]);

        $this->set('salesReturn', $salesReturn);
        $this->set('_serialize', ['salesReturn']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $salesReturn = $this->SalesReturns->newEntity();
        if ($this->request->is('post')) {

           $a_details = array();
            if(     isset($this->request->data['product_id']) && !empty($this->request->data['product_id']) 
                &&  isset($this->request->data['quantity_received']) && !empty($this->request->data['quantity_received']) 
                &&  isset($this->request->data['product_serial'])
            )
            {
                $a_product_id = $this->request->data['product_id'];
                $a_qty = $this->request->data['quantity_received'];
                $a_product_serial = $this->request->data['product_serial'];
                foreach ($a_product_id as $key => $i_product_id) {
                    $a_temp = array();
                    $a_temp['product_id'] = $a_product_id[$key];
                    $a_temp['quantity_received'] = $a_qty[$key];
                    $a_temp['product_serial'] = $a_product_serial[$key];
                    array_push($a_details, $a_temp);
                }
            }
            unset($this->request->data['product_id']);
            unset($this->request->data['quantity_received']);
            unset($this->request->data['product_serial']);
            $this->request->data['sales_return_details'] = $a_details;   

            $salesReturn = $this->SalesReturns->patchEntity($salesReturn, $this->request->data);
            if ($this->SalesReturns->save($salesReturn)) {
                $this->Flash->success(__('The sales return has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sales return could not be saved. Please, try again.'));
            }
        }
        $officialReceipts = $this->SalesReturns->OfficialReceipts->find('list', ['limit' => 200]);
        $depots = $this->SalesReturns->Depots->find('list', ['limit' => 200]);
        $users = $this->SalesReturns->Users->find('list', ['limit' => 200]);
        $salesReturnStatus = $this->SalesReturns->SalesReturnStatus->find('list', ['limit' => 200]);
        $Products = TableRegistry::get('Products');
        $product_list = $Products->find()
                    ->contain(['ProductColors', 'ProductDimensions', 'ProductUnits']);
        $this->set(compact('salesReturn', 'officialReceipts', 'depots', 'users', 'salesReturnStatus', 'product_list'));
        $this->set('_serialize', ['salesReturn']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sales Return id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $SalesReturnDetails = TableRegistry::get('SalesReturnDetails');
        $salesReturn = $this->SalesReturns->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $SalesReturnDetails->deleteAll(['sales_return_id' => $id]);
            $a_details = array();
            if(     isset($this->request->data['product_id']) && !empty($this->request->data['product_id']) 
                &&  isset($this->request->data['quantity_received']) && !empty($this->request->data['quantity_received']) 
                &&  isset($this->request->data['product_serial'])
            )
            {
                $a_product_id = $this->request->data['product_id'];
                $a_qty = $this->request->data['quantity_received'];
                $a_product_serial = $this->request->data['product_serial'];
                foreach ($a_product_id as $key => $i_product_id) {
                    $a_temp = array();
                    $a_temp['product_id'] = $a_product_id[$key];
                    $a_temp['quantity_received'] = $a_qty[$key];
                    $a_temp['product_serial'] = $a_product_serial[$key];
                    array_push($a_details, $a_temp);
                }
                $this->request->data['sales_return_details'] = $a_details;
            }
            unset($this->request->data['product_id']);
            unset($this->request->data['quantity_received']);
            unset($this->request->data['product_serial']); 
            
            $salesReturn = $this->SalesReturns->patchEntity($salesReturn, $this->request->data);
            if ($this->SalesReturns->save($salesReturn)) {
                $this->Flash->success(__('The sales return has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sales return could not be saved. Please, try again.'));
            }
        }
        $officialReceipts = $this->SalesReturns->OfficialReceipts->find('list', ['limit' => 200]);
        $depots = $this->SalesReturns->Depots->find('list', ['limit' => 200]);
        $users = $this->SalesReturns->Users->find('list', ['limit' => 200]);
        $salesReturnStatus = $this->SalesReturns->SalesReturnStatus->find('list', ['limit' => 200]);
        $transac_detail_list = $SalesReturnDetails->find()
                                        ->where(['sales_return_id' => $id]);
        $Products = TableRegistry::get('Products');
        $product_list = $Products->find()
                    ->contain(['ProductColors', 'ProductDimensions', 'ProductUnits']);
        $this->set(compact('salesReturn', 'officialReceipts', 'depots', 'users', 'salesReturnStatus', 'product_list', 'transac_detail_list'));
        $this->set('_serialize', ['salesReturn']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sales Return id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $salesReturn = $this->SalesReturns->get($id);
        if ($this->SalesReturns->delete($salesReturn)) {
            $this->Flash->success(__('The sales return has been deleted.'));
        } else {
            $this->Flash->error(__('The sales return could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
