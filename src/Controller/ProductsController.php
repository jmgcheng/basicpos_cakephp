<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;

/**
 * Products Controller
 *
 * @property \App\Model\Table\ProductsTable $Products
 */
class ProductsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['ProductStatus', 'ProductColors', 'ProductDimensions', 'ProductUnits']
        ];
        $products = $this->paginate($this->Products);

        $this->set(compact('products'));
        $this->set('_serialize', ['products']);
    }

    /**
     * View method
     *
     * @param string|null $id Product id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $product = $this->Products->get($id, [
            'contain' => ['ProductStatus', 'ProductColors', 'ProductDimensions', 'ProductUnits', 'DeliveryReceiptDetails', 'ExcludedInventoryDetails', 'IncludedInventoryDetails', 'MissedPoDetails', 'OfficialReceiptDetails', 'PurchaseOrderDetails', 'ReceivedPoDetails', 'ReceivedToDetails', 'SalesInvoiceDetails', 'TransferOrderDetails', 'WrongSendPoDetails']
        ]);

        $this->set('product', $product);
        $this->set('_serialize', ['product']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $product = $this->Products->newEntity();
        if ($this->request->is('post')) {
            $product = $this->Products->patchEntity($product, $this->request->data);
            if ($this->Products->save($product)) {
                $this->Flash->success(__('The product has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The product could not be saved. Please, try again.'));
            }
        }
        $productStatus = $this->Products->ProductStatus->find('list', ['limit' => 200]);
        $productColors = $this->Products->ProductColors->find('list', ['limit' => 200]);
        $productDimensions = $this->Products->ProductDimensions->find('list', ['limit' => 200]);
        $productUnits = $this->Products->ProductUnits->find('list', ['limit' => 200]);
        $this->set(compact('product', 'productStatus', 'productColors', 'productDimensions', 'productUnits'));
        $this->set('_serialize', ['product']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Product id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $product = $this->Products->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $product = $this->Products->patchEntity($product, $this->request->data);
            if ($this->Products->save($product)) {
                $this->Flash->success(__('The product has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The product could not be saved. Please, try again.'));
            }
        }
        $productStatus = $this->Products->ProductStatus->find('list', ['limit' => 200]);
        $productColors = $this->Products->ProductColors->find('list', ['limit' => 200]);
        $productDimensions = $this->Products->ProductDimensions->find('list', ['limit' => 200]);
        $productUnits = $this->Products->ProductUnits->find('list', ['limit' => 200]);
        $this->set(compact('product', 'productStatus', 'productColors', 'productDimensions', 'productUnits'));
        $this->set('_serialize', ['product']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Product id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $product = $this->Products->get($id);
        if ($this->Products->delete($product)) {
            $this->Flash->success(__('The product has been deleted.'));
        } else {
            $this->Flash->error(__('The product could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }


    /**/
    public function stock()
    {
        
        $i_depot_id = ( isset($_GET['depot_id']) && !empty($_GET['depot_id']) ) ? $_GET['depot_id'] : '';
        $i_qa = ( isset($_GET['qa']) && !empty($_GET['qa']) ) ? $_GET['qa'] : '';
        $i_product_dimension_id = ( isset($_GET['product_dimension_id']) && !empty($_GET['product_dimension_id']) ) ? $_GET['product_dimension_id'] : '';
        $i_product_color_id = ( isset($_GET['product_color_id']) && !empty($_GET['product_color_id']) ) ? $_GET['product_color_id'] : '';
        $i_product_unit_name = ( isset($_GET['product_unit_name']) && !empty($_GET['product_unit_name']) ) ? $_GET['product_unit_name'] : '';
        $i_product_status_id = ( isset($_GET['product_status_id']) && !empty($_GET['product_status_id']) ) ? $_GET['product_status_id'] : '';
        $i_product_name_sort = ( isset($_GET['product_name_sort']) && !empty($_GET['product_name_sort']) ) ? $_GET['product_name_sort'] : '';
        $s_product_color_name_sort = ( isset($_GET['product_color_name_sort']) && !empty($_GET['product_color_name_sort']) ) ? $_GET['product_color_name_sort'] : '';
        $s_product_dimension_name_sort = ( isset($_GET['product_dimension_name_sort']) && !empty($_GET['product_dimension_name_sort']) ) ? $_GET['product_dimension_name_sort'] : '';
        $i_limit = ( isset($_GET['i_limit']) && !empty($_GET['i_limit']) ) ? $_GET['i_limit'] : '';
        $i_offset = ( isset($_GET['i_offset']) && !empty($_GET['i_offset']) ) ? $_GET['i_offset'] : '';

        $connection = ConnectionManager::get('default');
        $stock_results = $connection->execute("CALL stocks_quantities('$i_depot_id', '$i_qa', '$i_product_dimension_id', '$i_product_color_id', '$i_product_unit_name', '$i_product_status_id', '$i_product_name_sort', '$s_product_color_name_sort', '$s_product_dimension_name_sort', '$i_limit', '$i_offset');")->fetchAll('assoc');

        $this->set(compact('stock_results'));
    }
}
