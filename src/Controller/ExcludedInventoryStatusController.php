<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ExcludedInventoryStatus Controller
 *
 * @property \App\Model\Table\ExcludedInventoryStatusTable $ExcludedInventoryStatus
 */
class ExcludedInventoryStatusController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $excludedInventoryStatus = $this->paginate($this->ExcludedInventoryStatus);

        $this->set(compact('excludedInventoryStatus'));
        $this->set('_serialize', ['excludedInventoryStatus']);
    }

    /**
     * View method
     *
     * @param string|null $id Excluded Inventory Status id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $excludedInventoryStatus = $this->ExcludedInventoryStatus->get($id, [
            'contain' => ['ExcludedInventories']
        ]);

        $this->set('excludedInventoryStatus', $excludedInventoryStatus);
        $this->set('_serialize', ['excludedInventoryStatus']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $excludedInventoryStatus = $this->ExcludedInventoryStatus->newEntity();
        if ($this->request->is('post')) {
            $excludedInventoryStatus = $this->ExcludedInventoryStatus->patchEntity($excludedInventoryStatus, $this->request->data);
            if ($this->ExcludedInventoryStatus->save($excludedInventoryStatus)) {
                $this->Flash->success(__('The excluded inventory status has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The excluded inventory status could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('excludedInventoryStatus'));
        $this->set('_serialize', ['excludedInventoryStatus']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Excluded Inventory Status id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $excludedInventoryStatus = $this->ExcludedInventoryStatus->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $excludedInventoryStatus = $this->ExcludedInventoryStatus->patchEntity($excludedInventoryStatus, $this->request->data);
            if ($this->ExcludedInventoryStatus->save($excludedInventoryStatus)) {
                $this->Flash->success(__('The excluded inventory status has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The excluded inventory status could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('excludedInventoryStatus'));
        $this->set('_serialize', ['excludedInventoryStatus']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Excluded Inventory Status id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $excludedInventoryStatus = $this->ExcludedInventoryStatus->get($id);
        if ($this->ExcludedInventoryStatus->delete($excludedInventoryStatus)) {
            $this->Flash->success(__('The excluded inventory status has been deleted.'));
        } else {
            $this->Flash->error(__('The excluded inventory status could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
