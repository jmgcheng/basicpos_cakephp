<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * DeliveryReceipts Controller
 *
 * @property \App\Model\Table\DeliveryReceiptsTable $DeliveryReceipts
 */
class DeliveryReceiptsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SalesInvoices', 'Users', 'DeliveryReceiptStatus']
        ];
        $deliveryReceipts = $this->paginate($this->DeliveryReceipts);

        $this->set(compact('deliveryReceipts'));
        $this->set('_serialize', ['deliveryReceipts']);
    }

    /**
     * View method
     *
     * @param string|null $id Delivery Receipt id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $deliveryReceipt = $this->DeliveryReceipts->get($id, [
            'contain' => ['SalesInvoices', 'Users', 'DeliveryReceiptStatus', 'DeliveryReceiptDetails']
        ]);

        $this->set('deliveryReceipt', $deliveryReceipt);
        $this->set('_serialize', ['deliveryReceipt']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $deliveryReceipt = $this->DeliveryReceipts->newEntity();
        if ($this->request->is('post')) {

            $a_details = array();
            if(     isset($this->request->data['product_id']) && !empty($this->request->data['product_id']) 
                &&  isset($this->request->data['quantity_delivering']) && !empty($this->request->data['quantity_delivering']) 
                &&  isset($this->request->data['product_serial'])
            )
            {
                $a_product_id = $this->request->data['product_id'];
                $a_qty = $this->request->data['quantity_delivering'];
                $a_product_serial = $this->request->data['product_serial'];
                foreach ($a_product_id as $key => $i_product_id) {
                    $a_temp = array();
                    $a_temp['product_id'] = $a_product_id[$key];
                    $a_temp['quantity_delivering'] = $a_qty[$key];
                    $a_temp['product_serial'] = $a_product_serial[$key];
                    array_push($a_details, $a_temp);
                }
            }
            unset($this->request->data['product_id']);
            unset($this->request->data['quantity_delivering']);
            unset($this->request->data['product_serial']);
            $this->request->data['delivery_receipt_details'] = $a_details; 

            $deliveryReceipt = $this->DeliveryReceipts->patchEntity($deliveryReceipt, $this->request->data);
            if ($this->DeliveryReceipts->save($deliveryReceipt)) {
                $this->Flash->success(__('The delivery receipt has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The delivery receipt could not be saved. Please, try again.'));
            }
        }
        $salesInvoices = $this->DeliveryReceipts->SalesInvoices->find('list', ['limit' => 200]);
        $users = $this->DeliveryReceipts->Users->find('list', ['limit' => 200]);
        $deliveryReceiptStatus = $this->DeliveryReceipts->DeliveryReceiptStatus->find('list', ['limit' => 200]);
        $Products = TableRegistry::get('Products');
        $product_list = $Products->find()
                    ->contain(['ProductColors', 'ProductDimensions', 'ProductUnits']);
        $this->set(compact('deliveryReceipt', 'salesInvoices', 'users', 'deliveryReceiptStatus', 'product_list'));
        $this->set('_serialize', ['deliveryReceipt']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Delivery Receipt id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $DeliveryReceiptDetails = TableRegistry::get('DeliveryReceiptDetails');
        $deliveryReceipt = $this->DeliveryReceipts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $DeliveryReceiptDetails->deleteAll(['delivery_receipt_id' => $id]);
            $a_details = array();
            if(     isset($this->request->data['product_id']) && !empty($this->request->data['product_id']) 
                &&  isset($this->request->data['quantity_delivering']) && !empty($this->request->data['quantity_delivering']) 
                &&  isset($this->request->data['product_serial'])
            )
            {
                $a_product_id = $this->request->data['product_id'];
                $a_qty = $this->request->data['quantity_delivering'];
                $a_product_serial = $this->request->data['product_serial'];
                foreach ($a_product_id as $key => $i_product_id) {
                    $a_temp = array();
                    $a_temp['product_id'] = $a_product_id[$key];
                    $a_temp['quantity_delivering'] = $a_qty[$key];
                    $a_temp['product_serial'] = $a_product_serial[$key];
                    array_push($a_details, $a_temp);
                }
                $this->request->data['delivery_receipt_details'] = $a_details;
            }
            unset($this->request->data['product_id']);
            unset($this->request->data['quantity_delivering']);
            unset($this->request->data['product_serial']);  
            
            $deliveryReceipt = $this->DeliveryReceipts->patchEntity($deliveryReceipt, $this->request->data);
            if ($this->DeliveryReceipts->save($deliveryReceipt)) {
                $this->Flash->success(__('The delivery receipt has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The delivery receipt could not be saved. Please, try again.'));
            }
        }
        $salesInvoices = $this->DeliveryReceipts->SalesInvoices->find('list', ['limit' => 200]);
        $users = $this->DeliveryReceipts->Users->find('list', ['limit' => 200]);
        $deliveryReceiptStatus = $this->DeliveryReceipts->DeliveryReceiptStatus->find('list', ['limit' => 200]);
        $transac_detail_list = $DeliveryReceiptDetails->find()
                                        ->where(['delivery_receipt_id' => $id]);
        $Products = TableRegistry::get('Products');
        $product_list = $Products->find()
                    ->contain(['ProductColors', 'ProductDimensions', 'ProductUnits']);
        $this->set(compact('deliveryReceipt', 'salesInvoices', 'users', 'deliveryReceiptStatus', 'product_list', 'transac_detail_list'));
        $this->set('_serialize', ['deliveryReceipt']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Delivery Receipt id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $deliveryReceipt = $this->DeliveryReceipts->get($id);
        if ($this->DeliveryReceipts->delete($deliveryReceipt)) {
            $this->Flash->success(__('The delivery receipt has been deleted.'));
        } else {
            $this->Flash->error(__('The delivery receipt could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
