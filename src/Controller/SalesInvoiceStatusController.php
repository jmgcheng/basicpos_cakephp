<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SalesInvoiceStatus Controller
 *
 * @property \App\Model\Table\SalesInvoiceStatusTable $SalesInvoiceStatus
 */
class SalesInvoiceStatusController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $salesInvoiceStatus = $this->paginate($this->SalesInvoiceStatus);

        $this->set(compact('salesInvoiceStatus'));
        $this->set('_serialize', ['salesInvoiceStatus']);
    }

    /**
     * View method
     *
     * @param string|null $id Sales Invoice Status id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $salesInvoiceStatus = $this->SalesInvoiceStatus->get($id, [
            'contain' => ['SalesInvoices']
        ]);

        $this->set('salesInvoiceStatus', $salesInvoiceStatus);
        $this->set('_serialize', ['salesInvoiceStatus']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $salesInvoiceStatus = $this->SalesInvoiceStatus->newEntity();
        if ($this->request->is('post')) {
            $salesInvoiceStatus = $this->SalesInvoiceStatus->patchEntity($salesInvoiceStatus, $this->request->data);
            if ($this->SalesInvoiceStatus->save($salesInvoiceStatus)) {
                $this->Flash->success(__('The sales invoice status has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sales invoice status could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('salesInvoiceStatus'));
        $this->set('_serialize', ['salesInvoiceStatus']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sales Invoice Status id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $salesInvoiceStatus = $this->SalesInvoiceStatus->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $salesInvoiceStatus = $this->SalesInvoiceStatus->patchEntity($salesInvoiceStatus, $this->request->data);
            if ($this->SalesInvoiceStatus->save($salesInvoiceStatus)) {
                $this->Flash->success(__('The sales invoice status has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sales invoice status could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('salesInvoiceStatus'));
        $this->set('_serialize', ['salesInvoiceStatus']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sales Invoice Status id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $salesInvoiceStatus = $this->SalesInvoiceStatus->get($id);
        if ($this->SalesInvoiceStatus->delete($salesInvoiceStatus)) {
            $this->Flash->success(__('The sales invoice status has been deleted.'));
        } else {
            $this->Flash->error(__('The sales invoice status could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
