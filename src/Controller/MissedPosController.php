<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * MissedPos Controller
 *
 * @property \App\Model\Table\MissedPosTable $MissedPos
 */
class MissedPosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['PurchaseOrders', 'Depots', 'Users', 'MissedPoStatus']
        ];
        $missedPos = $this->paginate($this->MissedPos);

        $this->set(compact('missedPos'));
        $this->set('_serialize', ['missedPos']);
    }

    /**
     * View method
     *
     * @param string|null $id Missed Po id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $missedPo = $this->MissedPos->get($id, [
            'contain' => ['PurchaseOrders', 'Depots', 'Users', 'MissedPoStatus', 'MissedPoDetails']
        ]);

        $this->set('missedPo', $missedPo);
        $this->set('_serialize', ['missedPo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $missedPo = $this->MissedPos->newEntity();
        if ($this->request->is('post')) {

            $a_details = array();
            if(     isset($this->request->data['product_id']) && !empty($this->request->data['product_id']) 
                &&  isset($this->request->data['quantity_missed']) && !empty($this->request->data['quantity_missed']) 
            )
            {
                $a_product_id = $this->request->data['product_id'];
                $a_qty = $this->request->data['quantity_missed'];
                foreach ($a_product_id as $key => $i_product_id) {
                    $a_temp = array();
                    $a_temp['product_id'] = $a_product_id[$key];
                    $a_temp['quantity_missed'] = $a_qty[$key];
                    array_push($a_details, $a_temp);
                }
            }
            unset($this->request->data['product_id']);
            unset($this->request->data['quantity_missed']);
            $this->request->data['missed_po_details'] = $a_details;

            $missedPo = $this->MissedPos->patchEntity($missedPo, $this->request->data);
            if ($this->MissedPos->save($missedPo)) {
                $this->Flash->success(__('The missed po has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The missed po could not be saved. Please, try again.'));
            }
        }
        $purchaseOrders = $this->MissedPos->PurchaseOrders->find('list', ['limit' => 200]);
        $depots = $this->MissedPos->Depots->find('list', ['limit' => 200]);
        $users = $this->MissedPos->Users->find('list', ['limit' => 200]);
        $missedPoStatus = $this->MissedPos->MissedPoStatus->find('list', ['limit' => 200]);
        $Products = TableRegistry::get('Products');
        $product_list = $Products->find()
                    ->contain(['ProductColors', 'ProductDimensions', 'ProductUnits']);
        $this->set(compact('missedPo', 'purchaseOrders', 'depots', 'users', 'missedPoStatus', 'product_list'));
        $this->set('_serialize', ['missedPo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Missed Po id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $MissedPoDetails = TableRegistry::get('MissedPoDetails');
        $missedPo = $this->MissedPos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $MissedPoDetails->deleteAll(['missed_po_id' => $id]);
            $a_details = array();
            if(     isset($this->request->data['product_id']) && !empty($this->request->data['product_id']) 
                &&  isset($this->request->data['quantity_missed']) && !empty($this->request->data['quantity_missed']) 
            )
            {
                $a_product_id = $this->request->data['product_id'];
                $a_qty = $this->request->data['quantity_missed'];
                foreach ($a_product_id as $key => $i_product_id) {
                    $a_temp = array();
                    $a_temp['product_id'] = $a_product_id[$key];
                    $a_temp['quantity_missed'] = $a_qty[$key];
                    array_push($a_details, $a_temp);
                }
                $this->request->data['missed_po_details'] = $a_details;
            }
            unset($this->request->data['product_id']);
            unset($this->request->data['quantity_missed']);

            $missedPo = $this->MissedPos->patchEntity($missedPo, $this->request->data);
            if ($this->MissedPos->save($missedPo)) {
                $this->Flash->success(__('The missed po has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The missed po could not be saved. Please, try again.'));
            }
        }
        $purchaseOrders = $this->MissedPos->PurchaseOrders->find('list', ['limit' => 200]);
        $depots = $this->MissedPos->Depots->find('list', ['limit' => 200]);
        $users = $this->MissedPos->Users->find('list', ['limit' => 200]);
        $missedPoStatus = $this->MissedPos->MissedPoStatus->find('list', ['limit' => 200]);
        $transac_detail_list = $MissedPoDetails->find()
                                        ->where(['missed_po_id' => $id]);
        $Products = TableRegistry::get('Products');
        $product_list = $Products->find()
                    ->contain(['ProductColors', 'ProductDimensions', 'ProductUnits']);
        $this->set(compact('missedPo', 'purchaseOrders', 'depots', 'users', 'missedPoStatus', 'product_list', 'transac_detail_list'));
        $this->set('_serialize', ['missedPo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Missed Po id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $missedPo = $this->MissedPos->get($id);
        if ($this->MissedPos->delete($missedPo)) {
            $this->Flash->success(__('The missed po has been deleted.'));
        } else {
            $this->Flash->error(__('The missed po could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
