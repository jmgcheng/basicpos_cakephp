<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * WrongSendPoStatus Controller
 *
 * @property \App\Model\Table\WrongSendPoStatusTable $WrongSendPoStatus
 */
class WrongSendPoStatusController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $wrongSendPoStatus = $this->paginate($this->WrongSendPoStatus);

        $this->set(compact('wrongSendPoStatus'));
        $this->set('_serialize', ['wrongSendPoStatus']);
    }

    /**
     * View method
     *
     * @param string|null $id Wrong Send Po Status id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $wrongSendPoStatus = $this->WrongSendPoStatus->get($id, [
            'contain' => ['WrongSendPos']
        ]);

        $this->set('wrongSendPoStatus', $wrongSendPoStatus);
        $this->set('_serialize', ['wrongSendPoStatus']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $wrongSendPoStatus = $this->WrongSendPoStatus->newEntity();
        if ($this->request->is('post')) {
            $wrongSendPoStatus = $this->WrongSendPoStatus->patchEntity($wrongSendPoStatus, $this->request->data);
            if ($this->WrongSendPoStatus->save($wrongSendPoStatus)) {
                $this->Flash->success(__('The wrong send po status has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The wrong send po status could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('wrongSendPoStatus'));
        $this->set('_serialize', ['wrongSendPoStatus']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Wrong Send Po Status id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $wrongSendPoStatus = $this->WrongSendPoStatus->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $wrongSendPoStatus = $this->WrongSendPoStatus->patchEntity($wrongSendPoStatus, $this->request->data);
            if ($this->WrongSendPoStatus->save($wrongSendPoStatus)) {
                $this->Flash->success(__('The wrong send po status has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The wrong send po status could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('wrongSendPoStatus'));
        $this->set('_serialize', ['wrongSendPoStatus']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Wrong Send Po Status id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $wrongSendPoStatus = $this->WrongSendPoStatus->get($id);
        if ($this->WrongSendPoStatus->delete($wrongSendPoStatus)) {
            $this->Flash->success(__('The wrong send po status has been deleted.'));
        } else {
            $this->Flash->error(__('The wrong send po status could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
