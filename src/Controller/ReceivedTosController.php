<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * ReceivedTos Controller
 *
 * @property \App\Model\Table\ReceivedTosTable $ReceivedTos
 */
class ReceivedTosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['TransferOrders', 'Depots', 'Users', 'ReceivedToStatus']
        ];
        $receivedTos = $this->paginate($this->ReceivedTos);

        $this->set(compact('receivedTos'));
        $this->set('_serialize', ['receivedTos']);
    }

    /**
     * View method
     *
     * @param string|null $id Received To id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $receivedTo = $this->ReceivedTos->get($id, [
            'contain' => ['TransferOrders', 'Depots', 'Users', 'ReceivedToStatus', 'ReceivedToDetails']
        ]);

        $this->set('receivedTo', $receivedTo);
        $this->set('_serialize', ['receivedTo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $receivedTo = $this->ReceivedTos->newEntity();
        if ($this->request->is('post')) {

            $a_details = array();
            if(     isset($this->request->data['product_id']) && !empty($this->request->data['product_id']) 
                &&  isset($this->request->data['quantity_received']) && !empty($this->request->data['quantity_received']) 
                &&  isset($this->request->data['product_serial'])
            )
            {
                $a_product_id = $this->request->data['product_id'];
                $a_qty = $this->request->data['quantity_received'];
                $a_product_serial = $this->request->data['product_serial'];
                foreach ($a_product_id as $key => $i_product_id) {
                    $a_temp = array();
                    $a_temp['product_id'] = $a_product_id[$key];
                    $a_temp['quantity_received'] = $a_qty[$key];
                    $a_temp['product_serial'] = $a_product_serial[$key];
                    array_push($a_details, $a_temp);
                }
            }
            unset($this->request->data['product_id']);
            unset($this->request->data['quantity_received']);
            unset($this->request->data['product_serial']);
            $this->request->data['received_to_details'] = $a_details; 

            $receivedTo = $this->ReceivedTos->patchEntity($receivedTo, $this->request->data);
            if ($this->ReceivedTos->save($receivedTo)) {
                $this->Flash->success(__('The received to has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The received to could not be saved. Please, try again.'));
            }
        }
        $transferOrders = $this->ReceivedTos->TransferOrders->find('list', ['limit' => 200]);
        $depots = $this->ReceivedTos->Depots->find('list', ['limit' => 200]);
        $users = $this->ReceivedTos->Users->find('list', ['limit' => 200]);
        $receivedToStatus = $this->ReceivedTos->ReceivedToStatus->find('list', ['limit' => 200]);
        $Products = TableRegistry::get('Products');
        $product_list = $Products->find()
                    ->contain(['ProductColors', 'ProductDimensions', 'ProductUnits']);
        $this->set(compact('receivedTo', 'transferOrders', 'depots', 'users', 'receivedToStatus', 'product_list'));
        $this->set('_serialize', ['receivedTo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Received To id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ReceivedToDetails = TableRegistry::get('ReceivedToDetails');
        $receivedTo = $this->ReceivedTos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $ReceivedToDetails->deleteAll(['received_to_id' => $id]);
            $a_details = array();
            if(     isset($this->request->data['product_id']) && !empty($this->request->data['product_id']) 
                &&  isset($this->request->data['quantity_received']) && !empty($this->request->data['quantity_received']) 
                &&  isset($this->request->data['product_serial'])
            )
            {
                $a_product_id = $this->request->data['product_id'];
                $a_qty = $this->request->data['quantity_received'];
                $a_product_serial = $this->request->data['product_serial'];
                foreach ($a_product_id as $key => $i_product_id) {
                    $a_temp = array();
                    $a_temp['product_id'] = $a_product_id[$key];
                    $a_temp['quantity_received'] = $a_qty[$key];
                    $a_temp['product_serial'] = $a_product_serial[$key];
                    array_push($a_details, $a_temp);
                }
                $this->request->data['received_to_details'] = $a_details;
            }
            unset($this->request->data['product_id']);
            unset($this->request->data['quantity_received']);
            unset($this->request->data['product_serial']);  
            
            $receivedTo = $this->ReceivedTos->patchEntity($receivedTo, $this->request->data);
            if ($this->ReceivedTos->save($receivedTo)) {
                $this->Flash->success(__('The received to has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The received to could not be saved. Please, try again.'));
            }
        }
        $transferOrders = $this->ReceivedTos->TransferOrders->find('list', ['limit' => 200]);
        $depots = $this->ReceivedTos->Depots->find('list', ['limit' => 200]);
        $users = $this->ReceivedTos->Users->find('list', ['limit' => 200]);
        $receivedToStatus = $this->ReceivedTos->ReceivedToStatus->find('list', ['limit' => 200]);
        $transac_detail_list = $ReceivedToDetails->find()
                                        ->where(['received_to_id' => $id]);
        $Products = TableRegistry::get('Products');
        $product_list = $Products->find()
                    ->contain(['ProductColors', 'ProductDimensions', 'ProductUnits']);
        $this->set(compact('receivedTo', 'transferOrders', 'depots', 'users', 'receivedToStatus', 'product_list', 'transac_detail_list'));
        $this->set('_serialize', ['receivedTo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Received To id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $receivedTo = $this->ReceivedTos->get($id);
        if ($this->ReceivedTos->delete($receivedTo)) {
            $this->Flash->success(__('The received to has been deleted.'));
        } else {
            $this->Flash->error(__('The received to could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
