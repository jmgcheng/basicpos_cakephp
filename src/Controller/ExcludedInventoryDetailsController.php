<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ExcludedInventoryDetails Controller
 *
 * @property \App\Model\Table\ExcludedInventoryDetailsTable $ExcludedInventoryDetails
 */
class ExcludedInventoryDetailsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['ExcludedInventories', 'Products']
        ];
        $excludedInventoryDetails = $this->paginate($this->ExcludedInventoryDetails);

        $this->set(compact('excludedInventoryDetails'));
        $this->set('_serialize', ['excludedInventoryDetails']);
    }

    /**
     * View method
     *
     * @param string|null $id Excluded Inventory Detail id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $excludedInventoryDetail = $this->ExcludedInventoryDetails->get($id, [
            'contain' => ['ExcludedInventories', 'Products']
        ]);

        $this->set('excludedInventoryDetail', $excludedInventoryDetail);
        $this->set('_serialize', ['excludedInventoryDetail']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $excludedInventoryDetail = $this->ExcludedInventoryDetails->newEntity();
        if ($this->request->is('post')) {
            $excludedInventoryDetail = $this->ExcludedInventoryDetails->patchEntity($excludedInventoryDetail, $this->request->data);
            if ($this->ExcludedInventoryDetails->save($excludedInventoryDetail)) {
                $this->Flash->success(__('The excluded inventory detail has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The excluded inventory detail could not be saved. Please, try again.'));
            }
        }
        $excludedInventories = $this->ExcludedInventoryDetails->ExcludedInventories->find('list', ['limit' => 200]);
        $products = $this->ExcludedInventoryDetails->Products->find('list', ['limit' => 200]);
        $this->set(compact('excludedInventoryDetail', 'excludedInventories', 'products'));
        $this->set('_serialize', ['excludedInventoryDetail']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Excluded Inventory Detail id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $excludedInventoryDetail = $this->ExcludedInventoryDetails->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $excludedInventoryDetail = $this->ExcludedInventoryDetails->patchEntity($excludedInventoryDetail, $this->request->data);
            if ($this->ExcludedInventoryDetails->save($excludedInventoryDetail)) {
                $this->Flash->success(__('The excluded inventory detail has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The excluded inventory detail could not be saved. Please, try again.'));
            }
        }
        $excludedInventories = $this->ExcludedInventoryDetails->ExcludedInventories->find('list', ['limit' => 200]);
        $products = $this->ExcludedInventoryDetails->Products->find('list', ['limit' => 200]);
        $this->set(compact('excludedInventoryDetail', 'excludedInventories', 'products'));
        $this->set('_serialize', ['excludedInventoryDetail']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Excluded Inventory Detail id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $excludedInventoryDetail = $this->ExcludedInventoryDetails->get($id);
        if ($this->ExcludedInventoryDetails->delete($excludedInventoryDetail)) {
            $this->Flash->success(__('The excluded inventory detail has been deleted.'));
        } else {
            $this->Flash->error(__('The excluded inventory detail could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
