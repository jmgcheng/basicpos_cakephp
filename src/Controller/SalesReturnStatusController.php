<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SalesReturnStatus Controller
 *
 * @property \App\Model\Table\SalesReturnStatusTable $SalesReturnStatus
 */
class SalesReturnStatusController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $salesReturnStatus = $this->paginate($this->SalesReturnStatus);

        $this->set(compact('salesReturnStatus'));
        $this->set('_serialize', ['salesReturnStatus']);
    }

    /**
     * View method
     *
     * @param string|null $id Sales Return Status id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $salesReturnStatus = $this->SalesReturnStatus->get($id, [
            'contain' => ['SalesReturns']
        ]);

        $this->set('salesReturnStatus', $salesReturnStatus);
        $this->set('_serialize', ['salesReturnStatus']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $salesReturnStatus = $this->SalesReturnStatus->newEntity();
        if ($this->request->is('post')) {
            $salesReturnStatus = $this->SalesReturnStatus->patchEntity($salesReturnStatus, $this->request->data);
            if ($this->SalesReturnStatus->save($salesReturnStatus)) {
                $this->Flash->success(__('The sales return status has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sales return status could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('salesReturnStatus'));
        $this->set('_serialize', ['salesReturnStatus']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sales Return Status id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $salesReturnStatus = $this->SalesReturnStatus->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $salesReturnStatus = $this->SalesReturnStatus->patchEntity($salesReturnStatus, $this->request->data);
            if ($this->SalesReturnStatus->save($salesReturnStatus)) {
                $this->Flash->success(__('The sales return status has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sales return status could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('salesReturnStatus'));
        $this->set('_serialize', ['salesReturnStatus']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sales Return Status id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $salesReturnStatus = $this->SalesReturnStatus->get($id);
        if ($this->SalesReturnStatus->delete($salesReturnStatus)) {
            $this->Flash->success(__('The sales return status has been deleted.'));
        } else {
            $this->Flash->error(__('The sales return status could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
