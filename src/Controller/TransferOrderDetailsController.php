<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TransferOrderDetails Controller
 *
 * @property \App\Model\Table\TransferOrderDetailsTable $TransferOrderDetails
 */
class TransferOrderDetailsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['TransferOrders', 'Products']
        ];
        $transferOrderDetails = $this->paginate($this->TransferOrderDetails);

        $this->set(compact('transferOrderDetails'));
        $this->set('_serialize', ['transferOrderDetails']);
    }

    /**
     * View method
     *
     * @param string|null $id Transfer Order Detail id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $transferOrderDetail = $this->TransferOrderDetails->get($id, [
            'contain' => ['TransferOrders', 'Products']
        ]);

        $this->set('transferOrderDetail', $transferOrderDetail);
        $this->set('_serialize', ['transferOrderDetail']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $transferOrderDetail = $this->TransferOrderDetails->newEntity();
        if ($this->request->is('post')) {
            $transferOrderDetail = $this->TransferOrderDetails->patchEntity($transferOrderDetail, $this->request->data);
            if ($this->TransferOrderDetails->save($transferOrderDetail)) {
                $this->Flash->success(__('The transfer order detail has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The transfer order detail could not be saved. Please, try again.'));
            }
        }
        $transferOrders = $this->TransferOrderDetails->TransferOrders->find('list', ['limit' => 200]);
        $products = $this->TransferOrderDetails->Products->find('list', ['limit' => 200]);
        $this->set(compact('transferOrderDetail', 'transferOrders', 'products'));
        $this->set('_serialize', ['transferOrderDetail']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Transfer Order Detail id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $transferOrderDetail = $this->TransferOrderDetails->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $transferOrderDetail = $this->TransferOrderDetails->patchEntity($transferOrderDetail, $this->request->data);
            if ($this->TransferOrderDetails->save($transferOrderDetail)) {
                $this->Flash->success(__('The transfer order detail has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The transfer order detail could not be saved. Please, try again.'));
            }
        }
        $transferOrders = $this->TransferOrderDetails->TransferOrders->find('list', ['limit' => 200]);
        $products = $this->TransferOrderDetails->Products->find('list', ['limit' => 200]);
        $this->set(compact('transferOrderDetail', 'transferOrders', 'products'));
        $this->set('_serialize', ['transferOrderDetail']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Transfer Order Detail id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $transferOrderDetail = $this->TransferOrderDetails->get($id);
        if ($this->TransferOrderDetails->delete($transferOrderDetail)) {
            $this->Flash->success(__('The transfer order detail has been deleted.'));
        } else {
            $this->Flash->error(__('The transfer order detail could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
