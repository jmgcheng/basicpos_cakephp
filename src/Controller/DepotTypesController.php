<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DepotTypes Controller
 *
 * @property \App\Model\Table\DepotTypesTable $DepotTypes
 */
class DepotTypesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $depotTypes = $this->paginate($this->DepotTypes);

        $this->set(compact('depotTypes'));
        $this->set('_serialize', ['depotTypes']);
    }

    /**
     * View method
     *
     * @param string|null $id Depot Type id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $depotType = $this->DepotTypes->get($id, [
            'contain' => ['Depots']
        ]);

        $this->set('depotType', $depotType);
        $this->set('_serialize', ['depotType']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $depotType = $this->DepotTypes->newEntity();
        if ($this->request->is('post')) {
            $depotType = $this->DepotTypes->patchEntity($depotType, $this->request->data);
            if ($this->DepotTypes->save($depotType)) {
                $this->Flash->success(__('The depot type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The depot type could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('depotType'));
        $this->set('_serialize', ['depotType']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Depot Type id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $depotType = $this->DepotTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $depotType = $this->DepotTypes->patchEntity($depotType, $this->request->data);
            if ($this->DepotTypes->save($depotType)) {
                $this->Flash->success(__('The depot type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The depot type could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('depotType'));
        $this->set('_serialize', ['depotType']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Depot Type id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $depotType = $this->DepotTypes->get($id);
        if ($this->DepotTypes->delete($depotType)) {
            $this->Flash->success(__('The depot type has been deleted.'));
        } else {
            $this->Flash->error(__('The depot type could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
