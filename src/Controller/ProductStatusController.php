<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ProductStatus Controller
 *
 * @property \App\Model\Table\ProductStatusTable $ProductStatus
 */
class ProductStatusController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $productStatus = $this->paginate($this->ProductStatus);

        $this->set(compact('productStatus'));
        $this->set('_serialize', ['productStatus']);
    }

    /**
     * View method
     *
     * @param string|null $id Product Status id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $productStatus = $this->ProductStatus->get($id, [
            'contain' => ['Products']
        ]);

        $this->set('productStatus', $productStatus);
        $this->set('_serialize', ['productStatus']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $productStatus = $this->ProductStatus->newEntity();
        if ($this->request->is('post')) {
            $productStatus = $this->ProductStatus->patchEntity($productStatus, $this->request->data);
            if ($this->ProductStatus->save($productStatus)) {
                $this->Flash->success(__('The product status has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The product status could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('productStatus'));
        $this->set('_serialize', ['productStatus']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Product Status id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $productStatus = $this->ProductStatus->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $productStatus = $this->ProductStatus->patchEntity($productStatus, $this->request->data);
            if ($this->ProductStatus->save($productStatus)) {
                $this->Flash->success(__('The product status has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The product status could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('productStatus'));
        $this->set('_serialize', ['productStatus']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Product Status id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $productStatus = $this->ProductStatus->get($id);
        if ($this->ProductStatus->delete($productStatus)) {
            $this->Flash->success(__('The product status has been deleted.'));
        } else {
            $this->Flash->error(__('The product status could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
