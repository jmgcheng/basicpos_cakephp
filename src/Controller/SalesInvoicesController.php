<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * SalesInvoices Controller
 *
 * @property \App\Model\Table\SalesInvoicesTable $SalesInvoices
 */
class SalesInvoicesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Depots', 'Users', 'SalesInvoiceStatus']
        ];
        $salesInvoices = $this->paginate($this->SalesInvoices);

        $this->set(compact('salesInvoices'));
        $this->set('_serialize', ['salesInvoices']);
    }

    /**
     * View method
     *
     * @param string|null $id Sales Invoice id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $salesInvoice = $this->SalesInvoices->get($id, [
            'contain' => ['Depots', 'Users', 'SalesInvoiceStatus', 'DeliveryReceipts', 'OfficialReceipts', 'SalesInvoiceDetails']
        ]);

        $this->set('salesInvoice', $salesInvoice);
        $this->set('_serialize', ['salesInvoice']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $salesInvoice = $this->SalesInvoices->newEntity();
        if ($this->request->is('post')) {

            $a_details = array();
            if(     isset($this->request->data['product_id']) && !empty($this->request->data['product_id']) 
                &&  isset($this->request->data['quantity_to_release']) && !empty($this->request->data['quantity_to_release']) 
            )
            {
                $a_product_id = $this->request->data['product_id'];
                $a_qty = $this->request->data['quantity_to_release'];
                foreach ($a_product_id as $key => $i_product_id) {
                    $a_temp = array();
                    $a_temp['product_id'] = $a_product_id[$key];
                    $a_temp['quantity_to_release'] = $a_qty[$key];
                    array_push($a_details, $a_temp);
                }
            }
            unset($this->request->data['product_id']);
            unset($this->request->data['quantity_to_release']);
            $this->request->data['sales_invoice_details'] = $a_details;

            $salesInvoice = $this->SalesInvoices->patchEntity($salesInvoice, $this->request->data);
            if ($this->SalesInvoices->save($salesInvoice)) {
                $this->Flash->success(__('The sales invoice has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sales invoice could not be saved. Please, try again.'));
            }
        }
        $depots = $this->SalesInvoices->Depots->find('list', ['limit' => 200]);
        $users = $this->SalesInvoices->Users->find('list', ['limit' => 200]);
        $salesInvoiceStatus = $this->SalesInvoices->SalesInvoiceStatus->find('list', ['limit' => 200]);
        $Products = TableRegistry::get('Products');
        $product_list = $Products->find()
                    ->contain(['ProductColors', 'ProductDimensions', 'ProductUnits']);
        $this->set(compact('salesInvoice', 'depots', 'users', 'salesInvoiceStatus', 'product_list'));
        $this->set('_serialize', ['salesInvoice']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sales Invoice id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $SalesInvoiceDetails = TableRegistry::get('SalesInvoiceDetails');
        $salesInvoice = $this->SalesInvoices->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $SalesInvoiceDetails->deleteAll(['sales_invoice_id' => $id]);
            $a_details = array();
            if(     isset($this->request->data['product_id']) && !empty($this->request->data['product_id']) 
                &&  isset($this->request->data['quantity_to_release']) && !empty($this->request->data['quantity_to_release']) 
            )
            {
                $a_product_id = $this->request->data['product_id'];
                $a_qty = $this->request->data['quantity_to_release'];
                foreach ($a_product_id as $key => $i_product_id) {
                    $a_temp = array();
                    $a_temp['product_id'] = $a_product_id[$key];
                    $a_temp['quantity_to_release'] = $a_qty[$key];
                    array_push($a_details, $a_temp);
                }
                $this->request->data['sales_invoice_details'] = $a_details;
            }
            unset($this->request->data['product_id']);
            unset($this->request->data['quantity_to_release']);            
            
            $salesInvoice = $this->SalesInvoices->patchEntity($salesInvoice, $this->request->data);
            if ($this->SalesInvoices->save($salesInvoice)) {
                $this->Flash->success(__('The sales invoice has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sales invoice could not be saved. Please, try again.'));
            }
        }
        $depots = $this->SalesInvoices->Depots->find('list', ['limit' => 200]);
        $users = $this->SalesInvoices->Users->find('list', ['limit' => 200]);
        $salesInvoiceStatus = $this->SalesInvoices->SalesInvoiceStatus->find('list', ['limit' => 200]);
        $transac_detail_list = $SalesInvoiceDetails->find()
                                        ->where(['sales_invoice_id' => $id]);
        $Products = TableRegistry::get('Products');
        $product_list = $Products->find()
                    ->contain(['ProductColors', 'ProductDimensions', 'ProductUnits']);        
        $this->set(compact('salesInvoice', 'depots', 'users', 'salesInvoiceStatus', 'product_list', 'transac_detail_list'));
        $this->set('_serialize', ['salesInvoice']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sales Invoice id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $salesInvoice = $this->SalesInvoices->get($id);
        if ($this->SalesInvoices->delete($salesInvoice)) {
            $this->Flash->success(__('The sales invoice has been deleted.'));
        } else {
            $this->Flash->error(__('The sales invoice could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
