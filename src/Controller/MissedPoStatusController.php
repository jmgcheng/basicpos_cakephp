<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MissedPoStatus Controller
 *
 * @property \App\Model\Table\MissedPoStatusTable $MissedPoStatus
 */
class MissedPoStatusController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $missedPoStatus = $this->paginate($this->MissedPoStatus);

        $this->set(compact('missedPoStatus'));
        $this->set('_serialize', ['missedPoStatus']);
    }

    /**
     * View method
     *
     * @param string|null $id Missed Po Status id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $missedPoStatus = $this->MissedPoStatus->get($id, [
            'contain' => ['MissedPos']
        ]);

        $this->set('missedPoStatus', $missedPoStatus);
        $this->set('_serialize', ['missedPoStatus']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $missedPoStatus = $this->MissedPoStatus->newEntity();
        if ($this->request->is('post')) {
            $missedPoStatus = $this->MissedPoStatus->patchEntity($missedPoStatus, $this->request->data);
            if ($this->MissedPoStatus->save($missedPoStatus)) {
                $this->Flash->success(__('The missed po status has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The missed po status could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('missedPoStatus'));
        $this->set('_serialize', ['missedPoStatus']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Missed Po Status id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $missedPoStatus = $this->MissedPoStatus->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $missedPoStatus = $this->MissedPoStatus->patchEntity($missedPoStatus, $this->request->data);
            if ($this->MissedPoStatus->save($missedPoStatus)) {
                $this->Flash->success(__('The missed po status has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The missed po status could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('missedPoStatus'));
        $this->set('_serialize', ['missedPoStatus']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Missed Po Status id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $missedPoStatus = $this->MissedPoStatus->get($id);
        if ($this->MissedPoStatus->delete($missedPoStatus)) {
            $this->Flash->success(__('The missed po status has been deleted.'));
        } else {
            $this->Flash->error(__('The missed po status could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
