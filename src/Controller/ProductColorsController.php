<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ProductColors Controller
 *
 * @property \App\Model\Table\ProductColorsTable $ProductColors
 */
class ProductColorsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $productColors = $this->paginate($this->ProductColors);

        $this->set(compact('productColors'));
        $this->set('_serialize', ['productColors']);
    }

    /**
     * View method
     *
     * @param string|null $id Product Color id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $productColor = $this->ProductColors->get($id, [
            'contain' => ['Products']
        ]);

        $this->set('productColor', $productColor);
        $this->set('_serialize', ['productColor']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $productColor = $this->ProductColors->newEntity();
        if ($this->request->is('post')) {
            $productColor = $this->ProductColors->patchEntity($productColor, $this->request->data);
            if ($this->ProductColors->save($productColor)) {
                $this->Flash->success(__('The product color has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The product color could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('productColor'));
        $this->set('_serialize', ['productColor']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Product Color id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $productColor = $this->ProductColors->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $productColor = $this->ProductColors->patchEntity($productColor, $this->request->data);
            if ($this->ProductColors->save($productColor)) {
                $this->Flash->success(__('The product color has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The product color could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('productColor'));
        $this->set('_serialize', ['productColor']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Product Color id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $productColor = $this->ProductColors->get($id);
        if ($this->ProductColors->delete($productColor)) {
            $this->Flash->success(__('The product color has been deleted.'));
        } else {
            $this->Flash->error(__('The product color could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
