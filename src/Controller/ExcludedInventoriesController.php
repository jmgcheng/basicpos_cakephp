<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * ExcludedInventories Controller
 *
 * @property \App\Model\Table\ExcludedInventoriesTable $ExcludedInventories
 */
class ExcludedInventoriesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Depots', 'Users', 'ExcludedInventoryStatus']
        ];
        $excludedInventories = $this->paginate($this->ExcludedInventories);

        $this->set(compact('excludedInventories'));
        $this->set('_serialize', ['excludedInventories']);
    }

    /**
     * View method
     *
     * @param string|null $id Excluded Inventory id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $excludedInventory = $this->ExcludedInventories->get($id, [
            'contain' => ['Depots', 'Users', 'ExcludedInventoryStatus', 'ExcludedInventoryDetails']
        ]);

        $this->set('excludedInventory', $excludedInventory);
        $this->set('_serialize', ['excludedInventory']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $excludedInventory = $this->ExcludedInventories->newEntity();
        if ($this->request->is('post')) {

           $a_details = array();
            if(     isset($this->request->data['product_id']) && !empty($this->request->data['product_id']) 
                &&  isset($this->request->data['quantity_excluded']) && !empty($this->request->data['quantity_excluded']) 
                &&  isset($this->request->data['product_serial'])
            )
            {
                $a_product_id = $this->request->data['product_id'];
                $a_qty = $this->request->data['quantity_excluded'];
                $a_product_serial = $this->request->data['product_serial'];
                foreach ($a_product_id as $key => $i_product_id) {
                    $a_temp = array();
                    $a_temp['product_id'] = $a_product_id[$key];
                    $a_temp['quantity_excluded'] = $a_qty[$key];
                    $a_temp['product_serial'] = $a_product_serial[$key];
                    array_push($a_details, $a_temp);
                }
            }
            unset($this->request->data['product_id']);
            unset($this->request->data['quantity_excluded']);
            unset($this->request->data['product_serial']);
            $this->request->data['excluded_inventory_details'] = $a_details;  

            $excludedInventory = $this->ExcludedInventories->patchEntity($excludedInventory, $this->request->data);
            if ($this->ExcludedInventories->save($excludedInventory)) {
                $this->Flash->success(__('The excluded inventory has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The excluded inventory could not be saved. Please, try again.'));
            }
        }
        $depots = $this->ExcludedInventories->Depots->find('list', ['limit' => 200]);
        $users = $this->ExcludedInventories->Users->find('list', ['limit' => 200]);
        $excludedInventoryStatus = $this->ExcludedInventories->ExcludedInventoryStatus->find('list', ['limit' => 200]);
        $Products = TableRegistry::get('Products');
        $product_list = $Products->find()
                    ->contain(['ProductColors', 'ProductDimensions', 'ProductUnits']);
        $this->set(compact('excludedInventory', 'depots', 'users', 'excludedInventoryStatus', 'product_list'));
        $this->set('_serialize', ['excludedInventory']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Excluded Inventory id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ExcludedInventoryDetails = TableRegistry::get('ExcludedInventoryDetails');
        $excludedInventory = $this->ExcludedInventories->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $ExcludedInventoryDetails->deleteAll(['excluded_inventory_id' => $id]);
            $a_details = array();
            if(     isset($this->request->data['product_id']) && !empty($this->request->data['product_id']) 
                &&  isset($this->request->data['quantity_excluded']) && !empty($this->request->data['quantity_excluded']) 
                &&  isset($this->request->data['product_serial'])
            )
            {
                $a_product_id = $this->request->data['product_id'];
                $a_qty = $this->request->data['quantity_excluded'];
                $a_product_serial = $this->request->data['product_serial'];
                foreach ($a_product_id as $key => $i_product_id) {
                    $a_temp = array();
                    $a_temp['product_id'] = $a_product_id[$key];
                    $a_temp['quantity_excluded'] = $a_qty[$key];
                    $a_temp['product_serial'] = $a_product_serial[$key];
                    array_push($a_details, $a_temp);
                }
                $this->request->data['excluded_inventory_details'] = $a_details;
            }
            unset($this->request->data['product_id']);
            unset($this->request->data['quantity_excluded']);
            unset($this->request->data['product_serial']);   
            
            $excludedInventory = $this->ExcludedInventories->patchEntity($excludedInventory, $this->request->data);
            if ($this->ExcludedInventories->save($excludedInventory)) {
                $this->Flash->success(__('The excluded inventory has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The excluded inventory could not be saved. Please, try again.'));
            }
        }
        $depots = $this->ExcludedInventories->Depots->find('list', ['limit' => 200]);
        $users = $this->ExcludedInventories->Users->find('list', ['limit' => 200]);
        $excludedInventoryStatus = $this->ExcludedInventories->ExcludedInventoryStatus->find('list', ['limit' => 200]);
        $transac_detail_list = $ExcludedInventoryDetails->find()
                                        ->where(['excluded_inventory_id' => $id]);
        $Products = TableRegistry::get('Products');
        $product_list = $Products->find()
                    ->contain(['ProductColors', 'ProductDimensions', 'ProductUnits']);
        $this->set(compact('excludedInventory', 'depots', 'users', 'excludedInventoryStatus', 'product_list', 'transac_detail_list'));
        $this->set('_serialize', ['excludedInventory']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Excluded Inventory id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $excludedInventory = $this->ExcludedInventories->get($id);
        if ($this->ExcludedInventories->delete($excludedInventory)) {
            $this->Flash->success(__('The excluded inventory has been deleted.'));
        } else {
            $this->Flash->error(__('The excluded inventory could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
