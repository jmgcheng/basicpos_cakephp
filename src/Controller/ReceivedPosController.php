<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * ReceivedPos Controller
 *
 * @property \App\Model\Table\ReceivedPosTable $ReceivedPos
 */
class ReceivedPosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['PurchaseOrders', 'Depots', 'Users', 'ReceivedPoStatus']
        ];
        $receivedPos = $this->paginate($this->ReceivedPos);

        $this->set(compact('receivedPos'));
        $this->set('_serialize', ['receivedPos']);
    }

    /**
     * View method
     *
     * @param string|null $id Received Po id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $receivedPo = $this->ReceivedPos->get($id, [
            'contain' => ['PurchaseOrders', 'Depots', 'Users', 'ReceivedPoStatus', 'ReceivedPoDetails']
        ]);

        $this->set('receivedPo', $receivedPo);
        $this->set('_serialize', ['receivedPo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $receivedPo = $this->ReceivedPos->newEntity();
        if ($this->request->is('post')) {

           $a_details = array();
            if(     isset($this->request->data['product_id']) && !empty($this->request->data['product_id']) 
                &&  isset($this->request->data['quantity_received']) && !empty($this->request->data['quantity_received']) 
                &&  isset($this->request->data['product_serial'])
            )
            {
                $a_product_id = $this->request->data['product_id'];
                $a_qty = $this->request->data['quantity_received'];
                $a_product_serial = $this->request->data['product_serial'];
                foreach ($a_product_id as $key => $i_product_id) {
                    $a_temp = array();
                    $a_temp['product_id'] = $a_product_id[$key];
                    $a_temp['quantity_received'] = $a_qty[$key];
                    $a_temp['product_serial'] = $a_product_serial[$key];
                    array_push($a_details, $a_temp);
                }
            }
            unset($this->request->data['product_id']);
            unset($this->request->data['quantity_received']);
            unset($this->request->data['product_serial']);
            $this->request->data['received_po_details'] = $a_details;

            $receivedPo = $this->ReceivedPos->patchEntity($receivedPo, $this->request->data);
            if ($this->ReceivedPos->save($receivedPo)) {
                $this->Flash->success(__('The received po has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The received po could not be saved. Please, try again.'));
            }
        }
        $purchaseOrders = $this->ReceivedPos->PurchaseOrders->find('list', ['limit' => 200]);
        $depots = $this->ReceivedPos->Depots->find('list', ['limit' => 200]);
        $users = $this->ReceivedPos->Users->find('list', ['limit' => 200]);
        $receivedPoStatus = $this->ReceivedPos->ReceivedPoStatus->find('list', ['limit' => 200]);
        $Products = TableRegistry::get('Products');
        $product_list = $Products->find()
                    ->contain(['ProductColors', 'ProductDimensions', 'ProductUnits']);
        $this->set(compact('receivedPo', 'purchaseOrders', 'depots', 'users', 'receivedPoStatus', 'product_list'));
        $this->set('_serialize', ['receivedPo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Received Po id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ReceivedPoDetails = TableRegistry::get('ReceivedPoDetails');
        $receivedPo = $this->ReceivedPos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $ReceivedPoDetails->deleteAll(['received_po_id' => $id]);
            $a_details = array();
            if(     isset($this->request->data['product_id']) && !empty($this->request->data['product_id']) 
                &&  isset($this->request->data['quantity_received']) && !empty($this->request->data['quantity_received']) 
                &&  isset($this->request->data['product_serial'])
            )
            {
                $a_product_id = $this->request->data['product_id'];
                $a_qty = $this->request->data['quantity_received'];
                $a_product_serial = $this->request->data['product_serial'];
                foreach ($a_product_id as $key => $i_product_id) {
                    $a_temp = array();
                    $a_temp['product_id'] = $a_product_id[$key];
                    $a_temp['quantity_received'] = $a_qty[$key];
                    $a_temp['product_serial'] = $a_product_serial[$key];
                    array_push($a_details, $a_temp);
                }
                $this->request->data['received_po_details'] = $a_details;
            }
            unset($this->request->data['product_id']);
            unset($this->request->data['quantity_received']);
            unset($this->request->data['product_serial']);

            $receivedPo = $this->ReceivedPos->patchEntity($receivedPo, $this->request->data);
            if ($this->ReceivedPos->save($receivedPo)) {
                $this->Flash->success(__('The received po has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The received po could not be saved. Please, try again.'));
            }
        }
        $purchaseOrders = $this->ReceivedPos->PurchaseOrders->find('list', ['limit' => 200]);
        $depots = $this->ReceivedPos->Depots->find('list', ['limit' => 200]);
        $users = $this->ReceivedPos->Users->find('list', ['limit' => 200]);
        $receivedPoStatus = $this->ReceivedPos->ReceivedPoStatus->find('list', ['limit' => 200]);
        $transac_detail_list = $ReceivedPoDetails->find()
                                        ->where(['received_po_id' => $id]);
        $Products = TableRegistry::get('Products');
        $product_list = $Products->find()
                    ->contain(['ProductColors', 'ProductDimensions', 'ProductUnits']);
        $this->set(compact('receivedPo', 'purchaseOrders', 'depots', 'users', 'receivedPoStatus', 'product_list', 'transac_detail_list'));
        $this->set('_serialize', ['receivedPo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Received Po id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $receivedPo = $this->ReceivedPos->get($id);
        if ($this->ReceivedPos->delete($receivedPo)) {
            $this->Flash->success(__('The received po has been deleted.'));
        } else {
            $this->Flash->error(__('The received po could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
