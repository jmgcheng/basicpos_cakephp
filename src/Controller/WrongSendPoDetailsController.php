<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * WrongSendPoDetails Controller
 *
 * @property \App\Model\Table\WrongSendPoDetailsTable $WrongSendPoDetails
 */
class WrongSendPoDetailsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['WrongSendPos', 'Products']
        ];
        $wrongSendPoDetails = $this->paginate($this->WrongSendPoDetails);

        $this->set(compact('wrongSendPoDetails'));
        $this->set('_serialize', ['wrongSendPoDetails']);
    }

    /**
     * View method
     *
     * @param string|null $id Wrong Send Po Detail id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $wrongSendPoDetail = $this->WrongSendPoDetails->get($id, [
            'contain' => ['WrongSendPos', 'Products']
        ]);

        $this->set('wrongSendPoDetail', $wrongSendPoDetail);
        $this->set('_serialize', ['wrongSendPoDetail']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $wrongSendPoDetail = $this->WrongSendPoDetails->newEntity();
        if ($this->request->is('post')) {
            $wrongSendPoDetail = $this->WrongSendPoDetails->patchEntity($wrongSendPoDetail, $this->request->data);
            if ($this->WrongSendPoDetails->save($wrongSendPoDetail)) {
                $this->Flash->success(__('The wrong send po detail has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The wrong send po detail could not be saved. Please, try again.'));
            }
        }
        $wrongSendPos = $this->WrongSendPoDetails->WrongSendPos->find('list', ['limit' => 200]);
        $products = $this->WrongSendPoDetails->Products->find('list', ['limit' => 200]);
        $this->set(compact('wrongSendPoDetail', 'wrongSendPos', 'products'));
        $this->set('_serialize', ['wrongSendPoDetail']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Wrong Send Po Detail id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $wrongSendPoDetail = $this->WrongSendPoDetails->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $wrongSendPoDetail = $this->WrongSendPoDetails->patchEntity($wrongSendPoDetail, $this->request->data);
            if ($this->WrongSendPoDetails->save($wrongSendPoDetail)) {
                $this->Flash->success(__('The wrong send po detail has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The wrong send po detail could not be saved. Please, try again.'));
            }
        }
        $wrongSendPos = $this->WrongSendPoDetails->WrongSendPos->find('list', ['limit' => 200]);
        $products = $this->WrongSendPoDetails->Products->find('list', ['limit' => 200]);
        $this->set(compact('wrongSendPoDetail', 'wrongSendPos', 'products'));
        $this->set('_serialize', ['wrongSendPoDetail']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Wrong Send Po Detail id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $wrongSendPoDetail = $this->WrongSendPoDetails->get($id);
        if ($this->WrongSendPoDetails->delete($wrongSendPoDetail)) {
            $this->Flash->success(__('The wrong send po detail has been deleted.'));
        } else {
            $this->Flash->error(__('The wrong send po detail could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
