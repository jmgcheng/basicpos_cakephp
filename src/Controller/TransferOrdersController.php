<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * TransferOrders Controller
 *
 * @property \App\Model\Table\TransferOrdersTable $TransferOrders
 */
class TransferOrdersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Depots', 'Users', 'TransferOrderStatus']
        ];
        $transferOrders = $this->paginate($this->TransferOrders);

        $this->set(compact('transferOrders'));
        $this->set('_serialize', ['transferOrders']);
    }

    /**
     * View method
     *
     * @param string|null $id Transfer Order id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $transferOrder = $this->TransferOrders->get($id, [
            'contain' => ['Depots', 'Users', 'TransferOrderStatus', 'ReceivedTos', 'TransferOrderDetails']
        ]);

        $this->set('transferOrder', $transferOrder);
        $this->set('_serialize', ['transferOrder']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $transferOrder = $this->TransferOrders->newEntity();
        if ($this->request->is('post')) {

            $a_details = array();
            if(     isset($this->request->data['product_id']) && !empty($this->request->data['product_id']) 
                &&  isset($this->request->data['quantity_transfering']) && !empty($this->request->data['quantity_transfering']) 
                &&  isset($this->request->data['product_serial'])
            )
            {
                $a_product_id = $this->request->data['product_id'];
                $a_qty = $this->request->data['quantity_transfering'];
                $a_product_serial = $this->request->data['product_serial'];
                foreach ($a_product_id as $key => $i_product_id) {
                    $a_temp = array();
                    $a_temp['product_id'] = $a_product_id[$key];
                    $a_temp['quantity_transfering'] = $a_qty[$key];
                    $a_temp['product_serial'] = $a_product_serial[$key];
                    array_push($a_details, $a_temp);
                }
            }
            unset($this->request->data['product_id']);
            unset($this->request->data['quantity_transfering']);
            unset($this->request->data['product_serial']);
            $this->request->data['transfer_order_details'] = $a_details;  

            $transferOrder = $this->TransferOrders->patchEntity($transferOrder, $this->request->data);
            if ($this->TransferOrders->save($transferOrder)) {
                $this->Flash->success(__('The transfer order has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The transfer order could not be saved. Please, try again.'));
            }
        }
        $depots = $this->TransferOrders->Depots->find('list', ['limit' => 200]);
        $users = $this->TransferOrders->Users->find('list', ['limit' => 200]);
        $transferOrderStatus = $this->TransferOrders->TransferOrderStatus->find('list', ['limit' => 200]);
        $Products = TableRegistry::get('Products');
        $product_list = $Products->find()
                    ->contain(['ProductColors', 'ProductDimensions', 'ProductUnits']);
        $this->set(compact('transferOrder', 'depots', 'users', 'transferOrderStatus', 'product_list'));
        $this->set('_serialize', ['transferOrder']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Transfer Order id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $TransferOrderDetails = TableRegistry::get('TransferOrderDetails');
        $transferOrder = $this->TransferOrders->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $TransferOrderDetails->deleteAll(['transfer_order_id' => $id]);
            $a_details = array();
            if(     isset($this->request->data['product_id']) && !empty($this->request->data['product_id']) 
                &&  isset($this->request->data['quantity_transfering']) && !empty($this->request->data['quantity_transfering']) 
                &&  isset($this->request->data['product_serial'])
            )
            {
                $a_product_id = $this->request->data['product_id'];
                $a_qty = $this->request->data['quantity_transfering'];
                $a_product_serial = $this->request->data['product_serial'];
                foreach ($a_product_id as $key => $i_product_id) {
                    $a_temp = array();
                    $a_temp['product_id'] = $a_product_id[$key];
                    $a_temp['quantity_transfering'] = $a_qty[$key];
                    $a_temp['product_serial'] = $a_product_serial[$key];
                    array_push($a_details, $a_temp);
                }
                $this->request->data['transfer_order_details'] = $a_details;
            }
            unset($this->request->data['product_id']);
            unset($this->request->data['quantity_transfering']);
            unset($this->request->data['product_serial']);  
            
            $transferOrder = $this->TransferOrders->patchEntity($transferOrder, $this->request->data);
            if ($this->TransferOrders->save($transferOrder)) {
                $this->Flash->success(__('The transfer order has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The transfer order could not be saved. Please, try again.'));
            }
        }
        $depots = $this->TransferOrders->Depots->find('list', ['limit' => 200]);
        $users = $this->TransferOrders->Users->find('list', ['limit' => 200]);
        $transferOrderStatus = $this->TransferOrders->TransferOrderStatus->find('list', ['limit' => 200]);
        $transac_detail_list = $TransferOrderDetails->find()
                                        ->where(['transfer_order_id' => $id]);
        $Products = TableRegistry::get('Products');
        $product_list = $Products->find()
                    ->contain(['ProductColors', 'ProductDimensions', 'ProductUnits']);
        $this->set(compact('transferOrder', 'depots', 'users', 'transferOrderStatus', 'product_list', 'transac_detail_list'));
        $this->set('_serialize', ['transferOrder']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Transfer Order id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $transferOrder = $this->TransferOrders->get($id);
        if ($this->TransferOrders->delete($transferOrder)) {
            $this->Flash->success(__('The transfer order has been deleted.'));
        } else {
            $this->Flash->error(__('The transfer order could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
