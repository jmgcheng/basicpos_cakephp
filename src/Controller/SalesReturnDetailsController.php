<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SalesReturnDetails Controller
 *
 * @property \App\Model\Table\SalesReturnDetailsTable $SalesReturnDetails
 */
class SalesReturnDetailsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SalesReturns', 'Products']
        ];
        $salesReturnDetails = $this->paginate($this->SalesReturnDetails);

        $this->set(compact('salesReturnDetails'));
        $this->set('_serialize', ['salesReturnDetails']);
    }

    /**
     * View method
     *
     * @param string|null $id Sales Return Detail id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $salesReturnDetail = $this->SalesReturnDetails->get($id, [
            'contain' => ['SalesReturns', 'Products']
        ]);

        $this->set('salesReturnDetail', $salesReturnDetail);
        $this->set('_serialize', ['salesReturnDetail']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $salesReturnDetail = $this->SalesReturnDetails->newEntity();
        if ($this->request->is('post')) {
            $salesReturnDetail = $this->SalesReturnDetails->patchEntity($salesReturnDetail, $this->request->data);
            if ($this->SalesReturnDetails->save($salesReturnDetail)) {
                $this->Flash->success(__('The sales return detail has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sales return detail could not be saved. Please, try again.'));
            }
        }
        $salesReturns = $this->SalesReturnDetails->SalesReturns->find('list', ['limit' => 200]);
        $products = $this->SalesReturnDetails->Products->find('list', ['limit' => 200]);
        $this->set(compact('salesReturnDetail', 'salesReturns', 'products'));
        $this->set('_serialize', ['salesReturnDetail']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sales Return Detail id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $salesReturnDetail = $this->SalesReturnDetails->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $salesReturnDetail = $this->SalesReturnDetails->patchEntity($salesReturnDetail, $this->request->data);
            if ($this->SalesReturnDetails->save($salesReturnDetail)) {
                $this->Flash->success(__('The sales return detail has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sales return detail could not be saved. Please, try again.'));
            }
        }
        $salesReturns = $this->SalesReturnDetails->SalesReturns->find('list', ['limit' => 200]);
        $products = $this->SalesReturnDetails->Products->find('list', ['limit' => 200]);
        $this->set(compact('salesReturnDetail', 'salesReturns', 'products'));
        $this->set('_serialize', ['salesReturnDetail']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sales Return Detail id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $salesReturnDetail = $this->SalesReturnDetails->get($id);
        if ($this->SalesReturnDetails->delete($salesReturnDetail)) {
            $this->Flash->success(__('The sales return detail has been deleted.'));
        } else {
            $this->Flash->error(__('The sales return detail could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
