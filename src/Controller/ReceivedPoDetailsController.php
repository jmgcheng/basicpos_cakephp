<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ReceivedPoDetails Controller
 *
 * @property \App\Model\Table\ReceivedPoDetailsTable $ReceivedPoDetails
 */
class ReceivedPoDetailsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['ReceivedPos', 'Products']
        ];
        $receivedPoDetails = $this->paginate($this->ReceivedPoDetails);

        $this->set(compact('receivedPoDetails'));
        $this->set('_serialize', ['receivedPoDetails']);
    }

    /**
     * View method
     *
     * @param string|null $id Received Po Detail id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $receivedPoDetail = $this->ReceivedPoDetails->get($id, [
            'contain' => ['ReceivedPos', 'Products']
        ]);

        $this->set('receivedPoDetail', $receivedPoDetail);
        $this->set('_serialize', ['receivedPoDetail']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $receivedPoDetail = $this->ReceivedPoDetails->newEntity();
        if ($this->request->is('post')) {
            $receivedPoDetail = $this->ReceivedPoDetails->patchEntity($receivedPoDetail, $this->request->data);
            if ($this->ReceivedPoDetails->save($receivedPoDetail)) {
                $this->Flash->success(__('The received po detail has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The received po detail could not be saved. Please, try again.'));
            }
        }
        $receivedPos = $this->ReceivedPoDetails->ReceivedPos->find('list', ['limit' => 200]);
        $products = $this->ReceivedPoDetails->Products->find('list', ['limit' => 200]);
        $this->set(compact('receivedPoDetail', 'receivedPos', 'products'));
        $this->set('_serialize', ['receivedPoDetail']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Received Po Detail id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $receivedPoDetail = $this->ReceivedPoDetails->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $receivedPoDetail = $this->ReceivedPoDetails->patchEntity($receivedPoDetail, $this->request->data);
            if ($this->ReceivedPoDetails->save($receivedPoDetail)) {
                $this->Flash->success(__('The received po detail has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The received po detail could not be saved. Please, try again.'));
            }
        }
        $receivedPos = $this->ReceivedPoDetails->ReceivedPos->find('list', ['limit' => 200]);
        $products = $this->ReceivedPoDetails->Products->find('list', ['limit' => 200]);
        $this->set(compact('receivedPoDetail', 'receivedPos', 'products'));
        $this->set('_serialize', ['receivedPoDetail']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Received Po Detail id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $receivedPoDetail = $this->ReceivedPoDetails->get($id);
        if ($this->ReceivedPoDetails->delete($receivedPoDetail)) {
            $this->Flash->success(__('The received po detail has been deleted.'));
        } else {
            $this->Flash->error(__('The received po detail could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
