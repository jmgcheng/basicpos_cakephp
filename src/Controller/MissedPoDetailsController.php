<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * MissedPoDetails Controller
 *
 * @property \App\Model\Table\MissedPoDetailsTable $MissedPoDetails
 */
class MissedPoDetailsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['MissedPos', 'Products']
        ];
        $missedPoDetails = $this->paginate($this->MissedPoDetails);

        $this->set(compact('missedPoDetails'));
        $this->set('_serialize', ['missedPoDetails']);
    }

    /**
     * View method
     *
     * @param string|null $id Missed Po Detail id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $missedPoDetail = $this->MissedPoDetails->get($id, [
            'contain' => ['MissedPos', 'Products']
        ]);

        $this->set('missedPoDetail', $missedPoDetail);
        $this->set('_serialize', ['missedPoDetail']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $missedPoDetail = $this->MissedPoDetails->newEntity();
        if ($this->request->is('post')) {
            $missedPoDetail = $this->MissedPoDetails->patchEntity($missedPoDetail, $this->request->data);
            if ($this->MissedPoDetails->save($missedPoDetail)) {
                $this->Flash->success(__('The missed po detail has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The missed po detail could not be saved. Please, try again.'));
            }
        }
        $missedPos = $this->MissedPoDetails->MissedPos->find('list', ['limit' => 200]);
        $products = $this->MissedPoDetails->Products->find('list', ['limit' => 200]);
        $this->set(compact('missedPoDetail', 'missedPos', 'products'));
        $this->set('_serialize', ['missedPoDetail']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Missed Po Detail id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $missedPoDetail = $this->MissedPoDetails->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $missedPoDetail = $this->MissedPoDetails->patchEntity($missedPoDetail, $this->request->data);
            if ($this->MissedPoDetails->save($missedPoDetail)) {
                $this->Flash->success(__('The missed po detail has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The missed po detail could not be saved. Please, try again.'));
            }
        }
        $missedPos = $this->MissedPoDetails->MissedPos->find('list', ['limit' => 200]);
        $products = $this->MissedPoDetails->Products->find('list', ['limit' => 200]);
        $this->set(compact('missedPoDetail', 'missedPos', 'products'));
        $this->set('_serialize', ['missedPoDetail']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Missed Po Detail id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $missedPoDetail = $this->MissedPoDetails->get($id);
        if ($this->MissedPoDetails->delete($missedPoDetail)) {
            $this->Flash->success(__('The missed po detail has been deleted.'));
        } else {
            $this->Flash->error(__('The missed po detail could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
