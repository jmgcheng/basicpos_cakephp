<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * OfficialReceiptStatus Controller
 *
 * @property \App\Model\Table\OfficialReceiptStatusTable $OfficialReceiptStatus
 */
class OfficialReceiptStatusController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $officialReceiptStatus = $this->paginate($this->OfficialReceiptStatus);

        $this->set(compact('officialReceiptStatus'));
        $this->set('_serialize', ['officialReceiptStatus']);
    }

    /**
     * View method
     *
     * @param string|null $id Official Receipt Status id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $officialReceiptStatus = $this->OfficialReceiptStatus->get($id, [
            'contain' => ['OfficialReceipts']
        ]);

        $this->set('officialReceiptStatus', $officialReceiptStatus);
        $this->set('_serialize', ['officialReceiptStatus']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $officialReceiptStatus = $this->OfficialReceiptStatus->newEntity();
        if ($this->request->is('post')) {
            $officialReceiptStatus = $this->OfficialReceiptStatus->patchEntity($officialReceiptStatus, $this->request->data);
            if ($this->OfficialReceiptStatus->save($officialReceiptStatus)) {
                $this->Flash->success(__('The official receipt status has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The official receipt status could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('officialReceiptStatus'));
        $this->set('_serialize', ['officialReceiptStatus']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Official Receipt Status id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $officialReceiptStatus = $this->OfficialReceiptStatus->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $officialReceiptStatus = $this->OfficialReceiptStatus->patchEntity($officialReceiptStatus, $this->request->data);
            if ($this->OfficialReceiptStatus->save($officialReceiptStatus)) {
                $this->Flash->success(__('The official receipt status has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The official receipt status could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('officialReceiptStatus'));
        $this->set('_serialize', ['officialReceiptStatus']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Official Receipt Status id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $officialReceiptStatus = $this->OfficialReceiptStatus->get($id);
        if ($this->OfficialReceiptStatus->delete($officialReceiptStatus)) {
            $this->Flash->success(__('The official receipt status has been deleted.'));
        } else {
            $this->Flash->error(__('The official receipt status could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
