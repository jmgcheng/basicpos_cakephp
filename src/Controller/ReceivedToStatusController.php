<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ReceivedToStatus Controller
 *
 * @property \App\Model\Table\ReceivedToStatusTable $ReceivedToStatus
 */
class ReceivedToStatusController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $receivedToStatus = $this->paginate($this->ReceivedToStatus);

        $this->set(compact('receivedToStatus'));
        $this->set('_serialize', ['receivedToStatus']);
    }

    /**
     * View method
     *
     * @param string|null $id Received To Status id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $receivedToStatus = $this->ReceivedToStatus->get($id, [
            'contain' => ['ReceivedTos']
        ]);

        $this->set('receivedToStatus', $receivedToStatus);
        $this->set('_serialize', ['receivedToStatus']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $receivedToStatus = $this->ReceivedToStatus->newEntity();
        if ($this->request->is('post')) {
            $receivedToStatus = $this->ReceivedToStatus->patchEntity($receivedToStatus, $this->request->data);
            if ($this->ReceivedToStatus->save($receivedToStatus)) {
                $this->Flash->success(__('The received to status has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The received to status could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('receivedToStatus'));
        $this->set('_serialize', ['receivedToStatus']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Received To Status id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $receivedToStatus = $this->ReceivedToStatus->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $receivedToStatus = $this->ReceivedToStatus->patchEntity($receivedToStatus, $this->request->data);
            if ($this->ReceivedToStatus->save($receivedToStatus)) {
                $this->Flash->success(__('The received to status has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The received to status could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('receivedToStatus'));
        $this->set('_serialize', ['receivedToStatus']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Received To Status id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $receivedToStatus = $this->ReceivedToStatus->get($id);
        if ($this->ReceivedToStatus->delete($receivedToStatus)) {
            $this->Flash->success(__('The received to status has been deleted.'));
        } else {
            $this->Flash->error(__('The received to status could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
