<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * IncludedInventoryStatus Controller
 *
 * @property \App\Model\Table\IncludedInventoryStatusTable $IncludedInventoryStatus
 */
class IncludedInventoryStatusController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $includedInventoryStatus = $this->paginate($this->IncludedInventoryStatus);

        $this->set(compact('includedInventoryStatus'));
        $this->set('_serialize', ['includedInventoryStatus']);
    }

    /**
     * View method
     *
     * @param string|null $id Included Inventory Status id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $includedInventoryStatus = $this->IncludedInventoryStatus->get($id, [
            'contain' => ['IncludedInventories']
        ]);

        $this->set('includedInventoryStatus', $includedInventoryStatus);
        $this->set('_serialize', ['includedInventoryStatus']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $includedInventoryStatus = $this->IncludedInventoryStatus->newEntity();
        if ($this->request->is('post')) {
            $includedInventoryStatus = $this->IncludedInventoryStatus->patchEntity($includedInventoryStatus, $this->request->data);
            if ($this->IncludedInventoryStatus->save($includedInventoryStatus)) {
                $this->Flash->success(__('The included inventory status has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The included inventory status could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('includedInventoryStatus'));
        $this->set('_serialize', ['includedInventoryStatus']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Included Inventory Status id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $includedInventoryStatus = $this->IncludedInventoryStatus->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $includedInventoryStatus = $this->IncludedInventoryStatus->patchEntity($includedInventoryStatus, $this->request->data);
            if ($this->IncludedInventoryStatus->save($includedInventoryStatus)) {
                $this->Flash->success(__('The included inventory status has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The included inventory status could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('includedInventoryStatus'));
        $this->set('_serialize', ['includedInventoryStatus']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Included Inventory Status id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $includedInventoryStatus = $this->IncludedInventoryStatus->get($id);
        if ($this->IncludedInventoryStatus->delete($includedInventoryStatus)) {
            $this->Flash->success(__('The included inventory status has been deleted.'));
        } else {
            $this->Flash->error(__('The included inventory status could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
