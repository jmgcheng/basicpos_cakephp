<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ReceivedPoStatus Controller
 *
 * @property \App\Model\Table\ReceivedPoStatusTable $ReceivedPoStatus
 */
class ReceivedPoStatusController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $receivedPoStatus = $this->paginate($this->ReceivedPoStatus);

        $this->set(compact('receivedPoStatus'));
        $this->set('_serialize', ['receivedPoStatus']);
    }

    /**
     * View method
     *
     * @param string|null $id Received Po Status id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $receivedPoStatus = $this->ReceivedPoStatus->get($id, [
            'contain' => ['ReceivedPos']
        ]);

        $this->set('receivedPoStatus', $receivedPoStatus);
        $this->set('_serialize', ['receivedPoStatus']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $receivedPoStatus = $this->ReceivedPoStatus->newEntity();
        if ($this->request->is('post')) {
            $receivedPoStatus = $this->ReceivedPoStatus->patchEntity($receivedPoStatus, $this->request->data);
            if ($this->ReceivedPoStatus->save($receivedPoStatus)) {
                $this->Flash->success(__('The received po status has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The received po status could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('receivedPoStatus'));
        $this->set('_serialize', ['receivedPoStatus']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Received Po Status id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $receivedPoStatus = $this->ReceivedPoStatus->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $receivedPoStatus = $this->ReceivedPoStatus->patchEntity($receivedPoStatus, $this->request->data);
            if ($this->ReceivedPoStatus->save($receivedPoStatus)) {
                $this->Flash->success(__('The received po status has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The received po status could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('receivedPoStatus'));
        $this->set('_serialize', ['receivedPoStatus']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Received Po Status id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $receivedPoStatus = $this->ReceivedPoStatus->get($id);
        if ($this->ReceivedPoStatus->delete($receivedPoStatus)) {
            $this->Flash->success(__('The received po status has been deleted.'));
        } else {
            $this->Flash->error(__('The received po status could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
