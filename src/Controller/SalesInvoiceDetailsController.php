<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SalesInvoiceDetails Controller
 *
 * @property \App\Model\Table\SalesInvoiceDetailsTable $SalesInvoiceDetails
 */
class SalesInvoiceDetailsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SalesInvoices', 'Products']
        ];
        $salesInvoiceDetails = $this->paginate($this->SalesInvoiceDetails);

        $this->set(compact('salesInvoiceDetails'));
        $this->set('_serialize', ['salesInvoiceDetails']);
    }

    /**
     * View method
     *
     * @param string|null $id Sales Invoice Detail id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $salesInvoiceDetail = $this->SalesInvoiceDetails->get($id, [
            'contain' => ['SalesInvoices', 'Products']
        ]);

        $this->set('salesInvoiceDetail', $salesInvoiceDetail);
        $this->set('_serialize', ['salesInvoiceDetail']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $salesInvoiceDetail = $this->SalesInvoiceDetails->newEntity();
        if ($this->request->is('post')) {
            $salesInvoiceDetail = $this->SalesInvoiceDetails->patchEntity($salesInvoiceDetail, $this->request->data);
            if ($this->SalesInvoiceDetails->save($salesInvoiceDetail)) {
                $this->Flash->success(__('The sales invoice detail has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sales invoice detail could not be saved. Please, try again.'));
            }
        }
        $salesInvoices = $this->SalesInvoiceDetails->SalesInvoices->find('list', ['limit' => 200]);
        $products = $this->SalesInvoiceDetails->Products->find('list', ['limit' => 200]);
        $this->set(compact('salesInvoiceDetail', 'salesInvoices', 'products'));
        $this->set('_serialize', ['salesInvoiceDetail']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Sales Invoice Detail id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $salesInvoiceDetail = $this->SalesInvoiceDetails->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $salesInvoiceDetail = $this->SalesInvoiceDetails->patchEntity($salesInvoiceDetail, $this->request->data);
            if ($this->SalesInvoiceDetails->save($salesInvoiceDetail)) {
                $this->Flash->success(__('The sales invoice detail has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The sales invoice detail could not be saved. Please, try again.'));
            }
        }
        $salesInvoices = $this->SalesInvoiceDetails->SalesInvoices->find('list', ['limit' => 200]);
        $products = $this->SalesInvoiceDetails->Products->find('list', ['limit' => 200]);
        $this->set(compact('salesInvoiceDetail', 'salesInvoices', 'products'));
        $this->set('_serialize', ['salesInvoiceDetail']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Sales Invoice Detail id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $salesInvoiceDetail = $this->SalesInvoiceDetails->get($id);
        if ($this->SalesInvoiceDetails->delete($salesInvoiceDetail)) {
            $this->Flash->success(__('The sales invoice detail has been deleted.'));
        } else {
            $this->Flash->error(__('The sales invoice detail could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
