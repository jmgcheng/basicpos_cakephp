<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * IncludedInventoryDetails Controller
 *
 * @property \App\Model\Table\IncludedInventoryDetailsTable $IncludedInventoryDetails
 */
class IncludedInventoryDetailsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['IncludedInventories', 'Products']
        ];
        $includedInventoryDetails = $this->paginate($this->IncludedInventoryDetails);

        $this->set(compact('includedInventoryDetails'));
        $this->set('_serialize', ['includedInventoryDetails']);
    }

    /**
     * View method
     *
     * @param string|null $id Included Inventory Detail id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $includedInventoryDetail = $this->IncludedInventoryDetails->get($id, [
            'contain' => ['IncludedInventories', 'Products']
        ]);

        $this->set('includedInventoryDetail', $includedInventoryDetail);
        $this->set('_serialize', ['includedInventoryDetail']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $includedInventoryDetail = $this->IncludedInventoryDetails->newEntity();
        if ($this->request->is('post')) {
            $includedInventoryDetail = $this->IncludedInventoryDetails->patchEntity($includedInventoryDetail, $this->request->data);
            if ($this->IncludedInventoryDetails->save($includedInventoryDetail)) {
                $this->Flash->success(__('The included inventory detail has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The included inventory detail could not be saved. Please, try again.'));
            }
        }
        $includedInventories = $this->IncludedInventoryDetails->IncludedInventories->find('list', ['limit' => 200]);
        $products = $this->IncludedInventoryDetails->Products->find('list', ['limit' => 200]);
        $this->set(compact('includedInventoryDetail', 'includedInventories', 'products'));
        $this->set('_serialize', ['includedInventoryDetail']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Included Inventory Detail id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $includedInventoryDetail = $this->IncludedInventoryDetails->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $includedInventoryDetail = $this->IncludedInventoryDetails->patchEntity($includedInventoryDetail, $this->request->data);
            if ($this->IncludedInventoryDetails->save($includedInventoryDetail)) {
                $this->Flash->success(__('The included inventory detail has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The included inventory detail could not be saved. Please, try again.'));
            }
        }
        $includedInventories = $this->IncludedInventoryDetails->IncludedInventories->find('list', ['limit' => 200]);
        $products = $this->IncludedInventoryDetails->Products->find('list', ['limit' => 200]);
        $this->set(compact('includedInventoryDetail', 'includedInventories', 'products'));
        $this->set('_serialize', ['includedInventoryDetail']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Included Inventory Detail id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $includedInventoryDetail = $this->IncludedInventoryDetails->get($id);
        if ($this->IncludedInventoryDetails->delete($includedInventoryDetail)) {
            $this->Flash->success(__('The included inventory detail has been deleted.'));
        } else {
            $this->Flash->error(__('The included inventory detail could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
