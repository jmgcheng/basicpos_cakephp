<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ProductDimensions Controller
 *
 * @property \App\Model\Table\ProductDimensionsTable $ProductDimensions
 */
class ProductDimensionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $productDimensions = $this->paginate($this->ProductDimensions);

        $this->set(compact('productDimensions'));
        $this->set('_serialize', ['productDimensions']);
    }

    /**
     * View method
     *
     * @param string|null $id Product Dimension id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $productDimension = $this->ProductDimensions->get($id, [
            'contain' => ['Products']
        ]);

        $this->set('productDimension', $productDimension);
        $this->set('_serialize', ['productDimension']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $productDimension = $this->ProductDimensions->newEntity();
        if ($this->request->is('post')) {
            $productDimension = $this->ProductDimensions->patchEntity($productDimension, $this->request->data);
            if ($this->ProductDimensions->save($productDimension)) {
                $this->Flash->success(__('The product dimension has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The product dimension could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('productDimension'));
        $this->set('_serialize', ['productDimension']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Product Dimension id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $productDimension = $this->ProductDimensions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $productDimension = $this->ProductDimensions->patchEntity($productDimension, $this->request->data);
            if ($this->ProductDimensions->save($productDimension)) {
                $this->Flash->success(__('The product dimension has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The product dimension could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('productDimension'));
        $this->set('_serialize', ['productDimension']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Product Dimension id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $productDimension = $this->ProductDimensions->get($id);
        if ($this->ProductDimensions->delete($productDimension)) {
            $this->Flash->success(__('The product dimension has been deleted.'));
        } else {
            $this->Flash->error(__('The product dimension could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
