<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PurchaseOrderDetails Controller
 *
 * @property \App\Model\Table\PurchaseOrderDetailsTable $PurchaseOrderDetails
 */
class PurchaseOrderDetailsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['PurchaseOrders', 'Products']
        ];
        $purchaseOrderDetails = $this->paginate($this->PurchaseOrderDetails);

        $this->set(compact('purchaseOrderDetails'));
        $this->set('_serialize', ['purchaseOrderDetails']);
    }

    /**
     * View method
     *
     * @param string|null $id Purchase Order Detail id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $purchaseOrderDetail = $this->PurchaseOrderDetails->get($id, [
            'contain' => ['PurchaseOrders', 'Products']
        ]);

        $this->set('purchaseOrderDetail', $purchaseOrderDetail);
        $this->set('_serialize', ['purchaseOrderDetail']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $purchaseOrderDetail = $this->PurchaseOrderDetails->newEntity();
        if ($this->request->is('post')) {
            $purchaseOrderDetail = $this->PurchaseOrderDetails->patchEntity($purchaseOrderDetail, $this->request->data);
            if ($this->PurchaseOrderDetails->save($purchaseOrderDetail)) {
                $this->Flash->success(__('The purchase order detail has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The purchase order detail could not be saved. Please, try again.'));
            }
        }
        $purchaseOrders = $this->PurchaseOrderDetails->PurchaseOrders->find('list', ['limit' => 200]);
        $products = $this->PurchaseOrderDetails->Products->find('list', ['limit' => 200]);
        $this->set(compact('purchaseOrderDetail', 'purchaseOrders', 'products'));
        $this->set('_serialize', ['purchaseOrderDetail']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Purchase Order Detail id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $purchaseOrderDetail = $this->PurchaseOrderDetails->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $purchaseOrderDetail = $this->PurchaseOrderDetails->patchEntity($purchaseOrderDetail, $this->request->data);
            if ($this->PurchaseOrderDetails->save($purchaseOrderDetail)) {
                $this->Flash->success(__('The purchase order detail has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The purchase order detail could not be saved. Please, try again.'));
            }
        }
        $purchaseOrders = $this->PurchaseOrderDetails->PurchaseOrders->find('list', ['limit' => 200]);
        $products = $this->PurchaseOrderDetails->Products->find('list', ['limit' => 200]);
        $this->set(compact('purchaseOrderDetail', 'purchaseOrders', 'products'));
        $this->set('_serialize', ['purchaseOrderDetail']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Purchase Order Detail id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $purchaseOrderDetail = $this->PurchaseOrderDetails->get($id);
        if ($this->PurchaseOrderDetails->delete($purchaseOrderDetail)) {
            $this->Flash->success(__('The purchase order detail has been deleted.'));
        } else {
            $this->Flash->error(__('The purchase order detail could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
