<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DeliveryReceiptDetails Controller
 *
 * @property \App\Model\Table\DeliveryReceiptDetailsTable $DeliveryReceiptDetails
 */
class DeliveryReceiptDetailsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['DeliveryReceipts', 'Products']
        ];
        $deliveryReceiptDetails = $this->paginate($this->DeliveryReceiptDetails);

        $this->set(compact('deliveryReceiptDetails'));
        $this->set('_serialize', ['deliveryReceiptDetails']);
    }

    /**
     * View method
     *
     * @param string|null $id Delivery Receipt Detail id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $deliveryReceiptDetail = $this->DeliveryReceiptDetails->get($id, [
            'contain' => ['DeliveryReceipts', 'Products']
        ]);

        $this->set('deliveryReceiptDetail', $deliveryReceiptDetail);
        $this->set('_serialize', ['deliveryReceiptDetail']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $deliveryReceiptDetail = $this->DeliveryReceiptDetails->newEntity();
        if ($this->request->is('post')) {
            $deliveryReceiptDetail = $this->DeliveryReceiptDetails->patchEntity($deliveryReceiptDetail, $this->request->data);
            if ($this->DeliveryReceiptDetails->save($deliveryReceiptDetail)) {
                $this->Flash->success(__('The delivery receipt detail has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The delivery receipt detail could not be saved. Please, try again.'));
            }
        }
        $deliveryReceipts = $this->DeliveryReceiptDetails->DeliveryReceipts->find('list', ['limit' => 200]);
        $products = $this->DeliveryReceiptDetails->Products->find('list', ['limit' => 200]);
        $this->set(compact('deliveryReceiptDetail', 'deliveryReceipts', 'products'));
        $this->set('_serialize', ['deliveryReceiptDetail']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Delivery Receipt Detail id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $deliveryReceiptDetail = $this->DeliveryReceiptDetails->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $deliveryReceiptDetail = $this->DeliveryReceiptDetails->patchEntity($deliveryReceiptDetail, $this->request->data);
            if ($this->DeliveryReceiptDetails->save($deliveryReceiptDetail)) {
                $this->Flash->success(__('The delivery receipt detail has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The delivery receipt detail could not be saved. Please, try again.'));
            }
        }
        $deliveryReceipts = $this->DeliveryReceiptDetails->DeliveryReceipts->find('list', ['limit' => 200]);
        $products = $this->DeliveryReceiptDetails->Products->find('list', ['limit' => 200]);
        $this->set(compact('deliveryReceiptDetail', 'deliveryReceipts', 'products'));
        $this->set('_serialize', ['deliveryReceiptDetail']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Delivery Receipt Detail id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $deliveryReceiptDetail = $this->DeliveryReceiptDetails->get($id);
        if ($this->DeliveryReceiptDetails->delete($deliveryReceiptDetail)) {
            $this->Flash->success(__('The delivery receipt detail has been deleted.'));
        } else {
            $this->Flash->error(__('The delivery receipt detail could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
