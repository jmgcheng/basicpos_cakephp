<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * WrongSendPos Controller
 *
 * @property \App\Model\Table\WrongSendPosTable $WrongSendPos
 */
class WrongSendPosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['PurchaseOrders', 'Depots', 'Users', 'WrongSendPoStatus']
        ];
        $wrongSendPos = $this->paginate($this->WrongSendPos);

        $this->set(compact('wrongSendPos'));
        $this->set('_serialize', ['wrongSendPos']);
    }

    /**
     * View method
     *
     * @param string|null $id Wrong Send Po id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $wrongSendPo = $this->WrongSendPos->get($id, [
            'contain' => ['PurchaseOrders', 'Depots', 'Users', 'WrongSendPoStatus', 'WrongSendPoDetails']
        ]);

        $this->set('wrongSendPo', $wrongSendPo);
        $this->set('_serialize', ['wrongSendPo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $wrongSendPo = $this->WrongSendPos->newEntity();
        if ($this->request->is('post')) {

           $a_details = array();
            if(     isset($this->request->data['product_id']) && !empty($this->request->data['product_id']) 
                &&  isset($this->request->data['quantity_wrong_send']) && !empty($this->request->data['quantity_wrong_send']) 
                &&  isset($this->request->data['product_serial'])
            )
            {
                $a_product_id = $this->request->data['product_id'];
                $a_qty = $this->request->data['quantity_wrong_send'];
                $a_product_serial = $this->request->data['product_serial'];
                foreach ($a_product_id as $key => $i_product_id) {
                    $a_temp = array();
                    $a_temp['product_id'] = $a_product_id[$key];
                    $a_temp['quantity_wrong_send'] = $a_qty[$key];
                    $a_temp['product_serial'] = $a_product_serial[$key];
                    array_push($a_details, $a_temp);
                }
            }
            unset($this->request->data['product_id']);
            unset($this->request->data['quantity_wrong_send']);
            unset($this->request->data['product_serial']);
            $this->request->data['wrong_send_po_details'] = $a_details;            

            $wrongSendPo = $this->WrongSendPos->patchEntity($wrongSendPo, $this->request->data);
            if ($this->WrongSendPos->save($wrongSendPo)) {
                $this->Flash->success(__('The wrong send po has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The wrong send po could not be saved. Please, try again.'));
            }
        }
        $purchaseOrders = $this->WrongSendPos->PurchaseOrders->find('list', ['limit' => 200]);
        $depots = $this->WrongSendPos->Depots->find('list', ['limit' => 200]);
        $users = $this->WrongSendPos->Users->find('list', ['limit' => 200]);
        $wrongSendPoStatus = $this->WrongSendPos->WrongSendPoStatus->find('list', ['limit' => 200]);
        $Products = TableRegistry::get('Products');
        $product_list = $Products->find()
                    ->contain(['ProductColors', 'ProductDimensions', 'ProductUnits']);        
        $this->set(compact('wrongSendPo', 'purchaseOrders', 'depots', 'users', 'wrongSendPoStatus', 'product_list'));
        $this->set('_serialize', ['wrongSendPo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Wrong Send Po id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $WrongSendPoDetails = TableRegistry::get('WrongSendPoDetails');
        $wrongSendPo = $this->WrongSendPos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $WrongSendPoDetails->deleteAll(['wrong_send_po_id' => $id]);
            $a_details = array();
            if(     isset($this->request->data['product_id']) && !empty($this->request->data['product_id']) 
                &&  isset($this->request->data['quantity_wrong_send']) && !empty($this->request->data['quantity_wrong_send']) 
                &&  isset($this->request->data['product_serial'])
            )
            {
                $a_product_id = $this->request->data['product_id'];
                $a_qty = $this->request->data['quantity_wrong_send'];
                $a_product_serial = $this->request->data['product_serial'];
                foreach ($a_product_id as $key => $i_product_id) {
                    $a_temp = array();
                    $a_temp['product_id'] = $a_product_id[$key];
                    $a_temp['quantity_wrong_send'] = $a_qty[$key];
                    $a_temp['product_serial'] = $a_product_serial[$key];
                    array_push($a_details, $a_temp);
                }
                $this->request->data['wrong_send_po_details'] = $a_details;
            }
            unset($this->request->data['product_id']);
            unset($this->request->data['quantity_wrong_send']);
            unset($this->request->data['product_serial']);

            $wrongSendPo = $this->WrongSendPos->patchEntity($wrongSendPo, $this->request->data);
            if ($this->WrongSendPos->save($wrongSendPo)) {
                $this->Flash->success(__('The wrong send po has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The wrong send po could not be saved. Please, try again.'));
            }
        }
        $purchaseOrders = $this->WrongSendPos->PurchaseOrders->find('list', ['limit' => 200]);
        $depots = $this->WrongSendPos->Depots->find('list', ['limit' => 200]);
        $users = $this->WrongSendPos->Users->find('list', ['limit' => 200]);
        $wrongSendPoStatus = $this->WrongSendPos->WrongSendPoStatus->find('list', ['limit' => 200]);
        $transac_detail_list = $WrongSendPoDetails->find()
                                        ->where(['wrong_send_po_id' => $id]);
        $Products = TableRegistry::get('Products');
        $product_list = $Products->find()
                    ->contain(['ProductColors', 'ProductDimensions', 'ProductUnits']);
        $this->set(compact('wrongSendPo', 'purchaseOrders', 'depots', 'users', 'wrongSendPoStatus', 'product_list', 'transac_detail_list'));
        $this->set('_serialize', ['wrongSendPo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Wrong Send Po id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $wrongSendPo = $this->WrongSendPos->get($id);
        if ($this->WrongSendPos->delete($wrongSendPo)) {
            $this->Flash->success(__('The wrong send po has been deleted.'));
        } else {
            $this->Flash->error(__('The wrong send po could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
