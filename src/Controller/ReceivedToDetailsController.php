<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ReceivedToDetails Controller
 *
 * @property \App\Model\Table\ReceivedToDetailsTable $ReceivedToDetails
 */
class ReceivedToDetailsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['ReceivedTos', 'Products']
        ];
        $receivedToDetails = $this->paginate($this->ReceivedToDetails);

        $this->set(compact('receivedToDetails'));
        $this->set('_serialize', ['receivedToDetails']);
    }

    /**
     * View method
     *
     * @param string|null $id Received To Detail id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $receivedToDetail = $this->ReceivedToDetails->get($id, [
            'contain' => ['ReceivedTos', 'Products']
        ]);

        $this->set('receivedToDetail', $receivedToDetail);
        $this->set('_serialize', ['receivedToDetail']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $receivedToDetail = $this->ReceivedToDetails->newEntity();
        if ($this->request->is('post')) {
            $receivedToDetail = $this->ReceivedToDetails->patchEntity($receivedToDetail, $this->request->data);
            if ($this->ReceivedToDetails->save($receivedToDetail)) {
                $this->Flash->success(__('The received to detail has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The received to detail could not be saved. Please, try again.'));
            }
        }
        $receivedTos = $this->ReceivedToDetails->ReceivedTos->find('list', ['limit' => 200]);
        $products = $this->ReceivedToDetails->Products->find('list', ['limit' => 200]);
        $this->set(compact('receivedToDetail', 'receivedTos', 'products'));
        $this->set('_serialize', ['receivedToDetail']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Received To Detail id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $receivedToDetail = $this->ReceivedToDetails->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $receivedToDetail = $this->ReceivedToDetails->patchEntity($receivedToDetail, $this->request->data);
            if ($this->ReceivedToDetails->save($receivedToDetail)) {
                $this->Flash->success(__('The received to detail has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The received to detail could not be saved. Please, try again.'));
            }
        }
        $receivedTos = $this->ReceivedToDetails->ReceivedTos->find('list', ['limit' => 200]);
        $products = $this->ReceivedToDetails->Products->find('list', ['limit' => 200]);
        $this->set(compact('receivedToDetail', 'receivedTos', 'products'));
        $this->set('_serialize', ['receivedToDetail']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Received To Detail id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $receivedToDetail = $this->ReceivedToDetails->get($id);
        if ($this->ReceivedToDetails->delete($receivedToDetail)) {
            $this->Flash->success(__('The received to detail has been deleted.'));
        } else {
            $this->Flash->error(__('The received to detail could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
