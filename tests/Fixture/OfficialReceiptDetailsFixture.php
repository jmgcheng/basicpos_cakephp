<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * OfficialReceiptDetailsFixture
 *
 */
class OfficialReceiptDetailsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'official_receipt_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'product_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'quantity_released' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'product_serial' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => '', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_indexes' => [
            'product_key' => ['type' => 'index', 'columns' => ['product_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['official_receipt_id', 'product_id', 'product_serial'], 'length' => []],
            'official_receipt_detail_key' => ['type' => 'unique', 'columns' => ['official_receipt_id', 'product_id', 'product_serial'], 'length' => []],
            'official_receipt_details_ibfk_1' => ['type' => 'foreign', 'columns' => ['official_receipt_id'], 'references' => ['official_receipts', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'official_receipt_details_ibfk_2' => ['type' => 'foreign', 'columns' => ['product_id'], 'references' => ['products', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'official_receipt_id' => 1,
            'product_id' => 1,
            'quantity_released' => 1,
            'product_serial' => '5d1036bd-f63a-4440-8180-f58f9469593c'
        ],
    ];
}
