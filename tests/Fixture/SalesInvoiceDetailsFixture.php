<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SalesInvoiceDetailsFixture
 *
 */
class SalesInvoiceDetailsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'sales_invoice_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'product_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'quantity_to_release' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'product_key' => ['type' => 'index', 'columns' => ['product_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['sales_invoice_id', 'product_id'], 'length' => []],
            'sales_invoice_detail_key' => ['type' => 'unique', 'columns' => ['sales_invoice_id', 'product_id'], 'length' => []],
            'sales_invoice_details_ibfk_1' => ['type' => 'foreign', 'columns' => ['sales_invoice_id'], 'references' => ['sales_invoices', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'sales_invoice_details_ibfk_2' => ['type' => 'foreign', 'columns' => ['product_id'], 'references' => ['products', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'sales_invoice_id' => 1,
            'product_id' => 1,
            'quantity_to_release' => 1
        ],
    ];
}
