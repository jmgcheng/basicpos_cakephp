<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TransferOrdersFixture
 *
 */
class TransferOrdersFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'releasing_depot_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'receiving_depot_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'prepared_by_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'approved_by_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'date_registration' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'comment' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'transfer_order_status_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'releasing_depot_key' => ['type' => 'index', 'columns' => ['releasing_depot_id'], 'length' => []],
            'receiving_depot_key' => ['type' => 'index', 'columns' => ['receiving_depot_id'], 'length' => []],
            'prepared_by_key' => ['type' => 'index', 'columns' => ['prepared_by_id'], 'length' => []],
            'approved_by_key' => ['type' => 'index', 'columns' => ['approved_by_id'], 'length' => []],
            'transfer_order_status_key' => ['type' => 'index', 'columns' => ['transfer_order_status_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'transfer_orders_ibfk_1' => ['type' => 'foreign', 'columns' => ['releasing_depot_id'], 'references' => ['depots', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'transfer_orders_ibfk_2' => ['type' => 'foreign', 'columns' => ['receiving_depot_id'], 'references' => ['depots', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'transfer_orders_ibfk_3' => ['type' => 'foreign', 'columns' => ['prepared_by_id'], 'references' => ['users', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'transfer_orders_ibfk_4' => ['type' => 'foreign', 'columns' => ['approved_by_id'], 'references' => ['users', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'transfer_orders_ibfk_5' => ['type' => 'foreign', 'columns' => ['transfer_order_status_id'], 'references' => ['transfer_order_status', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'releasing_depot_id' => 1,
            'receiving_depot_id' => 1,
            'prepared_by_id' => 1,
            'approved_by_id' => 1,
            'date_registration' => '2016-05-19 14:20:40',
            'comment' => 'Lorem ipsum dolor sit amet',
            'transfer_order_status_id' => 1
        ],
    ];
}
