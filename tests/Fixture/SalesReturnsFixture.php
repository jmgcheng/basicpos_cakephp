<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SalesReturnsFixture
 *
 */
class SalesReturnsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'official_receipt_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'depot_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'prepared_by_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'approved_by_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'date_registration' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'comment' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'sales_return_status_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'official_receipt_key' => ['type' => 'index', 'columns' => ['official_receipt_id'], 'length' => []],
            'depot_key' => ['type' => 'index', 'columns' => ['depot_id'], 'length' => []],
            'prepared_by_key' => ['type' => 'index', 'columns' => ['prepared_by_id'], 'length' => []],
            'approved_by_key' => ['type' => 'index', 'columns' => ['approved_by_id'], 'length' => []],
            'sales_return_status_key' => ['type' => 'index', 'columns' => ['sales_return_status_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'sales_returns_ibfk_1' => ['type' => 'foreign', 'columns' => ['official_receipt_id'], 'references' => ['official_receipts', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'sales_returns_ibfk_2' => ['type' => 'foreign', 'columns' => ['depot_id'], 'references' => ['depots', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'sales_returns_ibfk_3' => ['type' => 'foreign', 'columns' => ['prepared_by_id'], 'references' => ['users', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'sales_returns_ibfk_4' => ['type' => 'foreign', 'columns' => ['approved_by_id'], 'references' => ['users', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'sales_returns_ibfk_5' => ['type' => 'foreign', 'columns' => ['sales_return_status_id'], 'references' => ['sales_return_status', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'official_receipt_id' => 1,
            'depot_id' => 1,
            'prepared_by_id' => 1,
            'approved_by_id' => 1,
            'date_registration' => '2016-05-24 19:51:01',
            'comment' => 'Lorem ipsum dolor sit amet',
            'sales_return_status_id' => 1
        ],
    ];
}
