<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * WrongSendPoDetailsFixture
 *
 */
class WrongSendPoDetailsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'wrong_send_po_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'product_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'quantity_wrong_send' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'product_serial' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => '', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_indexes' => [
            'product_key' => ['type' => 'index', 'columns' => ['product_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['wrong_send_po_id', 'product_id', 'product_serial'], 'length' => []],
            'wrong_send_po_detail_key' => ['type' => 'unique', 'columns' => ['wrong_send_po_id', 'product_id', 'product_serial'], 'length' => []],
            'wrong_send_po_details_ibfk_1' => ['type' => 'foreign', 'columns' => ['wrong_send_po_id'], 'references' => ['wrong_send_pos', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'wrong_send_po_details_ibfk_2' => ['type' => 'foreign', 'columns' => ['product_id'], 'references' => ['products', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'wrong_send_po_id' => 1,
            'product_id' => 1,
            'quantity_wrong_send' => 1,
            'product_serial' => '3c1b429d-92f4-4ae6-bc22-bd035d8b1935'
        ],
    ];
}
