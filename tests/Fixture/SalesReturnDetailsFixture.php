<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SalesReturnDetailsFixture
 *
 */
class SalesReturnDetailsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'sales_return_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'product_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'quantity_received' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'product_serial' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => '', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_indexes' => [
            'product_key' => ['type' => 'index', 'columns' => ['product_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['sales_return_id', 'product_id', 'product_serial'], 'length' => []],
            'sales_return_detail_key' => ['type' => 'unique', 'columns' => ['sales_return_id', 'product_id', 'product_serial'], 'length' => []],
            'sales_return_details_ibfk_1' => ['type' => 'foreign', 'columns' => ['sales_return_id'], 'references' => ['sales_returns', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'sales_return_details_ibfk_2' => ['type' => 'foreign', 'columns' => ['product_id'], 'references' => ['products', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'sales_return_id' => 1,
            'product_id' => 1,
            'quantity_received' => 1,
            'product_serial' => '77357b88-08b6-45e5-8439-5d735ae0dbdd'
        ],
    ];
}
