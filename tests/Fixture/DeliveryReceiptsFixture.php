<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DeliveryReceiptsFixture
 *
 */
class DeliveryReceiptsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'sales_invoice_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'prepared_by_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'approved_by_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'date_registration' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'title' => ['type' => 'string', 'length' => 40, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'comment' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'delivery_receipt_status_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'sales_invoice_key' => ['type' => 'index', 'columns' => ['sales_invoice_id'], 'length' => []],
            'prepared_by_key' => ['type' => 'index', 'columns' => ['prepared_by_id'], 'length' => []],
            'approved_by_key' => ['type' => 'index', 'columns' => ['approved_by_id'], 'length' => []],
            'delivery_receipt_status_key' => ['type' => 'index', 'columns' => ['delivery_receipt_status_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'delivery_receipts_ibfk_1' => ['type' => 'foreign', 'columns' => ['sales_invoice_id'], 'references' => ['sales_invoices', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'delivery_receipts_ibfk_2' => ['type' => 'foreign', 'columns' => ['prepared_by_id'], 'references' => ['users', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'delivery_receipts_ibfk_3' => ['type' => 'foreign', 'columns' => ['approved_by_id'], 'references' => ['users', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'delivery_receipts_ibfk_4' => ['type' => 'foreign', 'columns' => ['delivery_receipt_status_id'], 'references' => ['delivery_receipt_status', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'sales_invoice_id' => 1,
            'prepared_by_id' => 1,
            'approved_by_id' => 1,
            'date_registration' => '2016-05-19 14:21:13',
            'title' => 'Lorem ipsum dolor sit amet',
            'comment' => 'Lorem ipsum dolor sit amet',
            'delivery_receipt_status_id' => 1
        ],
    ];
}
