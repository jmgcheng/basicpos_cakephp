<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ExcludedInventoryDetailsFixture
 *
 */
class ExcludedInventoryDetailsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'excluded_inventory_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'product_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'quantity_excluded' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'product_serial' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => '', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_indexes' => [
            'product_key' => ['type' => 'index', 'columns' => ['product_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['excluded_inventory_id', 'product_id', 'product_serial'], 'length' => []],
            'excluded_inventory_key' => ['type' => 'unique', 'columns' => ['excluded_inventory_id', 'product_id', 'product_serial'], 'length' => []],
            'excluded_inventory_details_ibfk_1' => ['type' => 'foreign', 'columns' => ['excluded_inventory_id'], 'references' => ['excluded_inventories', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'excluded_inventory_details_ibfk_2' => ['type' => 'foreign', 'columns' => ['product_id'], 'references' => ['products', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'excluded_inventory_id' => 1,
            'product_id' => 1,
            'quantity_excluded' => 1,
            'product_serial' => '48de0ce0-4b76-4d42-9840-81d817561a9a'
        ],
    ];
}
