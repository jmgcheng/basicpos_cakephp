<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DeliveryReceiptDetailsFixture
 *
 */
class DeliveryReceiptDetailsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'delivery_receipt_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'product_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'quantity_delivering' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'product_serial' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => '', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_indexes' => [
            'product_key' => ['type' => 'index', 'columns' => ['product_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['delivery_receipt_id', 'product_id', 'product_serial'], 'length' => []],
            'delivery_receipt_detail_key' => ['type' => 'unique', 'columns' => ['delivery_receipt_id', 'product_id', 'product_serial'], 'length' => []],
            'delivery_receipt_details_ibfk_1' => ['type' => 'foreign', 'columns' => ['delivery_receipt_id'], 'references' => ['delivery_receipts', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'delivery_receipt_details_ibfk_2' => ['type' => 'foreign', 'columns' => ['product_id'], 'references' => ['products', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'delivery_receipt_id' => 1,
            'product_id' => 1,
            'quantity_delivering' => 1,
            'product_serial' => '69f3abca-19b3-4aef-8423-d565da136c1c'
        ],
    ];
}
