<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * IncludedInventoryDetailsFixture
 *
 */
class IncludedInventoryDetailsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'included_inventory_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'product_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'quantity_included' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'product_serial' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => '', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_indexes' => [
            'product_key' => ['type' => 'index', 'columns' => ['product_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['included_inventory_id', 'product_id', 'product_serial'], 'length' => []],
            'included_inventory_key' => ['type' => 'unique', 'columns' => ['included_inventory_id', 'product_id', 'product_serial'], 'length' => []],
            'included_inventory_details_ibfk_1' => ['type' => 'foreign', 'columns' => ['included_inventory_id'], 'references' => ['included_inventories', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'included_inventory_details_ibfk_2' => ['type' => 'foreign', 'columns' => ['product_id'], 'references' => ['products', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'included_inventory_id' => 1,
            'product_id' => 1,
            'quantity_included' => 1,
            'product_serial' => 'd712c578-d81d-4bf6-9e69-484f5b990855'
        ],
    ];
}
