<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TransferOrderDetailsFixture
 *
 */
class TransferOrderDetailsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'transfer_order_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'product_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'quantity_transfering' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'product_serial' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => '', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_indexes' => [
            'product_key' => ['type' => 'index', 'columns' => ['product_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['transfer_order_id', 'product_id', 'product_serial'], 'length' => []],
            'transfer_order_key' => ['type' => 'unique', 'columns' => ['transfer_order_id', 'product_id', 'product_serial'], 'length' => []],
            'transfer_order_details_ibfk_1' => ['type' => 'foreign', 'columns' => ['transfer_order_id'], 'references' => ['transfer_orders', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'transfer_order_details_ibfk_2' => ['type' => 'foreign', 'columns' => ['product_id'], 'references' => ['products', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'transfer_order_id' => 1,
            'product_id' => 1,
            'quantity_transfering' => 1,
            'product_serial' => '01d4abf6-4448-4c27-a80d-85e4127e4fe2'
        ],
    ];
}
