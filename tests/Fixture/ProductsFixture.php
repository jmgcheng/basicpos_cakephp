<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ProductsFixture
 *
 */
class ProductsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'name' => ['type' => 'string', 'length' => 55, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'model' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'description' => ['type' => 'string', 'length' => 55, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'qty_low_alert' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'product_status_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'product_color_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'product_dimension_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'product_unit_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'product_status_key' => ['type' => 'index', 'columns' => ['product_status_id'], 'length' => []],
            'product_color_key' => ['type' => 'index', 'columns' => ['product_color_id'], 'length' => []],
            'product_dimension_key' => ['type' => 'index', 'columns' => ['product_dimension_id'], 'length' => []],
            'product_unit_key' => ['type' => 'index', 'columns' => ['product_unit_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'product_unique_key' => ['type' => 'unique', 'columns' => ['name', 'model', 'product_color_id', 'product_dimension_id', 'product_unit_id'], 'length' => []],
            'products_ibfk_1' => ['type' => 'foreign', 'columns' => ['product_status_id'], 'references' => ['product_status', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'products_ibfk_2' => ['type' => 'foreign', 'columns' => ['product_color_id'], 'references' => ['product_colors', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'products_ibfk_3' => ['type' => 'foreign', 'columns' => ['product_dimension_id'], 'references' => ['product_dimensions', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'products_ibfk_4' => ['type' => 'foreign', 'columns' => ['product_unit_id'], 'references' => ['product_units', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'created' => '2016-05-19 14:19:18',
            'name' => 'Lorem ipsum dolor sit amet',
            'model' => 'Lorem ipsum dolor ',
            'description' => 'Lorem ipsum dolor sit amet',
            'qty_low_alert' => 1,
            'product_status_id' => 1,
            'product_color_id' => 1,
            'product_dimension_id' => 1,
            'product_unit_id' => 1
        ],
    ];
}
