<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WrongSendPosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WrongSendPosTable Test Case
 */
class WrongSendPosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\WrongSendPosTable
     */
    public $WrongSendPos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.wrong_send_pos',
        'app.purchase_orders',
        'app.depots',
        'app.depot_types',
        'app.excluded_inventories',
        'app.included_inventories',
        'app.missed_pos',
        'app.users',
        'app.status_users',
        'app.roles',
        'app.roles_users',
        'app.missed_po_status',
        'app.missed_po_details',
        'app.products',
        'app.product_status',
        'app.product_colors',
        'app.product_dimensions',
        'app.product_units',
        'app.delivery_receipt_details',
        'app.excluded_inventory_details',
        'app.included_inventory_details',
        'app.official_receipt_details',
        'app.purchase_order_details',
        'app.received_po_details',
        'app.received_pos',
        'app.received_po_status',
        'app.received_to_details',
        'app.sales_invoice_details',
        'app.transfer_order_details',
        'app.wrong_send_po_details',
        'app.official_receipts',
        'app.received_tos',
        'app.sales_invoices',
        'app.purchase_order_status',
        'app.wrong_send_po_status'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('WrongSendPos') ? [] : ['className' => 'App\Model\Table\WrongSendPosTable'];
        $this->WrongSendPos = TableRegistry::get('WrongSendPos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WrongSendPos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
