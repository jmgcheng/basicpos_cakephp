<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MissedPoStatusTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MissedPoStatusTable Test Case
 */
class MissedPoStatusTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MissedPoStatusTable
     */
    public $MissedPoStatus;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.missed_po_status',
        'app.missed_pos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('MissedPoStatus') ? [] : ['className' => 'App\Model\Table\MissedPoStatusTable'];
        $this->MissedPoStatus = TableRegistry::get('MissedPoStatus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MissedPoStatus);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
