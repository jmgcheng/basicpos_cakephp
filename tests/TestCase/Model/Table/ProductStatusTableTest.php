<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProductStatusTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProductStatusTable Test Case
 */
class ProductStatusTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProductStatusTable
     */
    public $ProductStatus;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.product_status',
        'app.products'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ProductStatus') ? [] : ['className' => 'App\Model\Table\ProductStatusTable'];
        $this->ProductStatus = TableRegistry::get('ProductStatus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProductStatus);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
