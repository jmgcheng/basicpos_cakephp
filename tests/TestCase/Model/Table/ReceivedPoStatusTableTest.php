<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ReceivedPoStatusTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ReceivedPoStatusTable Test Case
 */
class ReceivedPoStatusTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ReceivedPoStatusTable
     */
    public $ReceivedPoStatus;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.received_po_status',
        'app.received_pos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ReceivedPoStatus') ? [] : ['className' => 'App\Model\Table\ReceivedPoStatusTable'];
        $this->ReceivedPoStatus = TableRegistry::get('ReceivedPoStatus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ReceivedPoStatus);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
