<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OfficialReceiptStatusTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OfficialReceiptStatusTable Test Case
 */
class OfficialReceiptStatusTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OfficialReceiptStatusTable
     */
    public $OfficialReceiptStatus;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.official_receipt_status',
        'app.official_receipts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('OfficialReceiptStatus') ? [] : ['className' => 'App\Model\Table\OfficialReceiptStatusTable'];
        $this->OfficialReceiptStatus = TableRegistry::get('OfficialReceiptStatus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OfficialReceiptStatus);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
