<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SalesReturnStatusTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SalesReturnStatusTable Test Case
 */
class SalesReturnStatusTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SalesReturnStatusTable
     */
    public $SalesReturnStatus;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sales_return_status',
        'app.sales_returns'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SalesReturnStatus') ? [] : ['className' => 'App\Model\Table\SalesReturnStatusTable'];
        $this->SalesReturnStatus = TableRegistry::get('SalesReturnStatus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SalesReturnStatus);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
