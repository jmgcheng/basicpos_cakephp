<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WrongSendPoStatusTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WrongSendPoStatusTable Test Case
 */
class WrongSendPoStatusTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\WrongSendPoStatusTable
     */
    public $WrongSendPoStatus;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.wrong_send_po_status',
        'app.wrong_send_pos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('WrongSendPoStatus') ? [] : ['className' => 'App\Model\Table\WrongSendPoStatusTable'];
        $this->WrongSendPoStatus = TableRegistry::get('WrongSendPoStatus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WrongSendPoStatus);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
