<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SalesReturnsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SalesReturnsTable Test Case
 */
class SalesReturnsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SalesReturnsTable
     */
    public $SalesReturns;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sales_returns',
        'app.official_receipts',
        'app.sales_invoices',
        'app.depots',
        'app.depot_types',
        'app.excluded_inventories',
        'app.users',
        'app.status_users',
        'app.roles',
        'app.roles_users',
        'app.excluded_inventory_status',
        'app.excluded_inventory_details',
        'app.products',
        'app.product_status',
        'app.product_colors',
        'app.product_dimensions',
        'app.product_units',
        'app.delivery_receipt_details',
        'app.delivery_receipts',
        'app.delivery_receipt_status',
        'app.included_inventory_details',
        'app.included_inventories',
        'app.included_inventory_status',
        'app.missed_po_details',
        'app.missed_pos',
        'app.purchase_orders',
        'app.purchase_order_status',
        'app.purchase_order_details',
        'app.received_pos',
        'app.received_po_status',
        'app.received_po_details',
        'app.wrong_send_pos',
        'app.wrong_send_po_status',
        'app.wrong_send_po_details',
        'app.missed_po_status',
        'app.official_receipt_details',
        'app.received_to_details',
        'app.received_tos',
        'app.transfer_orders',
        'app.transfer_order_status',
        'app.transfer_order_details',
        'app.received_to_status',
        'app.sales_invoice_details',
        'app.sales_invoice_status',
        'app.official_receipt_status',
        'app.sales_return_status',
        'app.sales_return_details'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SalesReturns') ? [] : ['className' => 'App\Model\Table\SalesReturnsTable'];
        $this->SalesReturns = TableRegistry::get('SalesReturns', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SalesReturns);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
