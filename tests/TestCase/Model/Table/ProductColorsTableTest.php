<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProductColorsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProductColorsTable Test Case
 */
class ProductColorsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProductColorsTable
     */
    public $ProductColors;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.product_colors',
        'app.products'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ProductColors') ? [] : ['className' => 'App\Model\Table\ProductColorsTable'];
        $this->ProductColors = TableRegistry::get('ProductColors', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProductColors);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
