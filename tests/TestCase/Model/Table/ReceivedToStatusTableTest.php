<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ReceivedToStatusTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ReceivedToStatusTable Test Case
 */
class ReceivedToStatusTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ReceivedToStatusTable
     */
    public $ReceivedToStatus;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.received_to_status',
        'app.received_tos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ReceivedToStatus') ? [] : ['className' => 'App\Model\Table\ReceivedToStatusTable'];
        $this->ReceivedToStatus = TableRegistry::get('ReceivedToStatus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ReceivedToStatus);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
