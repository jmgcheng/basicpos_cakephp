<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ExcludedInventoryStatusTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ExcludedInventoryStatusTable Test Case
 */
class ExcludedInventoryStatusTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ExcludedInventoryStatusTable
     */
    public $ExcludedInventoryStatus;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.excluded_inventory_status',
        'app.excluded_inventories'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ExcludedInventoryStatus') ? [] : ['className' => 'App\Model\Table\ExcludedInventoryStatusTable'];
        $this->ExcludedInventoryStatus = TableRegistry::get('ExcludedInventoryStatus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ExcludedInventoryStatus);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
