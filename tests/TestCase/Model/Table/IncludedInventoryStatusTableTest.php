<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\IncludedInventoryStatusTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\IncludedInventoryStatusTable Test Case
 */
class IncludedInventoryStatusTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\IncludedInventoryStatusTable
     */
    public $IncludedInventoryStatus;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.included_inventory_status',
        'app.included_inventories'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('IncludedInventoryStatus') ? [] : ['className' => 'App\Model\Table\IncludedInventoryStatusTable'];
        $this->IncludedInventoryStatus = TableRegistry::get('IncludedInventoryStatus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->IncludedInventoryStatus);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
