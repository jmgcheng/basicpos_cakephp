<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\IncludedInventoryDetailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\IncludedInventoryDetailsTable Test Case
 */
class IncludedInventoryDetailsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\IncludedInventoryDetailsTable
     */
    public $IncludedInventoryDetails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.included_inventory_details',
        'app.included_inventories',
        'app.depots',
        'app.depot_types',
        'app.excluded_inventories',
        'app.missed_pos',
        'app.purchase_orders',
        'app.users',
        'app.status_users',
        'app.roles',
        'app.roles_users',
        'app.purchase_order_status',
        'app.purchase_order_details',
        'app.products',
        'app.product_status',
        'app.product_colors',
        'app.product_dimensions',
        'app.product_units',
        'app.delivery_receipt_details',
        'app.excluded_inventory_details',
        'app.missed_po_details',
        'app.official_receipt_details',
        'app.official_receipts',
        'app.sales_invoices',
        'app.sales_invoice_status',
        'app.delivery_receipts',
        'app.sales_invoice_details',
        'app.official_receipt_status',
        'app.received_po_details',
        'app.received_pos',
        'app.received_po_status',
        'app.received_to_details',
        'app.transfer_order_details',
        'app.wrong_send_po_details',
        'app.wrong_send_pos',
        'app.wrong_send_po_status',
        'app.missed_po_status',
        'app.received_tos',
        'app.included_inventory_status'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('IncludedInventoryDetails') ? [] : ['className' => 'App\Model\Table\IncludedInventoryDetailsTable'];
        $this->IncludedInventoryDetails = TableRegistry::get('IncludedInventoryDetails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->IncludedInventoryDetails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
