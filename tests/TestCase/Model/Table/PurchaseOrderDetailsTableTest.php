<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PurchaseOrderDetailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PurchaseOrderDetailsTable Test Case
 */
class PurchaseOrderDetailsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PurchaseOrderDetailsTable
     */
    public $PurchaseOrderDetails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.purchase_order_details',
        'app.purchase_orders',
        'app.depots',
        'app.depot_types',
        'app.excluded_inventories',
        'app.included_inventories',
        'app.missed_pos',
        'app.official_receipts',
        'app.received_pos',
        'app.received_tos',
        'app.sales_invoices',
        'app.wrong_send_pos',
        'app.users',
        'app.status_users',
        'app.roles',
        'app.roles_users',
        'app.purchase_order_status',
        'app.products',
        'app.product_status',
        'app.product_colors',
        'app.product_dimensions',
        'app.product_units',
        'app.delivery_receipt_details',
        'app.excluded_inventory_details',
        'app.included_inventory_details',
        'app.missed_po_details',
        'app.official_receipt_details',
        'app.received_po_details',
        'app.received_to_details',
        'app.sales_invoice_details',
        'app.transfer_order_details',
        'app.wrong_send_po_details'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PurchaseOrderDetails') ? [] : ['className' => 'App\Model\Table\PurchaseOrderDetailsTable'];
        $this->PurchaseOrderDetails = TableRegistry::get('PurchaseOrderDetails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PurchaseOrderDetails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
