<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DepotTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DepotTypesTable Test Case
 */
class DepotTypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DepotTypesTable
     */
    public $DepotTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.depot_types',
        'app.depots'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DepotTypes') ? [] : ['className' => 'App\Model\Table\DepotTypesTable'];
        $this->DepotTypes = TableRegistry::get('DepotTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DepotTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
