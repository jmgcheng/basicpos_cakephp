<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DeliveryReceiptStatusTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DeliveryReceiptStatusTable Test Case
 */
class DeliveryReceiptStatusTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DeliveryReceiptStatusTable
     */
    public $DeliveryReceiptStatus;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.delivery_receipt_status',
        'app.delivery_receipts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DeliveryReceiptStatus') ? [] : ['className' => 'App\Model\Table\DeliveryReceiptStatusTable'];
        $this->DeliveryReceiptStatus = TableRegistry::get('DeliveryReceiptStatus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DeliveryReceiptStatus);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
