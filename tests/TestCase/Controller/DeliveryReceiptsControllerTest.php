<?php
namespace App\Test\TestCase\Controller;

use App\Controller\DeliveryReceiptsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\DeliveryReceiptsController Test Case
 */
class DeliveryReceiptsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.delivery_receipts',
        'app.sales_invoices',
        'app.depots',
        'app.depot_types',
        'app.excluded_inventories',
        'app.users',
        'app.status_users',
        'app.roles',
        'app.roles_users',
        'app.excluded_inventory_status',
        'app.excluded_inventory_details',
        'app.products',
        'app.product_status',
        'app.product_colors',
        'app.product_dimensions',
        'app.product_units',
        'app.delivery_receipt_details',
        'app.included_inventory_details',
        'app.included_inventories',
        'app.included_inventory_status',
        'app.missed_po_details',
        'app.missed_pos',
        'app.purchase_orders',
        'app.purchase_order_status',
        'app.purchase_order_details',
        'app.received_pos',
        'app.received_po_status',
        'app.received_po_details',
        'app.wrong_send_pos',
        'app.wrong_send_po_status',
        'app.wrong_send_po_details',
        'app.missed_po_status',
        'app.official_receipt_details',
        'app.official_receipts',
        'app.official_receipt_status',
        'app.received_to_details',
        'app.received_tos',
        'app.transfer_orders',
        'app.transfer_order_status',
        'app.transfer_order_details',
        'app.received_to_status',
        'app.sales_invoice_details',
        'app.sales_invoice_status',
        'app.delivery_receipt_status'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
