<?php
namespace App\Test\TestCase\Controller;

use App\Controller\PurchaseOrderDetailsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\PurchaseOrderDetailsController Test Case
 */
class PurchaseOrderDetailsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.purchase_order_details',
        'app.purchase_orders',
        'app.depots',
        'app.depot_types',
        'app.excluded_inventories',
        'app.included_inventories',
        'app.missed_pos',
        'app.official_receipts',
        'app.received_pos',
        'app.received_tos',
        'app.sales_invoices',
        'app.wrong_send_pos',
        'app.users',
        'app.status_users',
        'app.roles',
        'app.roles_users',
        'app.purchase_order_status',
        'app.products',
        'app.product_status',
        'app.product_colors',
        'app.product_dimensions',
        'app.product_units',
        'app.delivery_receipt_details',
        'app.excluded_inventory_details',
        'app.included_inventory_details',
        'app.missed_po_details',
        'app.official_receipt_details',
        'app.received_po_details',
        'app.received_to_details',
        'app.sales_invoice_details',
        'app.transfer_order_details',
        'app.wrong_send_po_details'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
