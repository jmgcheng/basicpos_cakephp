
# CakePHP Application - Basic POS Skeleton

Cake skeleton with Basic POS.

## Tasks To Do:
* db schema - init test /
* products - setup /
* db schema - remove header string in tables /
* purchase order - setup /
* received po - setup /
* purchase order - variable updates - controller & views /
* missed po - setup /
* wrong send po - setup /
* sales invoice - setup /
* official receipt - setup /
* included inventory - setup /
* excluded inventory - setup /
* transfer order - setup /
* received to - setup /
* delivery receipt - setup /
* products - stocks_quantities - sp /
* products - stock - controller /
* products - stock - view /
* check product qty consistency /
* apply user roles
* theme
* products - stock - view form
* -


## App Capabilities:
* -


## Current DB:
```
#!sql
CREATE TABLE status_users
(
    id INT(11) AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(55) NOT NULL,
    UNIQUE KEY (name)
);
CREATE TABLE users 
(
    id INT(11) AUTO_INCREMENT PRIMARY KEY,
    created DATETIME,
    modified DATETIME,
    username VARCHAR(55) NOT NULL,
    email VARCHAR(55) NOT NULL,
    password VARCHAR(255) NOT NULL,
    firstname VARCHAR(55) DEFAULT '',
    lastname VARCHAR(55) DEFAULT '',
    unique_key VARCHAR(255) NOT NULL,
    status_user_id int(11) DEFAULT 1,
    FOREIGN KEY status_user_key (status_user_id) REFERENCES status_users(id)
);
CREATE TABLE roles
(
    id INT(11) AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(55) NOT NULL,
    UNIQUE KEY (name)
);
CREATE TABLE roles_users
(
    user_id INT(11) NOT NULL,
    role_id INT(11) NOT NULL,
    PRIMARY KEY (user_id, role_id),
    FOREIGN KEY user_key (user_id) REFERENCES users(id),
    FOREIGN KEY role_key (role_id) REFERENCES roles(id)
);
INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'Admin'),
(2, 'User');
INSERT INTO `status_users` (`id`, `name`) VALUES
(1, 'activated'),
(2, 'banned'),
(3, 'deactivated'),
(4, 'email'),
(5, 'lock');



CREATE TABLE IF NOT EXISTS `product_colors` (
  id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name varchar(20) NOT NULL,
  UNIQUE KEY (name)
);
INSERT INTO `product_colors` (`id`, `name`) VALUES
(1, 'White'),
(2, 'C. Yellow'),
(3, 'LT. Blue'),
(4, 'AP. Green'),
(5, 'Y. Gold'),
(6, 'LT. Pink'),
(7, 'Cream'),
(8, 'Orange'),
(9, 'Black'),
(10, 'E. Green'),
(11, 'R. Blue'),
(12, 'AQ. Blue'),
(13, 'N. Blue'),
(14, 'Red'),
(15, 'Brown'),
(16, 'Peach'),
(17, 'Lilac'),
(18, 'Fuschia'),
(19, 'DK. Violet'),
(20, 'M. Green'),
(21, 'Maroon'),
(22, 'Turquoise'),
(23, 'Carnation'),
(24, 'Acid Gray'),
(25, 'B. Blue'),
(26, 'Mix TopDye'),
(27, 'DR. TopDye'),
(28, 'Hot Pink');
CREATE TABLE IF NOT EXISTS `product_dimensions` (
  id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name varchar(20) NOT NULL,
  UNIQUE KEY (name)
);
INSERT INTO `product_dimensions` (`id`, `name`) VALUES
(1, '10'),
(2, '12'),
(3, 'L'),
(4, 'M'),
(5, 'S'),
(6, 'XL'),
(7, 'XS'),
(8, 'XXL'),
(9, 'Free Size'),
(10, 'XXXL');
CREATE TABLE IF NOT EXISTS `product_status` (
  id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name varchar(20) NOT NULL,
  UNIQUE KEY (name)
);
INSERT INTO `product_status` (id, name) VALUES
(1, 'Activated'),
(2, 'Deactivated');
CREATE TABLE IF NOT EXISTS `product_units` (
  id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name varchar(20) NOT NULL,
  UNIQUE KEY (name)
);
INSERT INTO `product_units` (id, name) VALUES
(1, 'Pieces'),
(2, 'Kilo');
CREATE TABLE IF NOT EXISTS `products` (
  id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  created DATETIME,
  name varchar(55) NOT NULL,
  model varchar(20),
  description varchar(55),
  qty_low_alert int(11),
  product_status_id int(11) NOT NULL,
  product_color_id int(11) NOT NULL,
  product_dimension_id int(11) NOT NULL,
  product_unit_id int(11) NOT NULL,
  FOREIGN KEY product_status_key (product_status_id) REFERENCES product_status(id),
  FOREIGN KEY product_color_key (product_color_id) REFERENCES product_colors(id),
  FOREIGN KEY product_dimension_key (product_dimension_id) REFERENCES product_dimensions(id),
  FOREIGN KEY product_unit_key (product_unit_id) REFERENCES product_units(id),
  UNIQUE KEY product_unique_key (name, model, product_color_id, product_dimension_id, product_unit_id)
);





CREATE TABLE IF NOT EXISTS `depot_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(20) NOT NULL
);
INSERT INTO `depot_types` (`id`, `name`) VALUES
(1, 'Warehouse'),
(2, 'Store'),
(3, 'House');
CREATE TABLE IF NOT EXISTS `depots` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `depot_type_id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  FOREIGN KEY depot_type_key (depot_type_id) REFERENCES depot_types(id),
  UNIQUE KEY (name)
);
INSERT INTO `depots` (`id`, `depot_type_id`, `name`, `description`, `address`) VALUES
(1, 3, 'SDLG Building - 3rd ', '', '');




CREATE TABLE IF NOT EXISTS `purchase_order_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(20) NOT NULL
);
INSERT INTO `purchase_order_status` (`id`, `name`) VALUES
(1, 'Open'),
(2, 'Close');
CREATE TABLE IF NOT EXISTS `purchase_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `depot_id` int(11) NOT NULL DEFAULT 1,
  `prepared_by_id` int(11) NOT NULL DEFAULT 1,
  `approved_by_id` int(11),
  `date_registration` datetime NOT NULL,
  `title` varchar(40) NOT NULL,
  `order_description` varchar(255),
  `comment` varchar(255),
  `purchase_order_status_id` int(11) NOT NULL,
  FOREIGN KEY depot_key (depot_id) REFERENCES depots(id),
  FOREIGN KEY prepared_by_key (prepared_by_id) REFERENCES users(id),
  FOREIGN KEY approved_by_key (approved_by_id) REFERENCES users(id),
  FOREIGN KEY purchase_order_status_key (purchase_order_status_id) REFERENCES purchase_order_status(id),
  UNIQUE KEY (title)
);
CREATE TABLE IF NOT EXISTS `purchase_order_details` (
  `purchase_order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity_ordered` int(11) NOT NULL,
  PRIMARY KEY (purchase_order_id, product_id),
  FOREIGN KEY purchase_order_key (purchase_order_id) REFERENCES purchase_orders(id),
  FOREIGN KEY product_key (product_id) REFERENCES products(id),
  UNIQUE KEY po_detail_key (purchase_order_id, product_id) 
);




CREATE TABLE IF NOT EXISTS `received_po_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(20) NOT NULL,
  UNIQUE KEY (name)
);
INSERT INTO `received_po_status` (`id`, `name`) VALUES
(1, 'Open'),
(2, 'Close');
CREATE TABLE IF NOT EXISTS `received_pos` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `purchase_order_id` int(11) NOT NULL,
  `depot_id` int(11) NOT NULL DEFAULT 1,
  `prepared_by_id` int(11) NOT NULL DEFAULT 1,
  `approved_by_id` int(11),
  `date_registration` datetime NOT NULL,
  `comment` varchar(255),
  `received_po_status_id` int(11) NOT NULL,
  FOREIGN KEY purchase_order_key (purchase_order_id) REFERENCES purchase_orders(id),
  FOREIGN KEY depot_key (depot_id) REFERENCES depots(id),
  FOREIGN KEY prepared_by_key (prepared_by_id) REFERENCES users(id),
  FOREIGN KEY approved_by_key (approved_by_id) REFERENCES users(id),
  FOREIGN KEY received_po_status_key (received_po_status_id) REFERENCES received_po_status(id)
);
CREATE TABLE IF NOT EXISTS `received_po_details` (
  `received_po_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity_received` int(11) NOT NULL,
  `product_serial` varchar(255),
  PRIMARY KEY (received_po_id, product_id, product_serial),
  FOREIGN KEY received_po_key (received_po_id) REFERENCES received_pos(id),
  FOREIGN KEY product_key (product_id) REFERENCES products(id),
  UNIQUE KEY received_po_detail_key (received_po_id, product_id, product_serial)
);




CREATE TABLE IF NOT EXISTS `missed_po_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(20) NOT NULL,
  UNIQUE KEY (name)
);
INSERT INTO `missed_po_status` (`id`, `name`) VALUES
(1, 'Open'),
(2, 'Close');
CREATE TABLE IF NOT EXISTS `missed_pos` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `purchase_order_id` int(11) NOT NULL,
  `depot_id` int(11) NOT NULL DEFAULT 1,
  `received_by_id` int(11) NOT NULL DEFAULT 1,
  `approved_by_id` int(11),
  `date_registration` datetime NOT NULL,
  `comment` varchar(255),
  `missed_po_status_id` int(11) NOT NULL,
  FOREIGN KEY purchase_order_key (purchase_order_id) REFERENCES purchase_orders(id),
  FOREIGN KEY depot_key (depot_id) REFERENCES depots(id),
  FOREIGN KEY received_by_key (received_by_id) REFERENCES users(id),
  FOREIGN KEY approved_by_key (approved_by_id) REFERENCES users(id),
  FOREIGN KEY missed_po_status_key (missed_po_status_id) REFERENCES missed_po_status(id)
);
CREATE TABLE IF NOT EXISTS `missed_po_details` (
  `missed_po_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity_missed` int(11) NOT NULL,
  PRIMARY KEY (missed_po_id, product_id),
  FOREIGN KEY missed_po_key (missed_po_id) REFERENCES missed_pos(id),
  FOREIGN KEY product_key (product_id) REFERENCES products(id),
  UNIQUE KEY missed_po_detail_key (missed_po_id, product_id)
);



  

CREATE TABLE IF NOT EXISTS `wrong_send_po_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(20) NOT NULL,
  UNIQUE KEY (name)
); 
 INSERT INTO `wrong_send_po_status` (`id`, `name`) VALUES
(1, 'Open'),
(2, 'Close');
CREATE TABLE IF NOT EXISTS `wrong_send_pos` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `purchase_order_id` int(11) NOT NULL,
  `depot_id` int(11) NOT NULL DEFAULT 1,
  `received_by_id` int(11) NOT NULL DEFAULT 1,
  `approved_by_id` int(11),
  `date_registration` datetime NOT NULL,
  `comment` varchar(255),
  `wrong_send_po_status_id` int(11) NOT NULL,
  FOREIGN KEY purchase_order_key (purchase_order_id) REFERENCES purchase_orders(id),
  FOREIGN KEY depot_key (depot_id) REFERENCES depots(id),
  FOREIGN KEY received_by_key (received_by_id) REFERENCES users(id),
  FOREIGN KEY approved_by_key (approved_by_id) REFERENCES users(id),
  FOREIGN KEY wrong_send_po_status_key (wrong_send_po_status_id) REFERENCES wrong_send_po_status(id)
);
CREATE TABLE IF NOT EXISTS `wrong_send_po_details` (
  `wrong_send_po_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity_wrong_send` int(11) NOT NULL,
  `product_serial` varchar(255),
  PRIMARY KEY (wrong_send_po_id, product_id, product_serial),
  FOREIGN KEY wrong_send_po_key (wrong_send_po_id) REFERENCES wrong_send_pos(id),
  FOREIGN KEY product_key (product_id) REFERENCES products(id),
  UNIQUE KEY wrong_send_po_detail_key (wrong_send_po_id, product_id, product_serial)
);




CREATE TABLE IF NOT EXISTS `sales_invoice_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(20) NOT NULL,
  UNIQUE KEY (`name`)
);
INSERT INTO `sales_invoice_status` (`id`, `name`) VALUES
(1, 'Open'),
(2, 'Close');
CREATE TABLE IF NOT EXISTS `sales_invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `depot_id` int(11) NOT NULL DEFAULT 1,
  `prepared_by_id` int(11) NOT NULL DEFAULT 1,
  `approved_by_id` int(11),
  `date_registration` datetime NOT NULL,
  `title` varchar(50) NOT NULL,
  `comment` varchar(255),
  `sales_invoice_status_id` int(11) NOT NULL,
  FOREIGN KEY depot_key (depot_id) REFERENCES depots(id),
  FOREIGN KEY prepared_by_key (prepared_by_id) REFERENCES users(id),
  FOREIGN KEY approved_by_key (approved_by_id) REFERENCES users(id),
  FOREIGN KEY sales_invoice_status_key (sales_invoice_status_id) REFERENCES sales_invoice_status(id)
);
CREATE TABLE IF NOT EXISTS `sales_invoice_details` (
  `sales_invoice_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity_to_release` int(11) NOT NULL,
  PRIMARY KEY (sales_invoice_id, product_id),
  FOREIGN KEY sales_invoice_key (sales_invoice_id) REFERENCES sales_invoices(id),
  FOREIGN KEY product_key (product_id) REFERENCES products(id),
  UNIQUE KEY sales_invoice_detail_key (sales_invoice_id, product_id)
);





CREATE TABLE IF NOT EXISTS `official_receipt_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(20) NOT NULL,
  UNIQUE KEY (`name`)
);
INSERT INTO `official_receipt_status` (`id`, `name`) VALUES
(1, 'Open'),
(2, 'Close');
CREATE TABLE IF NOT EXISTS `official_receipts` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `sales_invoice_id` int(11) NOT NULL,
  `depot_id` int(11) NOT NULL DEFAULT 1,
  `prepared_by_id` int(11) NOT NULL DEFAULT 1,
  `approved_by_id` int(11),
  `date_registration` datetime NOT NULL,
  `comment` varchar(255),
  `official_receipt_status_id` int(11) NOT NULL,
  FOREIGN KEY sales_invoice_key (sales_invoice_id) REFERENCES sales_invoices(id),
  FOREIGN KEY depot_key (depot_id) REFERENCES depots(id),
  FOREIGN KEY prepared_by_key (prepared_by_id) REFERENCES users(id),
  FOREIGN KEY approved_by_key (approved_by_id) REFERENCES users(id),
  FOREIGN KEY official_receipt_status_key (official_receipt_status_id) REFERENCES official_receipt_status(id)
);
CREATE TABLE IF NOT EXISTS `official_receipt_details` (
  `official_receipt_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity_released` int(11) NOT NULL,
  `product_serial` varchar(255),
  PRIMARY KEY (official_receipt_id, product_id, product_serial),
  FOREIGN KEY official_receipt_key (official_receipt_id) REFERENCES official_receipts(id),
  FOREIGN KEY product_key (product_id) REFERENCES products(id),
  UNIQUE KEY official_receipt_detail_key (official_receipt_id, product_id, product_serial)
);

  



CREATE TABLE IF NOT EXISTS `included_inventory_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(20) NOT NULL,
  UNIQUE KEY (`name`)
);
INSERT INTO `included_inventory_status` (`id`, `name`) VALUES
(1, 'Open'),
(2, 'Closed');
CREATE TABLE IF NOT EXISTS `included_inventories` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `depot_id` int(11) NOT NULL DEFAULT 1,
  `prepared_by_id` int(11) NOT NULL,
  `date_registration` datetime NOT NULL,
  `comment` varchar(255),
  `included_inventory_status_id` int(11) NOT NULL,
  FOREIGN KEY depot_key (depot_id) REFERENCES depots(id),
  FOREIGN KEY prepared_by_key (prepared_by_id) REFERENCES users(id),
  FOREIGN KEY included_inventory_status_key (included_inventory_status_id) REFERENCES included_inventory_status(id)
);
CREATE TABLE IF NOT EXISTS `included_inventory_details` (
  `included_inventory_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity_included` int(11) NOT NULL,
  `product_serial` varchar(255),
  PRIMARY KEY (included_inventory_id, product_id, product_serial),
  FOREIGN KEY included_inventory_key (included_inventory_id) REFERENCES included_inventories(id),
  FOREIGN KEY product_key (product_id) REFERENCES products(id),
  UNIQUE KEY included_inventory_key (included_inventory_id, product_id, product_serial)
);



CREATE TABLE IF NOT EXISTS `excluded_inventory_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(20) NOT NULL,
  UNIQUE KEY (`name`)
);
INSERT INTO `excluded_inventory_status` (`id`, `name`) VALUES
(1, 'Open'),
(2, 'Closed');
CREATE TABLE IF NOT EXISTS `excluded_inventories` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `depot_id` int(11) NOT NULL DEFAULT 1,
  `prepared_by_id` int(11) NOT NULL,
  `date_registration` datetime NOT NULL,
  `comment` varchar(255),
  `excluded_inventory_status_id` int(11) NOT NULL,
  FOREIGN KEY depot_key (depot_id) REFERENCES depots(id),
  FOREIGN KEY prepared_by_key (prepared_by_id) REFERENCES users(id),
  FOREIGN KEY excluded_inventory_status_key (excluded_inventory_status_id) REFERENCES excluded_inventory_status(id)
);
CREATE TABLE IF NOT EXISTS `excluded_inventory_details` (
  `excluded_inventory_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity_excluded` int(11) NOT NULL,
  `product_serial` varchar(255),
  PRIMARY KEY (excluded_inventory_id, product_id, product_serial),
  FOREIGN KEY excluded_inventory_key (excluded_inventory_id) REFERENCES excluded_inventories(id),
  FOREIGN KEY product_key (product_id) REFERENCES products(id),
  UNIQUE KEY excluded_inventory_key (excluded_inventory_id, product_id, product_serial)
);






CREATE TABLE IF NOT EXISTS `transfer_order_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(20) NOT NULL,
  UNIQUE KEY (`name`)
);
INSERT INTO `transfer_order_status` (`id`, `name`) VALUES
(1, 'Open'),
(2, 'Closed');
CREATE TABLE IF NOT EXISTS `transfer_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `releasing_depot_id` int(11) NOT NULL,
  `receiving_depot_id` int(11) NOT NULL,
  `prepared_by_id` int(11) NOT NULL,
  `approved_by_id` int(11) NOT NULL,
  `date_registration` datetime NOT NULL,
  `comment` varchar(255),
  `transfer_order_status_id` int(11) NOT NULL,
  FOREIGN KEY releasing_depot_key (releasing_depot_id) REFERENCES depots(id),
  FOREIGN KEY receiving_depot_key (receiving_depot_id) REFERENCES depots(id),
  FOREIGN KEY prepared_by_key (prepared_by_id) REFERENCES users(id),
  FOREIGN KEY approved_by_key (approved_by_id) REFERENCES users(id),
  FOREIGN KEY transfer_order_status_key (transfer_order_status_id) REFERENCES transfer_order_status(id)
);
CREATE TABLE IF NOT EXISTS `transfer_order_details` (
  `transfer_order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity_transfering` int(11) NOT NULL,
  `product_serial` varchar(255),
  PRIMARY KEY (transfer_order_id, product_id, product_serial),
  FOREIGN KEY transfer_order_key (transfer_order_id) REFERENCES transfer_orders(id),
  FOREIGN KEY product_key (product_id) REFERENCES products(id),
  UNIQUE KEY transfer_order_key (transfer_order_id, product_id, product_serial)
);




CREATE TABLE IF NOT EXISTS `received_to_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(20) NOT NULL,
  UNIQUE KEY (`id`)
);
INSERT INTO `received_to_status` (`id`, `name`) VALUES
(1, 'Open'),
(2, 'Closed');
CREATE TABLE IF NOT EXISTS `received_tos` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `transfer_order_id` int(11) NOT NULL,
  `depot_id` int(11) NOT NULL,
  `prepared_by_id` int(11) NOT NULL,
  `received_by_id` int(11) NOT NULL,
  `date_registration` datetime NOT NULL,
  `comment` varchar(255),
  `received_to_status_id` int(11) NOT NULL,
  FOREIGN KEY transfer_order_key (transfer_order_id) REFERENCES transfer_orders(id),
  FOREIGN KEY depot_key (depot_id) REFERENCES depots(id),
  FOREIGN KEY prepared_by_key (prepared_by_id) REFERENCES users(id),
  FOREIGN KEY received_by_key (received_by_id) REFERENCES users(id),
  FOREIGN KEY received_to_status_key (received_to_status_id) REFERENCES received_to_status(id)
);
CREATE TABLE IF NOT EXISTS `received_to_details` (
  `received_to_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity_received` int(11) NOT NULL,
  `product_serial` varchar(255),
  PRIMARY KEY (received_to_id, product_id, product_serial),
  FOREIGN KEY received_to_key (received_to_id) REFERENCES received_tos(id),
  FOREIGN KEY product_key (product_id) REFERENCES products(id),
  UNIQUE KEY transfer_order_key (received_to_id, product_id, product_serial)
);




CREATE TABLE IF NOT EXISTS `delivery_receipt_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(20) NOT NULL,
  UNIQUE KEY (`name`)
);
INSERT INTO `delivery_receipt_status` (`id`, `name`) VALUES
(1, 'Open'),
(2, 'Closed');
CREATE TABLE IF NOT EXISTS `delivery_receipts` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `sales_invoice_id` int(11) NOT NULL,
  `prepared_by_id` int(11) NOT NULL,
  `approved_by_id` int(11) NOT NULL,
  `date_registration` datetime NOT NULL,
  `title` varchar(40) NOT NULL,
  `comment` varchar(255),
  `delivery_receipt_status_id` int(11) NOT NULL,
  FOREIGN KEY sales_invoice_key (sales_invoice_id) REFERENCES sales_invoices(id),
  FOREIGN KEY prepared_by_key (prepared_by_id) REFERENCES users(id),
  FOREIGN KEY approved_by_key (approved_by_id) REFERENCES users(id),
  FOREIGN KEY delivery_receipt_status_key (delivery_receipt_status_id) REFERENCES delivery_receipt_status(id)
);
CREATE TABLE IF NOT EXISTS `delivery_receipt_details` (
  `delivery_receipt_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity_delivering` int(11) NOT NULL,
  `product_serial` varchar(255),
  PRIMARY KEY (delivery_receipt_id, product_id, product_serial),
  FOREIGN KEY delivery_receipt_key (delivery_receipt_id) REFERENCES delivery_receipts(id),
  FOREIGN KEY product_key (product_id) REFERENCES products(id),
  UNIQUE KEY delivery_receipt_detail_key (delivery_receipt_id, product_id, product_serial)
);






CREATE TABLE IF NOT EXISTS `sales_return_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(20) NOT NULL,
  UNIQUE KEY (`name`)
);
INSERT INTO `sales_return_status` (`id`, `name`) VALUES
(1, 'Open'),
(2, 'Closed');
CREATE TABLE IF NOT EXISTS `sales_returns` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `official_receipt_id` int(11) NOT NULL,
  `depot_id` int(11) NOT NULL,
  `prepared_by_id` int(11) NOT NULL,
  `approved_by_id` int(11) NOT NULL,
  `date_registration` datetime NOT NULL,
  `comment` varchar(255),
  `sales_return_status_id` int(11) NOT NULL,
  FOREIGN KEY official_receipt_key (official_receipt_id) REFERENCES official_receipts(id),
  FOREIGN KEY depot_key (depot_id) REFERENCES depots(id),
  FOREIGN KEY prepared_by_key (prepared_by_id) REFERENCES users(id),
  FOREIGN KEY approved_by_key (approved_by_id) REFERENCES users(id),
  FOREIGN KEY sales_return_status_key (sales_return_status_id) REFERENCES sales_return_status(id)
);
CREATE TABLE IF NOT EXISTS `sales_return_details` (
  `sales_return_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity_received` int(11) NOT NULL,
  `product_serial` varchar(255),
  PRIMARY KEY (sales_return_id, product_id, product_serial),
  FOREIGN KEY sales_return_key (sales_return_id) REFERENCES sales_returns(id),
  FOREIGN KEY product_key (product_id) REFERENCES products(id),
  UNIQUE KEY sales_return_detail_key (sales_return_id, product_id, product_serial)
);




  



```



## SP:
```
#!sql


CREATE DEFINER=`root`@`localhost` PROCEDURE `stocks_quantities`(IN `depot_id` INT, IN `qa` INT, IN `product_dimension_id` INT, IN `product_color_id` INT, IN `product_unit_name` INT, IN `product_status_id` INT, IN `product_name_sort` VARCHAR(10) CHARSET utf8, IN `product_color_name_sort` VARCHAR(10) CHARSET utf8, IN `product_dimension_name_sort` VARCHAR(10) CHARSET utf8, IN `i_limit` INT, IN `i_offset` INT)
    NO SQL
begin

      IF i_limit = 0 OR i_limit = '' OR i_limit IS NULL THEN
          SET i_limit = 1000;
      END IF;

      IF i_offset = 0 OR i_offset = '' OR i_offset IS NULL THEN
          SET i_offset = 0;
      END IF;


      SELECT
        *
      FROM
        (
          SELECT
            products.id AS 'product_id',
            products.name AS 'product_name',
            products.model AS 'product_model',
            products.qty_low_alert AS 'product_qty_low_alert',
            COALESCE(tbl_included_inventories.product_total,0) AS 'included_inventory_total', 
            COALESCE(tbl_received_pos.product_total,0) AS 'received_po_total', 
            COALESCE(tbl_received_tos.product_total,0) AS 'received_to_total', 
            COALESCE(tbl_excluded_inventories.product_total,0) AS 'excluded_inventory_total', 
            COALESCE(tbl_transfer_orders.product_total,0) AS 'transfer_order_total', 
            COALESCE(tbl_official_receipts.product_total,0) AS 'official_receipt_total', 
            COALESCE(tbl_missed_pos.product_total,0) AS 'missed_purchased_order_total', 
            COALESCE(tbl_purchase_orders.product_total,0) AS 'purchased_order_total', 
            COALESCE(tbl_wrong_send_pos.product_total,0) AS 'wrong_send_purchased_order_total', 
            COALESCE(tbl_sales_returns.product_total,0) AS 'sales_return_total', 
            (
              COALESCE(tbl_purchase_orders.product_total,0)
              -
              (
                COALESCE(tbl_received_pos.product_total,0)
                +
                COALESCE(tbl_missed_pos.product_total,0)
              )
            ) AS 'af',
            (
              COALESCE(tbl_excluded_inventories.product_total,0) +
              COALESCE(tbl_transfer_orders.product_total,0) +
              COALESCE(tbl_official_receipts.product_total,0)
            ) AS 'tr', 
            (
              COALESCE(tbl_included_inventories.product_total,0) +
              COALESCE(tbl_received_pos.product_total,0) +
              COALESCE(tbl_received_tos.product_total,0) +
              COALESCE(tbl_sales_returns.product_total,0)
            ) -
            (
              COALESCE(tbl_excluded_inventories.product_total,0) +
              COALESCE(tbl_transfer_orders.product_total,0) +
              COALESCE(tbl_official_receipts.product_total,0)
            )
            + COALESCE(tbl_wrong_send_pos.product_total,0)
            AS 'qa',
            tbl_product_colors.id AS product_color_id,
            tbl_product_dimensions.id AS product_dimension_id,
            products.product_status_id AS product_status_id,
            tbl_product_colors.name AS product_color_name,
            tbl_product_dimensions.name AS product_dimension_name,
            tbl_product_units.name AS product_unit_name
          FROM 
            products
          LEFT JOIN
            (
              SELECT
                included_inventory_details.product_id,
                SUM(included_inventory_details.quantity_included) AS product_total
              FROM
                  included_inventories
              LEFT JOIN
                  included_inventory_details
                  ON
                  included_inventories.id = included_inventory_details.included_inventory_id
              WHERE 
                  included_inventories.depot_id = COALESCE(NULLIF(depot_id, ''), included_inventories.depot_id)
              GROUP BY
                included_inventory_details.product_id
            )
            AS tbl_included_inventories ON tbl_included_inventories.product_id = products.id
          LEFT JOIN
            (
              SELECT
                received_po_details.product_id,
                SUM(received_po_details.quantity_received) AS product_total
              FROM
                received_pos
              LEFT JOIN
                received_po_details
                ON
                received_pos.id = received_po_details.received_po_id  
              WHERE 
                  received_pos.depot_id = COALESCE(NULLIF(depot_id, ''), received_pos.depot_id)
              GROUP BY
                received_po_details.product_id  
            )
            AS tbl_received_pos ON tbl_received_pos.product_id = products.id
          LEFT JOIN
            (
              SELECT
                received_to_details.product_id,
                SUM(received_to_details.quantity_received) AS product_total
              FROM
                received_tos
              LEFT JOIN
                received_to_details
                ON
                received_tos.id = received_to_details.received_to_id  
              WHERE 
                  received_tos.depot_id = COALESCE(NULLIF(depot_id, ''), received_tos.depot_id)
              GROUP BY
                received_to_details.product_id
            )
            AS tbl_received_tos ON tbl_received_tos.product_id = products.id
          LEFT JOIN
            (
              SELECT
                excluded_inventory_details.product_id,
                SUM(excluded_inventory_details.quantity_excluded) AS product_total
              FROM
                excluded_inventories
              LEFT JOIN
                excluded_inventory_details
                ON
                excluded_inventories.id = excluded_inventory_details.excluded_inventory_id  
              WHERE 
                  excluded_inventories.depot_id = COALESCE(NULLIF(depot_id, ''), excluded_inventories.depot_id)
              GROUP BY
                excluded_inventory_details.product_id  
            )
            AS tbl_excluded_inventories ON tbl_excluded_inventories.product_id = products.id
          LEFT JOIN
            (
              SELECT
                transfer_order_details.product_id,
                SUM(transfer_order_details.quantity_transfering) AS product_total
              FROM
                transfer_orders
              LEFT JOIN
                transfer_order_details
                ON
                transfer_orders.id = transfer_order_details.transfer_order_id  
              WHERE 
                  transfer_orders.releasing_depot_id = COALESCE(NULLIF(depot_id, ''), transfer_orders.releasing_depot_id)
              GROUP BY
                transfer_order_details.product_id
            )
            AS tbl_transfer_orders ON tbl_transfer_orders.product_id = products.id
          LEFT JOIN
            (
              SELECT
                official_receipt_details.product_id,
                SUM(official_receipt_details.quantity_released) AS product_total
              FROM
                official_receipts
              LEFT JOIN
                official_receipt_details
                ON
                official_receipts.id = official_receipt_details.official_receipt_id  
              WHERE 
                  official_receipts.depot_id = COALESCE(NULLIF(depot_id, ''), official_receipts.depot_id)
              GROUP BY
                official_receipt_details.product_id  
            )
            AS tbl_official_receipts on tbl_official_receipts.product_id = products.id
          LEFT JOIN
            (
              SELECT
                missed_po_details.product_id,
                SUM(missed_po_details.quantity_missed) AS product_total
              FROM
                missed_pos
              LEFT JOIN
                missed_po_details
                ON
                missed_pos.id = missed_po_details.missed_po_id  
              WHERE 
                  missed_pos.depot_id = COALESCE(NULLIF(depot_id, ''), missed_pos.depot_id)
              GROUP BY
                missed_po_details.product_id  
            )
            AS tbl_missed_pos ON tbl_missed_pos.product_id = products.id
          LEFT JOIN
            (
              SELECT
                purchase_order_details.product_id,
                SUM(purchase_order_details.quantity_ordered) AS product_total
              FROM
                purchase_orders
              LEFT JOIN
                purchase_order_details
                ON
                purchase_orders.id = purchase_order_details.purchase_order_id  
              WHERE 
                  purchase_orders.depot_id = COALESCE(NULLIF(depot_id, ''), purchase_orders.depot_id)
              GROUP BY
                purchase_order_details.product_id  
            )
            AS tbl_purchase_orders ON tbl_purchase_orders.product_id = products.id
          LEFT JOIN
            (
              SELECT
                wrong_send_po_details.product_id,
                SUM(wrong_send_po_details.quantity_wrong_send) AS product_total
              FROM
                wrong_send_pos
              LEFT JOIN
                wrong_send_po_details
                ON
                wrong_send_pos.id = wrong_send_po_details.wrong_send_po_id  
              WHERE 
                  wrong_send_pos.depot_id = COALESCE(NULLIF(depot_id, ''), wrong_send_pos.depot_id)
              GROUP BY
                wrong_send_po_details.product_id  
            )
            AS tbl_wrong_send_pos ON tbl_wrong_send_pos.product_id = products.id
          LEFT JOIN
            (
              SELECT
                sales_return_details.product_id,
                SUM(sales_return_details.quantity_received) AS product_total
              FROM
                sales_returns
              LEFT JOIN
                sales_return_details
                ON
                sales_returns.id = sales_return_details.sales_return_id  
              WHERE 
                  sales_returns.depot_id = COALESCE(NULLIF(depot_id, ''), sales_returns.depot_id)
              GROUP BY
                sales_return_details.product_id  
            )
            AS tbl_sales_returns ON tbl_sales_returns.product_id = products.id
          LEFT JOIN
            (
              SELECT
                product_colors.id,
                product_colors.name
              FROM
                product_colors
            )
            AS tbl_product_colors ON tbl_product_colors.id = products.product_color_id
          LEFT JOIN
            (
              SELECT
                product_dimensions.id,
                product_dimensions.name
              FROM
                product_dimensions
            )
            AS tbl_product_dimensions ON tbl_product_dimensions.id = products.product_dimension_id
          LEFT JOIN
            (
              SELECT
                product_units.id,
                product_units.name
              FROM
                product_units
            )
            AS tbl_product_units ON tbl_product_units.id = products.product_unit_id
        ) inventory_quantity
      WHERE
        inventory_quantity.qa >= COALESCE(NULLIF(qa, ''), 0)
        AND
        inventory_quantity.product_dimension_id = COALESCE(NULLIF(product_dimension_id, ''), inventory_quantity.product_dimension_id)
        AND
        inventory_quantity.product_color_id = COALESCE(NULLIF(product_color_id, ''), inventory_quantity.product_color_id)
        AND
        inventory_quantity.product_unit_name = COALESCE(NULLIF(product_unit_name, ''), inventory_quantity.product_unit_name)  
        AND
        inventory_quantity.product_status_id = COALESCE(NULLIF(product_status_id, ''), inventory_quantity.product_status_id)
      ORDER BY

        CASE
          when product_name_sort <> 'asc' then 0
          when product_name_sort = 'asc' then inventory_quantity.product_name
        END ASC,       
        CASE
          when product_name_sort <> 'desc' then 0
          when product_name_sort = 'desc' then inventory_quantity.product_name
        END DESC,

        CASE
          when product_color_name_sort <> 'asc' then 0
          when product_color_name_sort = 'asc' then inventory_quantity.product_color_name
        END ASC,       
        CASE
          when product_color_name_sort <> 'desc' then 0
          when product_color_name_sort = 'desc' then inventory_quantity.product_color_name
        END DESC,

        CASE
          when product_dimension_name_sort <> 'asc' then 0
          when product_dimension_name_sort = 'asc' then inventory_quantity.product_dimension_name
        END ASC,       
        CASE
          when product_dimension_name_sort <> 'desc' then 0
          when product_dimension_name_sort = 'desc' then inventory_quantity.product_dimension_name
        END DESC

      LIMIT 
        i_limit
      OFFSET
        i_offset;


      end




```



--------------------------------------------------
## Bake:
bin/cake bake all status_users
bin/cake bake all users
bin/cake bake all roles
bin/cake bake all roles_users
bin/cake bake all product_colors
bin/cake bake all product_dimensions
bin/cake bake all product_status
bin/cake bake all product_units
bin/cake bake all products
bin/cake bake all depot_types
bin/cake bake all depots
bin/cake bake all purchase_order_status
bin/cake bake all purchase_orders
bin/cake bake all purchase_order_details
bin/cake bake all received_po_status
bin/cake bake all received_pos
bin/cake bake all received_po_details
bin/cake bake all missed_po_status
bin/cake bake all missed_pos
bin/cake bake all missed_po_details
bin/cake bake all wrong_send_po_status
bin/cake bake all wrong_send_pos
bin/cake bake all wrong_send_po_details
bin/cake bake all sales_invoice_status
bin/cake bake all sales_invoices
bin/cake bake all sales_invoice_details
bin/cake bake all official_receipt_status
bin/cake bake all official_receipts
bin/cake bake all official_receipt_details
bin/cake bake all included_inventory_status
bin/cake bake all included_inventories
bin/cake bake all included_inventory_details
bin/cake bake all excluded_inventory_status
bin/cake bake all excluded_inventories
bin/cake bake all excluded_inventory_details
bin/cake bake all transfer_order_status
bin/cake bake all transfer_orders
bin/cake bake all transfer_order_details
bin/cake bake all received_to_status
bin/cake bake all received_tos
bin/cake bake all received_to_details
bin/cake bake all delivery_receipt_status
bin/cake bake all delivery_receipts
bin/cake bake all delivery_receipt_details
bin/cake bake all sales_return_status
bin/cake bake all sales_returns
bin/cake bake all sales_return_details

--------------------------------------------------


## Setup:



--------------------------------------------------


## Setup - Fresh:
1. Download [Composer](http://getcomposer.org/doc/00-intro.md) or update `composer self-update`.
2. Run `php composer.phar create-project --prefer-dist cakephp/app [app_name]`.
If Composer is installed globally, run
```bash
composer create-project --prefer-dist cakephp/app [app_name]
```
3. Run browser and check if installation is ok
4. Create DB. Use schema provided
5. Read and edit `config/app.php` and setup the 'Datasources' and any other configuration relevant for your application.
6. Check if able to connect DB
7. Bake
```bash
bin/cake bake all

```

--------------------------------------------------


## Notes:
* -