CREATE TABLE IF NOT EXISTS `delivery_receipt_detail` (
  `i_drh_id` int(11) NOT NULL,
  `i_p_id` int(11) NOT NULL,
  `i_quantity_delivering` int(11) NOT NULL,
  `s_product_serial` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `delivery_receipt_header` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `i_sih_id` int(11) NOT NULL,
  `i_u_prepared_by` int(11) NOT NULL,
  `i_u_approved_by` int(11) NOT NULL,
  `s_date_registration` datetime NOT NULL,
  `s_title` varchar(40) NOT NULL,
  `s_comment` varchar(255) NOT NULL,
  `i_drsn_id` int(11) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `delivery_receipt_status_names` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_name` varchar(20) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

INSERT INTO `delivery_receipt_status_names` (`i_id`, `s_name`) VALUES
(1, 'Open'),
(2, 'Closed');

CREATE TABLE IF NOT EXISTS `excluded_inventory_detail` (
  `i_eih_id` int(11) NOT NULL,
  `i_p_id` int(11) NOT NULL,
  `i_quantity_excluded` int(11) NOT NULL,
  `s_product_serial` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `excluded_inventory_header` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `i_id_id` int(11) NOT NULL,
  `i_u_prepared_by` int(11) NOT NULL,
  `s_date_registration` datetime NOT NULL,
  `s_comment` varchar(255) NOT NULL,
  `i_eisn_id` int(11) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;


CREATE TABLE IF NOT EXISTS `excluded_inventory_status_names` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_name` varchar(20) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

INSERT INTO `excluded_inventory_status_names` (`i_id`, `s_name`) VALUES
(1, 'Open'),
(2, 'Closed');

CREATE TABLE IF NOT EXISTS `included_inventory_detail` (
  `i_iih_id` int(11) NOT NULL,
  `i_p_id` int(11) NOT NULL,
  `i_quantity_included` int(11) NOT NULL,
  `s_product_serial` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `included_inventory_header` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `i_id_id` int(11) NOT NULL,
  `i_u_prepared_by` int(11) NOT NULL,
  `s_date_registration` datetime NOT NULL,
  `s_comment` varchar(255) NOT NULL,
  `i_iisn_id` int(11) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;


CREATE TABLE IF NOT EXISTS `included_inventory_status_names` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_name` varchar(20) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;


INSERT INTO `included_inventory_status_names` (`i_id`, `s_name`) VALUES
(1, 'Open'),
(2, 'Closed');


CREATE TABLE IF NOT EXISTS `inventory_depot` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `i_idt_id` int(11) NOT NULL,
  `s_name` varchar(20) NOT NULL,
  `s_description` varchar(255) NOT NULL,
  `s_address` varchar(255) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;


INSERT INTO `inventory_depot` (`i_id`, `i_idt_id`, `s_name`, `s_description`, `s_address`) VALUES
(1, 3, 'SDLG Building - 3rd ', '', '');

CREATE TABLE IF NOT EXISTS `inventory_depot_type` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_type` varchar(20) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

INSERT INTO `inventory_depot_type` (`i_id`, `s_type`) VALUES
(1, 'Warehouse'),
(2, 'Store'),
(3, 'House');


CREATE TABLE IF NOT EXISTS `inventory_depot_users` (
  `i_u_id` int(11) NOT NULL,
  `i_id_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `inventory_depot_users` (`i_u_id`, `i_id_id`) VALUES
(1, 1),
(2, 1);

CREATE TABLE IF NOT EXISTS `missed_po_detail` (
  `i_mpoh_id` int(11) NOT NULL,
  `i_p_id` int(11) NOT NULL,
  `i_quantity_missed` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `missed_po_header` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `i_poh_id` int(11) NOT NULL,
  `i_id_id` int(11) NOT NULL,
  `i_u_received_by` int(11) NOT NULL,
  `i_u_approved_by` int(11) NOT NULL,
  `s_date_registration` datetime NOT NULL,
  `s_comment` varchar(255) NOT NULL,
  `i_mposn_id` int(11) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

CREATE TABLE IF NOT EXISTS `missed_po_status_names` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_name` varchar(20) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

INSERT INTO `missed_po_status_names` (`i_id`, `s_name`) VALUES
(1, 'Open'),
(2, 'Closed');

CREATE TABLE IF NOT EXISTS `official_receipt_detail` (
  `i_orh_id` int(11) NOT NULL,
  `i_p_id` int(11) NOT NULL,
  `i_quantity_released` int(11) NOT NULL,
  `s_product_serial` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `official_receipt_header` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `i_sih_id` int(11) NOT NULL,
  `i_id_id` int(11) NOT NULL,
  `i_u_prepared_by` int(11) NOT NULL,
  `i_u_approved_by` int(11) NOT NULL,
  `s_date_registration` datetime NOT NULL,
  `s_comment` varchar(255) NOT NULL,
  `i_orsn_id` int(11) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=570 ;

CREATE TABLE IF NOT EXISTS `official_receipt_status_names` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_name` varchar(20) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

CREATE TABLE IF NOT EXISTS `products` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_date_registration` datetime NOT NULL,
  `s_name` varchar(50) NOT NULL,
  `s_model` varchar(50) NOT NULL,
  `s_dimension` varchar(50) NOT NULL,
  `s_color` varchar(10) NOT NULL,
  `s_unit` varchar(15) NOT NULL,
  `s_description` varchar(50) NOT NULL,
  `i_qty_low_alert` int(11) NOT NULL,
  `i_psn_id` int(11) NOT NULL,
  `i_pc_id` int(11) NOT NULL,
  `i_pd_id` int(11) NOT NULL,
  `i_pu_id` int(11) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=390 ;


CREATE TABLE IF NOT EXISTS `product_colors` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_name` varchar(20) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;



CREATE TABLE IF NOT EXISTS `product_dimensions` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_name` varchar(20) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;


INSERT INTO `product_dimensions` (`i_id`, `s_name`) VALUES
(1, '10'),
(2, '12'),
(3, 'L'),
(4, 'M'),
(5, 'S'),
(6, 'XL'),
(7, 'XS'),
(8, 'XXL'),
(9, 'Free Size'),
(10, 'XXXL');

CREATE TABLE IF NOT EXISTS `product_reorder_alert_header` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_date_registration` datetime NOT NULL,
  `s_name` varchar(100) NOT NULL,
  `i_p_id` int(11) NOT NULL,
  `i_qty` int(11) NOT NULL,
  `i_prah_id` int(11) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `product_status_names` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_name` varchar(20) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

INSERT INTO `product_status_names` (`i_id`, `s_name`) VALUES
(1, 'Activated'),
(2, 'Deactivated');

CREATE TABLE IF NOT EXISTS `product_tags` (
  `i_p_id` int(11) NOT NULL,
  `i_ptn_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `product_tag_names` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_name` varchar(20) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `product_units` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_name` varchar(20) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

CREATE TABLE IF NOT EXISTS `purchase_order_detail` (
  `i_poh_id` int(11) NOT NULL,
  `i_p_id` int(11) NOT NULL,
  `i_quantity_ordered` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `purchase_order_header` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `i_id_id` int(11) NOT NULL,
  `i_u_prepared_by` int(11) NOT NULL,
  `i_u_approved_by` int(11) NOT NULL,
  `s_date_registration` datetime NOT NULL,
  `s_title` varchar(40) NOT NULL,
  `s_order_description` varchar(255) NOT NULL,
  `s_comment` varchar(255) NOT NULL,
  `i_posn_id` int(11) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

CREATE TABLE IF NOT EXISTS `purchase_order_revision` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `i_poh_id` int(11) NOT NULL,
  `s_date_updated` datetime NOT NULL,
  `i_u_revised_by` int(11) NOT NULL,
  `s_comment` varchar(255) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `purchase_order_status_names` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_name` varchar(20) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

CREATE TABLE IF NOT EXISTS `received_po_detail` (
  `i_rpoh_id` int(11) NOT NULL,
  `i_p_id` int(11) NOT NULL,
  `i_quantity_received` int(11) NOT NULL,
  `s_product_serial` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `received_po_header` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `i_poh_id` int(11) NOT NULL,
  `i_id_id` int(11) NOT NULL,
  `i_u_received_by` int(11) NOT NULL,
  `i_u_approved_by` int(11) NOT NULL,
  `s_date_registration` datetime NOT NULL,
  `s_comment` text NOT NULL,
  `i_rposn_id` int(11) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;


CREATE TABLE IF NOT EXISTS `received_po_status_names` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_name` varchar(20) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

CREATE TABLE IF NOT EXISTS `received_to_detail` (
  `i_rtoh_id` int(11) NOT NULL,
  `i_p_id` int(11) NOT NULL,
  `i_quantity_received` int(11) NOT NULL,
  `s_product_serial` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `received_to_header` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `i_toh_id` int(11) NOT NULL,
  `i_id_id` int(11) NOT NULL,
  `i_u_prepared_by` int(11) NOT NULL,
  `i_u_received_by` int(11) NOT NULL,
  `s_date_registration` datetime NOT NULL,
  `s_comment` varchar(255) NOT NULL,
  `i_rtosn_id` int(11) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `received_to_status_names` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_name` varchar(20) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;


CREATE TABLE IF NOT EXISTS `sales_invoice_detail` (
  `i_sih_id` int(11) NOT NULL,
  `i_p_id` int(11) NOT NULL,
  `i_quantity_to_release` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `sales_invoice_header` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `i_id_id` int(11) NOT NULL,
  `i_u_prepared_by` int(11) NOT NULL,
  `i_u_approved_by` int(11) NOT NULL,
  `s_date_registration` datetime NOT NULL,
  `s_title` varchar(40) NOT NULL,
  `s_comment` varchar(255) NOT NULL,
  `i_sisn_id` int(11) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=570 ;

CREATE TABLE IF NOT EXISTS `sales_invoice_revision` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `i_sih_id` int(11) NOT NULL,
  `s_date_updated` datetime NOT NULL,
  `i_u_revised_by` int(11) NOT NULL,
  `s_comment` varchar(255) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `sales_invoice_status_names` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_name` varchar(20) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

CREATE TABLE IF NOT EXISTS `sales_return_detail` (
  `i_srh_id` int(11) NOT NULL,
  `i_p_id` int(11) NOT NULL,
  `i_quantity_received` int(11) NOT NULL,
  `s_product_serial` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `sales_return_header` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `i_orh_id` int(11) NOT NULL,
  `i_id_id` int(11) NOT NULL,
  `i_u_prepared_by` int(11) NOT NULL,
  `i_u_approved_by` int(11) NOT NULL,
  `s_date_registration` datetime NOT NULL,
  `s_comment` varchar(255) NOT NULL,
  `i_srsn_id` int(11) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

CREATE TABLE IF NOT EXISTS `sales_return_status_names` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_name` varchar(20) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

CREATE TABLE IF NOT EXISTS `transfer_order_detail` (
  `i_toh_id` int(11) NOT NULL,
  `i_p_id` int(11) NOT NULL,
  `i_quantity_transfering` int(11) NOT NULL,
  `s_product_serial` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `transfer_order_header` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `i_releasing_id_id` int(11) NOT NULL,
  `i_receiving_id_id` int(11) NOT NULL,
  `i_u_prepared_by` int(11) NOT NULL,
  `i_u_approved_by` int(11) NOT NULL,
  `s_date_registration` datetime NOT NULL,
  `s_title` varchar(255) NOT NULL,
  `s_transfer_description` varchar(255) NOT NULL,
  `s_comment` varchar(255) NOT NULL,
  `i_tosn_id` int(11) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `transfer_order_status_names` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_name` varchar(20) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;


CREATE TABLE IF NOT EXISTS `users` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_date_registration` datetime NOT NULL,
  `s_email` varchar(50) NOT NULL,
  `s_username` varchar(50) NOT NULL,
  `s_firstname` varchar(50) NOT NULL,
  `s_lastname` varchar(50) NOT NULL,
  `s_password` varchar(255) NOT NULL,
  `s_unique_key` varchar(255) NOT NULL,
  `i_usn_id` int(11) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

CREATE TABLE IF NOT EXISTS `user_roles` (
  `i_u_id` int(11) NOT NULL,
  `i_urn_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `user_roles` (`i_u_id`, `i_urn_id`) VALUES
(1, 1),
(1, 3),
(0, 0),
(2, 1),
(1, 4),
(3, 3);

CREATE TABLE IF NOT EXISTS `user_role_names` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_name` varchar(20) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;


INSERT INTO `user_role_names` (`i_id`, `s_name`) VALUES
(1, 'Admin'),
(2, 'Users'),
(3, 'Products'),
(4, 'Inventory Depot'),
(5, 'Included Inventory'),
(6, 'Excluded Inventory'),
(7, 'Purchase Order'),
(8, 'Sales Order'),
(9, 'Official Receipt'),
(10, 'Delivery Receipt'),
(11, 'Received PO'),
(12, 'Missed PO'),
(13, 'Wrong Send PO'),
(14, 'Transfer Order'),
(15, 'Received TO');

CREATE TABLE IF NOT EXISTS `user_status_names` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_name` varchar(20) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

CREATE TABLE IF NOT EXISTS `wrong_send_po_detail` (
  `i_wspoh_id` int(11) NOT NULL,
  `i_p_id` int(11) NOT NULL,
  `i_quantity_ws` int(11) NOT NULL,
  `s_product_serial` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `wrong_send_po_header` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `i_poh_id` int(11) NOT NULL,
  `i_id_id` int(11) NOT NULL,
  `i_u_received_by` int(11) NOT NULL,
  `i_u_approved_by` int(11) NOT NULL,
  `s_date_registration` datetime NOT NULL,
  `s_comment` text NOT NULL,
  `i_wsposn_id` int(11) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;


CREATE TABLE IF NOT EXISTS `wrong_send_po_status_names` (
  `i_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_name` varchar(20) NOT NULL,
  PRIMARY KEY (`i_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

